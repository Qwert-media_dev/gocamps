<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// store map data 2177
Route::get('transfer', function () {
  dd('off');
});


Route::get('/validate', ['as' => 'saveMap', 'uses' => 'IndexController@saveMap']);
Route::get('data.json', ['uses' => 'IndexController@getJson']);
// Route::get('api/volunteers-data', ['uses' => 'admin\VolunteerController@table']);

 Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]],function(){

        Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
            Route::get('', function () { return view('admin.index'); });
            Route::group(['prefix' => 'schools', 'middleware' => ['auth']], function () {
                Route::get('/edit/{id}/', ['as' => 'admin-schools-edit', 'uses' => 'admin\SchoolController@edit']);
                Route::post('/edit/{id}/', ['as' => 'schools-update', 'uses' => 'admin\SchoolController@update']);
                Route::get('/create', ['as' => 'schools-view', 'uses' => 'admin\SchoolController@create']);
                Route::post('/create', ['as' => 'schools-store', 'uses' => 'admin\SchoolController@store']);
                Route::get('/delete/{id}', ['as' => 'admin-schools-delete', 'uses' => 'admin\SchoolController@destroy']);
                Route::post('/export/{Y}/', ['as' => 'admin-schools-export', 'uses' => 'admin\SchoolController@export']);
                Route::get('/getfiles', ['as' => 'admin-schools-files', 'uses' => 'admin\SchoolController@getFiles']);
                Route::post('/checkForm', ['as' => 'check-school-forms', 'uses' => 'admin\SchoolController@checkForm']);
                Route::get('merge/{id}/{similar}', ['as' => 'merge-school-forms', 'uses' => 'admin\SchoolController@mergeForms']);
                Route::get('/{Y}/', ['as' => 'admin-schools', 'uses' => 'admin\SchoolController@index']);
                Route::post('processing', ['as' => 'processing', 'uses' => 'admin\SchoolController@processing']);
                Route::get('2/{Y}/', ['as' => 'admin-schools2', 'uses' => 'admin\SchoolController@index2']);
                Route::post('sstorecol', ['as' => 'sstorecol', 'uses' => 'admin\SchoolController@saveUserCols']);
            });
            Route::group(['prefix' => 'volunteers', 'middleware' => ['auth']], function () {
                Route::get('/edit/{id}', ['as' => 'admin-volunteer-edit', 'uses' => 'admin\VolunteerController@edit']);
                Route::post('/edit/{id}', ['as' => 'volunteer-update', 'uses' => 'admin\VolunteerController@update']);
                Route::get('/create', ['as' => 'volunteers-view', 'uses' => 'admin\VolunteerController@create']);
                Route::post('/create', ['as' => 'volunteers-store', 'uses' => 'admin\VolunteerController@store']);
                Route::get('/delete/{id}', ['as' => 'admin-volunteer-delete', 'uses' => 'admin\VolunteerController@destroy']);
                Route::post('/export/{Y}', ['as' => 'admin-volunteer-export', 'uses' => 'admin\VolunteerController@export']);
                Route::get('/email/{id}/{mail}', ['as' => 'admin-volunteer-email', 'uses' => 'admin\VolunteerController@sendEmail']);
                Route::get('/getSchools/{id}', ['as' => 'get-volunteer-schools', 'uses' => 'admin\VolunteerController@getSchools']);
                Route::post('/checkForm', ['as' => 'check-volunteer-forms', 'uses' => 'admin\VolunteerController@checkForm']);
                Route::get('merge/{id}/{similar}', ['as' => 'merge-volunteer-forms', 'uses' => 'admin\VolunteerController@mergeForms']);
                Route::get('/{Y}/', ['as' => 'volunteers', 'uses' => 'admin\VolunteerController@index']);

                Route::post('processing', ['as' => 'vprocessing', 'uses' => 'admin\VolunteerController@processing']);
                Route::post('storecol', ['as' => 'storecol', 'uses' => 'admin\VolunteerController@saveUserCols']);
                Route::get('2/{Y}', ['as' => 'volunteers2', 'uses' => 'admin\VolunteerController@index2']);

            });
            Route::group(['prefix' => 'articles', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'articles', 'uses' => 'admin\ArticleController@index']);
                Route::get('/edit/{id}', ['as' => 'articles-edit', 'uses' => 'admin\ArticleController@edit']);
                Route::post('/edit/{id}', ['as' => 'article-update', 'uses' => 'admin\ArticleController@update']);
                Route::get('/create', ['as' => 'article-view', 'uses' => 'admin\ArticleController@create']);
                Route::post('/create', ['as' => 'article-store', 'uses' => 'admin\ArticleController@store']);
                Route::get('/delete/{id}', ['as' => 'admin-article-delete', 'uses' => 'admin\ArticleController@destroy']);
                Route::get('/unlink/{id}', ['as' => 'admin-article-unlink', 'uses' => 'admin\ArticleController@unlink']);
                Route::get('/removeimage/{id}', ['as' => 'admin-article-remove-image', 'uses' => 'admin\ArticleController@remove_image']);
                Route::get('/removegalimage/{id}/{index}', ['as' => 'admin-article-remove-galimage', 'uses' => 'admin\ArticleController@remove_galimage']);


            });
            Route::group(['prefix' => 'projects', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'projects', 'uses' => 'admin\ProjectController@index']);
                Route::get('/edit/{id}', ['as' => 'projects-edit', 'uses' => 'admin\ProjectController@edit']);
                Route::post('/edit/{id}', ['as' => 'projects-update', 'uses' => 'admin\ProjectController@update']);
                Route::post('/create', ['as' => 'project-store', 'uses' => 'admin\ProjectController@store']);
                Route::get('/create', ['as' => 'projects-create', 'uses' => 'admin\ProjectController@create']);
                Route::get('/delete/{id}', ['as' => 'projects-delete', 'uses' => 'admin\ProjectController@destroy']);
                Route::get('/unlink/{id}', ['as' => 'admin-project-unlink', 'uses' => 'admin\ProjectController@unlink']);



            });
            Route::group(['prefix' => 'categories', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'categories', 'uses' => 'admin\CategoryController@index']);
                Route::get('/edit/{id}', ['as' => 'categories-edit', 'uses' => 'admin\CategoryController@edit']);
                Route::post('/edit/{id}', ['as' => 'categories-update', 'uses' => 'admin\CategoryController@update']);
                Route::post('/create', ['as' => 'categories-store', 'uses' => 'admin\CategoryController@store']);
                Route::get('/create', ['as' => 'categories-create', 'uses' => 'admin\CategoryController@create']);
                Route::get('/delete/{id}', ['as' => 'categories-delete', 'uses' => 'admin\CategoryController@destroy']);
                Route::get('/unlink/{id}', ['as' => 'admin-category-unlink', 'uses' => 'admin\CategoryController@unlink']);



            });
            Route::group(['prefix' => 'users', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'users', 'uses' => 'admin\UserController@index']);
                Route::get('/edit/{id}', ['as' => 'users-edit', 'uses' => 'admin\UserController@edit']);
                Route::post('/edit/{id}', ['as' => 'users-update', 'uses' => 'admin\UserController@update']);
                Route::post('/create', ['as' => 'users-store', 'uses' => 'admin\UserController@store']);
                Route::get('/create', ['as' => 'users-create', 'uses' => 'admin\UserController@create']);
                Route::get('/delete/{id}', ['as' => 'users-delete', 'uses' => 'admin\UserController@destroy']);


            });
            Route::group(['prefix' => 'pages', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'pages', 'uses' => 'admin\PageController@index']);
                Route::get('/edit/{id}', ['as' => 'pages-edit', 'uses' => 'admin\PageController@edit']);
                Route::post('/edit/{id}', ['as' => 'pages-update', 'uses' => 'admin\PageController@update']);
                Route::get('/create', ['as' => 'pages-create', 'uses' => 'admin\PageController@create']);
                Route::get('/delete/{id}', ['as' => 'pages-destroy', 'uses' => 'admin\PageController@destroy']);
                Route::post('/create', ['as' => 'pages-update', 'uses' => 'admin\PageController@store']);
                Route::get('/unlink/{id}', ['as' => 'admin-pages-unlink', 'uses' => 'admin\PageController@unlink']);
            });
            Route::group(['prefix' => 'roles', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'roles', 'uses' => 'admin\RoleController@index']);
                Route::get('/edit/{id}', ['as' => 'roles-edit', 'uses' => 'admin\RoleController@edit']);
                Route::post('/edit/{id}', ['as' => 'roles-update', 'uses' => 'admin\RoleController@update']);
                Route::get('/create', ['as' => 'roles-edit', 'uses' => 'admin\RoleController@create']);
                Route::get('/delete/{id}', ['as' => 'roles-destroy', 'uses' => 'admin\RoleController@destroy']);
                Route::post('/create', ['as' => 'roles-update', 'uses' => 'admin\RoleController@store']);
            });
            Route::group(['prefix' => 'materials', 'middleware' => ['auth']], function () {
                Route::get('/', ['as' => 'materials', 'uses' => 'admin\MaterialController@index']);
                Route::get('/edit/{id}', ['as' => 'materials-edit', 'uses' => 'admin\MaterialController@edit']);
                Route::post('/edit/{id}', ['as' => 'material-update', 'uses' => 'admin\MaterialController@update']);
                Route::get('/create', ['as' => 'material-view', 'uses' => 'admin\MaterialController@create']);
                Route::post('/create', ['as' => 'material-store', 'uses' => 'admin\MaterialController@store']);
                Route::get('/delete/{id}', ['as' => 'admin-material-delete', 'uses' => 'admin\MaterialController@destroy']);
                Route::get('/unlink/{id}', ['as' => 'admin-material-unlink', 'uses' => 'admin\MaterialController@unlink']);
                Route::get('/removeimage/{id}', ['as' => 'admin-material-remove-image', 'uses' => 'admin\MaterialController@remove_image']);
            });
        });
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);


        Route::get('/projects/{id}', ['as' => 'project-view', 'uses' => 'ProjectController@show']);
        Route::get('/school/create', ['as' => 'school-create', 'uses' => 'SchoolController@create']);
        Route::post('/school/create', ['as' => 'school-store', 'uses' => 'SchoolController@store']);
        Route::get('/school/short-create', ['as' => 'school-short-create', 'uses' => 'SchoolController@short_create']);
        Route::post('/school/short-create', ['as' => 'school-short-store', 'uses' => 'SchoolController@short_store']);
        Route::get('/volunteer/create', ['as' => 'volunteer-create', 'uses' => 'VolunteerController@create']);
        Route::post('/volunteer/create', ['as' => 'volunteer-store', 'uses' => 'VolunteerController@store']);
        Route::get('/volunteer/short-create', ['as' => 'volunteer-short-create', 'uses' => 'VolunteerController@short_create']);
        Route::post('/volunteer/short-create', ['as' => 'volunteer-short-store', 'uses' => 'VolunteerController@short_store']);
        Route::get('/success', function () { return view('success'); });
        Route::get('/success_school', function () { return view('success_school'); });
        Route::get('/auth/login', ['as' => 'user-login', 'uses' => 'UserController@login']);
        Route::post('/auth/login', ['as' => 'user-login', 'uses' => 'UserController@auth']);
        Route::get('/auth/login', ['as' => 'user-login', 'uses' => 'UserController@login']);
        Route::get('/auth/logout', ['as' => 'user-logout', 'uses' => 'UserController@logout']);
        Route::get('/material/{slug}', ['as' => 'material-view', 'uses' => 'IndexController@material']);
        Route::get('/material/', ['as' => 'material', 'uses' => 'IndexController@materials']);
        Route::get('/category/{category}', ['as' => 'category', 'uses' => 'IndexController@category']);
        Route::get('/{slug}', ['as' => 'page', 'uses' => 'IndexController@page']);

        Route::get('/{category}/{slug}', ['as' => 'article', 'uses' => 'IndexController@article']);

        });
