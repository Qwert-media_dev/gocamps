<?php

return [
    'phones' => [
      '1' => '+38 (093)',
      '2' => '+38 (073)',
      '3' => '+38 (063)',
      '4' => '+38 (098)',
      '5' => '+38 (097)',
      '6' => '+38 (096)',
      '7' => '+38 (099)',
      '8' => '+38 (095)',
      '9' => '+38 (055)',
    ],
    'other_langs' => [
      '13' => '-',
      '1' => 'German - Native',
      '2' => 'German - Advanced',
      '3' => 'German - Upper intermediate',
      '4' => 'German - Intermediate',
      '5' => 'French - Native',
      '6' => 'French - Advanced',
      '7' => 'French - Upper intermediate',
      '8' => 'French - Intermediate',
      '9' => 'Spanish - Native',
      '10' => 'Spanish - Advanced',
      '11' => 'Spanish - Upper intermediate',
      '12' => 'Spanish - Intermediate',
    ],
    'hear' => [
      '1' => 'I saw a post on Facebook GoCamp website',
      '2' => 'My friend sent me a link to a Facebook post',
      '3' => 'I saw the post on Twitter',
      '4' => 'Internet search for volunteering opportunities',
      '5' => 'I saw opportunity on British Council website',
      '6' => 'I heard about GoCamp in my university',
      '7' => 'I received an email/newsletter from my university',
      '8' => 'I previously participated in GoCamp',
      '9' => 'I saw video in Youtube',
      '10' => 'Another GoCamp participant told me',
      '11' => 'I saw it on news/in press',
      '12' => 'Other',
    ],
    'dietary' => [
      '1' => 'Vegeterian',
      '2' => 'Vegan',
      '3' => 'Allergic',
      '4' => 'Other',
    ],
    'mails'=>array(
        '1' =>[
          'subject' => 'GoCamp Volunteering in Ukraine!',
          'label' => 'Application Reply',
        ],
        // '2' => [
        //   'subject'=>'Congratulations! You are GoCamp Volunteer!',
        //   'label'=>'GoCamp Afterschool Feb-Mar',
        // ],
        // '3' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp June1',
        // ],
        // '4' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp June2'
        // ],
        // '5' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp August1'
        // ],
        // '6' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp August2',
        // ],
        // '10' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp September1',
        // ],
        // '11' => [
        //   'subject' => 'Congratulations! You are GoCamp Volunteer!',
        //   'label' => 'GoCamp September2',
        // ],
        '12' => [
          'subject' => 'Congratulations! You are GoCamp Volunteer!',
          'label' => 'GoCamp May 30 - June 14',
        ],
        '13' => [
          'subject' => 'Congratulations! You are GoCamp Volunteer!',
          'label' => 'GoCamp June 06 - June 21',
        ],
        '14' => [
          'subject' => 'Congratulations! You are GoCamp Volunteer!',
          'label' => 'GoCamp August 08 - August 23',
        ],
        '15' => [
          'subject' => 'Congratulations! You are GoCamp Volunteer!',
          'label' => '3-month session',
        ],
        '7' => [
          'subject' => 'GoCamp',
          'label' => 'Refusal'
        ],
        '8' => [
          'subject' => 'GoCamp',
          'label' => 'Waiting list',
        ],
        '9' => [
          'subject' => 'Payment information',
          'label' => 'Payment',
        ],
        // '2'=>'GoCamp Volunteering in Ukraine!',
        // '3'=>'GoCamp Volunteering in Ukraine',#Child protection course invitation
        // '4'=>'Congratulations! You are GoCamp Volunteer!',
        // '5'=>'Congratulations! You are GoCamp Volunteer!',
        // '6'=>'Congratulations! You are GoCamp Volunteer!',
        // '7'=>'Congratulations! You are GoCamp Volunteer!',
        // '8'=>'GoCamp Volunteering in Ukraine',#Payment fee (оплата)
        // '9'=>'GoCamp Volunteering in Ukraine',#Pre-Departure informational package
        // '10'=>'GoCamp Volunteering in Ukraine',#Refusal (отказано)
        // '11'=>'GoCamp Volunteering in Ukraine',#Refusal (отказано)
    ),
    'order_status' => array(
        '0' => 'Волонтер не найден',
        '1' => 'Волонтер найден',
        '2' => 'Волонтер отозван',
    ),
    'status'=>array(
        '0' => 'новая',
        '1' => 'просмотрена',
        '2' => 'сконтактирована',
        '3' => 'назначено собеседование',
        '4' => 'прошел cобеседование',
        '5' => 'Волонтер найден',
        '6' => 'принята',
        '7' => 'Отказ',
        '8' => 'Отказали мы',
        ),
    'status_vol'=>array(
        '0' => 'новая',
        '1'=> 'просмотрена',
        '2'=>'сконтактирована',
        '3'=>'назначено собеседование',
        '12'=>'прошел собеседование',
        '4'=>'waiting list',
        '10'=>'не выходит на связь',
        '5'=>'собираем документы',
        '6'=>'собраны документы',
        '7'=>'уже волонтер - найдено школу',
        '8'=>'отказали мы',
        '9'=>'отказался сам',
        '11'=>'alumni',
        ),
    'strands'=>array(
        '0'=>'-',
        '1'=>'STEAM',
        '2'=>'Citizenship',
        '3'=>'Sports and Health',
        '4'=>'Leadership and Career Exploration'
        ),
    'quantity_children'=>array(
        '1'=>'до 30',
        '2'=>'30-50',
        '3'=>'50-100',
        '4'=>'100-200',
        '5'=>'200-300',
        '6'=>'300 +'
        ),
    'training_dates' => [
      '0'=>'Training 1',
      '1'=>'Training 2',
      '2'=>'Training 3',
      '3'=>'Training 4',
      '4'=>'Training 5',
    ],
    'goc_east' => [
      '0'=>'Session 1',
      '1'=>'Session 2',
      '2'=>'Session 3',
      '3'=>'Session 4',
      '4'=>'Session 5',
    ],
    'camp_type'=>array(
        '' => '',
        '1'=>'Пришкільний денний табір неповного дня (5 годин)',
        '2'=>'Пришкільний денний табір повного дня (8 годин)',
        '3'=>'Табір повного дня (з ночівлею)'
        ),
    'yes_no'=>array(
        '1'=>'yes',
        '2'=>'no'
        ),
    'skill'=>array(
        '1'=>'Native',
        '2'=>'Advancved',
        '3'=>'Upper Intermediate',
        '4'=>'Intermediate'
        ),
    'session'=>array(
        // '5' => '24.11.2017 &#13;&#10;—&#13;&#10; 15.12.2017',
        // '6' => '23.02.2018 &#13;&#10;—&#13;&#10; 16.03.2018',
        // '7' => '02.06.2018 &#13;&#10;—&#13;&#10; 20.06.2018',
        // '8' => '09.06.2018 &#13;&#10;—&#13;&#10; 27.06.2018',
        // '9' => '02.08.2018 &#13;&#10;—&#13;&#10; 17.08.2018',
        // '10' => '09.08.2018 &#13;&#10;—&#13;&#10; 23.08.2018',
        // '11' => '01.09.2018 &#13;&#10;—&#13;&#10; 19.09.2018',
        // '12' => '12.09.2018 &#13;&#10;—&#13;&#10; 28.09.2018',
        // '13' => 'Spring 2019',
        // '14' => 'June 2019',
        // '15' => 'July 2019',
        // '16' => 'August 2019',
        '17' => 'May 30 &#13;&#10;—&#13;&#10; June14',
        '18' => 'June 06 &#13;&#10;—&#13;&#10; June 21',
        '19' => 'August 08 &#13;&#10;—&#13;&#10; August 23',
        '20' => 'May 30 &#13;&#10;—&#13;&#10; August 23',
        ),
    'table_session'=>array(
        '5' => '24.11.2017-15.12.2017',
        '6' => '23.02.2018-16.03.2018',
        '7' => '02.06.2018-20.06.2018',
        '8' => '09.06.2018-27.06.2018',
        '9' => '02.08.2018-17.08.2018',
        '10' => '09.08.2018-23.08.2018',
        '11' => '01.09.2018-19.09.2018',
        '12' => '12.09.2018-28.09.2018',
        '13' => 'Spring 2019',
        '14' => 'June 2019',
        '15' => 'July 2019',
        '16' => 'August 2019',
        '17' => 'May 30-June14',
        '18' => 'June 06-June 21',
        '19' => 'August-August 23',
        '20' => 'May 30-August 23',
        ),
    'all_session'=>array(
        '1'=>'GoCamp 29.05.2017 - 16.06.2017',
        '4'=>'GoCamp 12.06.2017 - 23.06.2017',
        '2'=>'GoCamp AfterSchool 27.03.2017 - 28.04.2017',
        '3'=>'GoCampEast',
        '5' => '24.11.2017-15.12.2017',
        '6' => '23.02.2018-16.03.2018',
        '7' => '02.06.2018-20.06.2018',
        '8' => '09.06.2018-27.06.2018',
        '9' => '02.08.2018-17.08.2018',
        '10' => '09.08.2018-23.08.2018',
        '11' => '01.09.2018-19.09.2018',
        '12' => '12.09.2018-28.09.2018',
        '13' => 'Spring 2019',
        '14' => 'June 2019',
        '15' => 'July 2019',
        '16' => 'August 2019',
        '17' => 'May 30-June14',
        '18' => 'June 06-June 21',
        '19' => 'August-August 23',
        '20' => 'May 30-August 23',
        ),
    'project'=>array(
        '1'=>'GoCamp',
        '2'=>'GoCamp AfterSchool',
        '3'=>'GoCamp East'
    ),
    'project_admin'=>array(
        '1'=>'GoCamp',
        '2'=>'GoCamp AfterSchool',
        '3'=>'GoCamp East'
    ),
    'camp_lang'=>array(
        '4'=>'Англійська',
        '1'=>'Німецька',
        '2'=>'Іспанська',
        '3'=>'Французька'
    ),
    'city_type' => [
      '0' => 'Місто',
      '1' => 'СМТ',
      '2' => 'Село',
    ],
    'city_type_eng' => [
      '0' => 'City',
      '1' => 'Town',
      '2' => 'Village',
    ],
    'states'=>array(
        '',
        'Вінницька',
         'Волинська',
         'Дніпропетровська',
         'Донецька',
         'Житомирська',
         'Закарпатська',
         'Запорізька',
         'Івано-Франківська',
         'Київська',
         'Кіровоградська',
         'Луганська',
         'Львівська',
         'Миколаївська',
         'Одеська',
         'Полтавська',
         'Рівненська',
         'Сумська',
         'Тернопільська',
         'Харківська',
         'Херсонська',
         'Хмельницька',
         'Черкаська',
         'Чернігівська',
         'Чернівецька',
         'м. Київ',
        ),
    'states_eng'=>array(
        '',
         'Vinnytska',
         'Volynska',
         'Dnepropetrovska',
         'Donetska',
         'Zhitomirska',
         'Zakarpatska',
         'Zaporozhska',
         'Ivano-Frankivska',
         'Kievska',
         'Kirovogradska',
         'Luganska',
         'Lvovska',
         'Nikolaevska',
         'Odesska',
         'Poltavska',
         'Rivnenska',
         'Sumska',
         'Ternopilska',
         'Kharkivska',
         'Khersonska',
         'Khmelnytska',
         'Cherkaska',
         'Chernigovska',
         'Chernivetska',
         'Kyiv',
        ),
    'schoolColumns' => [
      'Main information' => [
        'id' => [
          'label' => 'ID',
          'hidden' => false,
        ],
        'title' => [
          'label' => 'Title',
          'hidden' => false,
        ],
        'state' => [
          'label' => 'State',
          'hidden' => false,
          'listed' => true,
        ],
        'city_type' => [
          'label' => 'City type',
          'hidden' => true,
          'listed' => true,
        ],
        'city' => [
          'label' => 'City',
          'hidden' => true,
        ],
        'railway_station' => [
          'label' => 'Наближча ЗС',
          'hidden' => true,
        ],
        'street' => [
          'label' => 'Street',
          'hidden' => false,
        ],
        'house' => [
          'label' => 'House',
          'hidden' => false,
        ],
        'comment' => [
          'label' => 'Comment',
          'hidden' => false,
        ],
        'title_eng' => [
          'label' => 'Title ENG',
          'hidden' => true,
        ],
        'state_eng' => [
          'label' => 'State ENG',
          'hidden' => true,
          'listed' => true,
        ],
        'city_type_eng' => [
          'label' => 'City type ENG',
          'hidden' => true,
          'listed' => true,
        ],
        'city_eng' => [
          'label' => 'City ENG',
          'hidden' => true,
        ],
        'railway_station_eng' => [
          'label' => 'Nearest station',
          'hidden' => true,
        ],
        'street_eng' => [
          'label' => 'Street ENG',
          'hidden' => false,
        ],
        'house_eng' => [
          'label' => 'House ENG',
          'hidden' => false,
        ],
        'manager' => [
          'label' => 'Manager',
          'hidden' => true,
        ],
      ],
      'Child quantity and project details' => [
        'quantity_children' => [
          'label' => 'Children quantity',
          'hidden' => true,
          'listed' => true,
        ],
        // 'quantity_children_work' => [
        //   'label' => 'Children work',
        //   'hidden' => true,
        // ],
        'share_camp' => [
          'label' => 'Share camp',
          'hidden' => true,
          'listed' => true,
        ],
        'type_camp' => [
          'label' => 'Camp type',
          'hidden' => true,
          'listed' => true,
        ],
        'paid' => [
          'label' => 'Paid school',
          'hidden' => true,
          'listed' => true
        ],
        'share_volunteer' => [
          'label' => 'Share volunteeer',
          'hidden' => true,
          'listed' => true,
        ],
        'rating' => [
          'label' => 'Rating',
          'hidden' => true,
          'listed' => true,
        ],
        'status' => [
          'label' => 'Status',
          'hidden' => false,
          'listed' => true
        ],
        'year' => [
          'label' => 'Year',
          'hidden' => true,
        ],
        'session' => [
          'label' => 'Sessions',
          'hidden' => true,
          'listed' => true,
        ],
        'project' => [
          'label' => 'Project',
          'hidden' => false,
          'listed' => true,
        ],
        'camp_lang' => [
          'label' => 'Camp languages',
          'hidden' => true,
          'listed' => true,
        ],
        'training' => [
          'label' => 'Training',
          'hidden' => true,
          'listed' => true
        ],
        'training_teacher' => [
          'label' => 'Training teacher',
          'hidden' => false,
        ],
        'training_dates' => [
          'label' => 'Training dates',
          'hidden' => true,
          'listed' => true
        ],
        'school_memorandum' => [
          'label' => 'School memorandum',
          'hidden' => true,
          'listed' => true
        ],
        'teacher_memorandum' => [
          'label' => 'Teacher memorandum',
          'hidden' => true,
          'listed' => true
        ],
        'plan' => [
          'label' => 'Plan',
          'hidden' => true,
          'listed' => true
        ],
        'report' => [
          'label' => 'Report',
          'hidden' => true,
          'listed' => true
        ],
        'goc_east' => [
          'label' => 'GoCampEast',
          'hidden' => true,
          'listed' => true
        ]
      ],
      'Other information' => [
        'experience' => [
          'label' => 'Experience',
          'hidden' => false,
          'listed' => true,
        ],
        'description' => [
          'label' => 'About exp',
          'hidden' => false,
        ],
        'description2' => [
          'label' => 'Why GoCamp',
          'hidden' => false,
        ],
        'about_school' => [
          'label' => 'About school',
          'hidden' => false,
        ],
        'about_city' => [
          'label' => 'About city',
          'hidden' => false,
        ],
        'about_school_eng' => [
          'label' => 'About school ENG',
          'hidden' => false,
        ],
        'about_city_eng' => [
          'label' => 'About city ENG',
          'hidden' => false,
        ],
        'video' => [
          'label' => 'Video link',
          'hidden' => false,
        ],
      ],
      'Contact information' => [
        'sdirector_name' => [
          'label' => 'Director name',
          'hidden' => false,
        ],
        'sdirector_email' => [
          'label' => 'Director email',
          'hidden' => false,
        ],
        'sdirector_phone' => [
          'label' => 'Director phone',
          'hidden' => false,
        ],
        'teacher_name' => [
          'label' => 'Teacher name',
          'hidden' => false,
        ],
        'teacher_email' => [
          'label' => 'Teacher email',
          'hidden' => false,
        ],
        'teacher_phone' => [
          'label' => 'Teacher phone',
          'hidden' => false,
        ],
        'school_phone' => [
          'label' => 'School phone',
          'hidden' => false,
        ],
        'school_email' => [
          'label' => 'School email',
          'hidden' => false,
        ],
        'volunteers' => [
          'label' => 'Volunteers',
          'hidden' => true,
        ],
        'shorttype' => [
          'label' => 'Short',
          'hidden' => true,
        ],
        'created_at' => [
          'label' => 'Registered',
          'hidden' => true,
        ],
      ],
    ],
    'documents' => [
      '0' => 'Passport',
      '1' => 'CV',
      '2' => 'Health Check',
      '3' => 'Residence Permit',
      '4' => 'Insurance',
      '5' => 'Criminal Check',
    ],
    'volunteerColumns' => [
      'Main information' => [
        'id' => [
          'label' => 'ID',
          'hidden' => false,
        ],
        'first_name' => [
          'label' => 'First Name',
          'hidden' => false,
        ],
        'last_name' => [
          'label' => 'Last Name',
          'hidden' => false,
        ],
        'citizenship' => [
          'label' => 'Citizenship',
          'hidden' => true,
          'listed' => true,
        ],
        'citizenship_old' => [
          'label' => 'Citizenship OLD',
          'hidden' => true,
        ],
        'passport' => [
          'label' => 'Passport',
          'hidden' => false,
        ],
        'birthday' => [
          'label' => 'Birthday',
          'hidden' => true,
        ],
        'gender' => [
          'label' => 'Gender',
          'hidden' => true,
          'listed' => true,
        ],
        'country' => [
          'label' => 'Country',
          'hidden' => true,
          'listed' => true,
        ],
        'country_old' => [
          'label' => 'Country OLD',
          'hidden' => true,
        ],
        'city' => [
          'label' => 'City',
          'hidden' => true,
        ],
        'email' => [
          'label' => 'Email',
          'hidden' => true,
        ],
        'skype' => [
          'label' => 'Skype',
          'hidden' => true,
        ],
        'state' => [
          'label' => 'State',
          'hidden' => true,
          'listed' => true,
        ],
        'schools' => [
          'label' => 'Schools',
          'hidden' => true
        ]
      ],
      'Link to social media' => [
        'facebook' => [
          'label' => 'Facebook',
          'hidden' => true,
        ],
        'twitter' => [
          'label' => 'Twitter',
          'hidden' => false,
        ],
        'google' => [
          'label' => 'Google+',
          'hidden' => false,
        ],
        'add_link' => [
          'label' => 'Additional link',
          'hidden' => false,
        ],
      ],
      'Additional contact information' => [
        'phone_code' => [
          'label' => 'Primary p. code',
          'hidden' => true,
          'listed' => true
        ],
        'phone' => [
          'label' => 'Primary phone',
          'hidden' => true,
        ],
        'alt_phone_code' => [
          'label' => 'Alt. p. code',
          'hidden' => false,
          'listed' => true
        ],
        'alt_phone' => [
          'label' => 'Alternate phone',
          'hidden' => false,
        ],
        'emergency_name' => [
          'label' => 'Emergency name',
          'hidden' => false,
        ],
        'emergency_phone_code' => [
          'label' => 'Em. p. code',
          'hidden' => false,
          'listed' => true
        ],
        'emergency_phone' => [
          'label' => 'Emergency phone',
          'hidden' => false,
        ],
      ],
      'Education' => [
        'been_in_ukraine' => [
          'label' => 'Been to Ukraine',
          'hidden' => true,
          'listed' => true,
        ],
        'capacity' => [
          'label' => 'In what capacity?',
          'hidden' => true
        ],
        'eng_level' => [
          'label' => 'English level',
          'hidden' => true,
          'listed' => true,
        ],
        'other_langs' => [
          'label' => 'Other languages',
          'hidden' => true,
          'listed' => true
        ],
        'education' => [
          'label' => 'Education',
          'hidden' => true,
        ],
      ],
      'Work' => [
        'workplace' => [
          'label' => 'Current workplace',
          'hidden' => true,
        ],
        'workplace_position' => [
          'label' => 'Workplace position',
          'hidden' => true,
        ],
        'work_describe' => [
          'label' => 'Work describe',
          'hidden' => false,
        ],
      ],
      'Volunteering' => [
        'about' => [
          'label' => 'About yourself',
          'hidden' => false,
        ],
        'why' => [
          'label' => 'Why volunteering',
          'hidden' => false,
        ],
        'facts' => [
          'label' => '3 facts about',
          'hidden' => false,
        ],
        'strand' => [
          'label' => 'Priority sphere',
          'hidden' => true,
          'listed' => true,
        ],
        'session' => [
          'label' => 'Session',
          'hidden' => false,
          'listed' => true
        ],
        'hear' => [
          'label' => 'Hear about',
          'hidden' => true,
          'listed' => true
        ],
        'hear_other' => [
          'label' => 'Hear other',
          'hidden' => true,
        ],
      ],
      'General questions' => [
        'dietary' => [
          'label' => 'Food restrictions',
          'hidden' => true,
          'listed' => true
        ],
        'dietary_other' => [
          'label' => 'Food other',
          'hidden' => true,
        ],
        'interested' => [
          'label' => 'Blog interest',
          'hidden' => true,
          'listed' => true,
        ],
        'special' => [
          'label' => 'Special needs',
          'hidden' => true,
        ],
        'preferences' => [
          'label' => 'Location<br/>preferences',
          'hidden' => true,
        ],
        'shootings' => [
          'label' => 'Agree photo/video',
          'hidden' => true,
          'listed' => true,
        ],
        'agree' => [
          'label' => 'Agree<br/>personal data',
          'hidden' => true,
          'listed' => true,
        ],
      ],
      'Other information' => [
        'tickets' => [
          'label' => 'Tickets',
          'hidden' => true,
          'listed' => true,
        ],
        'visa' => [
          'label' => 'Visa',
          'hidden' => true,
          'listed' => true,
        ],
        'payment' => [
          'label' => 'Payment',
          'hidden' => true,
          'listed' => true,
        ],
        'documents' => [
          'label' => 'Documents',
          'hidden' => true,
          'listed' => true
        ],
        'status' => [
          'label' => 'Status',
          'hidden' => false,
          'listed' => true,
        ],
        'manager' => [
          'label' => 'Manager',
          'hidden' => true,
        ],
        'comment' => [
          'label' => 'Manager comment',
          'hidden' => true,
        ],
        'created_at' => [
          'label' => 'Registered',
          'hidden' => true,
        ],
        'shorttype' => [
          'label' => 'Short',
          'hidden' => true,
        ],
      ]
    ],

    // export volunteer
    'volunteer-export' => [
        'strand' => 'strand',
        'birthday' => 'birthday',
        'gender' => 'gender',
        'passport' => 'passport',
        'email' => 'email',
        'skype' => 'skype',
        'social' => 'social',
        'phone' => 'phone',
        'alt_phone' => 'alt_phone',
        'last_name' => 'last_name',
        'first_name' => 'first_name',
        'middle_name' => 'middle_name',
        'gender' => 'gender',
        'country' => 'country',
        'city' => 'city',
        'street' => 'street',
        'been_in_ukraine' => 'been_in_ukraine',
        'capacity' => 'capacity',
        'eng_level' => 'eng_level',
        'other_lang' => 'other_lang',
        'education' => 'education',
        'describe' => 'describe',
        'about' => 'about',
        'why' => 'why',
        'sessions' => 'sessions',
        'hear' => 'hear',
        'dietary' => 'dietary',
        'citizenship' => 'citizenship',
        'special' => 'special',
        'intrested' => 'intrested',
        'alt_strand' => 'alt_strand',
        'priority' => 'priority',
        'German'  => 'German Skill',
        'Speak'  => 'Speak german',
        'status' => 'status',
        'preferences' => 'preferences',
        'payment' => 'payment',
        'tickets' => 'tickets',
        'visa' => 'visa',
        'school' => 'school',
        'comment' => 'comment',
    ],
];
