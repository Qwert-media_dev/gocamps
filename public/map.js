//<![CDATA[


// delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
var delay = 1;
var lat='50.4310719';
var lng='30.5232182';
var titles = [];

  // ====== Create map objects ======
  var infowindow = new google.maps.InfoWindow({maxWidth: 400});
  var latlng = new google.maps.LatLng(lat,lng);
  var mapOptions = {
    zoom: 7,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false
  }
  var geo = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  var bounds = new google.maps.LatLngBounds();
  map.data.loadGeoJson('data.json?l=' + locale);

  map.data.setStyle(function(feature) {
    var title = 'School';
    if (feature.getProperty('title')) {
      title = feature.getProperty('title');
    }
    return /** @type {google.maps.Data.StyleOptions} */({
      title: title,
      icon: '/img/pin.png'
    });
  });


  var contentString = 'Info';
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });


  map.data.addListener('click', function(event) {
    contentString = '<h4>' + event.feature.f.title + '</h4>' + '<p>' + event.feature.f.address + '</p>';
    infowindow.setContent(contentString);
    infowindow.setPosition(new google.maps.LatLng(event.feature.b.b.lat(),event.feature.b.b.lng()),);
    infowindow.open(map);
  });



  // ====== Geocoding ======
  function getAddress(search, next) {
    var title = search.title;
    var address = search.address;
    var mid = search.mid;

    // don't duplicate markers!
    if (titles.indexOf(title) != -1) {next(); return;}
    titles.push(title);

    if (search.lng && search.lat) {
      // console.log(search.lng);
      // console.log(search.lat);
      // console.log(address, search.lat, search.lng, title);
      createMarker(address, search.lat, search.lng, title);
      next();
      return;
    }

    geo.geocode({address: address}, function (results,status) {
        // If that was successful
        if (status == google.maps.GeocoderStatus.OK) {
          // Lets assume that the first marker is the one we want
          // console.log(results);
          var p = results[0].geometry.location;
          var plat=p.lat();
          var plng=p.lng();

          // console.log(lng);
          // console.log(lat);
          
          $.get('/validate', {tk:tk, lat:plat, lng:plng, mid:mid}, function(d) { /*console.log(d);*/ });

          // Output the data
          var msg = 'address="' + address + '" lat=' +plat+ ' lng=' +plng+ '(delay='+delay+'ms)<br>';
          // console.log(msg)
          // Create a marker
          createMarker(address, plat, plng, title);
        }
        // ====== Decode the error status ======
        else {
          // === if we were sending the requests to fast, try this one again and increase the delay
          if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            nextAddress--;
            delay++;
          } else {
            // var reason="Code "+status;
            // var msg = 'address="' + address + '" error=' +reason+ '(delay='+delay+'ms)<br>';
            // console.log(msg)
          }
        }
        next();
      });
  }

  // ======= Function to create a marker
  function createMarker(address, lat, lng, title) {
    var contentString = '<h4>' + title + '</h4>' + '<p>' + address + '</p>';
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat,lng),
      map: map,
      icon: '/img/pin.png',
      title: title
      // zIndex: Math.round(latlng.lat()*-100000)<<5
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.setContent(contentString);
      infowindow.open(map,marker);
    });

    // bounds.extend(marker.position);
  }

  // ======= An array of locations that we want to Geocode ========


  // ======= Global variable to remind us what to do next
  var nextAddress = 0;

  // ======= Function to call the next Geocode operation when the reply comes back

  function theNext() {
    if (nextAddress < addresses.length) {
      setTimeout(getAddress.bind(null, addresses[nextAddress], theNext), delay);
      nextAddress++;
    }
  }

  // ======= Call that function for the first time =======
  theNext();

// This Javascript is based on code provided by the
// Community Church Javascript Team
// http://www.bisphamchurch.org.uk/
// http://econym.org.uk/gmap/

//]]>
