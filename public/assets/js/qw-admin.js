Array.prototype.toggle = function(element) {
  var index = this.indexOf(element);
  if (index === -1) {
      this.push(element);
    } else {
      this.splice(index, 1);
    }
}
