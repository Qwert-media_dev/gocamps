<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year')->default('2017');
            $table->integer('school_id');
            $table->string('strand')->default('');
            $table->string('status')->default('');
            $table->string('quantity_children')->default('');
            $table->string('quantity_children_work')->default('');
            $table->string('project')->default('');
            $table->string('type_camp')->default('');
            $table->string('railway_station')->default('');
            $table->string('sdirector_name')->default('');
            $table->string('sdirector_phone')->default('');
            $table->string('sdirector_email')->default('');
            $table->string('cdirector_phone')->default('');
            $table->string('cdirector_email')->default('');
            $table->string('cdirector_name_ua')->default('');
            $table->string('cdirector_name_eng')->default('');
            $table->string('teacher_name')->default('');
            $table->string('teacher_phone')->default('');
            $table->string('teacher_email')->default('');
            $table->string('eteacher_name')->default('');
            $table->string('eteacher_phone')->default('');
            $table->string('eteacher_email')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools_years');
    }
}
