<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('birthday');
            $table->integer('gender')->default(0);
            $table->string('passport')->default('');
            $table->string('email')->default('');
            $table->string('skype')->default('');
            $table->string('social')->default('');
            $table->string('phone')->default('');
            $table->string('alt_phone')->default('');
            $table->string('strand')->default('');
            $table->string('last_name')->default('');
            $table->string('first_name')->default('');
            $table->string('country')->default('');
            $table->string('city')->default('');
            $table->string('street')->default('');
            $table->string('been_in_ukraine')->default('');
            $table->string('capacity')->default('');
            $table->string('eng_level')->default('');
            $table->string('other_lang')->default('');
            $table->string('education')->default('');
            $table->text('describe');
            $table->text('about');
            $table->text('why');
            $table->string('sessions')->default('');
            $table->string('hear')->default('');
            $table->string('dietary')->default('');
            $table->string('lang')->default('');
            $table->string('citizenship')->default('');
            $table->string('intrested')->default('');
            $table->string('special')->default('');
            $table->string('speak')->default('');
            $table->string('level')->default('');
            $table->string('priority')->default('');
            $table->string('alt_strand')->default('');
            $table->string('preferences')->default('');
            $table->string('cityzenship')->default('');
            $table->string('registered_at')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
