<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteerYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer_years', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('volunteer_id');
            $table->string('year')->default('2017')->default('');
            $table->string('manager')->default('');
            $table->text('comment');
            $table->integer('status')->default(0);
            $table->integer('tickets')->default(0);
            $table->integer('visa')->default(0);
            $table->integer('payment')->default(0);
            $table->text('sended_mails');
            $table->text('documents');
            $table->string('processed_at')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_years');
    }
}
