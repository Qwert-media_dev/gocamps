<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->string('pdf')->default('');
            $table->string('experience')->default('');
            $table->string('rate')->default('');
            $table->string('order_status')->default('');
            $table->string('video')->default('');
            $table->string('task')->default('');
            $table->string('city')->default('');
            $table->string('street')->default('');
            $table->string('street_eng')->default('');
            $table->string('state')->default('');
            $table->string('house')->default('');
            $table->string('description')->default('');
            $table->string('manager')->default('');
            $table->string('comment')->default('');
            $table->string('address')->default('');
            $table->string('description2')->default('');
            $table->string('camp_lang')->default('');
            $table->string('photos')->default('');
            $table->string('title_eng')->default('');
            $table->string('city_eng')->default('');
            $table->string('house_eng')->default('');
            $table->string('state_eng')->default('');
            $table->string('about_school_eng')->default('');
            $table->string('about_city_eng')->default('');
            $table->string('about_school')->default('');
            $table->string('about_city')->default('');
            $table->string('rating')->default('');
            $table->string('lat')->default('');
            $table->string('lng')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
