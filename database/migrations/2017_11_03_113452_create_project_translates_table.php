<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('lang');
            $table->string('title');
            $table->text('text');
            $table->text('accordion');
            $table->string('slug');
            $table->string('keywords');
            $table->string('description');
            $table->string('seo_title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_translates');
    }
}
