<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->string('lang');
            $table->string('title');
            $table->text('content');
            $table->string('slug');
            $table->string('seo_title');
            $table->string('description');
            $table->string('keywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_translates');
    }
}
