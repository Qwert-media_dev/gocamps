<?php

return [
    'header_what_is'=>'Що таке GoCamp',
    'news'=>'Новини',
    'not_found'=>'Не знайдено',
    'header_projects'=>'Проекти',
    'header_volunteer'=>'For volunteers',
    'header_school'=>'Школам',
    'header_contacts'=>'Контакти',
    'header_news'=>'Новини',
        'header_materials'=>'Матеріали',

	'slider_heading_slide1'=>'Що таке GoCamp?',
    'slider_heading_slide2'=>'GoCamp Волонтери',
    'slider_text_slide1'=>'GoCamp – це найбільша волонтерська програма в Східній Європі, в якій волонтери зі всієї планети мотивують школярів вивчати іноземні мови та відкривають світ для дітей з усіх куточків України.',
    'slider_text_slide2'=>'GoCamp is looking forward to your participation in a new wave of English summer camps for children in 2017, including those affected by the armed conflict in Eastern Ukraine. Consider this great opportunity to volunteer abroad!',
    'news_heading'=>'Новини'


];
