<?php

return [
    'header_what_is'=>'About GoCamp',
    'news'=>'News',
    'not_found'=>'Not Found',
    'header_projects'=>'Projects',
    'header_volunteer'=>'For volunteers',
    'header_school'=>'For schools',
    'header_contacts'=>'Contacts',
        'header_news'=>'News',
    'header_materials'=>'Materials',

    'slider_heading_slide1'=>'What is GoCamp?',
    'slider_heading_slide2'=>'GoCamp Volunteers',
    'slider_text_slide1'=>'GoCamp is the biggest volunteer programme in Eastern Europe, where volunteers from the whole globe motivate kids to learn foreign languages and introduce the world to children from all over Ukraine.',
    'slider_text_slide2'=>'Our volunteers are diverse. Each of them contributes unique personal experience, extraordinary success stories, skills, and interests to make every GoCamp special and encourage children to learn. Join us and make a change!',
    'news_heading'=>'News'
];
