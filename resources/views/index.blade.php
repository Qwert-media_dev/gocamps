@extends("$model->layout")
@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
@endsection


@section('header')
<div class="swiper-container">
  <div class="swiper-wrapper">
    <div class="start-home swiper-slide">
      <div class="start-home__inner">
        <h1 class="start-home__title"><?=Lang::get('main.slider_heading_slide1', array(), LaravelLocalization::getCurrentLocale())?></h1>
        <p class="start-home__text"><?=Lang::get('main.slider_text_slide1', array(), LaravelLocalization::getCurrentLocale())?></p>
        <a class="start-home__join btn" href="/volunteer">For volunteers</a>
        <a class="start-home__join btn" href="/school">For schools</a>
      </div>
    </div>
    <div class="start-home start-home--volunteers swiper-slide">
      <div class="start-home__inner">
       <h1 class="start-home__title"><?=Lang::get('main.slider_heading_slide2', array(), LaravelLocalization::getCurrentLocale())?></h1>
       <p class="start-home__text"><?=Lang::get('main.slider_text_slide2', array(), LaravelLocalization::getCurrentLocale())?></p>
       <a class="start-home__join btn" href="/volunteer">For volunteers</a>
       <a class="start-home__join btn" href="/school">For schools</a>
     </div>
   </div>
 </div>
 <div class="swiper-pagination"></div>
 <div class="swiper-button-prev"></div>
 <div class="swiper-button-next"></div>
</div>
@endsection


@section('content')
{!! html_entity_decode($model->content) !!}

<section class="home-news">
  <h2 class="home-news__title"><?=Lang::get('main.news_heading', array(), LaravelLocalization::getCurrentLocale())?></h2>
  <div class="home-news__list">
  @foreach ($articles as $article)
    <div class="home-news__item">
      @if(!empty($article->image) && isset( $article->image))
      <a href="/{{ LaravelLocalization::getCurrentLocale() . '/' . $category_slug . '/' . $article->slug }}">
        <img class="home-news__img" src="/image/{{ $article->image }}">
      </a>
      @endif

      <p class="home-news__text">
        <a href="/{{ LaravelLocalization::getCurrentLocale() . '/' . $category_slug . '/' . $article->slug }}">{{ $article->title }}</a>
      </p>
    </div>
  @endforeach
</section>

</div><!-- .container -->
<div class="container">

<section class="partners">
  <img src="img/partners.svg" alt="our partners">
</section>
@endsection


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>
<script>
var schools=<?=$schools?>;
var addresses = schools;
var tk = '{{ csrf_token() }}';
var locale = '{{ $locale }}';
</script>
@endsection
