@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner"><?=$model->name?></span>
</h1>
@endsection

@section('content')
{!! html_entity_decode($model->content) !!}
@endsection
