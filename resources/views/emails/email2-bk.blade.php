<p>Dear Volunteer,</p>
<p>Thank you for a fantastic interview for GoCamp Afterschool! We are happy to confirm your participation in the Afterschool programme as a volunteer! You will be a part of a nationwide project that brings modern and innovative camp experience to the Ukrainian children. You will become a language and cultural Ambassador in Ukraine!</p>
<p>After receiving this letter, you will also be contacted by the account manager via email. This person is going to be your main point of contact during the preparations before coming to Ukraine. All your further GoCamp-related communication will be conducted with your account manager.</p>
<p>
  <b>➢	Next steps</b><br/>
  Further steps that should be done are following:
  <ol>
    <li>Gathering required documents.
    <li>Discussing your placement with the manager via email or Skype. You will receive more details on possible options in a separate email from your account manager.</li>
    <li>Provide your ticket to Kyiv (within a month after receiving this letter).</li>
    <li>After we get your tickets, we will be able to confirm on the school. </li>
    <li>Receiving an e-introducing letter to the school representatives.</li>
    <li>Communication with local teachers, discussing the programme, preparing homework.</li>
    <li>Paying the participation fee.</li>
    <li>Arrival, passing the training for volunteers.</li>
    <li>Let’s GoCamp!</li>
  </ol>
</p>
<p><b>NB: </b>We are sending an e-introducing letter only <b>after the receiving tickets</b>. So, the sooner you send us your tickets, the earlier you will know your location.</p>
<p><b>Your next step: Collecting and submitting the documents for GoCamp.</b></p>
<p>Please, consider that you have a <b>one month term to submit all the documents</b> required for participation in GoCamp, except the health check (should be issued and sent to the GoCamp manager not earlier than two months before the start of the programme). Below is a list of documents we ask you to provide as scanned copies:
  <ul>
    <li>employment history (CV) </li>
    <li>copy of your valid international passport</li>
    <li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality) </li>
    <li>health check (statement of good health from your local doctor, is valid for three months before the closing date of the programme, March 16. If you have any health issues or chronic illnesses, please notify us!)</li>
    <li>criminal record (is valid for six months since the date of issue till the date of submission, so you’ll need to apply for a new one if you possess an older document)</li>
    <li>international medical insurance </li>
    <li>copy of tickets (note that the earlier you send us the tickets, the sooner we will start arranging a school for you).</li>
  </ul>
</p>
