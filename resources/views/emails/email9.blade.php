Dear Volunteer,
<br/>
<br/>
<br/>
We are happy to inform that you have almost completed your registration for <strong>GoCamp</strong>!
<br/>
<br/>
This is the 4th step of your application: <strong>Paying the Participation Fee</strong>. You can do it in the way that is most convenient for you:
<ol>
 	<li>Go to our website (<a href="http://gocamps.com.ua/en">http://gocamps.com.ua/en</a>) and push the button `Donate`</li>
 	<li>Make a bank transfer payment, find bank details here:</li>
</ol>
<a href="https://app.box.com/s/puc77sazj8t58kuw641yw8yd71nh2jul">https://app.box.com/s/puc77sazj8t58kuw641yw8yd71nh2jul</a>
<br/>
<br/>
Please, <strong>point out the purpose of payment</strong> - a donation from (name and surname) to specify your identity while doing this transaction.
<br/>
<br/>
<strong>The participation fee is 75$ or 70€ or 58£. </strong>
<br/>
Please, make a <strong>screenshot of your payment</strong> completion and share it with your manager or save the receipt.
<br/>
<br/>
<strong>N.B! </strong>If you cannot fulfil the payment, contact your bank and confirm you allow the transfer. The bank that issued your card may block payments under its security policy or financial monitoring.
<br/>
<br/>
In the <a href="https://app.box.com/s/puc77sazj8t58kuw641yw8yd71nh2jul">same box</a>, you can find the examples of GoCamp <strong>self-declaration document</strong> and <strong>volunteer’s agreement</strong>. Please, review both of these documents beforehand - you will sign the hard copies when you come to the orientation training.
<br/>
<br/>
Thank you for joining <strong>GoCamp </strong>programme! We are looking forward to meeting you!
<br/>
<br/>
<br/>
Best wishes,
<br/>
GoCamp Team
