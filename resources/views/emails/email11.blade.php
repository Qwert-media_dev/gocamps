Dear Volunteer,
<br/>
<br/>
<br/>
Thank you for a fantastic interview for <strong>GoCamp</strong>! We are happy to confirm your participation in the <strong>GoCamp </strong>programme as a volunteer! You will be a part of a nationwide project that brings modern and innovative camp experience to the Ukrainian children. You will become a language and cultural Ambassador in Ukraine!
<br/>
<br/>
After receiving this letter, you will also be contacted by the <strong>account manager</strong> via email. This person is going to be your main point of contact during the preparations before coming to Ukraine. All your further GoCamp-related communication will be conducted with your account manager.
<br/>
<br/>
<strong>Further steps that should be done are following:</strong>
<ol>
 	<li>Paying the <strong>participation fee </strong>(please try to do it ASAP as there is not much time left).</li>
 	<li>Discussing your placement with the manager. You will receive more details on possible options in a separate email from your CoCamp manager. Only after you proceed with the payment, we will be able to <strong>confirm on the school</strong>.</li>
 	<li>Start<strong> gathering the required documents</strong> and submit them <strong>ASAP</strong>.</li>
 	<li>Receiving an<strong> e-introducing letter</strong> to the school representatives.</li>
 	<li>Communication with local teachers and <strong>discussing the program</strong>.</li>
 	<li>Providing your <strong>ticket </strong>to Kyiv (latest - <strong>one month before the start</strong> of the program).</li>
 	<li>Arrival, passing the <strong>training for volunteers</strong>.</li>
 	<li>Let’s <strong>GoCamp</strong>!</li>
</ol>
<strong>NB</strong>: We are sending an e-introducing letter only <strong>after receiving a participation fee. </strong>So, the sooner you proceed with the payment, the earlier you know your location. Payment details will be forwarded to you in a separate letter.
<h3>Documents</h3>
Please, consider that you have a <strong>two weeks term to submit all the documents</strong> required for participation in <strong>GoCamp</strong>, except the health check (should be issued and sent to the GoCamp manager not earlier than two months before the start of the programme). Here is a list of documents we ask you to provide as scanned copies:
<ul>
 	<li>employment history (CV)</li>
 	<li>copy of your valid international passport</li>
 	<li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality)</li>
 	<li>health check (statement of good health from your local doctor (we don't need any specific form), is valid for three months before the closing date of the programme. If you have any health issues or chronic illnesses that you are concerned might hinder your performance as a volunteer, please notify us)</li>
 	<li>criminal record (is valid for six months since the date of issue till the date of submission, so you’ll need to apply for a new one if you possess an older document)</li>
 	<li>international medical insurance</li>
 	<li>copy of tickets (note that the earlier you send us the tickets, the sooner we will start arranging a school for you).</li>
</ul>
Criminal and health check terms
<br/>
<br/>
<strong>NB: </strong><u>All documents should be submitted in English</u>. You can translate them by yourself and put "I, YOUR NAME, hereby certify that this translation is full and correct", your signature and date on each page. In case you are translating your documents, please, send both the translation and the original.
<h3>Logistics and accommodations</h3>
We are providing:
<ul>
 	<li><strong>Pick-up</strong> from the airport <strong>on 12th of September only </strong>(To organize your transfer from the airport in the best way, we need to have a scanned copy of your flight/bus/train ticket from your location to Kyiv, Ukraine)</li>
 	<li><strong>Accommodation</strong> during the training session</li>
 	<li><strong>Meals </strong>during the training</li>
 	<li><strong>Transfer </strong>to the camp location</li>
 	<li>During the camp, accommodation and meals are provided by the school representatives (<strong>host family</strong>)</li>
 	<li><strong>Transfer </strong>from the camp location <strong>to the Kyiv central railway station</strong></li>
</ul>
<strong>NB: </strong>We do NOT provide transfers to the airport on your way back home and accommodation after the end of the programme. You need to arrange your stay for an extra-night in Kyiv (in case you will have one) and transfer to the airport by yourself. In case you need assistance or advice, contact your account manager.
<h3>Calendar</h3>
<strong>September 12</strong> - arrival day
<br/>
<strong>September 12-14</strong> - main training sessions
<br/>
<strong>September 15</strong>, second half of the day, evening - departure to the camp locations
<br/>
<strong>September 15</strong>, evening - <strong>September 16</strong>, morning - school representatives meet you at the railway station
<br/>
<strong>September 16-28</strong>- GoCamp!
<br/>
<strong>September 28</strong>, second half of the day or <strong>September 29</strong> - departure to Kyiv (the date depends on the distance to Kyiv and is negotiable, so let us know when you need to get back)
<br/>
<br/>
<strong>NB: </strong>The best option is to book your tickets from Kyiv on the dates starting from September 29. Yet, we are also thinking of organizing a wrap-up meeting after volunteers’ return to Kyiv, most likely on September 29. If you can spend some additional time in Kyiv, we will be happy to see you at the final meeting (consider that we are not providing an accommodation after the end of the programme).
<h3>Homework</h3>
Despite school representatives are responsible for preparing a schedule and program, you can also suggest your ideas. Apart from this, we will ask you to prepare a <strong>home task. </strong><a href="https://app.box.com/s/ih894pzg8kqj7homatt5ekebt5o7fs99">In this file</a>, is a list of possible variants - you can choose any or a few of them or create your own and easily make the camp entertaining and useful! By the way, during the training, there is going to be a country presentation session as well where you will present your native country with your compatriots. You will have some time to prepare something short and funny, for example, a song.
<br/>
Also, <a href="https://app.box.com/s/mlsk283vby6r1a4t1swmiyx9cdakt3ek">here</a> is some content developed by our colleagues and partners - <strong>games, songs</strong>, and other <strong>helpful materials</strong> to study beforehand and apply in the camp!
<br/>
<br/>
Should you have any questions do not hesitate to contact your manager.
<br/>
Also, welcome to join <a href="https://www.facebook.com/groups/324347421393732/">our Facebook group for volunteers</a>!
<br/>
<br/>
Thank you and let’s have an amazing journey together :)
<br/>
<br/>
<br/>
Best wishes,
<br/>
GoCamp team
