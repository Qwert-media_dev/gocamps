Dear Volunteer,
<br/>
Thank you for a fantastic interview for <strong>GoCamp Afterschool</strong>! We are happy to confirm your participation in the <strong>Afterschool </strong>programme as a volunteer! You will be a part of a nationwide project that brings modern and innovative camp experience to the Ukrainian children. You will become a language and cultural Ambassador in Ukraine!
After receiving this letter, you will also be contacted by the <strong>account manager</strong> via email. This person is going to be your main point of contact during the preparations before coming to Ukraine. All your further GoCamp-related communication will be conducted with your account manager.
<ul>
 	<li>Next steps</li>
</ul>
Further steps that should be done are following:
<ol>
 	<li>Gathering required documents.</li>
 	<li>Discussing your placement with the manager via email or Skype. You will receive more details on possible options in a separate email from your account manager.</li>
 	<li>Provide your <strong>ticket </strong>to Kyiv (within a month after receiving this letter).</li>
 	<li>After we get your tickets, we will be able to <strong>confirm on the school</strong>.</li>
 	<li>Receiving an<strong> e-introducing letter</strong> to the school representatives.</li>
 	<li>Communication with local teachers, <strong>discussing the programme</strong>, preparing homework.</li>
 	<li>Paying the <strong>participation fee</strong>.</li>
 	<li>Arrival, passing the <strong>training for volunteers</strong>.</li>
 	<li>Let’s <strong>GoCamp</strong>!</li>
</ol>
<strong>NB</strong>: We are sending an e-introducing letter only <strong>after the receiving tickets. </strong>So, the sooner you send us your tickets, the earlier you will know your location.
<strong>Your next step: Collecting and submitting the documents for GoCamp.</strong>
Please, consider that you have a <strong>one month term to submit all the documents</strong> required for participation in GoCamp, except the health check (should be issued and sent to the GoCamp manager not earlier than two months before the start of the programme). Below is a list of documents we ask you to provide as scanned copies:
<ul>
 	<li>employment history (CV)</li>
 	<li>copy of your valid international passport</li>
 	<li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality)</li>
 	<li>health check (statement of good health from your local doctor, is valid for three months before the closing date of the programme, March 16. If you have any health issues or chronic illnesses, please notify us!)</li>
 	<li>criminal record (is valid for six months since the date of issue till the date of submission, so you’ll need to apply for a new one if you possess an older document)</li>
 	<li>international medical insurance</li>
 	<li>copy of tickets (note that the earlier you send us the tickets, the sooner we will start arranging a school for you).</li>
</ul>
Criminal and health check terms
<br/>
<br/>
<table width="643" border="1">
  <tbody>
    <tr>
      <td width="321">Criminal check</td>
      <td width="321">Health check</td>
    </tr>
    <tr>
      <td width="321">should be issued not earlier than 6 months before submission to the GoCamp manager</td>
      <td width="321">should be issued not earlier than 16.12.2017</td>
    </tr>
  </tbody>
</table>
<br/>
NB: <u>All documents should be submitted in English</u>. You can translate them by yourself and put "I, YOUR NAME, hereby certify that this translation is full and correct", your signature and date on each page. In case you are translating your documents, please, send both the translation and the original
<ul>
 	<li>VISA</li>
</ul>
We assist with VISA issues by sending invitation letters but dealing with the application process is on you. Please, double check <a href="http://mfa.gov.ua/en/consular-affairs/entering-ukraine/visa-requirements-for-foreigners">here whether you need a visa</a> and inform your manager if it is required for your to enter Ukraine. In that case, add to the email a scanned copy of your passport and the address of the Ukrainian embassy you are going to apply to (country, city, street, house number, etc.)
<ul>
 	<li>Logistics and accommodations</li>
</ul>
We are providing:
<ul>
 	<li><strong>Pick-up</strong> from the airport (To organize your transfer from the airport in the best way, we need to have a scanned copy of your flight/bus/train ticket from your location to Kyiv, Ukraine)</li>
 	<li><strong>Accommodation</strong> during the training session</li>
 	<li><strong>Meals </strong>during the training</li>
 	<li><strong>Transfer </strong>to the camp location</li>
 	<li>During the camp, accommodation and meals are provided by the school representatives (<strong>host family</strong>)</li>
 	<li><strong>Transfer </strong>from the camp location <strong>to the Kyiv central railway station</strong></li>
</ul>
<strong>NB: </strong>We do NOT provide transfers to the airport on your way back home and accommodation after the end of the programme. You need to arrange your stay for an extra-night in Kyiv and transfer to the airport by yourself. In case you need assistance or advice, contact your account manager.
<ul>
 	<li>Calendar</li>
</ul>
<strong>February 23</strong> - arrival day
<br/>
<strong>February 23-25</strong> - main training sessions
<br/>
<strong>February 25</strong>, second half of the day, evening - departure to the camp locations
<br/>
<strong>February 25</strong>, evening - <strong>February 26</strong>, morning - school representatives meet you at the railway station
<br/>
<strong>February 26 - March 16</strong> - GoCamp!
<br/>
<strong>March 16</strong>, second half of the day- departure (the date is negotiable, let us know when you need to get back)
<ol start="17">
 	<li>We are thinking of organizing a wrap-up meeting after volunteers’ return to Kyiv, perhaps, on March 17. If you can spend some additional time in Kyiv, we will be happy to see you at the final meeting (consider that we are not providing an accommodation after the end of the programme).</li>
</ol>
<ul>
 	<li>Homework</li>
</ul>
Despite school representatives are responsible for preparing a schedule and program, you can also suggest your ideas. Apart from this, we will ask you to prepare a <strong>home task. </strong><a href="https://app.box.com/s/ih894pzg8kqj7homatt5ekebt5o7fs99">In this file</a>, is a list of possible variants - you can choose any or a few of them or create your own and easily make the camp entertaining and useful! By the way, during the training, there is going to be a country presentation session as well where you will present your native country with your compatriots. You will have some time to prepare something short and funny, for example, a song.

Also, <a href="https://app.box.com/s/mlsk283vby6r1a4t1swmiyx9cdakt3ek">here</a> is some content developed by our colleagues and partners - <strong>games, songs</strong>, and other <strong>helpful materials</strong> to study beforehand and apply in the camp!
<br/>
Should you have any questions do not hesitate to contact your manager.
<br/>
Thank you and let’s have an amazing journey together! :)
<br/>
<br/>
Best wishes,
<br/>
GoCamp team
