Dear Candidate,
<br/>
<br/>
<br/>
We thank you for your interest in <strong>GoCamp</strong>!
<br/>
<br/>
Unfortunately, we cannot offer you a volunteering position this year, but we invite you to apply for next year when GoGlobal language camp programme is going to be even larger in scale!
<br/>
<br/>
Connect with us on social media and be first to find out about the future volunteer opportunities <strong>GoCamp </strong>offers.
<br/>
<br/>
We regret that you were not chosen. Hope you will apply again for volunteer work with GoCamp!
<br/>
<br/>
<br/>
All the best,
<br/>
GoCamp team
