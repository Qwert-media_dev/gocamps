<p>Dear friend,</p>
<p>&nbsp;</p>
<p>Thank you for your interest in joining our school-based camp programme in Ukraine and congrats on passing the interview with our manager! We would be really glad to see you among <strong>GoCamp </strong>volunteers!</p>
<p>&nbsp;</p>
<p><strong>Please update your GoCamp manager </strong>(you will receive a separate email from your manager shortly)<strong> on your decision about the GoCamp session as soon as possible since the number of positions is getting fewer. </strong></p>
<p>&nbsp;</p>
<p>We are reminding you that, you can choose any of these sessions:</p>
<p>&nbsp;</p>
<p>3-month session:</p>
<p><strong>May 30 &ndash; Aug 23</strong></p>
<p>3 week session:</p>
<p><strong>May 30 - June 14 </strong></p>
<p><strong>June 06 &ndash; June 21</strong></p>
<p><strong>August 09 - August 23</strong></p>
<p>&nbsp;</p>
<p><strong>After the session will be agreed, the further steps that will have to be done are following:</strong></p>
<ul>
<li>Paying the <strong>participation fee </strong></li>
<li>Discussing your placement with the manager. You will receive more details on possible options in a separate email from your CoCamp manager. Only after you proceed with the payment, we will be able to <strong>confirm on the school</strong>.</li>
<li>Start<strong> gathering the required documents</strong> and submit them after the final decision concerning the session.</li>
<li>Receiving an<strong> e-introducing letter</strong> to the school representatives.</li>
<li>Communication with local teachers and <strong>discussing the program</strong>.</li>
<li>Providing your <strong>ticket </strong>to Kyiv (latest - <strong>one month before the start</strong> of the program).</li>
<li>Arrival, passing the <strong>training for volunteers</strong>.</li>
<li>Let&rsquo;s <strong>GoCamp</strong>!</li>
</ul>
<p>&nbsp;</p>
<p>The <strong>documents you will need to submit</strong> via email are:</p>
<ul>
<li>employment history (CV)</li>
</ul>
<ul>
<li>copy of your valid international passport</li>
</ul>
<ul>
<li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality)</li>
</ul>
<ul>
<li>health check (statement of good health from your local doctor, is valid for 3 months before the closing date of the programme. If you have any health issues or chronic illnesses that you are concerned might hinder your performance as a volunteer, please notify us)</li>
</ul>
<ul>
<li>criminal record (is valid for six months since the date of issue till the date of submission, so you&rsquo;ll need to apply for a new one if you possess an older document)</li>
</ul>
<ul>
<li>international medical insurance</li>
</ul>
<ul>
<li>copy of tickets.</li>
</ul>
<p>&nbsp;</p>
<p>NB: <u>All documents should be submitted in English</u>. You can translate them by yourself and put "I, YOUR NAME, hereby certify that this translation is full and correct", your signature and date on each page. In case you are translating your documents, please, send both the translation and the original.</p>
<p>&nbsp;</p>
<p>We sincerely hope that you are still willing to take part in a project that changes lives both of young Ukrainians and our volunteers and are hoping to receive your answer at your earliest convenience.</p>
<p>&nbsp;</p>
<p>Best wishes,</p>
<p>GoCamp team</p>
