<p>Dear Volunteer,</p>
<p>&nbsp;</p>
<p>Thank you for a fantastic interview for <strong>GoCamp</strong>! We are happy to confirm your participation in the <strong>GoCamp </strong>programme as a volunteer! You will be a part of a nationwide project that brings modern and innovative camp experience to the Ukrainian children. You will become a language and cultural Ambassador in Ukraine!</p>
<p>&nbsp;</p>
<p>After receiving this letter, you will also be contacted by the <strong>account manager</strong> via email. This person is going to be your main point of contact during the preparations before coming to Ukraine. All your further GoCamp-related communication will be conducted with your account manager.</p>
<p>&nbsp;</p>
<p><strong>Further steps that should be done are following:</strong></p>
<ul>
<li>Paying the <strong>participation fee</strong>.</li>
<li>Discussing your placement with the manager. You will receive more details on possible options in a separate email from your CoCamp manager. Only after you proceed with the payment, we will be able to <strong>confirm on the school</strong>.</li>
<li>Start<strong> gathering the required documents</strong> and submit them.</li>
<li>Receiving an<strong> e-introducing letter</strong> to the school representatives.</li>
<li>Communication with local teachers and <strong>discussing the program</strong>.</li>
<li>Providing your <strong>ticket </strong>to Kyiv (latest - <strong>one month before the start</strong> of the program).</li>
<li>Arrival, passing the <strong>training for volunteers</strong>.</li>
<li>Let&rsquo;s <strong>GoCamp</strong>!</li>
</ul>
<p>&nbsp;</p>
<p><strong>NB</strong>: We are sending an e-introducing letter only <strong>after receiving a participation fee. </strong>So, the sooner you proceed with the payment, the earlier you know your location. Payment details will be forwarded to you in a separate letter.</p>
<p>&nbsp;</p>
<ul type="disc">
<li>Documents</li>
</ul>
<p>&nbsp;</p>
<p>Please, consider that the health check should be issued and sent to the GoCamp manager not earlier than 2 months before the start of the programme. Here is a list of documents we ask you to provide as scanned copies:</p>
<ul>
<li>employment history (CV)</li>
</ul>
<ul>
<li>copy of your valid international passport</li>
</ul>
<ul>
<li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality)</li>
</ul>
<ul>
<li>health check (statement of good health from your local doctor, is valid for 2 months before the closing date of the programme.If you have any health issues or chronic illnesses that you are concerned might hinder your performance as a volunteer, please notify us)</li>
</ul>
<ul>
<li>criminal record (is valid for six months since the date of issue till the date of submission, so you&rsquo;ll need to apply for a new one if you possess an older document)</li>
</ul>
<ul>
<li>international medical insurance</li>
</ul>
<ul>
<li>copy of tickets.</li>
</ul>
<p>&nbsp;</p>
<p>Criminal and health check terms</p>
<p>&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top">
<p>Criminal check</p>
</td>
<td valign="top">
<p>Health check</p>
</td>
</tr>
<tr>
<td valign="top">
<p>should be issued not earlier than 6 months before the start of the programme</p>
</td>
<td valign="top">
<p>should be issued not earlier than 2 months before the start of the programme</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>NB: <u>All documents should be submitted in English</u>. You can translate them by yourself and put "I, YOUR NAME, hereby certify that this translation is full and correct", your signature and date on each page. In case you are translating your documents, please, send both the translation and the original.</p>
<p>&nbsp;</p>
<ul type="disc">
<li>VISA</li>
</ul>
<p>We assist with VISA issues by sending invitation letters but dealing with the application process is on you. Please, double check <a href="http://mfa.gov.ua/en/consular-affairs/entering-ukraine/visa-requirements-for-foreigners">here whether you need a visa</a> and inform your manager if it is required for your to enter Ukraine. In that case, add to the email a scanned copy of your international passport and the address of the Ukrainian embassy you are going to apply to (country, city, street, house number, etc.)</p>
<p>&nbsp;</p>
<ul type="disc">
<li>Logistics and accommodations</li>
</ul>
<p>We are providing:</p>
<ul>
<li><strong>Pick-up</strong> from the airport on the arrival day (To organize your transfer from the airport in the best way, we need to have a scanned copy of your flight/bus/train ticket from your location to Kyiv, Ukraine)</li>
<li><strong>Accommodation</strong> during the training session</li>
<li><strong>Meals </strong>during the training session</li>
<li><strong>Transfer </strong>to the camp location</li>
<li>During the camp, accommodation and meals are provided by the school representatives (<strong>host family</strong>)</li>
<li><strong>Transfer </strong>from the camp location <strong>to the Kyiv central railway station</strong></li>
</ul>
<p><strong>NB: </strong>We do NOT provide transfers to the airport on your way back home and accommodation after the end of the programme. You need to arrange your stay for an extra-night in Kyiv (in case you will have one) and transfer to the airport by yourself. In case you need assistance or advice, contact your account manager.</p>
<p>&nbsp;</p>
<ul type="disc">
<li>Calendar</li>
</ul>
<p><strong>May 30</strong> - arrival day</p>
<p><strong>May 30 &ndash; June 2 </strong>&nbsp;- main training sessions</p>
<p><strong>June 2</strong>, second half of the day, evening - departure to the camp locations</p>
<p><strong>June 2</strong> -evening / <strong>June 3 -</strong> morning - school representatives meet you at the railway station</p>
<p><strong>June 3 - 14</strong> - GoCamp!</p>
<p><strong>June 14</strong>, second half of the day or <strong>June 15</strong> - departure to Kyiv (the date depends on the distance to Kyiv and is negotiable, so let us know when you need to get back)</p>
<p>&nbsp;</p>
<p><strong>NB. </strong>The best option is to book your tickets from Kyiv on the dates starting from June 14. Yet, we are also thinking of organizing a wrap-up meeting after volunteers&rsquo; return to Kyiv, most likely on June 15. If you can spend some additional time in Kyiv, we will be happy to see you at the final meeting (consider that we are not providing accommodation after the end of the programme).</p>
<p>&nbsp;</p>
<ul type="disc">
<li>Homework</li>
</ul>
<p>Despite school representatives are responsible for preparing a schedule and program, you can also suggest your ideas. Apart from this, we will ask you to prepare a <strong>home task. </strong><a href="https://app.box.com/s/ih894pzg8kqj7homatt5ekebt5o7fs99">In this file</a>, is a list of possible variants - you can choose any or a few of them or create your own and easily make the camp entertaining and useful! By the way, during the training, there is going to be a country presentation session as well where you will present your native country with your compatriots. You will have some time to prepare something short and funny, for example, a song.</p>
<p>Also, <a href="https://app.box.com/s/mlsk283vby6r1a4t1swmiyx9cdakt3ek">here</a> is some content developed by our colleagues and partners - <strong>games, songs</strong>, and other <strong>helpful materials</strong> to study beforehand and apply in the camp!</p>
<p>&nbsp;</p>
<p>Should you have any questions do not hesitate to contact your manager.</p>
<p>Also, welcome to join <a href="https://www.facebook.com/groups/1124258084388337/">our Facebook group for volunteers 2019</a>!</p>
<p>&nbsp;</p>
<p>Thank you and let&rsquo;s have an amazing journey together :)</p>
<p>&nbsp;</p>
<p>Best wishes,</p>
<p>GoCamp team</p>
