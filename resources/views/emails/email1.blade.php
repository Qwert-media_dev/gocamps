<p>Dear volunteer,</p>
<br/>
<p>Thank You for Applying!</p>
<p>We review applications on a rolling basis. Due to a high number of applications, you will receive a reply from our managers within the next two weeks. The best applicants will get an invitation for a Skype interview via e-mail.</p>
<p>Thank you very much for your patience, interest in our project, and your willingness to help the new generation of Ukrainians!</p>
<p>Best wishes,<br/>
GoCamp team</p>
