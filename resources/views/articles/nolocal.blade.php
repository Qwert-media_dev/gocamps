@extends("$model->layout")

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner"><?=$model->title?></span>
</h1>
@endsection

@section('content')
  <p style="font-size: 36px;">
    @if ( $model->lang == 'en' )
      Нажаль, ця новина доступна лише англійською
    @else
      Ufortunately this article available only on ukrainian
    @endif
  </p>
<div style="clear: both;"></div>
@endsection
