@extends("$model->layout")

@section('header')
  
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner"><?=$model->title?></span>
</h1>
@endsection

@section('content')
{!! html_entity_decode($model->body) !!}
<div>
@foreach(explode(',', $model->gallery) as $one)
	@if(!empty($one))
		<div style="float:left;max-width:300px;">
			<a class="gallery" rel="group" href="/gallerys/{{$one}}"><img style="max-width:100%" src="/gallerys/{{$one}}" alt="" /></a>
		</div>
	@endif
@endforeach
</div>
<div style="clear: both;"></div>
@endsection
