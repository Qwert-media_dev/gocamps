@extends('layouts.master')

@section('content')
<section class="registration-success">
  <h2 class="registration-success__title">Thank You for Applying! 

</h2>
  <p class="registration-success__text">Dear volunteer, 
<br />
Applications are being reviewed on a rolling basis. If we determine that you are a good match for our program, we will invite you for a Skype interview within 2 weeks.  

Best wishes,
GoCamp team</p>
</section>

<section class="partners">
  <img src="/img/partners.svg" alt="our partners">
</section>
@endsection
