@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/categories/edit/'.$category->category_id,'files'=>true]) !!}
  @if(count($errors))
    @foreach ($errors->all() as $error)
       <div>{{ $error }}</div>
   @endforeach
 @endif

  <div class="row">
    <div class="col-md-3 pull-right">
      <?= Form::submit('Save', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
    <div class="col-md-2 pull-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Delete category</button>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('title', 'Title:') ?>
        <?= Form::text('title', $category->title, ['placeholder'=>'Title', 'class' => 'form-control']) ?>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('slug', 'Slug:') ?>
        <?= Form::text('slug',  $category->slug, ['placeholder'=>'Slug', 'class' => 'form-control']) ?>
      </div>
    </div>
  </div>

<div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('layout', 'Layout:') ?>
        <?= Form::select('layout',  $layouts,$category->layout,['class' => 'form-control']) ?>
      </div>
    </div>
<!-- TABS -->
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
        </ul>
        <div class="tab-content">
                <!-- SEO -->
                <div id="seo" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_title', 'Seo Title:') ?>
                                <?= Form::text('seo_title',  $category->seo_title, ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_keywords', 'Seo Keywords:') ?>
                                <?= Form::text('keywords',  $category->keywords, ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= Form::label('description', 'Seo Description:') ?>
                                <?= Form::textArea('description',  $category->description, ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
  <input id="category_id" name="category_id" type="hidden" value="<?=$category->category_id?>" />
  <input id="old_id" name="old_id" type="hidden" value="<?=$category->category_id?>" />
  <br>
   <br>
  <input id="language" name="language" type="hidden" value="<?=$category->lang?>" />
  @if(!$have_translate)
   <div class="row">
      <div class="col-md-12">
        <a class="create-translate"> Create translate </a>
      </div>
    </div>
   <div class="modal-trans" id="mod" style="display:none">
       <select class="langs">
       @foreach($langs as $lang)
        @if($lang==LaravelLocalization::getCurrentLocale())
         <option value="<?=$lang?>" disabled><?=$lang?></option>
        @else
          <option value="<?=$lang?>"><?=$lang?></option>
        @endif
       @endforeach
       </select>
       <input class="ok" type="button" value="Translate">
       <span class="close" id="close-trans">X</span>
   </div>
   <br>
   <div class="row">
      <div class="col-md-12">
        <a class="add-translate"> Add translate to existing category </a>
      </div>
    </div>

    <div class="modal-add" id="mod" style="display:none">
       <select class="l-categories">
       @foreach($categories as $category)

         <option value="<?=$category->category_id?>"><?=$category->title?></option>
       @endforeach
       </select>
       <input class="add-ok" type="button" value="Translate">
       <span class="close" id="close-add">X</span>

   </div>
   @endif
    @if(count($have_langs)>1)
   <div class="row">
      <div class="col-md-12">
        This category have translates:<br>
       @foreach($have_langs as $one)
        @if($one->lang!=LaravelLocalization::getCurrentLocale())
          <a href="/{!! $one->lang !!}/admin/categories/edit/{{$category->category_id}}">{{$one->lang}}</a> <a href="/{!! $one->lang !!}/admin/categories/unlink/{{$category->category_id}}">Unlink</a><br>
        @endif
      @endforeach
    </div>

   @endif
  {!! Form::close() !!}
</div>
<script type="text/javascript">
$('.create-translate').click(function(){
  $('.modal-trans').css('display','block');
});
$('.add-translate').click(function(){
  $('.modal-add').css('display','block');
});
$('.ok').click(function(){
  $('#language').val($('.langs  option:selected').val());
  var id=$('#category_id').val();
  window.location.href="/"+$('#language').val()+"/admin/categories/create/?category_id="+id;
});

$('.id-translate').click(function(){
  $('.modal-add').css('display','block');
  $('#category_id').val($('.l-categories  option:selected').val());
});
$('.l-categories').change(function(){
  console.log($(this).val());
  $('#category_id').val($(this).val());
});
$('.add-ok').click(function(){
  var id=$('.l-categories  option:selected').val();
  var locale=$('.l-langs  option:selected').val();
  $('#category_id').val($('.l-categories  option:selected').val());

  $('form').submit();

});
$('#close-trans').click(function(){$('.modal-trans').hide();});
$('#close-add').click(function(){$('.modal-add').hide();});
</script>
<style type="text/css">
#mod {
    padding: 20px;
    border: 1px solid;
    width: 25%;
}</style>
 <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing category <?=$category->title?> </h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/categorys/delete/{{$category->category_id}}'">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>

@include('admin/footer')
