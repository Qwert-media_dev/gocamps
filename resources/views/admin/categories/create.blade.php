@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<div>
@if(count($errors))
    @foreach ($errors->all() as $error)
       <div>{{ $error }}</div>
   @endforeach
 @endif
  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/articles/create','files'=>true]) !!}
  <div class="row">
    <div class="col-md-3 pull-right">
      <?= Form::submit('Save', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('title', 'Title:') ?>
        <?= Form::text('title', '', ['placeholder'=>'Title', 'class' => 'form-control']) ?>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('slug', 'Slug:') ?>
        <?= Form::text('slug',  '', ['placeholder'=>'Slug', 'class' => 'form-control']) ?>
      </div>
    </div>


  </div>


  </div>
<div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('layout', 'Layout:') ?>
        <?= Form::select('layout',  $layouts,['class' => 'form-control']) ?>
      </div>
    </div>
<!-- TABS -->
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
        </ul>
        <div class="tab-content">
                <!-- SEO -->
                <div id="seo" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_title', 'Seo Title:') ?>
                                <?= Form::text('seo_title',  '', ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_keywords', 'Seo Keywords:') ?>
                                <?= Form::text('keywords',  '', ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= Form::label('description', 'Seo Description:') ?>
                                <?= Form::textArea('description',  '', ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
  </div>
  {!! Form::close() !!}
</div>
@include('admin/footer')
