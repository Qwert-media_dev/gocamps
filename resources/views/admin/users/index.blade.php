@include('admin/header')
  <button type="button" class="btn btn-primary" onclick="window.location.href='/admin/users/create'">Add user</button>  
  <table cellspacing="1" id= "table" class="tablesorter table">
    <thead>
    <tr>
        <th>id</th>
        <th>Email</th>      
        <th></th>      
        <th></th>      
      
    </tr>
    </thead>
    <tbody>
    @foreach($users as $key=>$user)
      <tr>
        <td>{{ $user->id}}</td>
        <td>{{ $user->email }}</td>
       <td  class="actions"><button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/users/edit/{{$user->id}}'"><span class="glyphicon glyphicon-pencil"></button> 
       @if(Auth::user()->isAdmin())
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-<?=$key?>"><span class="glyphicon glyphicon-remove"></button>
        </td>   <!-- Modal -->
          <div id="myModal-<?=$key?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing user <?=$user->email?> </h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>
                   
                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/users/delete/{{$user->id}}'">Yes</button> 
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>
          @endif
      </tr>
        
      
    @endforeach
  </tbody>
    </table>
    
<script type="text/javascript">
 $(document).ready(function(){
    $('table').DataTable( {
        "order": [[ 0, "desc" ]],
        "stateSave": true,
        "pageLength": 100,
        "bLengthChange":false
        
    } );
});
</script>
@include('admin/footer')