@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@if(count($errors))
  @foreach ($errors->all() as $error)
     <div>{{ $error }}</div>
 @endforeach
@endif
{!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/users/create','files'=>true]) !!}
  <div class="row">
    <div class="col-md-3 pull-right">
      {{ Form::submit('Save', ['class'=>'btn btn-success btn-flat']) }}
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('email', 'Email:') }}
        {{ Form::text('email', '', ['placeholder'=>'Email', 'class' => 'form-control']) }}
      </div>
    </div>

    <div class="col-md9 ">
      <div class="form-group ">
        {{ Form::label('password', 'Password:') }}
        <br>
        {{ Form::password('password', ['placeholder'=>'Password', 'class' => 'form-control editor']) }}
      </div>
    </div>

    <div class="col-md-9 ">
      <div class="form-group ">
        {{ Form::label('role', 'Role:') }}
        {{ Form::select('role',  $roles, '', ['placeholder'=>'Choose', 'class' => 'form-control editor']) }}
      </div>
    </div>

  </div>
{!! Form::close() !!}

@include('admin/footer')
