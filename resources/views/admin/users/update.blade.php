@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
@if(count($errors))
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif
{!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/users/edit/'.$user->id,'files'=>true]) !!}
   <div class="row">
    <div class="col-md-3 pull-right">
      {{ Form::submit('Save', ['class'=>'btn btn-success btn-flat']) }}
    </div>
      <div class="col-md-2 pull-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Delete user</button>
    </div>
    <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        {{ Form::label('email', 'Email:') }}
        {{ Form::text('email', $user->email, ['placeholder'=>'Email', 'class' => 'form-control']) }}
      </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-3 ">
      <div class="form-group ">
        {{ Form::label('password', 'Password:') }}
        <br>
        {{Form::password('password',['class' => 'form-control editor'])}}

      </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-3 ">
      <div class="form-group ">
        {{ Form::label('role', 'Role:') }}
        {{ Form::select('role', $roles, $role->role_id, ['class' => 'form-control editor']) }}
      </div>
    </div>
    </div>

  </div>
 <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing user {{ $user->email }} </h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/users/delete/{{$user->user_id}}'">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>

{!! Form::close() !!}

@include('admin/footer')
