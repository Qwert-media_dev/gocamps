@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "#editor",
  theme: "modern",
  width: 680,
  forced_root_block:"",
  height: 300,
  verify_html : false,
  autoresize_on_init: true,
  subfolder:"",
  extended_valid_elements :"span[*]",
  force_p_newlines : false,
force_br_newlines : false,
convert_newlines_to_brs : false,
remove_linebreaks : true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<div>

  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/pages/create','files'=>true]) !!}
   <input type="hidden" name="page_id" value="{{$page_id}}">
 @if(count($errors))
     @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach
  @endif
  <div class="row">
    <div class="col-md-3 pull-right">
      <?= Form::submit('Save', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('title', 'Title:') ?>
        <?= Form::text('title', '', ['placeholder'=>'Title', 'class' => 'form-control']) ?>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('slug', 'Slug:') ?>
        <?= Form::text('slug',  '', ['placeholder'=>'Slug', 'class' => 'form-control']) ?>
      </div>
    </div>
    <div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('content', 'Body:') ?>
        <?= Form::textArea('content',  '', ['placeholder'=>'body', 'class' => 'form-control','id'=>'editor']) ?>
      </div>
    </div>
    <div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('layout', 'Layout:') ?>
        <?= Form::select('layout',  $layouts, ['class' => 'form-control']) ?>
      </div>
    </div>
<!-- TABS -->
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
        </ul>
        <div class="tab-content">
                <!-- SEO -->
                <div id="seo" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_title', 'Seo Title:') ?>
                                <?= Form::text('seo_title',  '', ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_keywords', 'Seo Keywords:') ?>
                                <?= Form::text('keywords',  '', ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= Form::label('description', 'Seo Description:') ?>
                                <?= Form::textArea('description',  '', ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
  {!! Form::close() !!}
</div>

@include('admin/footer')
