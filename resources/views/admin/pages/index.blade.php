﻿@include('admin/header')
  <button type="button" class="btn btn-primary" onclick="window.location.href='/admin/pages/create'">Add page</button>  
  <table cellspacing="1" id= "table" class="tablesorter table-bordered">
    <thead> 
    <tr>
        <th>id</th>
        <th>Название</th>      
        <th></th>          
      
    </tr>
    </thead> 
  <tbody>
    @foreach($pages as $page)
      <tr>
        <td>{{ $page->page_id}}</td>
        <td>{{ $page->title }}</td>
        <td  class="actions"><button type="button" class="btn btn-primary" onclick="window.location.href='/admin/pages/edit/{{$page->page_id}}'"><span class="glyphicon glyphicon-pencil"></button></td>

      </tr>
        
      
    @endforeach
  </tbody>
    
    </table>

  <script type="text/javascript">
 $(document).ready(function(){
    $('table').DataTable( {
        "order": [[ 0, "desc" ]],
        "stateSave": true,
        "pageLength": 100,
        "bLengthChange":false
        
    } );
});
</script>

@include('admin/footer')