@include('admin/header')
  <button type="button" class="btn btn-primary" onclick="window.location.href='/admin/articles/create'">Add article</button>  
  <table cellspacing="1" id= "table" class="tablesorter table-bordered">
    <thead> 
    <tr>
        <th>id</th>
        <th>Название</th>      
        <th></th>        
      
    </tr>
  </thead> 
  <tbody>
    @foreach($articles as $key=>$article)
      <tr>
        <td>{{ $article->article_id}}</td>
        <td>{{ $article->title }}</td>
        <td class="actions"><button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/articles/edit/{{$article->article_id}}'"><span class="glyphicon glyphicon-pencil"></button> 
       @if(Auth::user()->isAdmin())
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-<?=$key?>"><span class="glyphicon glyphicon-remove"></button>
        </td>   <!-- Modal -->
          <div id="myModal-<?=$key?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing article <?=$article->title?> </h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>
                   
                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/articles/delete/{{$article->article_id}}'">Yes</button> 
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>
          @endif
      </tr>
        
      
    @endforeach
  </tbody>
    </table>
    
<script type="text/javascript">
 $(document).ready(function(){
    $('table').DataTable( {
        "order": [[ 0, "desc" ]],
        "stateSave": true,
        "pageLength": 100,
        "bLengthChange":false
        
    } );
});
</script>
@include('admin/footer')