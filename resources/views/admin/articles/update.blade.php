@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "#editor",
  theme: "modern",
  width: 680,
  forced_root_block:"",
  height: 300,
  verify_html : false,
  autoresize_on_init: true,
  subfolder:"",
  extended_valid_elements :"span[*]",
  force_p_newlines : false,
force_br_newlines : false,
convert_newlines_to_brs : false,
remove_linebreaks : true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<div>
  @if(count($errors))
    @foreach ($errors->all() as $error)
       <div>{{ $error }}</div>
   @endforeach
 @endif
  <br><br>
  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/articles/edit/'.$article->article_id,'files'=>true]) !!}

  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        {{ Form::label('title', 'Title:') }}
        {{ Form::text('title', $article->title, ['placeholder'=>'Title', 'class' => 'form-control']) }}
      </div>
    </div>
    <div class="col-md-3 pull-right">
      <div class="btn-group-vertical">
          {{ Form::submit('Save', ['class'=>'btn btn-success btn-flat']) }}
          <br>
          <button type="button" class="btn btn-primary" onclick="window.location.href='/{{$category_slug}}/{{$article->slug}}'">Go to article</button>
          <br>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Delete article</button>
      </div>
      </div>
    <div class="col-md-9">
      <div class="form-group">
        {{ Form::label('slug', 'Slug:') }}
        {{ Form::text('slug',  $article->slug, ['placeholder'=>'Slug', 'class' => 'form-control']) }}
      </div>
    </div>
    <div class="col-md-9 ">
      <div class="form-group ">
        {{ Form::label('body', 'Body:') }}
        {{ Form::textArea('body',  $article->body, ['placeholder'=>'body', 'class' => 'form-control','id'=>'editor']) }}
      </div>
    </div>
    <div class="col-md-3 pull-right">
      {{ Form::label('image', 'Image:') }}
      {{ Form::file('image', ['class' => 'form-control']) }}
      @if(!empty($article->image))
        <div id="article-image">
        <img  width="200" height="200" src="/image/{{$article->image}}">
        <br>
         <button id="remove-image"type="button" class="btn btn-primary" data-toggle="modal">Remove image</button>
       </div>
      @endif
    </div>
    <div class="col-md-3 pull-right">
      {{ Form::label('gallery', 'Gallery:') }}
      <input type="file" name="gallery[]" class="form-control" name="image[]" multiple placeholder="Upload Image">
      <?php $gallery=explode(',',$article->gallery); ?>
      @if(isset($gallery[0])&&!empty($gallery[0]))
        @foreach($gallery as $key=>$one)
          @if(!empty($one))
            <div id="gallery-image-{{$key}}">
              <img  width="200" height="200" src="/gallerys/{{$one}}">
              <br>
               <button id="remove-gallery-{{$key}}"type="button" class="btn btn-primary" data-toggle="modal">Remove image</button>
            </div>
          @endif
         @endforeach
      @endif
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 ">
      <div class="form-group">
        {{ Form::label('category', 'Category:') }}
        {{ Form::select('category', $categories,$article->category_id,['class' => 'form-control']) }}
      </div>
    </div>
  </div>

  </div>
<div class="col-md-9 ">
      <div class="form-group ">
        {{ Form::label('layout', 'Layout:') }}
        {{ Form::select('layout',  $layouts,$article->layout,['class' => 'form-control']) }}
      </div>
    </div>
<!-- TABS -->
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
        </ul>
        <div class="tab-content">
                <!-- SEO -->
                <div id="seo" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('seo_title', 'Seo Title:') }}
                                {{ Form::text('seo_title',  $article->seo_title, ['placeholder'=>'Seo Title', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('seo_keywords', 'Seo Keywords:') }}
                                {{ Form::text('keywords',  $article->keywords, ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                {{ Form::label('description', 'Seo Description:') }}
                                {{ Form::textArea('description',  $article->description, ['placeholder'=>'Seo Description', 'class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
  <input id="article_id" name="article_id" type="hidden" value="{{$article->article_id}}" />
  <input id="old_id" name="old_id" type="hidden" value="{{$article->article_id}}" />
  <br>
   <br>
  <input id="language" name="language" type="hidden" value="{{$article->lang}}" />
  @if(!$have_translate)
    <div class="row">
      <div class="col-md-12">
        <a class="create-translate"> Create translate </a>
      </div>
    </div>
   <div class="modal-trans" id="mod" style="display:none">
       <select class="langs">
       @foreach($langs as $lang)
        @if($lang==LaravelLocalization::getCurrentLocale())
         <option value="{{$lang}}" disabled>{{$lang}}</option>
        @else
          <option value="{{$lang}}">{{$lang}}</option>
        @endif
       @endforeach
       </select>
       <input class="ok" type="button" value="Translate">
       <span class="close" id="close-trans">X</span>
   </div>
   <br>
    <div class="row">
      <div class="col-md-12">
        <a class="add-translate"> Add translate to existing article </a>
      </div>
    </div>
    <div class="modal-add" id="mod" style="display:none">
       <select class="l-articles">
       @foreach($articles as $article_tr)

         <option value="{{$article_tr->article_id}}">{{$article_tr->title}}</option>
       @endforeach
       </select>
       <input class="add-ok" type="button" value="Translate">
       <span class="close" id="close-add">X</span>

   </div>
   </div>
   @endif
   @if(count($have_langs)>1)
   <div class="row">
      <div class="col-md-12">
       У данной статьи есть переводы:<br>
       @foreach($have_langs as $one)
        @if($one->lang!=LaravelLocalization::getCurrentLocale())
          <a href="/{!! $one->lang !!}/admin/articles/edit/{{$article->article_id}}">{{$one->lang}}</a> <a href="/{!! $one->lang !!}/admin/articles/unlink/{{$article->article_id}}">Unlink</a><br>
        @endif
      @endforeach
    </div>

   @endif

  {!! Form::close() !!}
</div>
<script type="text/javascript">
$('.create-translate').click(function(){
  $('.modal-trans').css('display','block');
});
$('.add-translate').click(function(){
  $('.modal-add').css('display','block');
});
$('.ok').click(function(){
  $('#language').val($('.langs  option:selected').val());
  var id=$('#article_id').val();
  window.location.href="/"+$('#language').val()+"/admin/articles/create/?article_id="+id;
});

$('.id-translate').click(function(){
  $('.modal-add').css('display','block');
  $('#article_id').val($('.l-articles  option:selected').val());
});
$('.l-articles').change(function(){
  console.log($(this).val());
  $('#article_id').val($(this).val());
});
$('.add-ok').click(function(){
  var id=$('.l-articles  option:selected').val();
  var locale=$('.l-langs  option:selected').val();
  $('#article_id').val($('.l-articles  option:selected').val());

  $('form').submit();

});
$('#close-trans').click(function(){$('.modal-trans').hide();});
$('#close-add').click(function(){$('.modal-add').hide();});

$('#remove-image').click(function(){
  $.ajax({
      url:"/admin/articles/removeimage/{{$article->article_id}}",
      method:"GET",
      success:function(){console.log('success');$('#article-image').remove();}
  });
});
$('[id^=remove-gallery]').click(function(){

  var id=$('[id^=remove-gallery]')[0].id
  id=id.substr(id.length-1)
  console.log(id);
  $.ajax({
      url:"/admin/articles/removegalimage/{{$article->article_id}}/"+id,
      method:"GET",
      success:function(){console.log('success');$('#gallery-image-'+id).remove();}
  });
})
</script>
<style type="text/css">
#mod {
    padding: 20px;
    border: 1px solid;
    width: 25%;
}</style>
 <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing article {{$article->title}}</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/articles/delete/{{$article->article_id}}'">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>

@include('admin/footer')
