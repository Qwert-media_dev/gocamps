@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: ".editor",
  theme: "modern",
  width: 680,
  forced_root_block:"",
  height: 300,
  verify_html : false,
  autoresize_on_init: true,
  subfolder:"",
  extended_valid_elements :"span[*]",
  force_p_newlines : false,
force_br_newlines : false,
convert_newlines_to_brs : false,
remove_linebreaks : true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<div>

{!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/admin/projects/edit/'.$project->project_id,'files'=>true]) !!}
    @if(count($errors))
     @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach
  @endif
  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        <?= Form::label('title', 'Title:') ?>
        <?= Form::text('title', $project->title, ['placeholder'=>'Title', 'class' => 'form-control']) ?>
      </div>
    </div>
   <div class="col-md-3 pull-right">
      <div class="btn-group-vertical">
          <?= Form::submit('Save', ['class'=>'btn btn-success btn-flat']) ?>
          <br>
          <button type="button" class="btn btn-primary" onclick="window.location.href='/projects/{{$project->project_id}}'">Go to project</button>
          <br>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Delete project</button>
      </div>
      </div>
    <div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('body', 'Body:') ?>
        <?= Form::textArea('text',  $project->text, ['placeholder'=>'Body', 'class' => 'form-control editor']) ?>
      </div>
    </div>
    <div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('accordion', 'Accordion:') ?>
        <?= Form::textArea('accordion',  $project->accordion, ['placeholder'=>'accordion', 'class' => 'form-control editor']) ?>
      </div>
    </div>

  </div>
 <div class="col-md-9 ">
      <div class="form-group ">
        <?= Form::label('layout', 'Layout:') ?>
        <?= Form::select('layout',  $layouts,$project->layout,['class' => 'form-control']) ?>
      </div>
    </div>

<!-- TABS -->
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
        </ul>
        <div class="tab-content">
                <!-- SEO -->
                <div id="seo" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_title', 'Seo Title:') ?>
                                <?= Form::text('seo_title',  $project->seo_title, ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= Form::label('seo_keywords', 'Seo Keywords:') ?>
                                <?= Form::text('keywords',  $project->keywords, ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= Form::label('description', 'Seo Description:') ?>
                                <?= Form::textArea('description',  $project->description, ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
  {!! Form::submit('Обновить'); !!}
  <br>
  <input id="project_id" name="project_id" type="hidden" value="<?=$project->project_id?>" />
  <input id="old_id" name="old_id" type="hidden" value="<?=$project->project_id?>" />
  <br>
   <br>
  <input id="language" name="language" type="hidden" value="<?=$project->lang?>" />
  @if(!$have_translate)
   <a class="create-translate"> Create translate </a>
   <div class="modal-trans" id="mod" style="display:none">
       <select class="langs">
       @foreach($langs as $lang)
        @if($lang==LaravelLocalization::getCurrentLocale())
         <option value="<?=$lang?>" disabled><?=$lang?></option>
        @else
          <option value="<?=$lang?>"><?=$lang?></option>
        @endif
       @endforeach
       </select>
       <input class="ok" type="button" value="Translate">
       <span class="close" id="close-trans">X</span>
   </div>
   <br>
   <a class="add-translate"> Add translate to existing project </a>

    <div class="modal-add" id="mod" style="display:none">
       <select class="l-projects">
       @foreach($projects as $project)

         <option value="<?=$project->project_id?>"><?=$project->title?></option>
       @endforeach
       </select>
       <input class="add-ok" type="button" value="Translate">
       <span class="close" id="close-add">X</span>

   </div>
   </div>
   @endif
   This project have translates:<br>
    @foreach($have_langs as $one)
    @if($one->lang!=LaravelLocalization::getCurrentLocale())
      <a href="/{!! $one->lang !!}/admin/projects/edit/{{$project->project_id}}">{{$one->lang}}</a> <a href="/{!! $one->lang !!}/admin/projects/unlink/{{$project->project_id}}">Unlink</a><br>
    @endif
  @endforeach
  {!! Form::close() !!}
</div>
<script type="text/javascript">
$('.create-translate').click(function(){
  $('.modal-trans').css('display','block');
});
$('.add-translate').click(function(){
  $('.modal-add').css('display','block');
});
$('.ok').click(function(){
  $('#language').val($('.langs  option:selected').val());
  var id=$('#project_id').val();
  window.location.href="/"+$('#language').val()+"/admin/projects/create/?project_id="+id;
});

$('.id-translate').click(function(){
  $('.modal-add').css('display','block');
  $('#project_id').val($('.l-projects  option:selected').val());
});
$('.l-projects').change(function(){
  console.log($(this).val());
  $('#project_id').val($(this).val());
});
$('.add-ok').click(function(){
  var id=$('.l-projects  option:selected').val();
  var locale=$('.l-langs  option:selected').val();
  $('#project_id').val($('.l-projects  option:selected').val());

  $('form').submit();

});
$('#close-trans').click(function(){$('.modal-trans').hide();});
$('#close-add').click(function(){$('.modal-add').hide();});
</script>
<style type="text/css">
#mod {
    padding: 20px;
    border: 1px solid;
    width: 25%;
}</style>
 <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing project <?=$project->title?> </h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/projects/delete/{{$project->project_id}}'">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>

            </div>
          </div>

@include('admin/footer')
