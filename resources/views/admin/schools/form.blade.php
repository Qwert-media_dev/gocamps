@include('admin/header')

<link rel="stylesheet" href="/chosen.css">


@if( $errors->all() )
  <div class="row">
    <div class="col-md-12">
    @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
    @endforeach
    </div>
  </div>
@endif

{{ Form::open(['url' => $action, 'files'=>true, 'class'=>'school-reg__form1 form', 'name'=>'school-form']) }}
  <div class="row">
    <div class="col-xs-12">
      <h1 class="page-header">
        {{ $title }}
        @if ($currentYear->id && $currentYear->type)
          <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#shortManual">Manual merge</button>
        @endif
        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#delete-modal">Delete year</button>
        <button class="btn btn-primary fixed">Save changes</button>
      </h1>
    </div>
    <div class="col-xs-12">
      @if ($currentYear->id)
        <div class="pull-right">
          @foreach($years as $year)
            <a class="btn btn-{{$currentYear->year == $year->year ? 'success' : 'default' }}" href="{{ route('admin-schools-edit', ['id' => $year->id]) }}">
              {{ $year->year }}
            </a>
          @endforeach
        </div>
      @endif

      <ul class="nav nav-tabs" role="tablist">
        <li class="active">
          <a href="#anketa" aria-controls="anketa" role="tab" data-toggle="tab">School info</a>
        </li>
        @if ($currentYear->id)
          <li><a href="#yadd" aria-controls="yadd" role="tab" data-toggle="tab">Add year</a></li>
        @endif
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="anketa">
          <div class="row">
            <div class="form-group col-xs-12 col-md-6">
              {{ Form::label('title', 'Повна назва навчального закладу') }}
              {{ Form::text('title', $currentYear->title, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('city', 'Населенний пункт') }}
              {{ Form::text('city', $currentYear->city, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-2">
              {{ Form::label('city_type', 'Тип') }}
              {{ Form::select('city_type', $city_type, $currentYear->city_type, ['class' => 'form-control', 'placeholder' => 'Оберіть тип']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
              {{ Form::label('state', 'Область') }}
              {{ Form::select('state', $states, $currentYear->state, ['class' => 'form-control', 'placeholder' => 'Оберіть область']) }}
            </div>
            <div class="form-group col-xs-8 col-md-4">
              {{ Form::label('street', 'Вулиця') }}
              {{ Form::text('street', $currentYear->street, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-4 col-md-1">
              {{ Form::label('house', 'Будинок') }}
              {{ Form::text('house', $currentYear->house, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-4 col-md-4">
              {{ Form::label('railway_station', 'Найближча З\Д станція') }}
              {{ Form::text('railway_station', $currentYear->railway_station, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-2">
              {{ Form::label('comment', 'Коментар') }}
            </div>
            <div class="form-group col-xs-12 col-md-10">
              {{ Form::textarea('comment', $currentYear->comment, ['class' => 'form-control', 'rows' => 3]) }}
            </div>
            <div class="form-group col-xs-12 col-md-6">
              {{ Form::label('title', 'Full name of school') }}
              {{ Form::text('title_eng', $currentYear->title_eng, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('city', 'City/town/village') }}
              {{ Form::text('city_eng', $currentYear->city_eng, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-2">
              {{ Form::label('city_type_eng', 'Type') }}
              {{ Form::select('city_type_eng', $city_type_eng, $currentYear->city_type_eng, ['class' => 'form-control', 'placeholder' => 'Choose type']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
              {{ Form::label('state', 'Region/Oblast') }}
              {{ Form::select('state_eng', $states_eng, $currentYear->state_eng, ['class'=>'form-control', 'placeholder' => 'Choose state']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('street_eng', 'Street') }}
              {{ Form::text('street_eng', $currentYear->street_eng, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-1">
              {{ Form::label('house', 'Building') }}
              {{ Form::text('house_eng', $currentYear->house_eng, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-4 col-md-4">
              {{ Form::label('railway_station_eng', 'Nearest station') }}
              {{ Form::text('railway_station_eng', $currentYear->railway_station_eng, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('manager', 'Менеджер',['class'=>'col-third']) }}
              {{ Form::select('manager', $managers, $currentYear->manager,['class'=>'form-control', 'id'=>'volunteers']) }}
            </div>
            @if ($currentYear->id)
              <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('created_at', 'Дата реєстрації') }}
                  {{ Form::text('created_at', $currentYear->created_at, ['class'=>'form-control']) }}
              </div>
              <div class="form-group col-xs-12 col-md-12">
                {!! Form::label('volunteers', 'Volunteers'); !!}
                <input name="volunteers" type="hidden">
                <select multiple="multiple" name="volunteers[]" class="form-control chosen-autocomplete">
                  @foreach($currentYear->volunteersList as $V)
                    <option value="{{ $V->id }}" @if(key_exists($V->id, $currentYear->volunteersAdded))selected="selected"@endif>{{ $V->first_name }} {{ $V->last_name }}</option>
                  @endforeach
                </select>
              </div>
            @endif
          </div>

          @if ($currentYear->id)
            <h2 aria-expanded="false" aria-controls="project-details">
              <a data-toggle="collapse" href="#project-details" aria-expanded="false" aria-controls="project-details">
                Child quantity and project details
              </a>
            </h2>
            <div class="collapse" id="project-details">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('quantity_children', 'На яку кількість дітей ви очікуєте в таборі?') }}
                  {{ Form::select('quantity_children', $quantity_children, $currentYear->quantity_children, ['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('quantity_children_work', 'Яка кількість дітей буде працювати за обраним напрямом?') }}
                  {{ Form::text('quantity_children_work', $currentYear->quantity_children_work, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('share_school', 'Готовність відкриття табору для дітей інших шкіл') }}
                  {{ Form::select('share_camp', [1=>'Так',0=>'Ні'], $currentYear->share_camp, ['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('camp_type', 'Оберіть тип табору') }}
                  {{ Form::select('type_camp', $camp_type, $currentYear->type_camp, ['class'=>'form-control', 'placeholder' => 'Оберіть тип']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('paid', 'Платна школа') }}
                  {{ Form::select('paid', [0=>'Ні',1=>'Так'], $currentYear->paid,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('share_volunteer', 'Готовність ділити волонтера') }}
                  {{ Form::select('share_volunteer', [1=>'Так',0=>'Ні'], $currentYear->share_volunteer, ['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('rating', 'Рейтинг') }}
                  {{ Form::select('rating', $rating, $currentYear->rating,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('status', 'Статус') }}
                  {{ Form::select('status', $status, $currentYear->status, ['class'=>'form-control', 'placeholder' => 'Оберіть статус']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('year', 'Змінити рік') }}
                  {{ Form::text('year', $currentYear->year, ['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-5">
                  {{ Form::label('', 'Дати табору') }}<br/>
                  @foreach($session as $key => $value)
                    {{ Form::checkbox("session[]", $key, (in_array($key, $currentYear->session)), ['id'=>'session-' . $key]) }}
                    {{ Form::label('session-' . $key, $value) }} <br/>
                  @endforeach
                </div>
                <div class="form-group col-xs-12 col-md-3">
                  {{ Form::label('project', 'Оберіть проект') }}<br/>
                  @foreach($project as $key => $value)
                    {{ Form::checkbox('project[]', $key, in_array($key, $currentYear->project), ['id'=>'project-' . $key]) }}
                    {{ Form::label('project-' . $key, $value) }}
                    <br>
                  @endforeach
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('camp_lang', 'Мова табору') }}<br/>
                  @foreach($camp_lang as $key => $value)
                    {{ Form::checkbox("camp_lang[]", $key, (in_array($key, $currentYear->camp_lang)), ['id'=>'camp_lang-' . $key]) }}
                    {{ Form::label('camp_lang-' . $key, $value) }} <br/>
                  @endforeach
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('training', 'Чи відвідала школа тренінг?') }}
                  {{ Form::select('training', [0=>'Ні',1=>'Так'], $currentYear->training,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('school_memorandum', 'Меморандум зі школою') }}
                  {{ Form::select('school_memorandum', [0=>'Ні',1=>'Так'], $currentYear->school_memorandum,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('plan', 'Надісланий план') }}
                  {{ Form::select('plan', [0=>'Ні',1=>'Так'], $currentYear->plan,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('training_teacher', 'ПІБ вчителя, що був на тернінгу') }}
                  {{ Form::text('training_teacher', $currentYear->training_teacher,['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('teacher_memorandum', 'Меморандум з вичителем') }}
                  {{ Form::select('teacher_memorandum', [0=>'Ні',1=>'Так'], $currentYear->teacher_memorandum,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('report', 'Надісланий репорт') }}
                  {{ Form::select('report', [0=>'Ні',1=>'Так'], $currentYear->report,['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('training_dates', 'Дати тренінгу') }}<br/>
                  @foreach($training_dates as $key => $value)
                    {{ Form::checkbox("training_dates[]", $key, (in_array($key, $currentYear->training_dates)), ['id'=>'training_dates-' . $key]) }}
                    {{ Form::label('training_dates-' . $key, $value) }} <br/>
                  @endforeach
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('goc_east', 'GoCamps East') }}<br/>
                  @foreach($goc_east as $key => $value)
                    {{ Form::checkbox("goc_east[]", $key, (in_array($key, $currentYear->goc_east)), ['id'=>'goc_east-' . $key]) }}
                    {{ Form::label('goc_east-' . $key, $value) }} <br/>
                  @endforeach
                </div>
              </div>
            </div>

            <h2>
              <a data-toggle="collapse" href="#other-info" aria-expanded="false" aria-controls="other-info">
                Other information
              </a>
            </h2>
            <div class="collapse" id="other-info">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('experience', 'Чи має школа досвід робити в обраному напрямку/ах?') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::select('experience', array('1'=>'Так', '0'=>'Нi'), $currentYear->experience, ['class'=>'form-control', 'placeholder' => 'Оберіть варіант']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('description', 'Опишіть його') }}
                  <small class="input-tip">(до 200 слів)</small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('description', $currentYear->description, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('description2', 'Коротко обґрунтуйте, чому ви хочете взяти участь в GoCamps?') }}
                  <small class="input-tip">(до 200 слів)</small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('description2', $currentYear->description2, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('about_school', 'Розкажіть нам про свою школу') }}
                  <small class="input-tip">
                    Опиc має бути українською мовою і може включати посилання на сайт та соціальні мережі школи.
                    <br>(до 200 слів без посилань)
                  </small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('about_school', $currentYear->about_school, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('about_city', 'Розкажіть нам про свій населений пункт') }}
                  <small class="input-tip">Опис має бути українською мовою (для влонтерів) і може містити посилання на сайти про ваш населений пункт,<br>(до 200 слів без посилань)</small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('about_city', $currentYear->about_city, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('about_school_eng', 'Tell us about your school') }}
                  <small class="input-tip">Розкажіть нам про свою школу. Опиc має бути англійською мовою і може включати посилання на сайт та соціальні мережі школи. <br>(до 200 слів без посилань)</small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('about_school_eng', $currentYear->about_school_eng, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('about_city', 'Tell us about your city/town/village') }}
                  <small class="input-tip">Опис має бути англійською мовою (для влонтерів) і може містити посилання на сайти про ваш населений пункт,<br>(до 200 слів без посилань)</small>
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textArea('about_city_eng', $currentYear->about_city_eng, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('video', 'Посилання на відео "Запросіть волонтера до своєї школи"') }}
                  {{ Form::text('video', $currentYear->video, ['class'=>'form-control']) }}
                </div>
              </div>
            </div>

            {{-- <h2 aria-expanded="false" aria-controls="creative-task">
              <a data-toggle="collapse" href="#creative-task" aria-expanded="false" aria-controls="creative-task">
                Creative task
              </a>
            </h2>
            <div class="collapse" id="creative-task">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('video', 'Посилання на відео "Запросіть волонтера до своєї школи"') }}
                  {{ Form::text('video', $currentYear->video, ['class'=>'form-control']) }}
                </div>
              </div>
            </div> --}}

            <h2 aria-expanded="false" aria-controls="staff-info">
              <a data-toggle="collapse" href="#staff-info" aria-expanded="false" aria-controls="staff-info">
                Contact information
              </a>
            </h2>
            <div class="collapse" id="staff-info">
              <div class="row">
                <div class="form-group col-xs-12 col-md-12">
                  {{ Form::label('address', 'Директор школи') }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('sdirector_name', 'ПІБ') }}
                  {{ Form::text('sdirector_name', $currentYear->sdirector_name, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('sdirector_phone', 'Контактний телефон') }}
                  {{ Form::text('sdirector_phone', $currentYear->sdirector_phone, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('sdirector_email', 'Електронна адреса') }}
                  {{ Form::text('sdirector_email', $currentYear->sdirector_email, ['class' => 'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12">
                  <label>
                    <span>Вчитель англійської мови, відповідальний за розробку програми</span>
                    <small class="input-tip">(може бути той самий, що й Директор табору, <br> буде запрошений на тренінг)</small>
                  </label>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('teacher_name', 'ПІБ') }}
                  {{ Form::text('teacher_name', $currentYear->teacher_name, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('teacher_phone', 'Контактний телефон') }}
                  {{ Form::text('teacher_phone', $currentYear->teacher_phone, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('teacher_email', 'Електронна адреса') }}
                  {{ Form::text('teacher_email', $currentYear->teacher_email, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::label('school_phone', 'Телефон школи') }}
                  {{ Form::text('school_phone', $currentYear->school_phone, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::label('school_email', 'Електронна адреса школи') }}
                  {{ Form::text('school_email', $currentYear->school_email, ['class' => 'form-control']) }}
                </div>
              </div>
            </div>
          @endif
        </div>

        @if ($currentYear->id)
          <div class="tab-pane" id="yadd">
            <div class="row">
              <div class="form-group col-xs-12 col-md-2">
                {{ Form::submit('Додати рік',['class'=>'btn btn-success']) }}
              </div>
              <div class="form-group col-xs-12 col-md-6">
                {{ Form::select('add_year', [
                  '0' => '',
                  '2016-2017' => '2016-2017',
                  '2017-2018' => '2017-2018',
                  '2018-2019' => '2018-2019',
                  '2019-2020' => '2019-2020'
                ], '', ['class'=>'form-control']) }}
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
{{ Form::close() }}
@if ($currentYear->id)
  <div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Removing school {{ $currentYear->title }} for {{ $currentYear->year }} year</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/schools/delete/{{$currentYear->id}}'">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  @if ($currentYear->type)
    <div id="shortManual" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title">Merge forms manualy</h2>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <label>Enter the long form title (part of it)</label>
              </div>
              {{ Form::open(['url' => route('check-school-forms'), 'id' => 'checkForm']) }}
                <input type="hidden" name="currentId" value="{{ $currentYear->id }}"/>
                <div class="col-xs-12 col-md-8">
                  <input name="title" type="text" class="form-control">
                </div>
                <div class="col-xs-12 col-md-4">
                  <button type="submit" class="btn btn-primary">Check</button>
                </div>
              {{ Form::close() }}
            </div>
            <div class="row">
              <div id="formResult" class="col-md-12 text-center">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  @endif
@endif
<script src="/chosen.jquery.min.js"></script>
<script>

$(document).ready( function() {
  var changed = false;
  $(window).bind('beforeunload', function () {
    $(':input').blur();
    if (changed == true)
      return "Are you sure you want to exit? Please complete sign up or the app will get deleted.";
  });
  $(':input').on('change', function() {
    changed = true;
  });
  $('form').submit(function(){$(window).unbind("beforeunload");})

  $('#checkForm').submit( function(e) {
    $('#formResult').html('')
    if ($('input[name="title"]').val() == '') {
      $('#formResult').text('Enter ID');
    } else {
      var action = $(this).attr('action'),
          params = $(this).serialize();
      $.post(action, params, function(response) {
        $('#formResult').html(response);
      })
    }
    e.preventDefault();
  });
  $(document).on('click', '#formResult > a.btn', function(e) {
    if ( !confirm('Are you shure?') ) {
      e.preventDefault();
    };
  });
    $(".chosen-autocomplete").chosen({
      placeholder_text_single: ' '
    });

    $('a.toggle-vol').on('click', function(e) {
      var vid = $(this).data('vid'),
          label = $(this).html(),
          block = $('.vols-list'),
          exist = block.find('[data-vid="'+vid+'"]');

      if ( exist.length != 0 ) {
        exist.remove();
      } else {
        block.append('<a href="/admin/volunteers/edit/last/'+vid+'" target="_blank" data-vid="'+vid+'"><input name="volunteers[]" type="hidden" value="'+vid+'">'+label+'</a>');
      }
      $(this).toggleClass('toggled');
      e.preventDefault();
    })
});
</script>

@include('admin/footer')
