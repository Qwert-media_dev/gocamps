@include('admin/header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Schools
      <div class="pull-right">
        <button id="reset" class="btn btn-primary">Reset filters</button>
        @if(Auth::user()->isAdmin())
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-export">Export Schools</button>
        @endif
        <a class="btn btn-primary" href="{{ route('schools-view') }}">Add school</a>
      </div>
      <div class="dropdown pull-right">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ $YEAR }}
        </button>
        <ul class="dropdown-menu">
          <li><a href="{{ route('admin-schools', 'All') }}">{{ "All" }}</a></li>
          @foreach($years as $year)
            <li><a href="{{ route('admin-schools', $year) }}">{{ $year }}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="dropdown pull-right">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Add / Hide columns
        </button>
        <ul class="dropdown-menu columns">
          @foreach($columns as $colGroup)
            @foreach ($colGroup as $cKey => $col)
              @if ($col['hidden'])
                <li><a class="dropdown-item column-toggle" data-key={{ $cKey }}>{!! $col['label'] !!}</a></li>
              @endif
            @endforeach
          @endforeach
        </ul>
      </div>
    </h1>
  </div>
  <div class="col-lg-12">
    <table id= "table" class="table table-hover table-responsive tablesorter table-bordered">
      <thead>
        <tr>
            <th class="tb-sm">ID</th>
            <th class="tb-lg">Name</th>
            <th class="tb-md">Status</th>
            <th class="tb-md">State</th>
            <th class="tb-md">Project</th>
            @foreach ($columns as $colGroup)
              @foreach ($colGroup as $cKey => $col)
                @if ($col['hidden'])
                  <th class="hidden toggled {{ $cKey }}">{!! $col['label'] !!}</th>
                @endif
              @endforeach
            @endforeach
            <th class="tb-sm">Action</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th class="tb-sm"></th>
          <th class="tb-lg"></th>
          <th class="tb-md" data-key="status"></th>
          <th class="tb-md" data-key="state"></th>
          <th class="tb-md" data-key="project"></th>
          @foreach ($columns as $colGroup)
            @foreach ($colGroup as $cKey => $col)
              @if ($col['hidden'])
                <th class="hidden toggled {{ $cKey }}" data-key="{{ $cKey }}"></th>
              @endif
            @endforeach
          @endforeach
          <th class="tb-sm"></th>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($schools as $sKey => $S)
          <tr data-id="{{ $S->id }}" data-type="{{ $S->type }}">
            <td class="tb-sm">{{ $S->id }}</td>
            <td class="tb-lg">
              <a href="{{ route('admin-schools-edit', ['id' => $S->id] ) }}" target="_blank">
                {{ $S->title }}
              </a>
            </td>
            <td class="tb-md">{{ $status[(int)$S->status] }}</td>
            <td class="tb-md">{{ ($S->state == NULL) ? '-' : $state[(int)$S->state] }}</td>
            <td class="tb-md">
              @foreach($S->project as $value)
                @if(!empty($value))
                  {{ $project[$value] }},
                @endif
              @endforeach
            </td>
            @foreach ($columns as $colGroup)
              @foreach ($colGroup as $cKey => $col)
                @if ($col['hidden'])
                  <td class="hidden toggled {{ $cKey }}">
                    @if ($cKey == 'volunteers')
                      @foreach ($S->volunteers as $V)
                        <a href="{{ route('admin-volunteer-edit', $V->id) }}" target="_blank">{{ $V->first_name }} {{ $V->last_name }}</a><br/>
                      @endforeach
                      @continue
                    @endif

                    @if (isset($col['listed']))
                      @if (in_array($cKey,['session','camp_lang','training_dates','goc_east']))
                        @foreach($S->$cKey as $value)
                          {{ $$cKey[$value] }},
                        @endforeach
                      @else
                        {{ (isset($$cKey[$S->$cKey])) ? $$cKey[$S->$cKey] : '-' }}
                      @endif
                    @else
                      {{ $S->$cKey }}
                    @endif
                  </td>
                @endif
              @endforeach
            @endforeach
            <td class="tb-sm">
              @if(Auth::user()->isAdmin())
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-<?=$S->id?>"><span class="glyphicon glyphicon-remove"></button>

                <!-- Modal -->
                <div id="myModal-<?=$S->id?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Removing school {{ $S->title }} for {{ $S->year }} year</h4>
                      </div>
                      <div class="modal-body">
                        <p>Are you sure?</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/schools/delete/{{$S->id}}'">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<!-- Modal 4 export -->
<div id="myModal-export" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Columns for store in file</h4>
      </div>
      <form id="export" action={{ route('admin-schools-export', $YEAR) }}>
        <div class="modal-body">
            <?php $count = 0; ?>
            @foreach ($columns as $name => $colBlock)
              <h2 aria-expanded="{{ (!$count) }}" aria-controls="columns-{{ $count }}">
                <a data-toggle="collapse" href="#columns-{{ $count }}" aria-expanded="{{ (!$count) }}" aria-controls="columns-{{ $count }}">
                  {{ $name }}
                </a>
              </h2>
              <div class="collapse {{ (!$count) ? 'in' : '' }}" id="columns-{{ $count }}">
                <div class="row">
                  @foreach ($colBlock as $key => $col)
                    <div class="col-xs-6 col-sm-4">
                      <input type="checkbox" id="f-{{ $key }}" name="col[{{ $key }}]" value="1" {{ (!$count) ? 'checked' : ''}}/>
                      <label class="check-label" for="f-{{ $key }}"> {!! $col['label'] !!}</label>
                    </div>
                  @endforeach
                </div>
              </div>
              <?php ++$count; ?>
            @endforeach
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="sebutton" class="btn btn-primary">Export</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Modal 4 export -->

<script src="/assets/js/qw-admin.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  var visible = [];
  $('a.column-toggle').on('click', function() {
    var key = $(this).data('key');
    $(this).toggleClass('active');
    visible.toggle(key);
    $('.toggled.' + key).toggleClass('hidden');
  });

  $('#sebutton').on('click', function() {
    var form = $('form#export'),
        data = form.serialize(),
        action = form.attr('action');

    $.each($('#table tfoot th'), function(index, th) {
      var _th = $(th),
          key = _th.data('key'),
          filter = null;
      filter = _th.find('select option:selected').val() || _th.find('input').val() || null;
      if (key != null && filter != null)
      data += '&filters%5B' + key + '%5D=' + filter;
    });
    location.href = action + '?' + data;
  });

  // $('#table tfoot th').click(function() {
  //   console.log($(this).index());
  // });

  var $DT = $('#table').DataTable( {
      "order": [[ 0, "desc" ]],
      "stateSave": true,
      "searching": true,
      "ordering": true,
      "autoWidth": false,
      "autoHeight": false,
      "processing": true,
      // "serverSide": true,
      // "ajax": {
      //       "url": "{{ route('processing') }}",
      //       "type": "POST"
      //   },
      "info": true,
      initComplete: function () {
         this.api().columns([2,3,4,5,12,13,18,20,22,23,24,25,27,35,36]).every( function () {
           var column = this;
           var select = $('<select ><option value=""></option></select>')
               .appendTo( $(column.footer()).empty() );

           var rows = [];
           column.data().unique().sort().each( function ( d, j ) {
             var tmp = d.split(',');
             for (var i = tmp.length - 1; i >= 0; i--) {
               if (tmp[i].trim().length>0){
                 if ($.inArray(tmp[i].trim(), rows) == -1) {
                   rows.push(tmp[i].trim());
                   select.append( '<option value="'+tmp[i].trim()+'">'+tmp[i].trim()+'</option>' );
                 }
               }
             }
           } );
           // select.select2().val(column.search()).trigger('change');
          } );
      },
      fnDrawCallback: function (settings) {
        $('td.toggled:not(.hidden)').addClass('hidden');
        $.each(visible, function(index, key) {
          $('#table td.toggled.' + key).toggleClass('hidden');
        });
      },
  }).columns().search('').draw();

  $('#table tfoot select').on( 'change', function () {
    var search = $(this).val();

     $DT.column($(this).parent().index())
       .search(search, true, false)
       .draw();
  });
  $('#table tfoot input').on( 'change', function () {
      var val = $.fn.dataTable.util.escapeRegex($(this).val());
      $DT.column($(this).parent().index())
          .search( val ? val: '', true, false )
          .draw();
  });
  $('#reset').click(function () {
    $('#table tfoot select').val([]).change();
    $('#table tfoot input').val(null).change();
    $('#table th.toggled:not(.hidden)').addClass('hidden');
    $('a.column-toggle').removeClass('active');
    visible = [];
    $DT
     .columns()
     .search('')
     .draw();
  });

});
</script>
@include('admin/footer')
