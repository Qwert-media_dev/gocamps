@include('admin/header')
<div class="row">
  @if($errors->has())
    <div class="col-xs-12">
       @foreach ($errors->all() as $error)
          <div >{{ $error }}</div>
      @endforeach
    </div>
  @endif
</div>

{!! Form::open(['url' =>  '/'.LaravelLocalization::getCurrentLocale().'/admin/schools/create', 'files'=>true, 'class'=>'school-reg__form1 form', 'name'=>'school-form']) !!}
  <div class="row">
    <div class="form-group col-xs-12 col-md-6">
      {!! Form::submit('Создать!',['class'=>'btn btn-success']); !!}
    </div>
    <div class="form-group col-xs-12 col-md-6">
      {!! Form::label('title', 'Повна назва навчального закладу',['class'=>'col-third']); !!}
      {!! Form::text('title','',['class'=>'col'])!!}
    </div>
    <div class="form-group col-xs-12 col-md-6">
      {!! Form::label('title', 'Full name of school',['class'=>'col-third']); !!}
      {!! Form::text('title_eng','',['class'=>'col'])!!}
    </div>
    <div class="form-group col-xs-12 col-md-6">
      {!! Form::label('address', 'Адреса',['class'=>'col-third']); !!}

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('state', 'Область'); !!}
          {!! Form::select('state', $states); !!}
        </div>
        <div class="input-item">
          {!! Form::label('city', 'Населенний пункт'); !!}
          {!! Form::text('city')!!}
        </div>
        <div class="input-item">
          {!! Form::label('street', 'Вулиця'); !!}
          {!! Form::text('street')!!}
        </div>
        <div class="input-item">
          {!! Form::label('house', 'Будинок'); !!}
          {!! Form::text('house')!!}
        </div>
      </div>
    </div>
     <div class="form-group row">
      <div class="col-third">
        {!! Form::label('comment', 'Коментар'); !!}
      </div>
      {!! Form::textArea('comment','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('manager', 'Менеджер'); !!}
      </div>
      {!! Form::text('manager','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      {!! Form::label('status', 'Статус',['class'=>'col-third']); !!}

      {!! Form::select('status', $status) !!}
    </div>

    <div class="form-group row">
      {!! Form::label('address_eng', 'Address',['class'=>'col-third']); !!}

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('state', 'Region/Oblast'); !!}
          {!! Form::select('state_eng', $states_eng); !!}
        </div>
        <div class="input-item">
          {!! Form::label('city', 'City/town/village'); !!}
          {!! Form::text('city_eng')!!}
        </div>
        <div class="input-item">
          {!! Form::label('street_eng', 'Street'); !!}
          {!! Form::text('street_eng')!!}
        </div>
        <div class="input-item">
          {!! Form::label('house', 'Building'); !!}
          {!! Form::text('house_eng')!!}
        </div>
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('strand', 'Напрямок', ['class'=>'col-third']) !!}

      <div>
        @foreach($strands as $key => $value)
          {!! Form::checkbox('strand[]', $key, false, ['id'=>'strand-' . $key]); !!}
          {!! Form::label('strand-' . $key, $value); !!}
          <br>
        @endforeach
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('experience', 'Чи має школа досвід робити в обраному напрямку/ах?',['class'=>'col-third']); !!}

      {!! Form::radio('experience', '1', ['class'=>'col']); !!} {!! Form::label('experience', 'Так'); !!}
      {!! Form::radio('experience', '0', ['class'=>'col']); !!} {!! Form::label('experience', 'Нi'); !!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('description', 'Якщо так, коротко опишіть його'); !!}
        <small class="input-tip">(до 200 слів)</small>
      </div>
      {!! Form::textArea('description','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('description2', 'Якщо ні, коротко обгрунтуйте, чому ви обираєте цей напрямок?'); !!}
        <small class="input-tip">(до 200 слів)</small>
      </div>
      {!! Form::textArea('description2','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('about_school', 'Розкажіть нам про свою школу'); !!}
        <small class="input-tip">Опиc має бути українською мовою і може включати посилання на сайт та соціальні мережі школи. <br>(до 200 слів без посилань)</small>
      </div>
      {!! Form::textArea('about_school','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('about_city', 'Розкажіть нам про свій населений пункт'); !!}
        <small class="input-tip">Опис має бути українською мовою (для влонтерів) і може містити посилання на сайти про ваш населений пункт,<br>(до 200 слів без посилань)</small>
      </div>
      {!! Form::textArea('about_city','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('about_school_eng', 'Tell us about your school'); !!}
        <small class="input-tip">Розкажіть нам про свою школу. Опиc має бути англійською мовою і може включати посилання на сайт та соціальні мережі школи. <br>(до 200 слів без посилань)</small>
      </div>
      {!! Form::textArea('about_school_eng','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      <div class="col-third">
        {!! Form::label('about_city', 'Tell us about your city/town/village'); !!}
        <small class="input-tip">Опис має бути англійською мовою (для влонтерів) і може містити посилання на сайти про ваш населений пункт,<br>(до 200 слів без посилань)</small>
      </div>
      {!! Form::textArea('about_city_eng','',['class'=>'col'])!!}
    </div>
    <div class="form-group row">
      {!! Form::label('quantity_children', 'На яку кількість дітей ви очікуєте в таборі?',['class'=>'col-third']); !!}

      {!! Form::select('quantity_children', $quantity_children) !!}
    </div>
    <div class="form-group row">
      {!! Form::label('quantity_children_work', 'Яка кількість дітей буде працювати за обраним напрямом?',['class'=>'col-third']); !!}

      {!! Form::text('quantity_children_work')!!}
    </div>
    <div class="form-group row">
      {!! Form::label('project', 'Оберіть проект', ['class'=>'col-third']); !!}

     <div>
        @foreach($project as $key => $value)
          {!! Form::checkbox('project[]', $key, false, ['id'=>'project-' . $key]); !!}
          {!! Form::label('project-' . $key, $value); !!}
          <br>
        @endforeach
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('camp_type', 'Оберіть тип табору', ['class'=>'col-third']); !!}

      {!! Form::select('type_camp', $camp_type) !!}
    </div>
    <div class="form-group row">
      {!! Form::label('address', 'Директор школи',['class'=>'col-third']); !!}

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('sdirector_name', 'ПІБ'); !!}
          {!! Form::text('sdirector_name')!!}
        </div>
        <div class="input-item">
          {!! Form::label('sdirector_phone', 'контактний телефон'); !!}
          {!! Form::text('sdirector_phone')!!}
        </div>
        <div class="input-item">
          {!! Form::label('sdirector_email', 'електронна адреса'); !!}
          {!! Form::text('sdirector_email')!!}
        </div>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-third">
        <span>Директор табору</span>
        <small class="input-tip"> основна комунікація зі школою буде проводитися через директора табору англійською мовою</small>
      </div>

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('cdirector_name_ua', 'ПІБ українською мовою '); !!}
          {!! Form::text('cdirector_name_ua')!!}
        </div>
        <div class="input-item">
          {!! Form::label('cdirector_name_eng', 'ПІБ англійською мовою '); !!}
          {!! Form::text('cdirector_name_eng')!!}
        </div>

        <div class="input-item">
          {!! Form::label('cdirector_email', 'електронна адреса'); !!}
          {!! Form::text('cdirector_email')!!}
        </div>
        <div class="input-item">
          {!! Form::label('cdirector_phone', 'контактний телефон'); !!}
          {!! Form::text('cdirector_phone')!!}
        </div>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-third">
        <span>Вчитель англійської мови, відповідальний за розробку програми</span>
        <small class="input-tip">(може бути той самий, що й Директор табору, <br> буде запрошений на тренінг)</small>
      </div>

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('eteacher_name', 'ПІБ'); !!}
          {!! Form::text('eteacher_name')!!}
        </div>
        <div class="input-item">
          {!! Form::label('eteacher_phone', 'контактний телефон'); !!}
          {!! Form::text('eteacher_phone')!!}
        </div>
        <div class="input-item">
          {!! Form::label('eteacher_email', 'електронна адреса'); !!}
          {!! Form::text('eteacher_email')!!}
        </div>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-third">
        <span>Вчитель-предметник (за напрямком), відповідальний за розробку програми</span>
        <small class="input-tip">(може бути той самий, що й Директор табору, <br> буде запрошений на тренінг, за умови володіння англійською або німецькою мовою)</small>
      </div>

      <div class="inputs-group">
        <div class="input-item">
          {!! Form::label('teacher_name', 'ПІБ'); !!}
          {!! Form::text('teacher_name')!!}
        </div>
        <div class="input-item">
          {!! Form::label('teacher_phone', 'контактний телефон'); !!}
          {!! Form::text('teacher_phone')!!}
        </div>
        <div class="input-item">
          {!! Form::label('teacher_email', 'електронна адреса'); !!}
          {!! Form::text('teacher_email')!!}
        </div>
      </div>
    </div>
    <div class="form-group row">
      <span class="col-third">Креативне завдання "Запросіть волонтера до своєї школи"</span>
    </div>
    <div class="form-group row">
      {!! Form::label('video', 'Посилання на відео "Запросіть волонтера до своєї школи"',['class'=>'col-third']); !!}
      {!! Form::text('video','',['class'=>'col'])!!}
    </div>
      <div class="form-group row">
        <span class="col-third">Якщо завдяння виконане в іншому форматі, завантежте його тут</span>
        <label class="col row">
          <span class="file-name-field col">Файл не выбран</span>
          <input type="file" id="task-file" name="pdf">
        </label>
      </div>
      <div class="form-group row">
        <span class="col-third">Завантажте фото школи <small class="input-tip">(максимум 5)</small></span>
        <label class="col row">
          <span class="file-name-field col">Файл не выбран</span>
          <input type="file" id="photos" name="photos[]" multiple="">
        </label>
      </div>
    <div class="form-group row">
      {!! Form::label('camp_lang', 'Мова табору',['class'=>'col-third']); !!}

      {!! Form::select('camp_lang',$camp_lang) !!}
    </div>
    <div class="form-group row">
      {!! Form::label('rating', 'Рейтинг',['class'=>'col-third']); !!}

      {!! Form::select('rating', $rating) !!}
    </div>
   {!! Form::submit('Создать!',['class'=>'btn btn-success']); !!}

    {!! Form::close() !!}
    </section>

@include('admin/footer')
