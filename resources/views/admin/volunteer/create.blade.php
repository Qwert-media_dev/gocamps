@include('admin/header')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="/admin-style.css">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
  .col {
    width: 100%;
  }
</style>


@if($errors->has())
@foreach ($errors->all() as $error)
<div>{{ $error }}</div>
@endforeach
@endif
<section class="school-reg">
  @if($errors->has())
  @foreach ($errors->all() as $error)
  <div>{{ $error }}</div>
  @endforeach
  @endif
  {!! Form::open(['url' =>  '/'.LaravelLocalization::getCurrentLocale().'/admin/volunteers/create', 'files' => true, 'class' => 'school-reg__form1 form', 'name' => 'volunteer-form']) !!}
  <div class="form-group row">
  {!! Form::submit('Создать!',['class'=>'btn btn-success']); !!}
  </div>
  <div class="form-group row">
    {!! Form::label('lastname', 'Last Name *',['class'=>'col-third']); !!}
    {!! Form::text('lastname','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('firstname', 'First Name *',['class'=>'col-third']); !!}
    {!! Form::text('firstname','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('state', 'State',['class'=>'col-third']); !!}
    {!! Form::select('state',$states,null,['class'=>"col",'id'=>'state'])!!}
  </div>
  <div id="preload"  style="position: fixed;margin-left: 30%;border: 1px solid;background: gainsboro;display: none">
    <label class="col-third">loading...</label><img class="" src="/ajax-loader.gif">
  </div>
  <div class="form-group row">
    {!! Form::label('school', 'School',['class'=>'col-third']); !!}
    {!! Form::select('school',$schools,null,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('tickets', 'Tickets',['class'=>'col-third']); !!}

    {!! Form::select('tickets',$yes_no,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('visa', 'Visa',['class'=>'col-third']); !!}

    {!! Form::select('visa',$yes_no,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('payment', 'Payment',['class'=>'col-third']); !!}

    {!! Form::select('payment',$yes_no,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('status', 'Status',['class'=>'col-third']); !!}

    {!! Form::select('status',$status,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('manager', 'Manager',['class'=>'col-third']); !!}
    {!! Form::select('manager',$managers,null,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('comment', 'Manager comment',['class'=>'col-third']); !!}
    {!! Form::textArea('comment','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('middlename', 'Middle Name',['class'=>'col-third']); !!}
    {!! Form::text('middlename','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('citizenship', 'Citizenship *',['class'=>'col-third']); !!}

    {!! Form::text('citizenship','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('passport', 'Passport number *',['class'=>'col-third']); !!}
    {!! Form::text('passport','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('birthday', 'Birthday *',['class'=>'col-third']); !!}
    {!! Form::text('birthday','',['class'=>'col','id'=>'datepicker'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('gender', 'Gender *',['class'=>'col-third']); !!}

    {!! Form::select('gender', array('male','female'),['class'=>'col']) !!}
  </div>
  <div class="form-group row">
    {!! Form::label('address', 'Address',['class'=>'col-third']); !!}

    <div class="inputs-group">
      <div class="input-item">
        {!! Form::label('country', 'Country *'); !!}
        {!! Form::text('country')!!}
      </div>
      <div class="input-item">
        {!! Form::label('city', 'City *'); !!}
        {!! Form::text('city')!!}
      </div>
        <!-- <div class="input-item" id='state'>
        {!! Form::label('state', 'State'); !!}
        {!! Form::select('state',$states)!!}
      </div> -->
      <div class="input-item">
        {!! Form::label('street', 'Street, bil, app. '); !!}
        {!! Form::text('street')!!}
      </div>
    </div>
  </div>
  <div class="form-group row">
    {!! Form::label('email', 'Email *',['class'=>'col-third']); !!}
    {!! Form::text('email','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('skype', 'Skype',['class'=>'col-third']); !!}
    {!! Form::text('skype','',['class'=>'col'])!!}
  </div>

  <div class="form-group row">
    {!! Form::label('social', 'Link to your social media (FB, VK, Twitter, etc.) *',['class'=>'col-third']); !!}

    {!! Form::text('social','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('phone', 'Primary phone number [include country code] *',['class'=>'col-third']); !!}

    {!! Form::text('phone','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('alt_phone', 'Alternate phone number [include country code]',['class'=>'col-third']); !!}

    {!! Form::text('alt_phone','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('been_in_ukraine', 'Have you been to Ukraine before? (For foreiners)',['class'=>'col-third']); !!}

    {!! Form::radio('been_in_ukraine', '1',['class'=>'col']) !!}{!! Form::label('been_in_ukraine', 'Yes'); !!}
    {!! Form::radio('been_in_ukraine', '0',['class'=>'col']) !!}{!! Form::label('been_in_ukraine', 'No'); !!}

  </div>
  <div class="form-group row">
    {!! Form::label('capacity', 'If yes, in what capacity (For foreiners) ',['class'=>'col-third']); !!}

    {!! Form::text('capacity','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('capacity', 'If yes, in what capacity (For foreiners) ',['class'=>'col-third']); !!}

    {!! Form::text('capacity','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('eng_level', 'What is your level of knowledge of English? *',['class'=>'col-third']); !!}

    {!! Form::select('eng_level', $skill,['class'=>'col']) !!}
  </div>
  <div class="form-group row">
    {!! Form::label('other_lang', 'What other languages do you know?',['class'=>'col-third']); !!}

    {!! Form::text('other_lang','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('education', 'Education (Institution, field of study, degree/graduation year) *',['class'=>'col-third']); !!}

    {!! Form::text('education','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('describe', 'Describe any relevant work, volunteer, or personal experience that has prepared you for working at GoCamps *',['class'=>'col-third']); !!}

    {!! Form::textArea('describe','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('about', 'Please tell us more about yourself, including hobbies and interests *',['class'=>'col-third']); !!}

    {!! Form::textArea('about','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('why', 'Why would you like to volunteer at GoCamps? *',['class'=>'col-third']); !!}

    {!! Form::textArea('why','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('strand', 'Strand',['class'=>'col-third']); !!}

    {!! Form::select('strand', $strand,['class'=>'col']) !!}
  </div>
  <div class="form-group row">
    {!! Form::label('sessions', 'Please select the session(s) you wish to apply for *',['class'=>'col-third']); !!}

    <div>
      @foreach($session as $key => $value)
      {!! Form::checkbox('session[]', $key, false, ['id'=>'session-' . $key]); !!}
      {!! Form::label('session-' . $key, $value); !!}
      <br>
      @endforeach
    </div>
  </div>
  <div class="form-group row">
    {!! Form::label('hear', 'How did you hear about GoCamps? *',['class'=>'col-third']); !!}

    {!! Form::text('hear','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('dietary', 'Do you have any dietary restrictions? *',['class'=>'col-third']); !!}

    {!! Form::text('dietary','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('intrested', 'Are you interested in contributing to the online blog about your experience in Ukraine? ',['class'=>'col-third']); !!}

    {!! Form::text('intrested','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('special', 'Do you have any special needs that we should be informed about?',['class'=>'col-third']); !!}

    {!! Form::text('special','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('speak', 'Do you speak German?',['class'=>'col-third']); !!}

    {!! Form::radio('speak', '1', ['class'=>'col']); !!} {!! Form::label('speak', 'Yes'); !!}
    {!! Form::radio('speak', '0', ['class'=>'col']); !!} {!! Form::label('speak', 'No'); !!}
  </div>
  <div class="form-group row">
    {!! Form::label('level', 'If yes what is your level of German? Native Advanced Upper intermidiate Intermidiate',['class'=>'col-third']); !!}

    {!! Form::select('level',$skill,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('priority', 'Choose the priority camp strand',['class'=>'col-third']); !!}

    {!! Form::text('priority','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('alt_strand', 'Choose the alternative camp strand',['class'=>'col-third']); !!}

    {!! Form::select('alt_strand',$strand,['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('preferences', 'Do you have any preferences for the location of the camp site (We do not guarantee the location you point out. The earlier you submit the application the more chances we will have to satisfy your preference',['class'=>'col-third']); !!}

    {!! Form::text('preferences','',['class'=>'col'])!!}
  </div>

  {!! Form::submit('Создать!',['class'=>'btn btn-success']); !!}

  {!! Form::close() !!}
</section>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0"
    });
  } );
  $(document).ready(function () {
    var $modelSelect = $('[name^=school]');

    $('[name^=state]').on('change.select2', function () {
      $('#preload').show();
      var data = $(this).val();
      if(!data.length || data[0] === '') {
        return;
      };
      $modelSelect.empty().append('<option value="" selected></option>');
      $.ajax({
        headers: {
          'X-CSRF-Token': $('input[name="csrf-token"]').val()
        },
        url: '/admin/volunteers/getschools',
        method: 'get',
        data: {
          'data': data
        },
        success: function (data) {
          console.log(data);

          $.each(data, function (k, v) {
            $modelSelect.append('<option value=' + k + '>'+ v + '</option>');
          });
          $('#preload').hide();
        }
      });
    });
  });
</script>
<script>
  document.getElementById('session-2').disabled = true;
  document.getElementById('session-3').disabled = true;
</script>

@include('admin/footer')
