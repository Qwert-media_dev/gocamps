@include('admin/header')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <link rel="stylesheet" href="/chosen.css"> --}}

@if( $errors->all() )
  <div class="row">
    <div class="col-xs-12">
    @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
    @endforeach
    </div>
  </div>
@endif

{{ Form::open(['url' => $action, 'files'=>true, 'name'=>'volunteer-form']) }}
  <div class="row">
    <div class="col-xs-12">
      <h1 class="page-header">
        {{ $title }}
        @if ($currentYear->id && $currentYear->type)
          <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#shortManual">Manual merge</button>
          @if ($similar)
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#short">Similar forms</button>
          @endif
        @endif
        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#delete-modal">Delete year</button>
        <button class="btn btn-primary fixed">Save changes</button>
      </h1>
    </div>
    <div class="col-xs-12">
      @if ($currentYear->id)
        <div class="pull-right">
          @foreach($years as $year)
            <a class="btn btn-{{$currentYear->year == $year->year ? 'success' : 'default' }}" href="{{ route('admin-volunteer-edit', ['id' => $year->id]) }}">
              {{ $year->year }}
            </a>
          @endforeach
        </div>
      @endif

      <ul class="nav nav-tabs" role="tablist">
        <li class="active">
          <a href="#anketa" aria-controls="anketa" role="tab" data-toggle="tab">Volunteer info</a>
        </li>
        @if ($currentYear->id)
          <li><a href="#yadd" aria-controls="yadd" role="tab" data-toggle="tab">Add year</a></li>
          <li><a href="#email" aria-controls="email" role="tab" data-toggle="tab">Send emails</a></li>
        @endif
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="anketa">
          <div class="row">
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('first_name', 'First Name',['class'=>'']) }}
              {{ Form::text('first_name', $currentYear->first_name, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
              {{ Form::label('last_name', 'Last Name',['class'=>'']) }}
              {{ Form::text('last_name', $currentYear->last_name, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
                {{ Form::label('cityzenship', 'Citizenship', ['class'=>'']) }}
                <span>Old: {{$currentYear->citizenship_old}}</span>
                {{ Form::select('citizenship', $countries, $currentYear->citizenship, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
                {{ Form::label('passport', 'Passport number', ['class'=>'']) }}
                {{ Form::text('passport', $currentYear->passport, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
                {{ Form::label('birthday', 'Birthday', ['class'=>'']) }}
                {{ Form::text('birthday', $currentYear->birthday, ['class'=>'form-control','id'=>'datepicker']) }}
            </div>
            <div class="form-group col-xs-12 col-md-4">
                {{ Form::label('gender', 'Gender', ['class'=>'']) }}
                {{ Form::select('gender', $gender, $currentYear->gender, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
                {{ Form::label('country', 'Country') }}
                <span>Old: {{$currentYear->country_old}}</span>
                {{ Form::select('country', $countries, $currentYear->country, ['id'=>'country', 'class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city', $currentYear->city, ['class'=>'form-control']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
              {{ Form::label('email', 'Email') }}
              {{ Form::text('email', $currentYear->email, ['class'=>'form-control','id'=>'vol-email']) }}
            </div>
            <div class="form-group col-xs-12 col-md-3">
              {{ Form::label('skype', 'Skype') }}
              {{ Form::text('skype', $currentYear->skype, ['class'=>'form-control']) }}
            </div>
            @if ($currentYear->id)
              <div class="form-group col-xs-12 col-md-4">
                {{ Form::label('state', 'State') }}
                <input name="state" type="hidden" value="">
                <select id="state-select" multiple="multiple" name="state[]" class="form-control" data-url="{{ route('get-volunteer-schools', $currentYear->id) }}">
                  @foreach($states as $id => $title)
                    <option value="{{ $id }}" @if(in_array($id, $currentYear->state))selected="selected"@endif>{{ $title }}</option>
                  @endforeach
                </select>
                {{-- {{ Form::select('state[]', $states, $currentYear->stateArr, ['multiple', 'class'=>'form-control', 'id' => 'state-select', 'data-url' => route('get-volunteer-schools', $currentYear->id)]) }} --}}
              </div>
              <div class="form-group col-xs-8">
                {!! Form::label('schools', 'Schools'); !!}
                <input name="schools" type="hidden" value=" ">
                <select id="schools-select" multiple="multiple" name="schools[]" class="form-control">
                  @foreach($currentYear->getStateSchools() as $id => $title)
                    <option value="{{ $id }}" @if(in_array($title, $currentYear->schoolsAdded))selected="selected"@endif>{{ $title }}</option>
                  @endforeach
                </select>
              </div>
            @endif
          </div>

          @if ($currentYear->id)
            <h2 aria-expanded="false" aria-controls="social">
              <a data-toggle="collapse" href="#social" aria-expanded="false" aria-controls="social">
                Link to your social media (FB, VK, Twitter, etc.)
              </a>
            </h2>
            <div class="collapse" id="social">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('facebook', 'Facebook') }}
                    {{ Form::text('facebook', $currentYear->facebook, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('twitter', 'Twitter') }}
                    {{ Form::text('twitter', $currentYear->twitter, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('google', 'Google+') }}
                    {{ Form::text('google', $currentYear->google, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12">
                    {{ Form::label('add_link', 'Any additional link') }}
                    {{ Form::textarea('add_link', $currentYear->add_link, ['class'=>'form-control', 'rows' => 3]) }}
                </div>
              </div>
            </div>

            <h2 aria-expanded="false" aria-controls="add-contact">
              <a data-toggle="collapse" href="#add-contact" aria-expanded="false" aria-controls="add-contact">
                Additional contact information
              </a>
            </h2>
            <div class="collapse" id="add-contact">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('phone', 'Primary phone number (include country code)') }}
                    <div class="row">
                      <div class="col-xs-5">
                        {{ Form::select('phone_code', $phones, $currentYear->phone_code, ['class'=>'form-control']) }}
                      </div>
                      <div class="col-xs-7">
                        {{ Form::text('phone', $currentYear->phone, ['class'=>'form-control']) }}
                      </div>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('alt_phone', 'Alternate phone number (include country code)') }}
                    <div class="row">
                      <div class="col-xs-5">
                        {{ Form::select('alt_phone_code', $phones, $currentYear->alt_phone_code, ['class'=>'form-control']) }}
                      </div>
                      <div class="col-xs-7">
                        {{ Form::text('alt_phone', $currentYear->alt_phone, ['class'=>'form-control']) }}
                      </div>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('emergency_name', 'Emergency contact (person`s name)') }}
                    {{ Form::text('emergency_name', $currentYear->emergency_name, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('emergency_phone', 'Emergency contact (person`s phone)') }}
                    <div class="row">
                      <div class="col-xs-5">
                        {{ Form::select('emergency_phone_code', $phones, $currentYear->emergency_phone_code, ['class'=>'form-control']) }}
                      </div>
                      <div class="col-xs-7">
                        {{ Form::text('emergency_phone', $currentYear->emergency_phone, ['class'=>'form-control']) }}
                      </div>
                    </div>
                </div>
              </div>
            </div>

            <h2 aria-expanded="false" aria-controls="education">
              <a data-toggle="collapse" href="#education" aria-expanded="false" aria-controls="education">
                Education
              </a>
            </h2>
            <div class="collapse" id="education">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('been_in_ukraine', 'Have you been to Ukraine before?') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::select('been_in_ukraine',array('1'=>'yes','0'=>'no'), $currentYear->been_in_ukraine, ['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('capacity', 'In what capacity?') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::textarea('capacity', $currentYear->capacity, ['class'=>'form-control', 'rows' => 3]) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('eng_level', 'What is your level of knowledge of English?') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::select('eng_level', $skill,$currentYear->eng_level, ['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('eng_level', 'What other languages do you know?') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::select('other_langs', $other_langs, $currentYear->other_langs, ['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('education', 'Education (Institution, field of study, degree/graduation year)') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::textarea('education', $currentYear->education,['class'=>'form-control', 'rows' => 3]) }}
                </div>
              </div>
            </div>

            <h2 aria-expanded="false" aria-controls="work">
              <a data-toggle="collapse" href="#work" aria-expanded="false" aria-controls="work">
                Work
              </a>
            </h2>
            <div class="collapse" id="work">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('workplace', 'Current workplace') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::text('workplace', $currentYear->workplace,['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('workplace_position', 'Current position in your workplace') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::text('workplace_position', $currentYear->workplace_position,['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('work_describe', 'Describe any relevant work, volunteering, or personal experience that would be useful for volunteering work at GoCamp') }}
                </div>
                <div class="form-group col-xs-12 col-md-6">
                  {{ Form::textarea('work_describe', $currentYear->work_describe,['class'=>'form-control', 'rows' => 3]) }}
                </div>
              </div>
            </div>

            <h2 aria-expanded="false" aria-controls="volunteering">
              <a data-toggle="collapse" href="#volunteering" aria-expanded="false" aria-controls="volunteering">
                Volunteering
              </a>
            </h2>
            <div class="collapse" id="volunteering">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('about', 'Please tell us more about yourself, including hobbies and interests') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textarea('about', $currentYear->about, ['class'=>'form-control', 'rows' => 3]) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('why', 'Why would you like to volunteer at GoCamps?') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textarea('why',$currentYear->why, ['class'=>'form-control', 'rows' => 3]) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('facts', 'Write 3 interesting facts about yourself') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textarea('facts',$currentYear->facts, ['class'=>'form-control', 'rows' => 3]) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('strand', 'Choose the priority sphere of your interest or specialisation') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::select('strand', $strand,$currentYear->strand,['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('', 'Please select the session(s) you wish to apply for') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  @foreach($session as $key => $value)
                    {{ Form::checkbox("session[]", $key, (in_array($key, $currentYear->session)), ['id'=>'session-' . $key]) }}
                    {{ Form::label('session-' . $key, $value) }} <br/>
                  @endforeach
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('hear', 'How did you hear about GoCamps?',['class'=>'col-third']) }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::select('hear', $hear, $currentYear->hear, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-8 col-md-offset-4">
                  {{ Form::text('hear_other', $currentYear->hear_other, ['class'=>'form-control']) }}
                </div>
              </div>
            </div>

            <h2 aria-expanded="false" aria-controls="questions">
              <a data-toggle="collapse" href="#questions" aria-expanded="false" aria-controls="questions">
                General questions
              </a>
            </h2>
            <div class="collapse" id="questions">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('dietary', 'Food restrictions') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::select('dietary', $dietary, $currentYear->dietary,['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-8 col-md-offset-4">
                  {{ Form::text('dietary_other', $currentYear->dietary_other,['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('intrested', 'Are you interested in contributing to the online blog about your experience in Ukraine? ') }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::select('interested', array('1'=>'yes','0'=>'no'), $currentYear->interested, ['class'=>'form-control']) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('special', 'Do you have any special needs that we should be informed about?') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textarea('special', $currentYear->special,['class'=>'form-control', 'rows' => 3]) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('preferences', 'Do you have any preferences for the location of the camp site (We do not guarantee the location you point out. The earlier you submit the application the more chances we will have to satisfy your preference') }}
                </div>
                <div class="form-group col-xs-12 col-md-8">
                  {{ Form::textarea('preferences',$currentYear->preferences,['class'=>'form-control', 'rows' => 3]) }}
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('shootings', 'During your volunteering, there might be photo- and video-shootings. Do you agree to be filmed during the GoCamp activities?') }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::select('shootings', array('1'=>'yes','0'=>'no'), $currentYear->shootings, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12">
                  {{ Form::checkbox('agree', 1, $currentYear->agree, ['id'=>'agree']) }}
                  {{ Form::label('agree', 'Yes, I give my permission for collection, processing and storage of my personal data that was provided by me.') }} <br/>
                </div>
              </div>
              {{-- <div class="row">
                @if ($currentYear->cv)
                  <div class="form-group col-xs-12 col-md-4">
                    {{ Form::label('task-file', 'Attached') }}
                    {!! $currentYear->cvl !!}
                  </div>
                @endif
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('task-file', 'Please, attach your CV in pdf format') }}
                </div>
                  <div class="form-group col-xs-12 col-md-4">
                    <input type="file" id="task-file" name="cv" accept="application/pdf">
                  </div>
              </div> --}}
            </div>

            <h2 aria-expanded="false" aria-controls="other">
              <a data-toggle="collapse" href="#other" aria-expanded="false" aria-controls="other">
                Other information
              </a>
            </h2>
            <div class="collapse" id="other">
              <div class="row">
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('tickets', 'Tickets', ['class'=>'col-third']) }}
                  {{ Form::select('tickets', array('1'=>'yes','0'=>'no'), $currentYear->tickets, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('visa', 'Visa',['class'=>'col-third']) }}
                  {{ Form::select('visa', array('1'=>'yes','0'=>'no'), $currentYear->visa, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('payment', 'Payment',['class'=>'col-third']) }}
                  {{ Form::select('payment', array('1'=>'yes','0'=>'no'), $currentYear->payment, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-4">
                  {{ Form::label('documents', 'Documents') }}
                </div>
                <div class="form-group col-xs-8">
                @foreach ($documents as $key => $title)
                  {{ Form::checkbox("documents[]", $key, in_array($key, $currentYear->documents), ['id'=>"doc-{$key}"]) }}
                  {{ Form::label("doc-{$key}", $title) }}<br/>
                @endforeach
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('status', 'Status',['class'=>'col-third']) }}
                  {{ Form::select('status', $status, $currentYear->status, ['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('manager', 'Manager',['class'=>'col-third']) }}
                  {{ Form::select('manager', $managers, $currentYear->manager,['class'=>'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  {{ Form::label('created_at', 'Registration date') }}
                  {{ Form::text('created_at', $currentYear->created_at,['class'=>'form-control', 'readonly']) }}
                </div>
                <div class="form-group col-xs-12">
                  {{ Form::label('comment', 'Manager comment') }}
                  {{ Form::textArea('comment', $currentYear->comment, ['class'=>'form-control', 'rows' => 5]) }}
                </div>
              </div>
            </div>
          @endif
        </div>

        @if ($currentYear->id)
          <div class="tab-pane" id="yadd">
            <div class="row">
              <div class="form-group col-xs-12 col-md-2">
                {{ Form::submit('Додати рік',['class'=>'btn btn-success']) }}
              </div>
              <div class="form-group col-xs-12 col-md-6">
                {{ Form::select('add_year', [
                  '0' => '',
                  '2016-2017' => '2016-2017',
                  '2017-2018' => '2017-2018',
                  '2018-2019' => '2018-2019',
                  '2019-2020' => '2019-2020'
                ], '', ['class'=>'form-control']) }}
              </div>
            </div>
          </div>
          <div class="tab-pane" id="email">
            <div class="row">
              @foreach ($mail as $mKey => $M)
                <div class="col-xs-12 col-md-4 text-center">
                  <a class="send btn btn-{{ (in_array($mKey,$currentYear->mails)) ? 'success' : 'primary' }}" href="{{ route('admin-volunteer-email', ['id' => $currentYear->id, 'mail' => $mKey]) }}">{{ $M['label'] }}</a>
                </div>
              @endforeach
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
{{ Form::close() }}

@if ($currentYear->id)
  <div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Removing volunteer {{ $currentYear->first_name }} {{ $currentYear->last_name }} for {{ $currentYear->year }} year</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <a id="delete" class="btn btn-primary" href="{{ route('admin-volunteer-delete', ['id' => $currentYear->id]) }}">Yes</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  @if ($currentYear->type)
    <div id="shortManual" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title">Merge forms manualy</h2>
          </div>
          <div class="modal-body">
            {{ Form::open(['url' => route('check-volunteer-forms'), 'id' => 'checkForm']) }}
              <div class="row">
                <div class="col-xs-5">
                  <label>First name</label>
                </div>
                <div class="col-xs-5">
                  <label>Last name</label>
                </div>
              </div>
              <div class="row">
                <input type="hidden" name="currentId" value="{{ $currentYear->id }}"/>
                <div class="col-xs-5 form-group">
                  <input name="firstname" type="text" class="form-control">
                </div>
                <div class="col-xs-5 form-group">
                  <input name="lastname" type="text" class="form-control">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-primary">Check</button>
                </div>
              </div>
            {{ Form::close() }}
            <div class="row">
              <div id="formResult" class="col-md-12 text-center">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    @if ($similar)
      <div id="short" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h2 class="modal-title">
                Similar forms for volunteer<br/>
                {{ $currentYear->first_name }} {{ $currentYear->last_name }} ({{ $currentYear->year }})
              </h2>
            </div>
            <div class="modal-body">
              @foreach ($similar as $SM)
                <h4>
                  <a href="{{ route('admin-volunteer-edit', $SM->id) }}" target="_blank">
                    {{ $SM->first_name }} {{ $SM->last_name }}
                  </a>
                  - last year ({{ $SM->year }})
                  @if ($currentYear->year == $SM->year)
                    <a href="{{ route('merge-volunteer-forms', ['id'=>$currentYear->id, 'similar'=>$SM->id]) }}" class="btn btn-default">Update form</a>
                  @else
                    <a href="{{ route('merge-volunteer-forms', ['id'=>$currentYear->id, 'similar'=>$SM->id]) }}" class="btn btn-success">Merge forms</a>
                  @endif
                </h4>
              @endforeach
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    @endif
  @endif
@endif

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
{{-- <script src="/chosen.jquery.min.js"></script> --}}
<script type="text/javascript">
$(document).ready(function() {
  var changed = false;
  $(window).bind('beforeunload', function () {
    $(':input').blur();
    if (changed == true)
      return "Are you sure you want to exit? Please complete sign up or the app will get deleted.";
  });
  $(':input').on('change', function() {
    changed = true;
  });
  $('form').submit(function(){$(window).unbind("beforeunload");})

  $('#checkForm').submit( function(e) {
    $('#formResult').text('')
    if ($('input[name="firstname"]').val() == '' && $('input[name="lastname"]').val() == '') {
      $('#formResult').text('Fill at least one field');
    } else {
      var action = $(this).attr('action'),
          params = $(this).serialize();
      $.post(action, params, function(response) {
        $('#formResult').html(response);
      })
    }
    e.preventDefault();
  });
  $(document).on('click', '#formResult > a.btn', function(e) {
    if ( !confirm('Are you shure?') ) {
      e.preventDefault();
    };
  });

  $("#schools-select, #state-select").select2();

  $('#state-select').change(function() {
    var action = $(this).data('url'),
        id = $(this).val();
    $.get(action, { state : id }, function(response) {
      $("#schools-select").children().remove();
      $.each(response, function(id, text) {
        $("#schools-select")
        .append(new Option(text, id, false, false))
        .trigger('change');
      });
    });
  })

  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0",
    dateFormat: 'dd/mm/yy',
  });

  $('a.send').on('click', function(e) {
    var button = $(this),
        action = button.attr('href')
    $.get(action, {}, function(response) {
      if (response.sended){
        alert('Success');
        button.removeClass('btn-primary');
        button.addClass('btn-success');
      }
    })
    e.preventDefault();
  });
});
</script>
@include('admin/footer')
