@include('admin/header')
<p></p>
<div class="row">
  <div class="col-md-12">
    <!-- <button id="export-button" type="button" class="btn btn-primary pull-right">Export volunteers</button> -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-export">Export volunteers</button>
    @if(Auth::user()->isAdmin())
    <button type="button" class="btn btn-primary pull-right" onclick="window.location.href='/admin/volunteers/create'">
      <span class="glyphicon glyphicon-plus"></span>&nbsp;Add volunteer
    </button>
    @endif
    <button id="reset-button" type="button" class="btn btn-primary pull-right">Reset filters</button>
  </div>
</div>
<p></p>

{{ csrf_field() }}
<table id= "table" class="table table-hover table-responsive tablesorter table-bordered">
  <thead>
    <tr>
      <th>id</th>
      <th>Name</th>
      <th>Lastname</th>
      <th>Status</th>
      <th>Strand</th>
      <th>Country</th>
      <th>Region</th>
      <th>Sessions</th>
      <th>Alt strand</th>
      <th>Deutsch speak</th>
      <th>Tickets</th>
      <th>Visa</th>
      <th>Payment</th>
      <th>Manager</th>
      <th></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th class="status-select"></th>
      <th class="strand-select"></th>
      <th class="country-select"></th>
      <th class="regions-select"></th>
      <th class="sessions-select"></th>
      <th class="altstrand-select"></th>
      <th class="deutsch-select"></th>
      <th class="ticket-select"></th>
      <th class="visa-select"></th>
      <th class="payment-select"></th>
      <th class="manager-select"></th>
      <th></th>
    </tr>
  </tfoot>
  <tbody>
    @foreach($volunteers as $key=>$volunteer)
    <tr>
      <td>{{ $volunteer->id}}</td>
      <td>{{ $volunteer->first_name }} {{ $volunteer->middle_name }}</td>
      <td>{{ $volunteer->last_name }}</td>
      <td>

        {{ $status[$volunteer->status]}}

      </td>
      <td>
        @foreach(explode(',',$volunteer->strand) as $value)
        {{ $strand[$value]}}
        @endforeach
      </td>
      <td>{{ $volunteer->country }}</td>
      <td>{{ isset($states[$volunteer->state]) ? $states[$volunteer->state] : '' }}</td>
      <td>
        @foreach(explode(',',$volunteer->sessions) as $value)
        @if(!empty($value))
        <?=$session[$value]." , "?>
        @endif
        @endforeach
      </td>
      <td>
       {{ $strand[$volunteer->alt_strand]}}
     </td>
     <td>
      @if($volunteer->speak==1)
      {{ 'Yes' }}
      @else
      {{ 'No' }}
      @endif

    </td>
    <td>
      @if($volunteer->tickets==1)
      {{ 'Yes' }}
      @else
      {{ 'No' }}
      @endif

    </td>
    <td>
      @if($volunteer->visa==1)
      {{ 'Yes' }}
      @else
      {{ 'No' }}
      @endif

    </td>
    <td>
      @if($volunteer->payment==1)
      {{ 'Yes' }}
      @else
      {{ 'No' }}
      @endif

    </td>
    <td>
      @if($volunteer->manager)
      {{$volunteer->manager}}
      @endif
    </td>
    <td class="actions"><button type="button" id="delete" class="btn btn-primary" onclick="window.open('/admin/volunteers/edit/{{$volunteer->id}}')"><span class="glyphicon glyphicon-pencil"></span></button>
     @if(Auth::user()->isAdmin())
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-<?=$key?>"><span class="glyphicon glyphicon-remove"></span></button>
     <!-- Modal -->
     <div id="myModal-<?=$key?>" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Removing volunteer {{ $volunteer->first_name }} {{ $volunteer->middle_name }} {{ $volunteer->second_name }} </h4>
          </div>
          <div class="modal-body">
            <p>Are you sure?</p>

          </div>
          <div class="modal-footer">
            <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href='/admin/volunteers/delete/{{$volunteer->id}}'">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </div>
        </div>

      </div>
    </div>
    @endif
  </td>
</tr>


@endforeach
</tbody>
</table>

<!-- Modal 4 export -->
<div id="myModal-export" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Columns for store in file</h4>
    </div>
    <div class="modal-body">
        <form id="emform">
        <table cellpadding="2" cellspacing="2">
        <thead>
            <tr>
            <th width="300">Column</th>
            <th width="300">Use in export</th>
            </tr>
            <tr><td colspan="2"><hr></td></tr>
        </thead>
        <tbody>
            @foreach ($exportColumns as $col => $set)
            <tr>
                <td align="right">
                    <label for="f-{{ $col }}"> {{ Config::get('list.volunteer-export.' . $col) }}</label>
                </td>
                <td>
                &nbsp;
                @if (isset($columns[$col]) && $columns[$col] == 1)
                <input type="checkbox" id="f-{{ $col }}" name="col[{{ $col }}]" value="1" checked="checked" />
                @else
                <input type="checkbox" id="f-{{ $col }}" name="col[{{ $col }}]" value="1" />
                @endif
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
       </form>
    </div>
    <div class="modal-footer">
      <button type="button" id="sebutton" class="btn btn-primary">Save&export</button> 
      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    </div>
  </div>

</div>
</div>
<!-- END Modal 4 export -->

</div>
</div>
</div>
<script type="text/javascript">
function removeDuplicates(num) {
  var x,
  len=num.length,
  out=[],
  obj={};

  for (x=0; x<len; x++) {
    obj[num[x]]=0;
  }
  for (x in obj) {
    out.push(x);
  }
  return out;
}
$(document).ready(function() {

  var $DT = $('#table').DataTable( {
    "order": [[ 0, "desc" ]],
    "stateSave": true,
    "pageLength": 100,
        // "bLengthChange":false,
        initComplete: function () {
         this.api().columns([3,4,5,6,7,8,9,10,11,12,13]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
          .appendTo( $(column.footer()).empty() )
          .on( 'change', function () {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
              );

            column
            .search( val ? val: '', true, false )
            .draw();
          } );
          column.data().unique().sort().each( function ( d, j ) {
            var tmp=d.split(',');
            for (var i = tmp.length - 1; i >= 0; i--) {
              if(tmp[i].trim().length>0){
                select.append( '<option value="'+tmp[i].trim()+'">'+tmp[i].trim()+'</option>' );
              }
            }
          } );
          $(".sessions-select option").each(function(){
            $(this).siblings("[value='"+ this.value+"']").remove();
          });
        } );

       }
     });

  $('#manager').change( function() {
    var gender = $(this).val();

    $DT.column(13).search(gender).draw();
  });

  $('#reset-button').on('click', function() {
    $('#table tfoot select').val('').change();
    $DT
      .search('')
     .columns()
     .search('')
     .draw();
  });

    $('#sebutton').on('click', function() { 
        window.open('/ua/admin/volunteers/export?status=' + 
                    $('.status-select select option:selected ').val() +
                    '&strand=' + $('.strand-select select option:selected ').val() + 
                    '&country=' + $('.country-select select option:selected ').val()+
                    '&state=' + $('.regions-select select option:selected ').val() + 
                    '&session=' + $('.sessions-select select option:selected ').val() +
                    '&altstrand=' + $('.altstrand-select select option:selected ').val() +
                    '&deutsch=' + $('.deutsch-select select option:selected ').val() +
                    '&ticket=' + $('.ticket-select select option:selected ').val() + 
                    '&visa=' + $('.visa-select select option:selected ').val() +
                    '&payment=' + $('.payment-select select option:selected ').val() +
                    '&manager=' + $('.manager-select select option:selected ').val() +
                    '&' + $('#emform').serialize()
        );

        $('#myModal-export').modal('toggle');
    });


});

/*$('#export-button').click(function() {
  var data= {
    'status':$('.status-select select option:selected ').val(),
    'strand':$('.strand-select select option:selected ').val(),
    'country':$('.country-select select option:selected ').val(),
    'state':$('.region-select select option:selected ').val(),
    'sessions':$('.sessions-select select option:selected ').val(),
    'altstrand':$('.altstrand-select select option:selected ').val(),
    'deutsch':$('.deutsch-select select option:selected ').val(),
    'ticket':$('.ticket-select select option:selected ').val(),
    'visa':$('.visa-select select option:selected ').val(),
    'payment':$('.payment-select select option:selected ').val(),
    'manager':$('.manager-select select option:selected ').val(),
  };
  $.ajax({
    'headers':{'X-CSRF-Token': $('input[name="_token"]').val()},
    'url':"/ua/admin/volunteers/export?status="+$('.status-select select option:selected ').val(),
    'type':'get',
    'data':data,
    'dataType':'json',
    complete: function(xhr, textStatus) {
      window.open('/ua/admin/volunteers/export?status='+$('.status-select select option:selected ').val()+'&strand='+$('.strand-select select option:selected ').val()+'&country='+$('.country-select select option:selected ').val()+'&state='+$('.region-select select option:selected ').val()+'&session='+$('.sessions-select select option:selected ').val()+'&altstrand='+$('.altstrand-select select option:selected ').val()+'&deutsch='+$('.deutsch-select select option:selected ').val()+'&ticket='+$('.ticket-select select option:selected ').val()+'&visa='+$('.visa-select select option:selected ').val()+'&payment='+$('.payment-select select option:selected ').val()+'&manager='+$('.manager-select select option:selected ').val(),'_blank');
    }
  });
});*/
</script>
<style type="text/css">
  #export-button {
    margin-right: 10px;
  }
  tfoot {
    display: table-header-group !important;
  }

  #table select {
   max-width: 100px;
 }
</style>
@include('admin/footer')
