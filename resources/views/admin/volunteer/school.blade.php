@foreach ($volSchools as $i => $sc)
<div class="statescdiv">
    <div class="form-group row">
        {!! Form::label('state', 'State', ['class'=>'col-third']) !!}
        {!! Form::select('state[' . $year->year . '][' . $i . ']', $states, $sc->state, ['class'=>'col-third selectstate','id'=>'state']) !!}
    </div>

    <div class="form-group row">
        {!! Form::label('school', 'School', ['class'=>'col-third']) !!}
        {!! Form::select('school[' . $year->year . '][' . $i . ']', $schools, $sc->id, ['class'=>'col schools']) !!}
    </div>
</div>
@endforeach