@include('admin/header')
<link rel="stylesheet" href="/admin-style.css">
<style>
  .col {
    width: 100%;
  }
</style>

<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0"
    });
  } );
</script>
<section class="school-reg">
  <h1 class="school-reg__title">Реєстрація волонтера</h1>
  @if($errors->has())
  @foreach ($errors->all() as $error)
  <div>{{ $error }}</div>
  @endforeach
  @endif
  <div class="row">
    <div class="col-md-12">
      {!! Form::open(['url' =>  '/'.LaravelLocalization::getCurrentLocale().'/admin/volunteers/edit/'.$volunteer->id,'files'=>true,'class'=>'']) !!}
      <input  id="vol-id" type="hidden" value="{{$volunteer->id}}">
      <div class="row">
       <div class="col-md-4 form-group">
         @if(Auth::user()->isAdmin())

        {!! Form::submit('Обновить!',['class'=>'btn btn-success']); !!}
        @endif
       </div>
        <div class="col-md-4 form-group">
          {!! Form::label('lastname', 'Last Name',['class'=>'']); !!}
          {!! Form::text('lastname',$volunteer->last_name,['class'=>'form-control'])!!}
        </div>
        <div class="col-md-4 form-group">
          {!! Form::label('firstname', 'First Name',['class'=>'']); !!}
          {!! Form::text('firstname',$volunteer->first_name,['class'=>'form-control'])!!}
        </div>
        <div class="col-md-4 form-group">
          {!! Form::label('middlename', 'Middle Name',['class'=>'']); !!}
          {!! Form::text('middlename',$volunteer->middle_name,['class'=>'form-control'])!!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('state', 'State',['class'=>'col-third']); !!}
        {!! Form::select('state',$states,$volunteer->state,['class'=>"col-third",'id'=>'state'])!!}
      </div>
      <div id="preload"  style="position: fixed;margin-left: 30%;border: 1px solid;background: gainsboro;display: none">
        <label class="col-third">loading...</label><img class="" src="/ajax-loader.gif">
      </div>
      <div class="form-group row">
        {!! Form::label('school', 'School',['class'=>'col-third']); !!}
        {!! Form::select('school',$schools,$volunteer->school_id,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('tickets', 'Tickets',['class'=>'col-third']); !!}

        {!! Form::select('tickets',array('1'=>'yes','0'=>'no'),$volunteer->tickets,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('visa', 'Visa',['class'=>'col-third']); !!}

        {!! Form::select('visa',array('1'=>'yes','0'=>'no'),$volunteer->visa,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('payment', 'Payment',['class'=>'col-third']); !!}

        {!! Form::select('payment',array('1'=>'yes','0'=>'no'),$volunteer->payment,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('status', 'Status',['class'=>'col-third']); !!}
        {!! Form::select('status',$status,$volunteer->status,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('manager', 'Manager',['class'=>'col-third']); !!}
        {!! Form::select('manager',$managers,$volunteer->manager,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('comment', 'Manager comment',['class'=>'col-third']); !!}
        {!! Form::textArea('comment',$volunteer->comment,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('created_at', 'Дата регистрации',['class'=>'col-third']); !!}
        {!! Form::text('created_at',$volunteer->created_at,['class'=>'col']) !!}
      </div>
      <div class="row">
        <div class="col-md-3 col-md-offset-3 form-group">
          {!! Form::label('cityzenship', 'Citizenship',['class'=>'']); !!}
          {!! Form::text('citizenship',$volunteer->citizenship,['class'=>'form-control'])!!}
        </div>

        <div class="col-md-3 form-group">
          {!! Form::label('passport', 'Passport number *',['class'=>'']); !!}
          {!! Form::text('passport',$volunteer->passport,['class'=>'form-group'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-3 col-md-offset-3 form-group">
          {!! Form::label('birthday', 'Birthday *',['class'=>'']); !!}
          {!! Form::text('birthday',$volunteer->birthday,['class'=>'form-group','id'=>'datepicker'])!!}
        </div>
        <div class="col-md-2 form-group">
          {!! Form::label('gender', 'Gender',['class'=>'']) !!}
          {!! Form::select('gender', array('male','female'),$volunteer->gender,['class'=>'form-group'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-3 col-md-offset-3 form-group">
          {!! Form::label('country', 'Country'); !!}
          {!! Form::text('country',$volunteer->country,['id'=>'country', 'class'=>'form-group'])!!}
        </div>
        <div class="col-md-3 form-group">
          {!! Form::label('city', 'City'); !!}
          {!! Form::text('city',$volunteer->city, ['class'=>'form-group'])!!}
        </div>
        <div class="col-md-3 form-group">
          {!! Form::label('street', 'Street, bil, app. '); !!}
          {!! Form::text('street',$volunteer->street, ['class'=>'form-group'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-3 col-md-offset-3 form-group">
          {!! Form::label('email', 'Email *',['class'=>'']); !!}
          {!! Form::text('email',$volunteer->email,['class'=>'form-group','id'=>'vol-email'])!!}
        </div>
        <div class="col-md-3 form-group">
          {!! Form::label('skype', 'Skype ',['class'=>'']); !!}
          {!! Form::text('skype',$volunteer->skype,['class'=>'form-group'])!!}
        </div>
      </div>

      <div class="form-group row">
        {!! Form::label('social', 'Link to your social media (FB, VK, Twitter, etc.) *',['class'=>'col-third']); !!}

        {!! Form::text('social',$volunteer->social,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('phone', 'Primary phone number [include country code] *',['class'=>'col-third']); !!}

        {!! Form::text('phone',$volunteer->phone,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('alt_phone', 'Alternate phone number [include country code]',['class'=>'col-third']); !!}

        {!! Form::text('alt_phone',$volunteer->alt_phone,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('been_in_ukraine', 'Have you been to Ukraine before? (For foreiners)',['class'=>'col-third']); !!}

        {!! Form::select('been_in_ukraine',array('1'=>'yes','0'=>'no'),$volunteer->been_in_ukraine,['class'=>'col']) !!}

      </div>
      <div class="form-group row">
        {!! Form::label('capacity', 'If yes, in what capacity (For foreiners) ',['class'=>'col-third']); !!}

        {!! Form::text('capacity',$volunteer->capacity,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('capacity', 'If yes, in what capacity (For foreiners) ',['class'=>'col-third']); !!}

        {!! Form::text('capacity',$volunteer->capacity,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('eng_level', 'What is your level of knowledge of English? *',['class'=>'col-third']); !!}

        {!! Form::select('eng_level', $skill,$volunteer->eng_level,['class'=>'col']) !!}
      </div>
      <div class="form-group row">
        {!! Form::label('other_lang', 'What is your level of knowledge of English? *',['class'=>'col-third']); !!}

        {!! Form::text('other_lang',$volunteer->other_lang,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('education', 'Education (Institution, field of study, degree/graduation year) *',['class'=>'col-third']); !!}

        {!! Form::text('education',$volunteer->education,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('describe', 'Describe any relevant work, volunteer, or personal experience that has prepared you for working at GoCamps *',['class'=>'col-third']); !!}

        {!! Form::textArea('describe',$volunteer->describe,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('about', 'Please tell us more about yourself, including hobbies and interests *',['class'=>'col-third']); !!}

        {!! Form::textArea('about',$volunteer->about,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('why', 'Why would you like to volunteer at GoCamps? *',['class'=>'col-third']); !!}

        {!! Form::textArea('why',$volunteer->why,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('strand', 'Strand',['class'=>'col-third']); !!}

        {!! Form::select('strand', $strand,$volunteer->strand,['class'=>'col']) !!}
      </div>
      <div class="form-group row">
        {!! Form::label('sessions', 'Please select the session(s) you wish to apply for *',['class'=>'col-third']); !!}

        @foreach($session as $key => $value)
        @if(in_array($key, $volunteer_sessions))
        {{-- !! Form::checkbox('session[]', $key, true, ['id'=>'session-' . $key]); !! --}}
        {!! Form::checkbox('session[]', $key, true, ['id'=>'session-' . $key]); !!}
        @else
        {!! Form::checkbox('session[]', $key, false, ['id'=>'session-' . $key]); !!}
        @endif
        {!! Form::label('session-' . $key, $value); !!}
        <br><br><br>
        @endforeach
      </div>
      <div class="form-group row">
        {!! Form::label('hear', 'How did you hear about GoCamps? *',['class'=>'col-third']); !!}

        {!! Form::text('hear',$volunteer->hear,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('dietary', 'Do you have any dietary restrictions? *',['class'=>'col-third']); !!}

        {!! Form::text('dietary',$volunteer->dietary,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('intrested', 'Are you interested in contributing to the online blog about your experience in Ukraine? ',['class'=>'col-third']); !!}

        {!! Form::text('intrested',$volunteer->intrested,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('special', 'Do you have any special needs that we should be informed about?',['class'=>'col-third']); !!}

        {!! Form::text('special',$volunteer->special,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('speak', 'Do you speak German?',['class'=>'col-third']); !!}

        {!! Form::select('speak',array('1'=>'Yes',0=>'No'),$volunteer->speak,['class'=>'col'])!!}
      </div>

      <div class="form-group row">
        {!! Form::label('level', 'If yes what is your level of German? Native Advanced Upper intermidiate Intermidiate',['class'=>'col-third']); !!}

        {!! Form::select('level',$skill,$volunteer->level,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('alt_strand', 'Choose the alternative camp strand',['class'=>'col-third']); !!}

        {!! Form::select('alt_strand',$strand,$volunteer->alt_strand,['class'=>'col'])!!}
      </div>
      <div class="form-group row">
        {!! Form::label('preferences', 'Do you have any preferences for the location of the camp site (We do not guarantee the location you point out. The earlier you submit the application the more chances we will have to satisfy your preference',['class'=>'col-third']); !!}

        {!! Form::text('preferences',$volunteer->preferences,['class'=>'col'])!!}
      </div>
      @if(Auth::user()->isAdmin())

      {!! Form::submit('Обновить!',['class'=>'btn btn-success']); !!}
      @endif
      <br>
      <br>
      <br>
      <div class="container">
        <div class="clearfix">
          <h3>Emails</h3>
          <table border="0">
            @foreach ($emailList as $emails)
            <tr align="center">
              <?php foreach ($emails as $key => $value) :?>
                <td>
                  @if(!isset($emails_arr[$key]))
                  <input id="send-message-{{$key}}" type="button" class="btn  btn-primary" value="{{$value}}"  data-toggle="modal" data-target="#modal-{{$key}}">
                  @else
                  <input id="{{$key}}"type="button" disabled class="send-email send-not btn  btn-primary" value="{{$value}}">
                  @endif
                  <div class="modal fade" id="modal-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Send message?</h4>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button  id="{{$key}}"type="button" class="send-email btn btn-primary ">Send message</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              <?php endforeach?>
            </tr>
            <tr align="center">
              <?php foreach ($emails as $key => $value) :?>
                <td>
                  @if(isset($emails_arr[$key]))
                  <a id="{{$key}}" class="send-another send-not"><span><i class="fa fa-refresh" aria-hidden="true"></i></span><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span></a>
                  @else
                  &nbsp;
                  @endif
                </td>
              <?php endforeach?>
            </tr>
            <tr><td colspan="4"><hr></td></tr>
            @endforeach
          </table>
        </div>
      </div>
      {!! Form::close() !!}

    </div>
  </div>
</section>
<script type="text/javascript">
  $('#country').change(function(){
    if($('#country').val()=='Украина' || $('#country').val()=='Ukraine'||$('#country').val()=='Україна')
      $('#state').css('display','block');
    else
      $('#state').css('display','none');
  });
  $('[class^=send-email]').click(function(){
    $('#send-message-'+this.id).prop('disabled', true);
    $('#modal-'+this.id).modal('toggle');
    $('.modal-backdrop').hide();
    console.log(this.id);
    $.ajax({
      headers:{ 'X-CSRF-Token': $('input[name="_token"]').val()},
      url:'/en/admin/volunteers/sendmail',
      data:{'id':this.id,'action':this.value,'email':$('#vol-email').val(),'vol_id':$('#vol-id').val()},
      complete:function(){}
    });

  });
  $('.send-another').click(function(){
    console.log(this);
    $(this).prop('disabled', true);
    $.ajax({
      headers:{ 'X-CSRF-Token': $('input[name="_token"]').val()},
      url:'/en/admin/volunteers/sendmail',
      data:{'id':this.id,'another':1,'email':$('#vol-email').val(),'vol_id':$('#vol-id').val()}
    });
  });
  $(document).ready(function () {
    var $modelSelect = $('[name^=school]');

    $('[name^=state]').on('change.select2', function () {
      $('#preload').show();
      var data = $(this).val();
      if(!data.length || data[0] === '') {
        return;
      };
      $modelSelect.empty().append('<option value="" selected></option>');
      $.ajax({
        headers: {
          'X-CSRF-Token': $('input[name="csrf-token"]').val()
        },
        url: '/admin/volunteers/getschools',
        method: 'get',
        data: {
          'data': data
        },
        success: function (data) {
          console.log(data);

          $.each(data, function (k, v) {
            $modelSelect.append('<option value=' + k + '>'+ v + '</option>');
          });
          $('#preload').hide();
        }
      });
    });
  });
</script>
<style type="text/css">
  .container {
    width: 1500px !important;
    max-width: 1500px !important;
  }
</style>
<script>
  //document.getElementById('session-2').disabled = true;
  //document.getElementById('session-3').disabled = true;
</script>

@include('admin/footer')
