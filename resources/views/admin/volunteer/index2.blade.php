@include('admin/header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Volunteers
      <div class="pull-right">
        <a href="#" class="btn btn-primary" id="reset">Reset filters</a>
        @if(Auth::user()->isAdmin())
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-export">Export volunteers</button>
        @endif
        <a class="btn btn-primary" href="{{ route('volunteers-view') }}">Add volunteer</a>
      </div>
      <div class="dropdown pull-right">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ $YEAR }}
        </button>
        <ul class="dropdown-menu">
          <li><a href="{{ route('volunteers2', 'All') }}">{{ "All" }}</a></li>
          @foreach ($years as $year)
            <li><a href="{{ route('volunteers2', $year) }}">{{ $year }}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="dropdown pull-right">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Add / Hide columns
        </button>
        <ul class="dropdown-menu columns columns-hider">
          <li>
            <a class="dropdown-item column-toggle" data-key="all" data-hidden="true" data-column="0">
              ALL
            </a>
          </li>
          <?php $iter = 4; ?>
          @foreach($columns as $colGroup)
            @foreach ($colGroup as $cKey => $col)
              @if ($col['hidden'])
                @if (isset($uv) && is_array($uv) && in_array($iter, $uv))
                <li>
                  <a class="dropdown-item column-toggle active" data-key="{{ $cKey }}" data-hidden="false" data-column="{{ $iter++ }}">
                    {!! $col['label'] !!}
                  </a>
                </li>
                @else
                <li>
                  <a class="dropdown-item column-toggle" data-key="{{ $cKey }}" data-hidden="true" data-column="{{ $iter++ }}">
                    {!! $col['label'] !!}
                  </a>
                </li>
                @endif
              @endif
            @endforeach
          @endforeach
        </ul>
      </div>
    </h1>
  </div>
  <div class="col-lg-12">
    <table id= "table" class="table table-hover table-responsive tablesorter table-bordered">
      <thead>
        <tr>
            <th class="tb-sm">ID</th>
            <th class="tb-lg">Name</th>
            <th class="tb-md">Status</th>
            <th class="tb-md">Sessions</th>
            @foreach ($columns as $colGroup)
              @foreach ($colGroup as $cKey => $col)
                @if ($col['hidden'])
                  <th class="hidden1 toggled1 {{ $cKey }}">{!! $col['label'] !!}</th>
                @endif
              @endforeach
            @endforeach
            <th class="tb-sm">Action</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th class="tb-sm"></th>
          <th class="tb-lg"></th>
          <th class="tb-md" data-key="status">{{ Form::select('status', $statusAr, isset($filters[2]['search']['value']) ? $filters[2]['search']['value'] : '', ['data-column' => 2]) }}</th>
          <th class="tb-md" data-key="documents">{{ Form::select('session', $session, isset($filters[3]['search']['value']) ? $filters[3]['search']['value'] : '', ['data-column' => 3]) }}</th>
          <?php $iter = 4; ?>
          @foreach ($columns as $colGroup)
            @foreach ($colGroup as $cKey => $col)
              @if (!$col['hidden'])
              @continue
              @elseif (in_array($cKey, $inputs))
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::text($cKey, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter, 'placeholder' => '"Enter" to search']) }}</th>
              @elseif (in_array($cKey, $selects))
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::select($cKey, $$cKey, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter]) }}</th>

              {{-- @elseif ($cKey == 'gender')
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::select('gender', $gender, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter]) }}</th>
              @elseif ($cKey == 'documents')
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::select('docs', $docsAr, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter]) }}</th>
              @elseif ($cKey == 'eng_level')
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::select('eng_level', $eng_level, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter]) }}</th>
              @elseif ($cKey == 'other_langs')
                <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}">{{ Form::select('other_langs', $other_langs, isset($filters[$iter]['search']['value']) ? $filters[$iter]['search']['value'] : '', ['data-column' => $iter]) }}</th> --}}
              @else
              <th class="hidden1 toggled1 {{ $cKey }}" data-key="{{ $cKey }}"></th>
              @endif

              <?php $iter++; ?>
            @endforeach
          @endforeach
          <th class="tb-sm"></th>
        </tr>
      </tfoot>

    </table>
  </div>
</div>

<div id="myModal-export" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Columns for store in file</h4>
      </div>
      <!-- <form id="export" action="{{ route('admin-volunteer-export', $YEAR) }}" method="POST"> -->
        {{ Form::open(['url' => route('admin-volunteer-export', $YEAR), 'method' => 'post', 'id' => 'export']) }}
        <div class="modal-body">
            <?php $count = 0; $iter = 0; ?>
            @foreach ($columns as $name => $colBlock)
              <h2 aria-expanded="{{ (!$count) }}" aria-controls="columns-{{ $count }}">
                <a data-toggle="collapse" href="#columns-{{ $count }}" aria-expanded="{{ (!$count) }}" aria-controls="columns-{{ $count }}">
                  {{ $name }}
                </a>
              </h2>
              <div class="collapse {{ (!$count) ? 'in' : '' }}" id="columns-{{ $count }}">
                <div class="row">
                  <div class="col-xs-6 col-sm-4">
                    <a href="javascript:;" class="checkall" data-group="{{ $count }}">All on/off</a>
                  </div>
                  @foreach ($colBlock as $key => $col)
                    <div class="col-xs-6 col-sm-4">
                      @if (isset($ue)
                          && is_array($ue)
                          && !in_array($iter, $ue)
                        )
                      <input type="checkbox" id="f-{{ $key }}" data-group="{{ $count }}" name="col[{{ $key }}]" value="{{ $iter }}" />
                      <label class="check-label" for="f-{{ $key }}"> {!! $col['label'] !!} </label>
                      @else
                      <input type="checkbox" id="f-{{ $key }}" data-group="{{ $count }}" name="col[{{ $key }}]" value="{{ $iter }}" checked="checked" />
                      <label class="check-label" for="f-{{ $key }}"> {!! $col['label'] !!} </label>
                      @endif
                    </div>
                    <?php ++$iter; ?>
                  @endforeach
                </div>
              </div>
              <?php ++$count; ?>
            @endforeach
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="sebutton" class="btn btn-primary">Export</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Modal 4 export -->

<script src="/assets/js/qw-admin.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  var $DT;
  var nosync;

  function syncToggle(p) {
    // console.log(p);
    var author = $('ul.columns-hider');
    var active = new Array();

    author.find('a.column-toggle').each(function(i,e) {
      var col = $(e).data('column');
      var st = $(e).hasClass('active');
      if (col == 0) return;
      if (st) {
        active.push(col);
      }
      $DT.column(col).visible(st);
    });
    // console.log(active);
    $.post('{{ route('storecol') }}', {a:active, _token: '{{ csrf_token() }}'}, function(){});
  }

  function reset() {
    nosync = true;
    $('#table select').val('').change();
    $('#table input').val('').change();
    nosync = false;
    // $('a.column-toggle').removeClass('active');
    // syncToggle();
    $DT
      .order( [ 0, 'desc' ] )
      .search('')
     .columns()
     .search('')
     .draw();
  }

  $('a.column-toggle').on('click', function(e) {
    e.preventDefault()
    var $this = $(this);
    var col = $this.data('column');

    if (col == '0') {
      if ($this.hasClass('active')) {
        $('a.column-toggle').removeClass('active');
      } else {
        $('a.column-toggle').addClass('active');
      }
    } else {
      $this.toggleClass('active');
    }
    syncToggle(1);
    // $DT.column(col).visible(!$DT.column(col).visible());

  });

  $DT = $('#table').DataTable( {
      "order": [[ 0, "desc" ]],
      "stateSave": true,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "autoHeight": false,
      "processing": true,
      "serverSide": true,
      "ajax": {
            "url": "{{ route('vprocessing') }}?y={{ $YEAR }}",
            "type": "POST"
        },
      "info": true
  });

  $DT.on('processing', function() {
    syncToggle(2);
    var indexes = $DT.rows().eq( 0 ).filter( function (rowIdx) {
      return $DT.cell( rowIdx, 42 ).data() == '1' ? true : false;
    });
    $DT.rows(indexes).nodes().to$().addClass('shorttype');
  });

  $('#table').on('change', 'select', function() {
    if (nosync) return true;
    var val = $.fn.dataTable.util.escapeRegex(
      $(this).val()
    );
    var colId = $(this).data('column');
    $DT.column(colId).search(val ? val : '', true, false).draw();
  });

  $('#table').on('change', 'input', function() {
    if (nosync) return true;
    var val = $.fn.dataTable.util.escapeRegex(
      $(this).val()
    );
    var colId = $(this).data('column');
    $DT.column(colId).search(val ? val : '', true, false).draw();
  });

  $('#reset').click(function () {
    reset();
  });

  syncToggle(3);
  $('#table select:first').trigger('change');
  // $DT.draw();

  $('#sebutton').on('click', function() {
    // var form = $('form#export'),
    //     data = form.serialize(),
    //     action = form.attr('action');

    // $.each($('#table tfoot th'), function(index, th) {
    //   var _th = $(th),
    //       key = _th.data('key'),
    //       filter = null;
    //   filter = _th.find('select option:selected').val() || _th.find('input').val() || null;
    //   if (key != null && filter != null)
    //   data += '&filters%5B' + key + '%5D=' + filter;
    // });
    // location.href = action + '?' + data;
    $('#export').submit();
  });

  $('a.checkall').on('click', function() {
    var $this = $(this);
    var gr = $this.data('group');

    if ($this.hasClass('checked')) {
      $('input[data-group=' + gr + ']').each(function(i,e) {
        $(e).attr('checked', false);
      });
    } else {
      $('input[data-group=' + gr + ']').each(function(i,e) {
        $(e).attr('checked', 'checked');
      });
    }
    $this.toggleClass('checked');
  });

  // ??
  $('a.checkall').removeClass('checked');

});
</script>
@include('admin/footer')
