<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Qwert CRM</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <link href="/assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/timeline/timeline.css" rel="stylesheet">

    <link href="/assets/css/qw-admin.css" rel="stylesheet">

    <!-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/> -->

    <link rel="stylesheet" type="text/css" href="/assets/datatables/datatables.min.css"/>
    <!-- <script type="text/javascript" src="/assets/datatables/datatables.min.js"></script> -->

    <!-- jQuery -->
    <script src="/assets/js/jquery-3.2.1.min.js"></script>

    @yield('custom_css')
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">QWERT CRM</a>
            </div>
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                    <li>
                      <a href="/admin/schools/2017-2018"><i class="fa fa-dashboard fa-fw"></i> Schools</a>
                    </li>
                    <li>
                      <a href="/admin/schools/2/2017-2018"><i class="fa fa-dashboard fa-fw"></i> Schools-new</a>
                    </li>
                    <li>
                      <a href="/admin/volunteers/2017-2018"><i class="fa fa-dashboard fa-fw"></i> Volunteers</a>
                    </li>
                    <li>
                      <a href="/admin/volunteers/2/2017-2018"><i class="fa fa-dashboard fa-fw"></i> Volunteers-new</a>
                    </li>
                  @if(Auth::user()->isAdmin())
                    <li>
                      <a href="/admin/articles"><i class="fa fa-dashboard fa-fw"></i> Articles</a>
                      <ul >
                        <li>
                          <a href="/en/admin/articles"><i class="fa  fa-fw"></i> View eng articles</a>
                        </li>
                        <li>
                          <a href="/ua/admin/articles"><i class="fa  fa-fw"></i> View ukr articles</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/admin/projects"><i class="fa fa-dashboard fa-fw"></i> Projects</a>
                      <ul >
                        <li>
                          <a href="/en/admin/projects"><i class="fa  fa-fw"></i> View eng projects</a>
                        </li>
                        <li>
                          <a href="/ua/admin/projects"><i class="fa  fa-fw"></i> View ukr projects</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/admin/categories"><i class="fa fa-dashboard fa-fw"></i> Categories</a>
                      <ul >
                        <li>
                          <a href="/en/admin/categories"><i class="fa  fa-fw"></i> View eng categories</a>
                        </li>
                        <li>
                          <a href="/ua/admin/categories"><i class="fa  fa-fw"></i> View ukr categories</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/admin/users"><i class="fa fa-dashboard fa-fw"></i> Users</a>
                    </li>
                    <li>
                      <a href="/admin/pages"><i class="fa fa-dashboard fa-fw"></i> Pages</a>
                      <ul >
                        <li>
                          <a href="/en/admin/pages"><i class="fa  fa-fw"></i> View eng pages</a>
                        </li>
                        <li>
                          <a href="/ua/admin/pages"><i class="fa  fa-fw"></i> View ukr pages</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/admin/materials"><i class="fa fa-dashboard fa-fw"></i> Materials</a>
                      <ul >
                        <li>
                          <a href="/en/admin/materials"><i class="fa  fa-fw"></i> View eng materials</a>
                        </li>
                        <li>
                          <a href="/ua/admin/materials"><i class="fa  fa-fw"></i> View ukr materials</a>
                        </li>
                      </ul>
                    </li>
                  @endif
                    <li>
                      <a href="{{ URL::route('user-logout') }}">Sign Out</a>
                    </li>
                  </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
