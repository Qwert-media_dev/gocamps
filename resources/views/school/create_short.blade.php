@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('header')
<div class="">
  <h1 class="page-header__page-title"><span class="page-header__page-title-inner">Реєстрація школи</span></h1>
</div>
@endsection

@section('content')
<section class="school-reg new-form">
  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/school/short-create', 'files'=>true, 'class'=>'school-reg__form form', 'name'=>'school-form-new']) !!}

  @if($errors->all())
    <ul style="text-align:center;list-style:none;border:1px solid red;border-radius:5px;padding:20px;">
      @foreach ($errors->all() as $error)
        <li style="color:red;font-size:20px;">{{ $error }}</li>
      @endforeach
    </ul>
  @endif

  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h3>Виконайте креативне завдання <br/>перш ніж заповнювати анкету</h3>
      <p>Будь ласка, заповнюйте форму згідно вимог українського правопису. З великої літери перше слово, іншу інформацію маленькими літерами. Не скорочуйте інформацію.</p>
    </div>
    <div class="form-new-col col-1-3">
      <h3>Вперше&nbsp;реєструєтесь на GoCamp?</h3>
      <a href="{{ route('school-create') }}" class="new-form-btn full-reg">Пройти Повну реєстрацю</a>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h2>Загальна інформація про школу</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('title', 'Повна назва навчального закладу *'); !!}
        {!! Form::text('title', '', ['data-required'=>'true'])!!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('state', 'Область *'); !!}
        {!! Form::select('state', $states, '', ['data-required'=>'true']); !!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('title_eng', 'Full name of school *'); !!}
        {!! Form::text('title_eng', '', ['data-required'=>'true'])!!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('state_eng', 'Region *'); !!}
        {!! Form::select('state_eng', $states_eng, '', ['data-required'=>'true']); !!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-row">
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('', 'Тип населеного пункту *'); !!}
            <div class="radio-group mobile-row">
              @foreach ($city_type as $key => $value)
                <div class="radio-group-item">
                  {!! Form::radio('city_type', $key, ($key == 0), ['id'=>'city_type_' . $key, 'data-required'=>'true']); !!}
                  {!! Form::label('city_type_' . $key, $value); !!}
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('city', 'Назва населеного пункту *'); !!}
            {!! Form::text('city', '', ['data-required'=>'true']) !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        <label></label>
        <p class="helper-text">В назві зазначте, будь ласка, лише назву населеного пункту, без вказання типу</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-row">
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('', 'Locality type *'); !!}
            <div class="radio-group mobile-row">
              @foreach ($city_type_eng as $key => $value)
                <div class="radio-group-item">
                  {!! Form::radio('city_type_eng', $key, ($key == 0), ['id'=>'city_type_eng_' . $key, 'data-required'=>'true']); !!}
                  {!! Form::label('city_type_eng_' . $key, $value); !!}
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('city_eng', 'Name of locality *'); !!}
            {!! Form::text('city_eng', '', ['data-required'=>'true']) !!}
          </div>
        </div>
      </div>
    </div>
    {{-- <div class="form-new-col col-1-3">
      <div class="form-group">
        <label></label>
        <p class="helper-text">В назві зазначте, будь ласка, лише назву населеного пункту, без вказання типу</p>
      </div>
    </div> --}}
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('railway_station', 'Назва найближчої залізничної станції *'); !!}
        {!! Form::text('railway_station','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h2>Кількість дітей та деталі проекту</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('quantity_children', 'На яку кількість дітей ви очікуєте в таборі? *'); !!}
      <div class="radio-group">
        @foreach($quantity_children as $key => $value)
          @if($key > 0)
            <div class="radio-group-item">
              {!! Form::radio('quantity_children', $key, ($key == 1), ['id'=>'quantity_children-' . $key, 'data-required'=>'true']); !!}
              {!! Form::label('quantity_children-' . $key, $value); !!}
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <label for="dates">Оберіть дати вашого табору <i>*</i><span>Ви можете обрати декілька можливих сесій, що підвищить вірогідність отримання волонтера школою</span></label>
      <div class="checkbox-group">
        @foreach($sessions as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('session[]', $key, 0, ['id'=>'session-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('session-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('type_camp', 'Оберіть тип табору *'); !!}
      <div class="radio-group mobile-col-1">
        @foreach($camp_type as $key => $value)
          @if($key > 0)
            <div class="radio-group-item">
              {!! Form::radio('type_camp', $key, ($key == 1), ['id'=>'type_camp-' . $key, 'data-required'=>'true']); !!}
              {!! Form::label('type_camp-' . $key, $value); !!}
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      <div class="form-group">
        {!! Form::label('share_volunteer', 'Чи готові ви ділити волонтера з іншою&#13;&#10; школою вашого населеного пункту? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('share_volunteer', '1', 1, ['id'=>'share_volunteer_1', 'data-required'=>'true']); !!}
            {!! Form::label('share_volunteer_1', 'Так, готовий'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('share_volunteer', '0', false, ['id'=>'share_volunteer_0', 'data-required'=>'true']); !!}
            {!! Form::label('share_volunteer_0', 'Ні, не готовий'); !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-2">
      <div class="form-group">
        {!! Form::label('share_camp', 'Чи готові ви відкрити двері табору&#13;&#10; для дітей з інших шкіл? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('share_camp', '1', 1, ['id'=>'share_camp_1', 'data-required'=>'true']); !!}
            {!! Form::label('share_camp_1', 'Так, готовий'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('share_camp', '0', false, ['id'=>'share_camp_0', 'data-required'=>'true']); !!}
            {!! Form::label('share_camp_0', 'Ні, не готовий'); !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('camp_lang', 'Мова табору'); !!}
      <div class="radio-group">
        @foreach($camp_lang as $key => $value)
          <div class="radio-group-item">
            {!! Form::radio('camp_lang', $key, ($key == 0), ['id'=>'camp_lang-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('camp_lang-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div> --}}
  <div class="form-row">
    <div class="form-new-col col-3">
      <label>Мова табору *</label>
      <div class="checkbox-group">
        @foreach($camp_lang as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('camp_lang[]', $key, ($key == 0), ['id'=>'camp_lang-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('camp_lang-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Контакти</h2>
      <h4>Директор школи</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('sdirector_name', 'ПІБ (прізвище, ім’я та по-батькові) *'); !!}
        {!! Form::text('sdirector_name','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('sdirector_phone', 'Номер телефону *'); !!}
        {!! Form::text('sdirector_phone','', ['data-required'=>'true','class'=>'phone-mask'])!!}
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('sdirector_email', 'Email *'); !!}
        {!! Form::text('sdirector_email', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Вчитель англійської мови, відповідальний за розробку програми</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('eteacher_name', 'ПІБ (прізвище, ім’я та по-батькові) *'); !!}
        {!! Form::text('teacher_name', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('eteacher_phone', 'Номер телефону *'); !!}
        {!! Form::text('teacher_phone','', ['data-required'=>'true','class'=>'phone-mask'])!!}
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('eteacher_email', 'Email *'); !!}
        {!! Form::text('teacher_email', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Креативне завдання</h2>
      <h4>Зняти відео на тему “Як участь у проекті GoCamp вплинула на дітей?”</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('video', 'Посилання на відео *'); !!}
        {!! Form::label('', 'Будь ласка, завантажте відео лише на youtube.com або vimeo.com. Посилання на інші джерела не будуть враховані.', ['class' => 'helper-text']); !!}
        {!! Form::text('video', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="info-text">
        <p><span>*</span> Дати табору можуть бути змінені організатором за попередньої письмової згоди школи.</p>
        <p>Кількість волонтерів на кожний з заїздів обмежена, всі волонтери самостійно обирають дати приїзду. <br/>Вірогідність отримати волонтера різна на кожному з заїздів. </p>
        <p>Ви можете обрати декілька можливих сесій, що підвищить вірогідність отримання волонтера школою.</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        <div class="checkbox-simple">
          {!! Form::checkbox('', '', false, ['id'=>'agree', 'data-required'=>'true']); !!}
          {!! Form::label('agree', 'Я погоджуюся на обробку персональних даних'); !!}
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::submit('Зареєструватися',['class'=>'new-form-btn']); !!}
      </div>
    </div>
  </div>

{!! csrf_field() !!}
  {!! Form::close() !!}
</section>

<!-- Modals -->
<div class="modal" id="modal">
  <p class="modal__title">Ви впевненні, що бажаєте відправити анкету без відео?</p>
  <button class="btn btn--small js-modal-submit">Так</button>
  <button class="btn btn--small js-modal-cansel">Ні</button>
</div>

<div id="modal-errors" class="modal">
  <p class="modal__title js-error">Вы уверенны что хотите отправить анкету без видео?</p>
  <button class="btn btn--small js-modal-cansel">Close</button>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/jquery.maskedinput.min.js"></script>
<script>
  $( function() {
    $('section.new-form form').on('submit', function(e){
      checkFields(e);
    });
  } );

  $(".phone-mask").mask("+380 99 999 99 99");

  function checkFields(event){
    $('[data-required="true"]').each(function(){
      var $this = $(this);
      if($this.is(':text') || $this.is('textarea')){
        if(!$this.val().length){
          $this.addClass('error');
        }else{
          if($this.attr('id') == 'email'){
            if(validateEmail($this.val())){
              $this.removeClass('error');
            }else{
              $this.addClass('error');
            }
          }else{
            $this.removeClass('error');
          }
        }
      }else if($this.is('select')){
        if($this.val() == 0){
          $this.addClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').addClass('error');
        }else{
          $this.removeClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').removeClass('error');
        }
      }else if($this.is(':radio')){
        var name = $this.attr("name");
        var checked = $(":radio[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":radio[name='"+name+"']").closest('.radio-group').addClass('error');
        }else{
          $(":radio[name='"+name+"']").closest('.radio-group').removeClass('error');
        }
      }else if($this.is(':checkbox')){
        var name = $this.attr("name");
        var checked = $(":checkbox[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":checkbox[name='"+name+"']").parent().addClass('error');
        }else{
          $(":checkbox[name='"+name+"']").parent().removeClass('error');
        }
      }
    });

    if($('.new-form .error').length != 0){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $('.new-form .error:first').closest('.form-row').offset().top
      }, 1000);
    }
  }
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)*(\+[a-z0-9-]+)?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  };
</script>
@endsection
