@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('header')
<div class="">
  <h1 class="page-header__page-title"><span class="page-header__page-title-inner">Реєстрація школи</span></h1>
</div>
@endsection

@section('content')
<section class="school-reg new-form">
  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/school/create', 'files'=>true, 'class'=>'school-reg__form form', 'name'=>'school-form-new']) !!}

  @if($errors->all())
    <ul style="text-align:center;list-style:none;border:1px solid red;border-radius:5px;padding:20px;">
      @foreach ($errors->all() as $error)
        <li style="color:red;font-size:20px;">{{ $error }}</li>
      @endforeach
    </ul>
  @endif

  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h3>Виконайте креативне завдання <br/>перш ніж заповнювати анкету</h3>
      <p>Будь ласка, заповнюйте форму згідно вимог українського правопису. З великої літери перше слово, іншу інформацію маленькими літерами. Не скорочуйте інформацію.</p>
    </div>
    <div class="form-new-col col-1-3">
      <h3>Вже реєструвалися на GoCamp?</h3>
      <a href="{{ route('school-short-store') }}" class="new-form-btn full-reg">Пройти коротку реєстрацію</a>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h2>Загальна інформація про школу</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('title', 'Повна назва навчального закладу *'); !!}
        {!! Form::text('title','', ['data-required'=>'true'])!!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('state', 'Область *'); !!}
        {!! Form::select('state', $states, '', ['data-required'=>'true']); !!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('title_eng', 'Full name of school *'); !!}
        {!! Form::text('title_eng', '', ['data-required'=>'true'])!!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('state_eng', 'Region *'); !!}
        {!! Form::select('state_eng', $states_eng, '', ['data-required'=>'true']); !!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-row">
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('', 'Тип населеного пункту *'); !!}
            <div class="radio-group mobile-row">
              @foreach ($city_type as $key => $value)
                <div class="radio-group-item">
                  {!! Form::radio('city_type', $key, ($key == 0), ['id'=>'city_type_' . $key, 'data-required'=>'true']); !!}
                  {!! Form::label('city_type_' . $key, $value); !!}
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('city', 'Назва населеного пункту *'); !!}
            {!! Form::text('city', '', ['data-required'=>'true']) !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        <label></label>
        <p class="helper-text">В назві зазначте, будь ласка, лише назву населеного пункту, без вказання типу</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-row">
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('', 'Locality type *'); !!}
            <div class="radio-group mobile-row">
              @foreach ($city_type_eng as $key => $value)
                <div class="radio-group-item">
                  {!! Form::radio('city_type_eng', $key, ($key == 0), ['id'=>'city_type_eng_' . $key, 'data-required'=>'true']); !!}
                  {!! Form::label('city_type_eng_' . $key, $value); !!}
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="form-new-col col-1-2">
          <div class="form-group">
            {!! Form::label('city_eng', 'Name of locality *'); !!}
            {!! Form::text('city_eng', '', ['data-required'=>'true']) !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('street_eng', 'Street *'); !!}
        {!! Form::text('street_eng', '', ['data-required'=>'true']) !!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('street', 'Вулиця / Проспект / Бульвар *'); !!}
        {!! Form::text('street','',['placeholder'=>'Наприклад, вул. Шевченка', 'data-required'=>'true']) !!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('house', 'Будинок (№) *'); !!}
        {!! Form::text('house','', ['data-required'=>'true']) !!}
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('railway_station', 'Найближча&nbsp;залізнична&nbsp;станція&nbsp;*'); !!}
        {!! Form::text('railway_station','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h2>Досвід школи</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('experience', 'Чи має школа досвід роботи в літньому мовному таборі? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('experience', '1', 0, ['id'=>'experience_1', 'data-required'=>'true']); !!}
            {!! Form::label('experience_1', 'Так, має'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('experience', '0', 1, ['id'=>'experience_0', 'data-required'=>'true']); !!}
            {!! Form::label('experience_0', 'Ні, не має'); !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-row" style="display:none;" id="exp-1">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('description', 'Коротко опишіть його (До 200 слів)'); !!}
        {!! Form::textArea('description','', ['data-maxwords' => 200])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row" id="exp-0">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('description', 'Коротко обґрунтуйте, чому ви хочете взяти участь в GoCamp? (До 200 слів) *'); !!}
        {!! Form::textArea('description2','', ['data-required' => 'true', 'data-maxwords' => 200])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Розкажіть про свою школу</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('about_school', 'Опиc має бути українською мовою і може включати посилання на сайт та соціальні мережі школи (до 200 слів без посилань) *'); !!}
        {!! Form::textArea('about_school','', ['data-required'=>'true', 'data-maxwords' => 200, 'data-urls' => 'true'])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Tell us about your school</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('about_school_eng', 'Опиc має бути англійською мовою і може включати посилання на сайт та соціальні мережі школи (до 200 слів без посилань) *'); !!}
        {!! Form::textArea('about_school_eng','', ['data-required'=>'true', 'data-maxwords' => 200, 'data-urls' => 'true'])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Розкажіть про своє місто (село)</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('about_city', 'Опиc має бути українською мовою і може містити посилання на сайти про ваш населений пункт (до 200 слів без посилань) *'); !!}
        {!! Form::textArea('about_city','', ['data-required'=>'true', 'data-maxwords' => 200, 'data-urls' => 'true'])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Tell us about your City / Town / Village</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('about_city_eng', 'Опиc має бути англійською мовою і може містити посилання на сайти про ваш населений пункт (до 200 слів без посилань) *'); !!}
        {!! Form::textArea('about_city_eng','', ['data-required'=>'true', 'data-maxwords' => 200, 'data-urls' => 'true'])!!}
        <p class="words-left">Залишилось <span>200</span> слів</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h2>Кількість дітей та деталі проекту</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('quantity_children', 'На яку кількість дітей ви очікуєте в таборі? *'); !!}
      <div class="radio-group">
        @foreach($quantity_children as $key => $value)
          @if($key > 0)
            <div class="radio-group-item">
              {!! Form::radio('quantity_children', $key, ($key == 1), ['id'=>'quantity_children-' . $key, 'data-required'=>'true']); !!}
              {!! Form::label('quantity_children-' . $key, $value); !!}
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <label for="dates">Оберіть дати вашого табору <i>*</i><span>Ви можете обрати декілька можливих сесій, що підвищить вірогідність отримання волонтера школою</span></label>
      <div class="checkbox-group">
        @foreach($sessions as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('session[]', $key, '', ['id'=>'session-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('session-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('type_camp', 'Оберіть тип табору *'); !!}
      <div class="radio-group mobile-col-1">
        @foreach($camp_type as $key => $value)
          @if($key > 0)
            <div class="radio-group-item">
              {!! Form::radio('type_camp', $key, ($key == 1), ['id'=>'type_camp-' . $key, 'data-required'=>'true']); !!}
              {!! Form::label('type_camp-' . $key, $value); !!}
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      <div class="form-group">
        {!! Form::label('share_volunteer', 'Чи готові ви ділити волонтера з іншою&#13;&#10; школою вашого населеного пункту? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('share_volunteer', '1', 1, ['id'=>'share_volunteer_1', 'data-required'=>'true']); !!}
            {!! Form::label('share_volunteer_1', 'Так, готовий'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('share_volunteer', '0', false, ['id'=>'share_volunteer_0', 'data-required'=>'true']); !!}
            {!! Form::label('share_volunteer_0', 'Ні, не готовий'); !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-2">
      <div class="form-group">
        {!! Form::label('share_camp', 'Чи готові ви відкрити двері табору&#13;&#10; для дітей з інших шкіл? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('share_camp', '1', 1, ['id'=>'share_camp_1', 'data-required'=>'true']); !!}
            {!! Form::label('share_camp_1', 'Так, готовий'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('share_camp', '0', false, ['id'=>'share_camp_0', 'data-required'=>'true']); !!}
            {!! Form::label('share_camp_0', 'Ні, не готовий'); !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('camp_lang', 'Мова табору *'); !!}
      <div class="radio-group">
        @foreach($camp_lang as $key => $value)
          <div class="radio-group-item">
            {!! Form::radio('camp_lang', $key, ($key == 0), ['id'=>'camp_lang-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('camp_lang-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div> --}}
  <div class="form-row">
    <div class="form-new-col col-3">
      <label>Мова табору *</label>
      <div class="checkbox-group">
        @foreach($camp_lang as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('camp_lang[]', $key, ($key == 0), ['id'=>'camp_lang-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('camp_lang-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Контакти</h2>
      <h4>Директор школи</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('sdirector_name', 'ПІБ (прізвище, ім’я та по-батькові) *'); !!}
        {!! Form::text('sdirector_name','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('sdirector_phone', 'Номер телефону *'); !!}
        {!! Form::text('sdirector_phone','', ['data-required'=>'true','class'=>'phone-mask'])!!}
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('sdirector_email', 'Email *'); !!}
        {!! Form::text('sdirector_email', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Вчитель англійської мови, відповідальний за розробку програми</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('eteacher_name', 'ПІБ (прізвище, ім’я та по-батькові) *'); !!}
        {!! Form::text('teacher_name', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('eteacher_phone', 'Номер телефону *'); !!}
        {!! Form::text('teacher_phone','', ['data-required'=>'true','class'=>'phone-mask'])!!}
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('eteacher_email', 'Email *'); !!}
        {!! Form::text('teacher_email','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Школа</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('school_phone', 'Шкільний номер телефону *'); !!}
        {!! Form::text('school_phone','', ['data-required'=>'true','class'=>'phone-mask'])!!}
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('school_email', 'Офіційний Email школи *'); !!}
        {!! Form::text('school_email', '', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Креативне завдання</h2>
      <h4>Зняти відео на тему “Запросіть волонтера до своєї школи”</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        {!! Form::label('video', 'Посилання на відео *'); !!}
        {!! Form::label('', 'Будь ласка, завантажте відео лише на youtube.com або vimeo.com. Посилання на інші джерела не будуть враховані.', ['class' => 'helper-text']); !!}
        {!! Form::text('video','', ['data-required'=>'true'])!!}
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="info-text">
        <p><span>*</span> Дати табору можуть бути змінені організатором за попередньої письмової згоди школи.</p>
        <p>Кількість волонтерів на кожний з заїздів обмежена, всі волонтери самостійно обирають дати приїзду. <br/>Вірогідність отримати волонтера різна на кожному з заїздів. </p>
        <p>Ви можете обрати декілька можливих сесій, що підвищить вірогідність отримання волонтера школою.</p>
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        <div class="checkbox-simple">
          {!! Form::checkbox('', '', false, ['id'=>'agree', 'data-required'=>'true']); !!}
          {!! Form::label('agree', 'Я погоджуюся на обробку персональних даних'); !!}
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::submit('Зареєструватися',['class'=>'new-form-btn']); !!}
      </div>
    </div>
  </div>
</section>

<!-- Modals -->
<div class="modal" id="modal">
  <p class="modal__title">Ви впевненні, що бажаєте відправити анкету без відео?</p>
  <button class="btn btn--small js-modal-submit">Так</button>
  <button class="btn btn--small js-modal-cansel">Ні</button>
</div>

<div id="modal-errors" class="modal">
  <p class="modal__title js-error">Вы уверенны что хотите отправить анкету без видео?</p>
  <button class="btn btn--small js-modal-cansel">Close</button>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/jquery.maskedinput.min.js"></script>
<script>
  $( function() {
    $('.new-form form').on('submit', function(e){
      checkFields(e);
    });

    $(".phone-mask").mask("+380 99 999 99 99");

    $('[data-maxwords]').on('input', function(e){
      var $this = $(this);
      var maxwords = parseInt($this.attr('data-maxwords'));

      if($this.val().length){
        var words = $this.val().match(/\S+/g);

        if($this.attr('data-urls')){
          var urlsIdx = [];
          for (var i = 0; i<words.length; i++){
            var url = isUrl(words[i]);
            if(url){
              urlsIdx.push(i);
            }
          }
          for (var i = urlsIdx.length -1; i >= 0; i--){
            words.splice(urlsIdx[i], 1);
          }
        }

        if (words.length > maxwords) {
          var trimmed = $(this).val().split(/\s+/, maxwords).join(" ");
          $this.val(trimmed + " ");
        }
        var wordsLeft = maxwords-words.length;
        $this.closest('.form-group').find('.words-left span').text(wordsLeft > 0 ? wordsLeft : 0);
      }else{
        $this.closest('.form-group').find('.words-left span').text(maxwords);
      }
    });

    $('[name="experience"]').click(function() {
      var show = 'exp-' + $(this).val(),
          hide = 'exp-' + Math.abs($(this).val()-1);

      if (show == 'exp-0') {
        $('[name="description2"]').attr('data-required', true);
      } else {
        $('[name="description2"]').removeAttr('data-required').removeClass('error');
      }

      $('#' + show).show();
      $('#' + hide).hide();
    })
  });

  function checkFields(event){
    $('[data-required="true"]').each(function(){
      var $this = $(this);
      if($this.is(':text') || $this.is('textarea')){
        if(!$this.val().length){
          $this.addClass('error');
        }else{
          if($this.attr('id') == 'email'){
            if(validateEmail($this.val())){
              $this.removeClass('error');
            }else{
              $this.addClass('error');
            }
          }else{
            $this.removeClass('error');
          }
        }
      }else if($this.is('select')){
        if($this.val() == 0){
          $this.addClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').addClass('error');
        }else{
          $this.removeClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').removeClass('error');
        }
      }else if($this.is(':radio')){
        var name = $this.attr("name");
        var checked = $(":radio[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":radio[name='"+name+"']").closest('.radio-group').addClass('error');
        }else{
          $(":radio[name='"+name+"']").closest('.radio-group').removeClass('error');
        }
      }else if($this.is(':checkbox')){
        var name = $this.attr("name");
        var checked = $(":checkbox[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":checkbox[name='"+name+"']").parent().addClass('error');
        }else{
          $(":checkbox[name='"+name+"']").parent().removeClass('error');
        }
      }
    });

    if($('.new-form .error').length != 0){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $('.new-form .error:first').closest('.form-row').offset().top
      }, 1000);
    }
  }
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)*(\+[a-z0-9-]+)?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  };

  function isUrl(s) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  }
</script>
@endsection
