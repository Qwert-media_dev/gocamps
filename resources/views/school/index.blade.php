@extends("$model->layout")

@section('header')
<h1 class="page-header__page-title page-header__page-title--school">
  <span class="page-header__page-title-inner">{!! $model->title !!}</span>
</h1>
@endsection

@section('content')
</div><!-- .container -->

{!! html_entity_decode($model->content) !!}

<!-- <section class="participant">
  <div class="participant__intro">

    <p>Заповнивши анкету сьогодні, вже завтра ваша школа може увійти до когорти найкрутіших кемпів в Україні та стати флагманом Української освітньої революції.</p>

    <a class="btn participant__join" href="{{ route('school-create') }}">Зареєструватися</a>
  </div>

  <div class="participant__benefits">
    <h2 class="participant__title">Що надає GoCamp школам</h2>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col col-third">
      <span class="our-goal__icon icon icon-display"></span>
        <p class="participant__benefit-text">Тренінги для вчителів </p>
      </div>
      <div class="participant__benefit col col-third">
      <span class="our-goal__icon icon icon-briefcase"></span>
        <p class="participant__benefit-text">Методичні матеріали</p>
      </div>
      <div class="participant__benefit col col-third">
      <span class="our-goal__icon icon icon-star-empty"></span>
        <p class="participant__benefit-text">Активності та проектні роботи</p>
      </div>
    </div>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col col-third">
      <span class="our-goal__icon icon icon-user"></span>
        <p class="participant__benefit-text">Іноземного волонтера</p>
      </div>
      <div class="participant__benefit col col-third">
      <span class="our-goal__icon icon icon-clock"></span>
        <p class="participant__benefit-text">24/7 підтримку</p>
      </div>
      <div class="participant__benefit col col-third">
      </div>
    </div>
  </div>

  <div class="participant__requirements">
    <div class="container">
      <h2 class="participant__requirements-title participant__title">Креативне завдання</h2>
      <p>Навесні -влітку 2017 року ми хочемо запросити 1000 волонтерів зі всього світу до України. Ми хочемо, щоб волонтери побачили не лише великі міста, а й відвідали наймальовничіші куточки нашої країни, про які ніхто в світі ще не чув. Допоможіть нам розповісти волонтерам про свої міста, містечка та села, запросіть їх до своєї школи!</p>
      <p> Креативна робота "Запроси волонтера до своєї школи" має бути виконана в форматі відео чи презентації (PowerPoint, PDF). </p>
      <p>Відео - не більше 90 секунд, презентація - не більше 5 сторінок. </p>
      <p>Ваша робота буде врахована під час процесу підбору волонтера. Тож саме від вас залежить, чи приїде до вас іноземний волонтер!</p>
    </div>
  </div>
  <div class="participant__steps">
    <div class="container">
      <h2 class="participant__steps-title participant__title">Як взяти участь в проекті?</h2>
      <ul class="participant__steps-list">
        <li>Ознаймитися з детальним описом проекту на сайті <a href="gocamps.com.ua/projects">gocamps.com.ua/projects</a>.</li>
        <li>Виконати креативне завдання</li>
        <li>Заповнити анкету на <a href="http://gocamps.com.ua/school/create">сторінці</a>. (Прийом заявок буде відкритий до 30 листопада)</li>
        <li>Прослухати вебінар про проект та задати питання до організаторів. Вебінар відбудеться 25 жовтня 2016 року. Оголошення про час та посинання на вебінар ви знайтете після 18 жовтня на сайті gocamps.com.ua та на наших сторінках в Фейсбуці</li>
        <li>Чекати на результат. Рейтингування шкіл і підбір волонтерів для шкіл відповідно до обраного напрямку. Підбір волонтерів відбувається відповідно до рейтинку школи. Ми оцінюємо креативне завдання, описи школи та населеного пункту як українською так і англійською мовами</li>
        <li>Познайомитися з волонтером</li>
        <li>Відвідати тренінг для вчителів</li>
        <li> Розробити програму спільно з волонтером</li>
        <li>Let's GoCamp</li>
      </ul>
    </div>
  </div>

  <div class="participant__questions">
    <h2 class="participant__questions-title participant__title">Q&A</h2>
    <div class="participant__questions-list fr-accordion js-fr-accordion">
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-1">Програма табору </p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-1"><span>
        Метод проекту / завдання Project-based learning and Task-based learning (TBL+PBL)<br />
        Ціннісний підхід від Несторівської групи Value-based approach from Nestor Group<br />
        Розвиток життєвих навичок Life Skills Development<br />
        Розвиток міжпредметних  зв’язків Content and Language Integrated Learning (CLIL)<br />
        Гейміфікація Gamification<br />
        Занурення в мовне середовище Language immersion<br />
        Принцип автономії дитини Child autonomy<br />
      </span></p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-2">Тренінг для вчителів</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-2"><span>
        В березні-квітні 2017 GoGlobal спільно з RELO з Американського посольства, Гете Інститутом та іншими парнерами проведе тренінг для вчителів пришкільних мовних таборів GoCamp, які будуть відповідати за складання навчальної частини програми.<br />
        Робоча мова тренінгів - англійська/німецька.
      </span></p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-3">Іноземний волонтер</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-3"><span>
        Для того, щоб табори пройшли не лише весело, але й ефективно і в дусі всебічного розвитку дитини, ми запропонуємо волонтерові обрати напрямок (STEAM, Citizenship, Career Exploration, Sports and Health). В квітні 2017 року школи та волонтери обміняються контактами. Потягом двох місяців до початку табору ви зможете спільно працювати над програмою, активностями та контентом.<br />
        Волонтери будуть представляти різні країни та національності і стануть культурними послами в Україні. У свою чергу ми матимемо унікальну можливість відкрити Україну світові, зламати стереотипи про нашу країну, поділитися своєю культурою з іноземними гостями. Тому обов’язковою умовою участі шкіл в проекті є забезпечення волонтерів проживанням в сім’ях (host families).
      </span></p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-4">Host family</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-4"><span>
        Проживання в сім’ї (host family) дає унікальні можливості як сім’ї, що  приймає волонтера, так і самому волонтерові.<br /><br />
        Переваги для школи:<br />
        дешево: не треба витрачати кошти школи або батьків<br /><br />
        Переваги для сім’ї:<br />
        можливість пізнати нову культуру<br />
        можливість знайти друзів за кордоном<br />
        можливість стати амбасадором української культури<br />
        можливість безкоштовно вчити мову<br /><br />
        Переваги для волонтера:<br />
        дешево: економія на проживанні та харчуванні<br />
        можливість поринути в українську культуру<br />
        можливість знайти нових друзів<br />
        можливість спробувати справжню домашню кухню
      </span></p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-5">Сфери відповідальності</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-5"><span>
        Директор ЗНЗ<br />
        - Забезпечення харчування<br />
        - медичного обслуговування<br />
        - доступу до обладнання та приміщень<br /><br />
        Директор табору<br />
        - Відповідає за розробку програми табору<br />
        - реалізацію програми табору<br /><br />
        Вчителі іноземної мови та інших предметів<br />
        - Відповідають за виконання навчальної частини програм<br /><br />
        Вожаті - старшокласники<br />
        - Відповідають за виконання розважальної частини програми<br /><br />
        Іноземні волонтери-вчителі<br />
        - Допомагають вчителям реалізувати навчальну частину програми, беруть активну учать в плануванні навчальної частини.<br /><br />
        Іноземні волонтери-вожаті<br />
        -Допомагають вожатим в реалізації розважальної частини програми, пасивна роль в плануванні навчальної частини
      </span></p>
    </div>
  </div>

  <div class="participant__gallery">
    <div class="participant__gallery-inner">
      <h2 class="participant__gallery-title participant__title">How it was in 2016</h2>
      <div class="participant__photos">
        <img src="/img/photo-2.jpg">
        <img src="/img/photo-2.jpg">
        <img src="/img/photo-2.jpg">
        <img src="/img/photo-2.jpg">
      </div>
    </div>
  </div>
</section> -->

<div class="container">
@endsection
