@extends('layouts.master')

@section('content')
<section class="school-reg">
  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/auth/login','files'=>true,'class'=>'school-reg__form form"']) !!}

  @if($errors->any())
  <h4>{{$errors->first()}}</h4>
  @endif

  <div class="form-group row">
    {!! Form::label('email', 'Email',['class'=>'col-third']); !!}
    {!! Form::text('email','',['class'=>'col'])!!}
  </div>
  <div class="form-group row">
    {!! Form::label('password', 'Password',['class'=>'col-third']); !!}
    {!! Form::password('password',['class'=>'col'])!!}
  </div>

  {!! Form::submit('Login!',['class'=>'form-submit btn btn--small','style'=>'margin-left:38%']); !!}

  {!! Form::close() !!}
</section>
@endsection
