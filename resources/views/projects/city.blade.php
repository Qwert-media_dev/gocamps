@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner">GoCamp City - пришкільні мовні табори</span>
</h1>
@endsection

@section('content')
<section class="project">
  <div class="project__desc">
    <p>Це новий начальний підхід, в якому наука, технології, інженерія, мистецтво і математика використовуються як відправний пункт до розвитку цікавості, діалогу та критичного мислення учнів. </p>
    <span>В результаті ми отримаємо учня, який:</span>
    <ul>
      <li>може ризикувати,</li>
      <li>бере активну учать в емпіричному навчанні (навчанні через  досвід),</li>
      <li>вирішує задачі,</li>
      <li>взаємодіє з іншими та</li>
      <li>задіяний в творчому процесі.</li>
    </ul>
  </div>
</section>
@endsection
