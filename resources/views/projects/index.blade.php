﻿@extends("$model->layout")
@section('header')
<h1 class="page-header__page-title page-header__page-title--projects">
  <span class="page-header__page-title-inner"><?=$model->title?></span>
</h1>
@endsection

@section('content')
{!! html_entity_decode($model->content) !!}
@endsection
