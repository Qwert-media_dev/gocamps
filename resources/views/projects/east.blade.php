@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner">GoCamp East</span>
</h1>
@endsection

@section('content')
<section class="project">
  <div class="project__desc">
    <p>Це новий формат позашкільної програми з поглибленим вивченням англійської мови. Після уроків діти зможуть взяти участь в реалізації захоплюючих проектів та завдань, спілкуючись англійською між собою та з іноземними волонтерами. За допомогою спеціально розробленої програми діти будуть навчатися працювати в команді, спілкуватися в багатокультурному середовищі, критично мислити та бути толерантними до інших.</p>
    <span>Місце проведення: to be determined</span>
    <br>
    <span>Тривалість програми: 2 тижні</span>
    <br>
    <span>Дати: __.06.2017 - __.07.2017</span>
  </div>

  <h2 class="project__types-title">Етапи проекту*</h2>
  <div class="project__type">
    <h3 class="project__type-title">Вересень 2016</h3>
    <div class="project__type-body">
      <p> Початок проекту, запуск конкурсу для шкіл, вебінар для шкіл </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Жовтень 2016</h3>
    <div class="project__type-body">
      <p> Початок відбору волонтерів </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Листопад 2016</h3>
    <div class="project__type-body">
      <p> Завершення конкурсу для шкіл</p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Грудень 2016</h3>
    <div class="project__type-body">
      <p>Рейтингування шкіл, розподіл за напрямками </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Січень 2017</h3>
    <div class="project__type-body">
      <p> Завершення відбору волонтерів </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Лютий 2017</h3>
    <div class="project__type-body">
      <p> Розподіл волонтерів по школам </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Березень 2017</h3>
    <div class="project__type-body">
      <p> Тренінги для вчителів та розробка програм </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Квітень 2017</h3>
    <div class="project__type-body">
      <p> Знайомство шкіл з волонтерами </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Травень 2017</h3>
    <div class="project__type-body">
      <p> Орієнтаційний тренінг для волонтерів </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Червень - Липень 2017 </h3>
    <div class="project__type-body">
      <p> Let’s GoCamp! </p>
    </div>
  </div>
  *Орієнтовні
</section>
@endsection
