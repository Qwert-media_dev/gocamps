@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner">GoCamp AfterSchool</span>
</h1>
@endsection

@section('content')
<section class="project">
  <div class="project__desc">
    <p>Це новий формат позашкільної програми з поглибленим вивченням англійської мови. Після уроків діти зможуть взяти участь в реалізації захоплюючих проектів та завдань, спілкуючись англійською між собою та з іноземними волонтерами. За допомогою спеціально розробленої програми діти будуть навчатися працювати в команді, спілкуватися в багатокультурному середовищі, критично мислити та бути толерантними до інших.</p>
    <span>Місце проведення: Київ та Київська область (опорні школи)</span>
    <br>
    <span>Тривалість пілотної програми: 5 тижнів (30 годин) Дати: 27.03.2017 - 28.04.2017</span>
    <br>
    <span>Дати: __.06.2017 - __.07.2017</span>
  </div>

  <h2 class="project__types-title">Етапи проекту*</h2>
  <div class="project__type">
    <h3 class="project__type-title">Вересень 2016</h3>
    <div class="project__type-body">
      <p>Початок проекту, запуск конкурсу для шкіл Жовтень 2016 </p>
      <p>Початок відбору волонтерів </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Листопад 2016</h3>
    <div class="project__type-body">
      <p> Завершення конкурсу для шкіл</p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Грудень 2016</h3>
    <div class="project__type-body">
      <p>Розподіл за напрямками, тренінг для шкіл 1, розробка програм Січень 2017 </p>
      <p>Завершення відбору волонтерів, тренінг для шкіл 2 </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Лютий 2017</h3>
    <div class="project__type-body">
      <p> Розробка програм, знайомство шкіл з волонтерами</p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Березень 2017</h3>
    <div class="project__type-body">
      <p> Орієнтаційний тренінг для волонтерів </p>
    </div>
  </div>
  <div class="project__type">
    <h3 class="project__type-title">Квітень 2017</h3>
    <div class="project__type-body">
      <p> Let’s GoCamp! </p>
    </div>
  </div>
  *Орієнтовні
</section>
@endsection
