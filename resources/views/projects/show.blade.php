@extends("$model->layout")

@section('header')
<h1 class="page-header__page-title page-header__page-title--projects">
  <span class="page-header__page-title-inner"><?=$model->title?></span>
</h1>
@endsection

@section('content')
<section class="model">
  <div class="model__desc">
    <?=$model->text?>
  </div>
  <?php if(isset($model->accordion )):?>
    <?=$model->accordion ?>
  <?php endif;?>

</section>
@endsection
