@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--projects">
  <span class="page-header__page-title-inner"><?=Lang::get('main.not_found', array(), LaravelLocalization::getCurrentLocale())?></span>
</h1>
@endsection

@section('content')
<section class="model">
  <div class="model__desc">
    404 <?=Lang::get('main.not_found', array(), LaravelLocalization::getCurrentLocale())?>
  </div>
</section>
@endsection
