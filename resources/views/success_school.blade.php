@extends('layouts.master')

@section('content')
<section class="registration-success">
  <h2 class="registration-success__title">
    Дякуємо за реєстрацію!
  </h2>
  <br />
  <p>
    Ваша школа успішно зареєстрована!
  </p>
  <p>
    У разі проходження першого етапу конкурсу ви отримаєте лист - підтвердження на вашу електрону адресу до 20 березня.
  </p>
  <p>
    З найкращими побажаннями, команда GoCamp!
  </p>
</section>

<section class="partners">
  <img src="/img/partners.svg" alt="our partners">
</section>
@endsection
