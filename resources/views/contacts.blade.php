@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title page-header__page-title--orange">
  <span class="page-header__page-title-inner"><?=$page->name?></span>
</h1>
@endsection

@section('content')
{!! html_entity_decode($page->content) !!}
@endsection
