@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/chosen.css">
@endsection

@section('header')
<div class="">
  <h1 class="page-header__page-title"><span class="page-header__page-title-inner">Apply as a volunteer</span></h1>
</div>
@endsection

@section('content')
<section class="school-reg new-form">

  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/volunteer/short-create', 'files' => true, 'class' => 'school-reg__form form', 'name' => 'volunteer-form-new']) !!}


  @if($errors->all())
    <ul style="text-align:center;list-style:none;border:1px solid red;border-radius:5px;padding:20px;">
      @foreach ($errors->all() as $error)
        <li style="color:red;font-size:20px;">{{ $error }}</li>
      @endforeach
    </ul>
  @endif

  <div class="form-row">
    <div class="form-new-col col-2-3">
      <h3>First time on GoCamps?</h3>
      <p style="color: red;">This short application form is ONLY for candidates, who has already applied for GoCamp before</p>
    </div>
    <div class="form-new-col col-1-3">
      <a href="{{ route('volunteer-create') }}" class="new-form-btn full-reg">Pass full Resgistration</a>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      {!! Form::label('first_name', 'First Name *'); !!}
      {!! Form::text('first_name','', ['data-required'=>'true'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('last_name', 'Last Name *'); !!}
      {!! Form::text('last_name','', ['data-required'=>'true'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('gender', 'Gender *'); !!}
      <div class="radio-group">
        <div class="radio-group-item">
          {!! Form::radio('gender', 0, 1, ['id'=>'gender-0', 'data-required'=>'true']); !!}
          {!! Form::label('gender-0', 'Male'); !!}
        </div>
        <div class="radio-group-item">
          {!! Form::radio('gender', 1, 0, ['id'=>'gender-1', 'data-required'=>'true']); !!}
          {!! Form::label('gender-1', 'Female'); !!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      {!! Form::label('citizenship', 'Citizenship *'); !!}
      {!! Form::select('citizenship', $countries, '', ['data-required'=>'true','class'=>'chosen-autocomplete'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('country', 'Country of current residence *'); !!}
      {!! Form::select('country', $countries, '', ['data-required'=>'true','class'=>'chosen-autocomplete'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('city', 'City *'); !!}
      {!! Form::text('city','', ['data-required'=>'true'])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('passport', 'Passport number / ID *'); !!}
      {!! Form::text('passport','', ['data-required'=>'true'])!!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('birthday', 'Birthday *'); !!}
      {!! Form::text('birthday','',['class'=>'datepicker', 'data-required'=>'true'])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('phone', 'Primary phone number (without spaces) *'); !!}
      <div class="form-row">
        <div class="form-new-col col-1-3">
          {!! Form::select('phone_code', $phones, '', ['data-required'=>'true','class'=>'chosen-autocomplete'])!!}
        </div>
        <div class="form-new-col col-2-3">
          {!! Form::text('phone', '', ['data-required'=>'true'])!!}
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('alt_phone', 'Alternative phone number (without spaces)'); !!}
      <div class="form-row">
        <div class="form-new-col col-1-3">
          {!! Form::select('alt_phone_code', $phones, '',['class'=>'chosen-autocomplete'])!!}
        </div>
        <div class="form-new-col col-2-3">
          {!! Form::text('alt_phone', '')!!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('email', 'Email *'); !!}
      {!! Form::text('email', '', ['data-required'=>'true'])!!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('skype', 'Skype *'); !!}
      {!! Form::text('skype','', ['data-required' => 'true'])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('sessions', 'Please, select the session(s) you want to apply for *'); !!}
      <div class="checkbox-group">
        @foreach($session as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('session[]', $key, false, ['id'=>'session-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('session-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-2-3">
      <div class="form-group">
        <div class="checkbox-simple">
          {!! Form::checkbox('agree', 1, false, ['id'=>'agree', 'data-required'=>'true']); !!}
          {!! Form::label('agree', 'I agree to the processing of personal data'); !!}
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::submit('Apply as a volunteer',['class'=>'new-form-btn']); !!}
      </div>
    </div>
  </div>
  {!! Form::close() !!}
</section>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/jquery.maskedinput.min.js"></script>
<script src="/chosen.jquery.min.js"></script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
      dateFormat: 'dd/mm/yy',
    });
    $('.new-form form').on('submit', function(e){
      checkFields(e);
    });
    $(".chosen-autocomplete").chosen({
      placeholder_text_single: ' '
    });
    $("select:not(.chosen-autocomplete)").chosen({
      placeholder_text_single: ' ',
      disable_search: true
    });
  } );

  $(".phone-mask").mask("+380 99 999 99 99");

  function checkFields(event){
    $('[data-required="true"]').each(function(){
      var $this = $(this);
      if($this.is(':text') || $this.is('textarea')){
        if(!$this.val().length){
          $this.addClass('error');
        }else{
          if($this.attr('id') == 'email'){
            if(validateEmail($this.val())){
              $this.removeClass('error');
            }else{
              $this.addClass('error');
            }
          }else{
            $this.removeClass('error');
          }
        }
      }else if($this.is('select')){
        if($this.val() == 0){
          $this.addClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').addClass('error');
        }else{
          $this.removeClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').removeClass('error');
        }
      }else if($this.is(':radio')){
        var name = $this.attr("name");
        var checked = $(":radio[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":radio[name='"+name+"']").closest('.radio-group').addClass('error');
        }else{
          $(":radio[name='"+name+"']").closest('.radio-group').removeClass('error');
        }
      }else if($this.is(':checkbox')){
        var name = $this.attr("name");
        var checked = $(":checkbox[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":checkbox[name='"+name+"']").parent().addClass('error');
        }else{
          $(":checkbox[name='"+name+"']").parent().removeClass('error');
        }
      }
    });

    if($('.new-form .error').length != 0){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $('.new-form .error:first').closest('.form-row').offset().top
      }, 1000);
    }
  }
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)*(\+[a-z0-9-]+)?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  };
</script>
<script>
  document.getElementById('session-2').disabled = false;
  document.getElementById('session-3').disabled = false;
</script>
@endsection
