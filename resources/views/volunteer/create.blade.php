@extends('layouts.master')

@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/chosen.css">
@endsection

@section('header')
<div class="">
  <h1 class="page-header__page-title"><span class="page-header__page-title-inner">Apply as a volunteer</span></h1>
</div>
@endsection

@section('content')
<section class="school-reg new-form">

  {!! Form::open(['url' => '/'.LaravelLocalization::getCurrentLocale().'/volunteer/create', 'files' => true, 'class' => 'school-reg__form form', 'name' => 'volunteer-form-new']) !!}


  @if($errors->all())
    <ul style="text-align:center;list-style:none;border:1px solid red;border-radius:5px;padding:20px;">
      @foreach ($errors->all() as $error)
        <li style="color:red;font-size:20px;">{{ $error }}</li>
      @endforeach
    </ul>
  @endif

  <div class="form-row centered">
    <div class="form-new-col col-2-3">
      <h3>Already registered on GoCamps?</h3>
    </div>
    <div class="form-new-col col-1-3">
      <a href="{{ route('volunteer-short-create') }}" class="new-form-btn full-reg">Pass short registration</a>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Personal information</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      {!! Form::label('first_name', 'First Name *'); !!}
      {!! Form::text('first_name','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('last_name', 'Last Name *'); !!}
      {!! Form::text('last_name','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('gender', 'Gender *'); !!}
      <div class="radio-group">
        <div class="radio-group-item">
          {!! Form::radio('gender', 0, 1, ['id'=>'gender-0', 'data-required'=>'true']); !!}
          {!! Form::label('gender-0', 'Male'); !!}
        </div>
        <div class="radio-group-item">
          {!! Form::radio('gender', 1, 0, ['id'=>'gender-1', 'data-required'=>'true']); !!}
          {!! Form::label('gender-1', 'Female'); !!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      {!! Form::label('citizenship', 'Citizenship *'); !!}
      {!! Form::select('citizenship', $countries, '', ['data-required'=>'true', 'class'=>'chosen-autocomplete'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('country', 'Country of current residence *'); !!}
      {!! Form::select('country', $countries, '', ['data-required'=>'true', 'class'=>'chosen-autocomplete'])!!}
    </div>
    <div class="form-new-col col-1-3">
      {!! Form::label('city', 'City *'); !!}
      {!! Form::text('city','', ['data-required'=>'true'])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('passport', 'Passport number / ID *'); !!}
      {!! Form::text('passport','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('birthday', 'Birthday *'); !!}
      {!! Form::text('birthday','',['class'=>'datepicker', 'data-required'=>'true']) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('phone', 'Primary phone number (without spaces) *'); !!}
      <div class="form-row">
        <div class="form-new-col col-1-3">
          {!! Form::select('phone_code', $phones, '', ['data-required'=>'true', 'class'=>'chosen-autocomplete'])!!}
        </div>
        <div class="form-new-col col-2-3">
          {!! Form::text('phone', '', ['data-required'=>'true'])!!}
        </div>
      </div>
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('alt_phone', 'Alternative phone number (without spaces)'); !!}
      <div class="form-row">
        <div class="form-new-col col-1-3">
          {!! Form::select('alt_phone_code', $phones, '', ['class'=>'chosen-autocomplete'])!!}
        </div>
        <div class="form-new-col col-2-3">
          {!! Form::text('alt_phone', '')!!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('email', 'Email *'); !!}
      {!! Form::text('email','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('skype', 'Skype *'); !!}
      {!! Form::text('skype','', ['data-required' => 'true'])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h4>Emergency contact</h4>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('emergency_name', 'Person`s name *'); !!}
      {!! Form::text('emergency_name','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('emergency_phone', 'Phone number (without spaces) *'); !!}
      <div class="form-row">
        <div class="form-new-col col-1-3">
          {!! Form::select('emergency_phone_code', $phones, '', ['data-required'=>'true', 'class'=>'chosen-autocomplete']) !!}
        </div>
        <div class="form-new-col col-2-3">
          {!! Form::text('emergency_phone','', ['data-required'=>'true']) !!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Social networks</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('facebook', 'Facebook'); !!}
      {!! Form::text('facebook','')!!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('twitter', 'Twitter'); !!}
      {!! Form::text('twitter','')!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('google', 'Google +'); !!}
      {!! Form::text('google','')!!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('add_link', 'Any additional link'); !!}
      {!! Form::text('add_link','')!!}
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Education</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('eng_level', 'What is your level of knowledge of English? *'); !!}
      <div class="radio-group">
        @foreach($skill as $key => $value)
          <div class="radio-group-item">
            {!! Form::radio('eng_level', $key, ($key == 1), ['id'=>'eng_level-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('eng_level-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('other_langs', 'Other languages you know from the list *'); !!}
      {!! Form::select('other_langs', $other_langs, '', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('education', 'Institution, field of study, degree/graduation year *'); !!}
      {!! Form::text('education','', ['data-required'=>'true']) !!}
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Work</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-2">
      {!! Form::label('workplace', 'Field of activity *'); !!}
      {!! Form::text('workplace','', ['data-required'=>'true']) !!}
    </div>
    <div class="form-new-col col-1-2">
      {!! Form::label('workplace_position', 'Current position in your workplace *'); !!}
      {!! Form::text('workplace_position','', ['data-required'=>'true']) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('work_describe', 'Describe any relevant work, volunteering, or personal experience that would be useful for volunteering work at GoCamp *'); !!}
      {!! Form::textArea('work_describe','', ['data-required'=>'true', 'data-maxwords'=>200]) !!}
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>Volunteering</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('about', 'Please, tell us more about yourself, including hobbies and interests *'); !!}
      {!! Form::textArea('about','', ['data-required'=>'true', 'data-maxwords'=>200]) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('why', 'Why would you like to volunteer at GoCamp? *'); !!}
      {!! Form::textArea('why','', ['data-required'=>'true', 'data-maxwords'=>200]) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('facts', 'Write 3 interesting facts about yourself *'); !!}
      {!! Form::textArea('facts','', ['data-required'=>'true', 'data-maxwords'=>200]) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('hear', 'How did you hear about GoCamp? *'); !!}
      {!! Form::select('hear', $hear, '', ['data-required'=>'true']); !!}
    </div>
  </div>
  <div class="form-row hear_other" style="display:none;">
    <div class="form-new-col col-3">
      {!! Form::label('hear_other', 'Please, write your answer – “How did you hear about GoCamp?”'); !!}
      {!! Form::text('hear_other','') !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('sessions', 'Please, select the session(s) you want to apply for *'); !!}
      <div class="checkbox-group">
        @foreach($session as $key => $value)
          <div class="checkbox-group-item">
            {!! Form::checkbox('session[]', $key, false, ['id'=>'session-' . $key, 'data-required'=>'true']); !!}
            {!! Form::label('session-' . $key, $value); !!}
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <h2>General questions</h2>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('dietary', 'Food restrictions (tell us if you’re vegan, for example)'); !!}
      {!! Form::select('dietary', $dietary, '')!!}
    </div>
  </div>
  <div class="form-row dietary_other" style="display:none;">
    <div class="form-new-col col-3">
      {!! Form::label('dietary_other', 'Please, write your answer – “Food restrictions”'); !!}
      {!! Form::text('dietary_other','') !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('been_in_ukraine', 'Have you been to Ukraine before? *'); !!}
      <div class="radio-group">
        <div class="radio-group-item">
          {!! Form::radio('been_in_ukraine', '1', 1, ['id'=>'been_in_ukraine_1', 'data-required'=>'true']); !!}
          {!! Form::label('been_in_ukraine_1', 'Yes, I have'); !!}
        </div>
        <div class="radio-group-item">
          {!! Form::radio('been_in_ukraine', '0', 0, ['id'=>'been_in_ukraine_0', 'data-required'=>'true']); !!}
          {!! Form::label('been_in_ukraine_0', 'No, I haven’t'); !!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row" id="capacity">
    <div class="form-new-col col-3">
      {!! Form::label('capacity', 'In what capacity?'); !!}
      {!! Form::textArea('capacity','', ['data-maxwords'=>200]) !!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      {!! Form::label('special', 'Do you have any special needs that we should be informed about?'); !!}
      {!! Form::textArea('special','', ['data-maxwords'=>200])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <label for="preferences">Do you have any preferences for the location of the camp site? <span>We do not guarantee the location you point out. The earlier you submit the application the more chances we will have to satisfy your preference</span></label>
      {!! Form::textArea('preferences','', ['data-maxwords'=>200])!!}
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3">
      <div class="form-group">
        {!! Form::label('intrested', 'Are you interested in writing a blog about your experience in&nbsp;Ukraine? *'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('interested', '1', 1, ['id'=>'interested_1', 'data-required'=>'true']); !!}
            {!! Form::label('interested_1', 'Yes, I am'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('interested', '0', 0, ['id'=>'interested_0', 'data-required'=>'true']); !!}
            {!! Form::label('interested_0', 'No, I’m not'); !!}
          </div>
        </div>
      </div>
    </div>
    <div class="form-new-col col-2-3">
      <div class="form-group">
        {!! Form::label('shootings', 'During your volunteering, there might be photo- and video-shootings. Do you agree to be filmed during the GoCamp activities?&nbsp;*'); !!}
        <div class="radio-group">
          <div class="radio-group-item">
            {!! Form::radio('shootings', '1', 1, ['id'=>'shootings_1', 'data-required'=>'true']); !!}
            {!! Form::label('shootings_1', 'I agree'); !!}
          </div>
          <div class="radio-group-item">
            {!! Form::radio('shootings', '0', 0, ['id'=>'shootings_0', 'data-required'=>'true']); !!}
            {!! Form::label('shootings_0', 'I disagree'); !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <hr>
  </div>
  <div class="form-row">
    <div class="form-new-col col-3">
      <div class="form-group">
        <div class="checkbox-simple smaller">
          {!! Form::checkbox( 'agree', 1, false, ['id'=>'agree', 'data-required'=>'true']); !!}
          {!! Form::label('agree', 'I confirm that NGO "Global Office" will collect, process and storage my personal data with the aim of keeping the database “The volunteers" of NGO "Global Office" and to prepare statistical and administrative information'); !!}
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-new-col col-1-3 push-1-3">
      <div class="form-group">
        {!! Form::submit('Apply as a volunteer',['class'=>'new-form-btn']); !!}
      </div>
    </div>
  </div>
  {!! Form::close() !!}
</section>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/jquery.maskedinput.min.js"></script>
<script src="/chosen.jquery.min.js"></script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
      dateFormat: 'dd/mm/yy',
    });
    $('.new-form form').on('submit', function(e){
      console.log('ok');
      checkFields(e);
    });
    $(".chosen-autocomplete").chosen({
      placeholder_text_single: ' '
    });
    $("select:not(.chosen-autocomplete)").chosen({
      placeholder_text_single: ' ',
      disable_search: true
    });
  } );

  $(".phone-mask").mask("+380 99 999 99 99");

  function checkFields(event){
    $('[data-required="true"]').each(function(){
      var $this = $(this);
      if($this.is(':text') || $this.is('textarea')){
        if(!$this.val().length){
          $this.addClass('error');
        }else{
          if($this.attr('id') == 'email'){
            if(validateEmail($this.val())){
              $this.removeClass('error');
            }else{
              $this.addClass('error');
            }
          }else{
            $this.removeClass('error');
          }
        }
      }else if($this.is('select')){
        if($this.val() == 0){
          $this.addClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').addClass('error');
        }else{
          $this.removeClass('error');
          $this.siblings('.chosen-container').find('.chosen-single').removeClass('error');
        }
      }else if($this.is(':radio')){
        var name = $this.attr("name");
        var checked = $(":radio[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":radio[name='"+name+"']").closest('.radio-group').addClass('error');
        }else{
          $(":radio[name='"+name+"']").closest('.radio-group').removeClass('error');
        }
      }else if($this.is(':checkbox')){
        var name = $this.attr("name");
        var checked = $(":checkbox[name='"+name+"']:checked");
        if(checked.length == 0) {
          $(":checkbox[name='"+name+"']").parent().addClass('error');
        }else{
          $(":checkbox[name='"+name+"']").parent().removeClass('error');
        }
      }
    });

    if($('.new-form .error').length != 0){
      event.preventDefault();
      $('html, body').animate({
        scrollTop: $('.new-form .error:first').closest('.form-row').offset().top
      }, 1000);
    }
  }
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)*(\+[a-z0-9-]+)?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  };

  $(document).on('change','[name="hear"]',function(){
    if($(this).find('option:last').val() == $(this).val()){
      $('.hear_other').show();
    }else{
      $('.hear_other').hide();
    }
  });
  $(document).on('change','[name="dietary"]',function(){
    if($(this).find('option:last').val() == $(this).val()){
      $('.dietary_other').show();
    }else{
      $('.dietary_other').hide();
    }
  });

  $(document).on('click','[name="been_in_ukraine"]',function(){
    if($(this).val() == 1){
      $('#capacity').show();
    }else{
      $('#capacity').hide();
    }

  });
</script>
<script>
  /*document.getElementById('session-2').disabled = false;
  document.getElementById('session-3').disabled = false;*/
</script>
@endsection
