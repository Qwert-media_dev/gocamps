@extends("$model->layout")

@section('header')
<h1 class="page-header__page-title page-header__page-title--volunteer">
  <span class="page-header__page-title-inner">{!! $model->title !!}</span>
</h1>
@endsection

@section('content')
</div><!-- .container -->
{!! html_entity_decode($model->content) !!}
<!-- <section class="participant">
  <div class="participant__intro">
    <p> Do you have or would like to get volunteer experience?</p>

    <p>Ukraine is calling for YOU to participate in a new wave of English summer camps for children in 2017, including those affected by the armed conflict in Eastern Ukraine.</p>
<p>
We are looking for English-speaking foreigners who would come to Ukraine and interact with the children in English, get great time, new friends and unforgettable experience!
</p><p>
You will get special trainings and opportunity to share experience in following spheres: STEAM (Science, Technology, Engineering, Art, Mathematics), Leadership and Career, Citizenship, Sports&Health!
</p><p>
Consider this great opportunity to volunteer abroad and discover Ukraine! Let’s change Ukraine together and make children`s dreams come true!
</p>
<p><b>You may choose one of the following option: </b><br />
May 29 - June 16, 2017 — 3 week GoCamps all over Ukraine (with free weekends and orientation training on May 26-28.<br />
March 27 - April 28, 2017 — 5 week GoCamp AfterSchool pilot project in Kyiv and Kyiv region (with free weekends and orientation training on March 25-26).</p>


    <a class="btn participant__join" href="{{ route('volunteer-create') }}">Apply now!</a>
  </div>

  <center>
    <iframe width="950" height="534" src="https://www.youtube.com/embed/g7xM-0j4PzQ" frameborder="0" allowfullscreen></iframe>
  </center>

  <div class="participant__benefits">
    <h2 class="participant__title">What you get as volunteer</h2>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-truck"></span>
        <p class="participant__benefit-text">transfer from the Kyiv airport and orientation training after arrival to Kyiv</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-question"></span>
        <p class="participant__benefit-text">pre-departure support by email</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-info"></span>
        <p class="participant__benefit-text">visa support</p>
      </div>
    </div>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-home3"></span>
        <p class="participant__benefit-text">accommodations</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-spoon-knife"></span>
        <p class="participant__benefit-text">partial meals</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-ticket"></span>
        <p class="participant__benefit-text">travel to your host community and back</p>
      </div>
    </div>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-mobile"></span>
        <p class="participant__benefit-text">SIM-cards for local calls and international sms</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-user-tie"></span>
        <p class="participant__benefit-text">support from office staff and a hotline 24/7</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-profile"></span>
        <p class="participant__benefit-text">certificate and recommendations </p>
      </div>
    </div>
    <div class="participant__benefits-list row">
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-happy"></span>
        <p class="participant__benefit-text">new friends</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-spell-check"></span>
        <p class="participant__benefit-text">educating young generation</p>
      </div>
      <div class="participant__benefit col-third">
        <span class="our-goal__icon icon icon-earth"></span>
        <p class="participant__benefit-text">discovering Ukraine</p>
      </div>
    </div>
  </div>

  <div class="participant__requirements">
    <div class="container">
      <h2 class="participant__requirements-title participant__title">Our volunteer</h2>
      <div class="participant__requirements-list row">
        <div class="participant__requirement col col-third">
          <img class="participant__req-ico" src="/img/icon_chart_small.png">
          <span class="participant__req-text">is at least 18 years old</span>
        </div>
        <div class="participant__requirement col col-third">
          <img class="participant__req-ico" src="/img/icon_chart_small.png">
          <span class="participant__req-text">speaks English fluently</span>
        </div>
        <div class="participant__requirement col col-third">
          <img class="participant__req-ico" src="/img/icon_chart_small.png">
          <span class="participant__req-text">enthusiastic to work with kids</span>
        </div>
      </div>
      <div class="participant__requirements-list row">
        <div class="participant__requirement col col-third">
          <img class="participant__req-ico" src="/img/icon_chart_small.png">
          <span class="participant__req-text">has interest or experience in the spheres of STEAM (Science, Technology, Engineering, Art, Mathematics), Citizenship, Leadership and Career Exploration, Sports and Health;
          </span>
        </div>
        <div class="participant__requirement col col-third">
          <img class="participant__req-ico" src="/img/icon_chart_small.png">
          <span class="participant__req-text">willing to make impact in Ukraine</span>
        </div>
      </div>
    </div>
  </div>

  <div class="participant__steps">
    <div class="container">
      <h2 class="participant__steps-title participant__title">Steps of application</h2>
      <ul class="participant__steps-list">
        <li>Fill out the online application form</li>
        <li>Skype interview with our manager</li>
        <li>Submit a full package of documents (the list of documents will be sent)</li>
        <li>Sign of Agreement online</li>
        <li><b>Pay the participation fee — 75$</b></li>
        <li>Get introduced to the local school teacher and host family</li>
        <li>Receive a full pre-departure information package.  </li>
      </ul>
      <div class="participant__steps-desc">
        <p>To complete the registration process you need to send us following documents:</p>
        <ul>
          <li>employment history (CV)</li>
          <li>copy of valid ID</li>
          <li>copy of residence permit or equivalent (if you live in a country other than the country of current nationality)</li>
          <li>health checks</li>
          <li>criminal record check</li>
          <li>international medical insurance</li>
          <li>copy of tickets</li>
        </ul>
        <p>All confirmed volunteers should do an online child protection course from British Council. The link will be sent after confirmation.</p>
        <p>Applications will be reviewed on a rolling basis. If we determine that you are a good match for our program, we will invite you for a Skype interview. <br> To apply, submit the application form by January 31, 2017.  The earlier you submit your application the more options you will have for your camp site preference. <br> The registration will be available starting October 10, 2016 on our website.</p>
      </div>
    </div>
  </div>

  <div class="participant__questions">
    <h2 class="participant__questions-title participant__title">Q&A</h2>
    <div class="participant__questions-list fr-accordion js-fr-accordion">
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-1">What activities should I do in camp?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-1"><span>
        As a Camp volunteer you are expected to act in a professional manner to provide our kids a quality and memorable stay while they are at camp. Along with the training, we encourage our volunteers to engage in activities they enjoy and can teach children more about, as well as the various cultures you come from according to one of four camp strand that you choose: STEAM (Science, Technology, Engineering, Art, Mathematics), Citizenship, Leadership and Career Exploration, Sports and Health.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-2">What are the camp strands?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-2"><span>
        GoCamp 2017 programs will be developed according to four strands:<br />
        STEAM is an educational approach to learning that uses Science, Technology, Engineering, the Arts and Mathematics as access points for guiding student inquiry, dialogue, and critical thinking. Students and teachers engaged in STEAM make more real-life connections so that school is not a place where you go to learn but instead becomes the entire experience of learning itself. <br /><br />
        Citizenship<br />
        According to UNESCO, citizenship education describes courses that educate people on recognizing, analyzing and investigating issues in their communities. These courses foster active citizenship in order to promote sustainable societies.These three objectives correspond both to educating the individual as a subject of ethics and law, and to educating citizens. These objectives suggest four major themes for citizenship education:<br />
        The relations between individuals and society: individual and collective freedoms, and rejection of any kind of discrimination.<br />
        The relations between citizens and the government: what is involved in democracy and the organization of the state.<br />
        The relations between the citizen and democratic life.<br />
        The responsibility of the individual and the citizen in the international community.
        <br /><br />
        Leadership and Career Exploration<br />
        Career exploration allows students to dig deeper into learning about careers in local and regional labour markets. Experiences are tailored to each student’s interests, which makes them feel involved and better informed for making future career and education choices. Adding leadership development component will make a program that facilitates the growth of young men and women who desire to be challenged in their transition to adulthood, rather than lingering in a continuation of childhood.<br /><br />
        Sports and Health<br />
        Health education builds students' knowledge, skills, and positive attitudes about health. Health education teaches about physical, mental, emotional and social health. It motivates students to improve and maintain their health, prevent disease, and reduce risky behaviors. Health education curricula and instruction help students learn skills they will use to make healthy choices throughout their lifetime. <br />Effective curricula result in positive changes in behavior that lower student risks around: alcohol, tobacco, and other drugs, injury prevention, mental and emotional health, nutrition, physical activity, prevention of diseases and sexuality and family life.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-3">What age are the children at camps?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-3"><span>
        School children aged 10 to 15. Camp groups will be age and English language ability mixed, and these may vary from school to school. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-4">Where will I go and for how long?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-4"><span>
        All regions of Ukraine except for Donetsk and Luhansk regions are covered. You will be sent to the region you showed interest in, however, no area is guaranteed due to the large number of volunteers. <br /><br />
        There are three camp formats: <br />
        - May  29 - June 16, 2017- 3 week GoCamps all over Ukraine (with free weekends and orientation training on May 26-28. <br />
        - March 27 - April 28, 2017 - 5 week GoCamp AfterSchool pilot project in Kyiv and Kyiv region  (with free weekends and orientation training on March 25-26).<br />
        - June-July 2017 - 2 week GoCamp East is a new format of overnight camp with intensive study of English for children from frontline areas of the Donetsk and Luhansk regions.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-5">What will be provided during the orientation training?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-5"><span>
        May 26-28, 2017 - GoCamp City -  Orientation training for volunteers <br />
        March 25-26, 2017 - GoCamp Afterschool - Orientation training for volunteers<br />
        Training will occur in Kyiv or Kyiv region. You will participate in training regarding the scope of your work – lessons planning, working with students, and delivering classes, you will have time and space to create activities for students, using advanced educational methodologies etc.<br />
        The orientation training will include:<Br />
        - a session on child protection policy<Br />
        - a crash course of the Ukrainian language<br />
        - practical advice on behavior and classroom management<br />
        - practical fun camp activities<br />
        - strand-related educational activities.<br /><br />
        GoGlobal provides:<br />
        transfer from the Kyiv airport<br />
        accommodations <br />
        meals.<br /></span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-6">When is the application deadline?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-6"><span>
        The application for GoCamp 2017 is January 31, 2017. The earlier you submit the application the more chances we will have to satisfy your preference of camp location. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-7">Who is the typical GoCamp volunteer?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-7"><span>
        GoGlobal volunteer should be at least 18 years old; speak English fluently; enthusiastic to work with kids; has interest or experience in the spheres of STEAM (Science, Technology, Engineering, Art, Mathematics), Citizenship, Leadership and Career Exploration, Sports and Health; willing to make impact in Ukraine.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-8">My country is not traditionally an English speaking country, but I am a near-native speaker; can I still apply?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-8"><span>
        Yes, we have had many successful volunteers who are near-native speakers. Applicants who are not native English speakers must demonstrate fluency and error-free speech in a Skype interview.</span>
      </p>
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-9">How are applications evaluated?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-9"><span>
        Applications are evaluated based on a number of criteria that are indicative of a candidate’s potential for successful participation in the program:<br />
        - English-language skills (applicants must demonstrate at least upper-intermediate level of English)<br />
        Experience working with children<br />
        Ability to work in a way that promotes the safety and well‐being of children<br />
        Skills of effective communication and engagement with children<br />
        General motivation. <br /></span>
      </p>
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-10">I applied to GoCamp in the past. Can I apply again?</p>

      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-10"><span>
        Past applicants who were not accepted to the program are welcome to reapply. If you have applied before and want to reapply in the future, you will have to submit an entirely new application, however, as we cannot transfer over materials from past applications. </span>
      </p>
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-11">I have already participated in GoCamp. Can I do the program again?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-11"><span>
        If you have participated before and want to do the program again, you will have to submit an entirely new application, however, as we cannot transfer over materials from past applications. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-12">What is the GoCamp application like? </p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-12"><span>
        Steps of application: <br />
        1) Fill out the online application form<br />
        2) Skype interview with our manager. The interviews usually last between 20-40 minutes<br />
        3) Submit a full package of documents (the list of documents will be sent)<br />
        4) Sign of Agreement online<br />
        5) Pay the participation fee<br />
        6) Get introduced to the local school teacher and host family. <br />
      </span></p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-13">When will I know if I have been accepted? What happens after the interview?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-13"><span>We will be in touch with you about your status within two weeks of your interview. If you are accepted into the program, we will email you information about confirming your participation and deadlines for the submission of required documents.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-14">Can couples do this program?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-14"><span>Couples are welcome to apply to the program. Each person must submit the application except of children under 18.  If both people are accepted to the program, we will take couple status into account when making placement decisions.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-15">Is there a program fee?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-15"><span>There is a $75 participation fee in the program. Applicants pay the fee after the confirmation of participation in the program. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-16">Does the program pay for my plane tickets?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-16"><span>The program does NOT cover airfare to and from Ukraine.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-17"> What other costs are associated with the program?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-17"><span>Volunteers are responsible for covering the cost of: airfare to and from Ukraine, obtaining their visas (transportation to and from consulates, doctor’s appointments, Criminal Background Checks, etc.), international medical insurance and bringing sufficient funds for their own personal needs (travel, leisure, etc.)</span>
      </p>
      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-18"> Should I fly to Kiev or somewhere closer to my camp?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-18"><span>The training week will occur around Kyiv and we encourage all of our volunteers to attend the training. After the training, transportation will be provided to the camp. However, knowing that plane tickets can be expensive, if the cost of a ticket to somewhere closer to your camp is cheaper it will not affect your ability to be a volunteer.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-19">Will I need a Visa?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-19"><span>Citizens of the United States, European Union, Canada, Australia, and New Zealand do not require visas among other countries. To inquire if Ukraine requires you to need a Visa please check with the Ukraine embassy in your country or at the Ministry of Foreign Affairs of Ukraine. http://mfa.gov.ua/en/consular-affairs/entering-ukraine/visa-requirements-for-foreigners
        GoGlobal support office will assist volunteers with this process, but it is ultimately the responsibility of the volunteer to contact their consulate and to gather and submit their paperwork.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-20">How will I arrive at my camp?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-20"><span>GoGlobal provides transportation for our volunteers free of charge to the nearest to the camp railway station after the orientation training in Kyiv. Local school representatives are expected to pick the volunteers up at the station and bring to the camp site.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-21">Who will I be working with at the camp?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-21"><span>Our volunteers will not be the only teachers at the camps. Camp staff comprises of English and other subject teachers from the participating schools. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-22">What is the time commitment?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-22"><span>The time and commitment required varies depending on the role and the needs of the local Camp. Generally, we require volunteers to be available for around 5 hours during the day, 5 days a week (Mon to Fri), and weekends are free. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-23">Are weekends free?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-23"><span>Saturdays and Sundays are free. During the weekends, you may travel and site-see. GoGlobal managers will be happy to provide advice and assistance. It is also possible to stay at the camp if you do not wish to visit nearby cities. Volunteers are welcome to explore Ukraine before or after the program at their own cost.</span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-24">What is meant by “food (partly) is provided”?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-24"><span>At most camps breakfast and lunch will be provided by the school. Dinner meals are provided at some, but not all, camps. If this is an issue, please contact your GoGlobal representative. </span>
      </p>

      <p class="participant__question-header fr-accordion__header js-fr-accordion__header" id="accordion-header-25">How much spending money should I bring?</p>
      <p class="participant__question-panel fr-accordion__panel js-fr-accordion__panel" id="accordion-panel-25"><span>The amount of spending money you should bring depends on how much you think you will spend while in Ukraine. The current exchange rate can be found here (http://coinmill.com/UAH_calculator.html#UAH). While keeping in mind that volunteers near smaller villages will spend less money during the week relative to volunteers in larger cities, GoGlobal recommends at least $100.00 (USD) for each week you will be in Ukraine.</span>
      </p>
-->
<!--26) What materials should I bring for my class?
Students love stamps, stickers, and other little rewards that you can give them during class. Stickers with English phrases (Well done! Fantastic job!) are relatively inexpensive, easy to pack, and motivating for the students. Books and magazines are also good options.Some number of supplies (like  paper, stationery, watercolors,footballs etc) will be provided by local schools. You are welcome to bring your laptop and any other supplies you may find useful but it is not required.
27) I don’t know Ukrainian or Russian! Will that be an issue?
Volunteers should only speak English, even if they know the local language. This is to foster an “English only” environment to encourage children to practice their language skills.
28) How safe is the area I will be teaching at?
If you got this far, then you are probably aware of the recent events that have troubled Ukraine. We will state this from the very beginning that the camp and you are in a safe environment, far away from any danger and isolated from all the political and social turmoil. Ukraine is at war with Russia in the eastern region of Donetsk and Luhansk, Crimean peninsula is occupied by Russia (marked pink on map). The rest of Ukraine is generally safe, everyday life goes normally. Our volunteers safety and security is our priority, so no volunteers are sent to the conflict areas.
29) Can I choose where I get placed in Ukraine?
The GoCamp application includes a section where you can list your general regional placement preferences. Due to the number of requests and limited availability in certain regions, however, we are simply unable to accommodate all applicants’ requests.The earlier you submit the application the more chances we will have to satisfy your preference.
30) How likely is it that I’ll get my preferred placement city?
It is not likely that volunteers are placed in specific cities that they request in their application. We get many requests for cities such as Kyiv, Lviv, Odessa and other such “tourist-friendly” areas. However, we have very limited placements in these areas compared to the amount of volunteers that request them. While we can’t guarantee to accommodate specific requests, we are often able to place volunteers in the type of placement that they would prefer. For this reason, we suggest that you research all of Ukraine and think about what kind of placement you would work best in. (Would you prefer to be in a rural area or urban area? In a cooler, forested climate? Or in south regions?). In all cases, we ask for your flexibility and understanding – we can’t guarantee anything. You will be placed where you are most needed.
31) Can I choose my accommodation?
Generally, GoCamp organizes the accommodation for volunteers, according to what is possible at the site. Volunteers will be hosted by local families.
32) Will GoGlobal pay for my accommodations if I arrive early?
We recommend that volunteers arrive within 1-2 days before the orientation training begins.  All extra-nights are covered by the volunteers.
33) What is the host family selection process?
Schools chosen to receive a volunteer are required to provide potential families with a relationship or connection to the school to host the volunteer in their home. GoGlobal requests various prerequisites from possible host families, such as: separate bedroom for the volunteer in a house in a safe neighborhood and, if possible, located near the school. Before the volunteer arrives, the family is provided with specific information about the volunteer that will live with them.
34) Can I make certain requests regarding my host family placement?
Volunteers have the opportunity to let us know personal preferences (non-smoker, vegetarian etc.) on the application form. Program staff will do their best to match these preferences, although in some cases, it may not be possible.
35) I’m a vegetarian. Can I still live with a host family?
Yes. Vegetarians are welcome to live with host families. We specifically ask families whether or not they are willing to host a vegetarian, and only place vegetarian volunteers with families that are open to this option. Volunteers should be aware that they will be offered the food that the host family eats, which may not necessarily be the food that they are accustomed to eating. With that said, volunteers may need to supplement their diet with other foods if necessary. In addition, if you have other types of dietary restrictions you will be responsible for supplementing your diet.
36) Will I have internet access in my placement?
Many host families do have internet in their home. Most schools also have internet access as well.
37) How will I communicate with people inside of Ukraine?
GoGlobal provides our volunteers with free SIM cards to be able to communicate with people while in Ukraine. If your phone is unlocked you can simply switch SIM cards, however, if it is not, there are cheap phones available for purchase.
38) When is the airport shuttle service available?
GoGlobal will organize the airport shuttle within 2 days before the orientation training begins. The shuttle is available 24 hours a day, so even if your flight arrives at 4 AM you will be provided shuttle services. Once volunteers have completed all steps in the pre-departure process and sent in their flight itinerary, volunteers will be sent the information to be able to use this service.
39) I would like to bring a gift for my host family. Any suggestions?
Your host family will probably appreciate anything you’d like to give, but if you’re looking for ideas, something traditional from your country is always a hit. Don’t feel obligated to bring your family a gift if you don’t have room in your suitcase or can’t think of anything you’d like to bring. They won’t be necessarily expecting a gift from you. It might be a good idea to wait until you have met them and know what they like, and then bring them a little gift from someplace you visit within Chile.-->
<!--</div>
</div>

<div class="participant__gallery">
  <div class="participant__gallery-inner">
    <h2 class="participant__gallery-title participant__title">How it was in 2016</h2>
    <div class="participant__photos">
      <iframe width="853" height="480" src="https://www.youtube.com/embed/FUoM9VKBopo" frameborder="0" allowfullscreen style="margin: 0 auto;"></iframe>
    </div>
  </div>
</div>
</section>
 -->
<div class="container">
  @endsection
