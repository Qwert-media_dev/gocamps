@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title">
  <span class="page-header__page-title-inner">{{$model->title}}</span>
</h1>
@endsection

@section('content')
{!! html_entity_decode($model->body) !!}

@endsection
