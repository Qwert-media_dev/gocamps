@extends('layouts.master')

@section('header')
<h1 class="page-header__page-title nn">
  <span class="page-header__page-title-inner"><?=Lang::get('main.news', array(), LaravelLocalization::getCurrentLocale())?></span>
</h1>
@endsection

@section('content')
<section class="news">
  @foreach($model as $one)
  <?php $timestamp =  strtotime($one->created_at); ?>
  <?php $date = date('d-m-Y', $timestamp); ?>
  <div class="news__item">
    <p class="news__date"><?=$date?></p>
    <a href="/{{LaravelLocalization::getCurrentLocale()}}/{{$category_slug}}/{{$one->slug}}"><h3 class="news__title"><?=$one->title?></h3></a>
    @if(isset($one->image))
    <a href="/{{LaravelLocalization::getCurrentLocale()}}/{{$category_slug}}/{{$one->slug}}"><img src="/image/{{$one->image}}" alt=""></a>
    @endif
    <p class="news__text"><?php //$one->body?></p>
  </div>
  @endforeach
</section>
@endsection
