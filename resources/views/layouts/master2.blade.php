<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta property = "og:title" content = "GoCamps Ukraine" />
  <meta property = "og:type" content = "article" />
  <meta property = "og:image" content = "http://gocamps.com.ua/img/prone.png" />
  <meta property = "og:description" content = "New wave of English summer camps for children in Ukraine" />
  <meta name="keywords" content="<?php if(isset($model->keywords) && !empty($model->keywords)) echo $model->keywords; ?>">
  <meta name="description" content="<?php  if(isset($model->description) && !empty($model->description)) echo $model->description;?>">
  <meta name="google-site-verification" content="S3G-xaDJGp9iz6A4KMv_-ZYTmW5EEZwJcWJjXn3Oagk" />
  <title><?php if(isset($model->seo_title) && !empty($model->seo_title)) echo $model->seo_title ;else echo "GoCamps" ?> — new wave of English summer camps for children in Ukraine</title>

  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  @yield('head')
  <link rel="stylesheet" href="/style.css">
</head>
<body>
  layout2
  <header class="page-header">
    <div class="page-header__top-menu">
      <div class="page-header__top-inner">
        <div class="page-header__top-left">
          <a class="page-header__logo" href="/">
            <img src="/img/logo.svg" alt="GoCamps logo" width="85" height="auto">
          </a>
          <ul class="page-header__langs-list">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            <li>
              <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                {{ $properties['native'] }}
              </a>
            </li>
            @endforeach
          </ul>
        </div>
        <button class="page-header__trigger hamburger" type="button" id="toggle-menu"><span></span></button>
        <ul class="page-header__nav">
          <li><a href="/"><?=Lang::get('main.header_what_is', array(), LaravelLocalization::getCurrentLocale())?></a></li>
          <li><a href="/projects"><?=Lang::get('main.header_projects', array(), LaravelLocalization::getCurrentLocale())?></a></li>
          <li><a href="/volunteer"><?=Lang::get('main.header_volunteer', array(), LaravelLocalization::getCurrentLocale())?></a></li>
          <li><a href="/school"><?=Lang::get('main.header_school', array(), LaravelLocalization::getCurrentLocale())?></a></li>
          <li><a href="/category/novini"><?=Lang::get('main.header_news', array(), LaravelLocalization::getCurrentLocale())?></a></li>
          <li><a href="/contacts"><?=Lang::get('main.header_contacts', array(), LaravelLocalization::getCurrentLocale())?></a></li>

          <!-- <li><a>Новини</a></li> -->
          <!-- <li><a>Ресурс</a></li> -->
        </ul>
        <div class="page-header__social-icons">
          <a href="https://www.facebook.com/gocampua/" target="_blank"><span class="icon-facebook"></span></a>
          <a href="https://www.instagram.com/goglobalua/" target="_blank"><span class="icon-instagram"></span></a>
          <a href="https://twitter.com/GoGlobalUA" target="_blank"><span class="icon-twitter"></span></a>
        </div>
      </div>
    </div>
    @yield('header')
  </header>

  <div class="container">
    @yield('content')
  </div><!-- .container -->

  <footer class="page-footer">
    <div class="page-footer__inner">
      <ul class="page-footer__nav">
       <li><a href="/"><?=Lang::get('main.header_what_is', array(), LaravelLocalization::getCurrentLocale())?></a></li>
       <li><a href="/projects"><?=Lang::get('main.header_projects', array(), LaravelLocalization::getCurrentLocale())?></a></li>
       <li><a href="/volunteer"><?=Lang::get('main.header_volunteer', array(), LaravelLocalization::getCurrentLocale())?></a></li>
       <li><a href="/school"><?=Lang::get('main.header_school', array(), LaravelLocalization::getCurrentLocale())?></a></li>
       <!-- <li><a href="">Новини</a></li> -->
       <!-- <li><a href="">Ресурс</a></li>  -->
     </ul>
     <p class="page-footer__copyright">GoCamps<br> 2016</p>
   </div>
 </footer>

 @yield('scripts')
 <script src="/bundle.js"></script>
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85347318-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
