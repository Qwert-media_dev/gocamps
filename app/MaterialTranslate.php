<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialTranslate extends Model
{
    protected $guarded = [];
    public $timestamps=false;
    public $table='materialTranslate';
    public function school()
    {
        return $this->belongsTo('App\Material');
    }

}
