<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $guarded = [];
    public $timestamps=false;

    public function translate()
    {
        return $this->hasOne('App\MaterialTranslate');
    }
}
