<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    protected $guarded = [];
    public $timestamps=false;
    public $table='users_roles';
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
