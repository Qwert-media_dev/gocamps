<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslate extends Model
{
    protected $guarded = [];
    public $timestamps=false;
    public $table='pageTranslate';
    public function page()
    {
        return $this->belongsTo('App\Page');
    }
}
