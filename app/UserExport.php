<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExport extends Model
{
    public $timestamps=false;
    public $table='user_export';
    protected $fillable = ['user_id', 'cols', 'type'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
