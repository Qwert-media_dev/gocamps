<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\School;
use DB;
use Carbon\Carbon;

class Volunteer extends Model
{
    protected $guarded = [];
    protected $table = 'volunteers-new';

    public function lastYear()
    {
        $last = $this->hasMany(VolunteerYear::class)->orderBy('year', 'desc')->first();
        return ($last) ? $last->year : '0';
    }

    public function getSimilarAttribute() {
      if ($this->type == 0)
        return false;

      $search = Volunteer::orderBy('year', 'desc')
                              ->where('id', '<>', $this->id)
                              ->where([
                                'type' => 0,
                                'first_name' => $this->first_name,
                                'last_name' => $this->last_name,
                              ]);
      $vols = $search->get()->unique('volunteer_id');
      if ( count( $vols->toArray() ) == 0 ) return false;

      $result = $search->get();
      $search->where('citizenship', $this->citizenship);
      $vols = $search->get()->unique('volunteer_id');
      if ( count( $vols->toArray() ) == 0 ) {
        return $result;
      }

      $result = $search->get();
      $search->where('passport', $this->passport);
      $vols = $search->get()->unique('volunteer_id');
      if ( count( $vols->toArray() ) == 0 ) {
        return $result;
      }

      return $search->get();

      // $search = ['first_name', 'last_name', 'citizenship', 'passport'];

      // $result = false;
      // foreach ($search as $filter) {
      //   $tmpSearch = $tmpSearch->where($filter, $this->$filter);
      //   $tmpVols = $tmpSearch->get()->unique('volunteer_id');
      //   $count = count( $tmpVols->toArray() );
      //   if ( $count == 0 ) {
      //     break;
      //   }
      //   $result = $tmpVols;
      // }
      // return (count($result) == 0) ? false : $result;
      // return false;
    }
    public function getStateAttribute($value) {
      return ($value === '') ? [] : explode(',', $value);
    }
    public function setStateAttribute($value) {
      $this->attributes['state'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getStateSchools( $state = -1 ) {
      $state = ( $state == -1 ) ? $this->state : $state;
      $resultQuery = School::where('year', $this->year)->whereIn('status', [5,6] );
      if ( $state ) {
        if ( !is_array($state) ) {
          $state = [$state];
        }
        $resultQuery = $resultQuery->whereIn( 'state',  $state );
      }
      return $resultQuery->pluck('title', 'id');
    }
    public function getSchoolsListAttribute() {
      return School::where('year', $this->year)->whereIn('status', [5,6])->pluck('title', 'id');
    }
    public function getSchoolsAddedAttribute() {
      $result = [];

      foreach ( $this->schools as $S ) {
        $result[(int)$S->id] = $S->title;
      }
      return $result;
    }
    public function getSchoolsAttribute() {
      return $this->hasManyThrough(
            'App\School',
            'App\SchoolVolunteer',
            'volunteer_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'school_year_id' // Local key on users table...
        )->get();
    }

    public function getBirthdayAttribute($value) {
      return date('d/m/Y', strtotime($value));
    }
    public function setBirthdayAttribute($value) {
      if ( !preg_match( '/^[0-3][0-9]\/[0-1][0-9]\/[1,2][0,1,9][0-9][0-9]$/', $value ) )
        dd('WORNG DATE FORMAT');
      $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
    public function getDocumentsAttribute($value) {
      return ($value === '') ? [] : explode(',', $value);
    }
    public function setDocumentsAttribute($value) {
      $this->attributes['documents'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getSessionAttribute($value) {
      return ($value == NULL) ? [] : explode(',', $value);
    }
    public function setSessionAttribute($value) {
      $this->attributes['session'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getCvnAttribute() {
      $parts = explode('/', $this->cv);
      return array_pop($parts);
    }
    public function getCvlAttribute() {
      return "<a href='{$this->cv}' target='_blank'>{$this->cvn}</a>";
    }
    public function getMailsAttribute($value) {
      return ( $value != NULL ) ? explode(',', $value) : [];
    }
    public function setMailsAttribute($value) {
      $this->attributes['mails'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
}
