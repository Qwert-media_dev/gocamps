<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslate extends Model
{
   protected $guarded = [];
   public $timestamps=false;
   public $table='categoryTranslate';
   public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
