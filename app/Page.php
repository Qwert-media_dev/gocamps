<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];
    public $timestamps=false;

    public function translate()
    {
        return $this->hasOne('App\PageTranslate');
    }
}
