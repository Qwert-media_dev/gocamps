<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolVolunteer extends Model
{
    protected $guarded = [];
    protected $table = 'school_volunteers';

    public function volunteer() {
      return $this->hasOne('App\Volunteer', 'id', 'volunteer_id');
    }
}
