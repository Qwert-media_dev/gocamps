<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    public function translate()
    {
        return $this->hasOne('App\ArticleTranslate');
    }
}
