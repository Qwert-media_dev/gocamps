<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTranslate extends Model
{
    public $timestamps=false;
    public $table='articleTranslate';
    protected $guarded = [];
    
    public function school()
    {
        return $this->belongsTo('App\Article');
    }

}
