<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $guarded = [];
    protected $table = 'schools-new';

    public function lastYear() {
        $last = $this->hasMany(SchoolsYear::class)->orderBy('year', 'desc')->first();
        return ($last) ? $last->year : '0';
    }
    public function getCampLangAttribute($value) {
      return ($value == '') ? [] : explode(',', $value);
    }
    public function setCampLangAttribute($value) {
      $this->attributes['camp_lang'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getSessionAttribute($value) {
      return ($value == '') ? [] : explode(',', $value);
    }
    public function setSessionAttribute($value) {
      $this->attributes['session'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getProjectAttribute($value) {
      return ($value == '') ? [] : explode(',', $value);
    }
    public function setProjectAttribute($value) {
      $this->attributes['project'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getTrainingDatesAttribute($value) {
      return ($value == '') ? [] : explode(',', $value);
    }
    public function setTrainingDatesAttribute($value) {
      $this->attributes['training_dates'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }
    public function getGocEastAttribute($value) {
      return ($value == '') ? [] : explode(',', $value);
    }
    public function setGocEastAttribute($value) {
      $this->attributes['goc_east'] =
        (is_array($value)) ? implode(',', $value) : $value;
    }

    public function getVolunteersListAttribute() {
      return Volunteer::where('year', $this->year)
                        ->whereIn('status', [5,6,7])
                        ->get();
    }
    public function getVolunteersAddedAttribute() {
      $result = [];

      foreach ( $this->volunteers as $V ) {
        $result[(int)$V->id] = $V->first_name . ' ' . $V->last_name;
      }
      return $result;
    }
    public function getVolunteersAttribute() {
      return $this->hasManyThrough(
            'App\Volunteer',
            'App\SchoolVolunteer',
            'school_year_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'volunteer_id' // Local key on users table...
        )->get();
    }
}
