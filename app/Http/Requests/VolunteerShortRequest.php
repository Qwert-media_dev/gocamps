<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VolunteerShortRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'last_name' => 'required',
          'first_name' => 'required',
          'gender' => 'required',
          'citizenship' => 'required',
          'country' => 'required',
          'city' => 'required',
          'passport' => 'required',
          'birthday' => 'required',
          'email' => 'required|email',
          'skype' => 'required',
          'phone' => 'required|numeric',
          'phone_code' => 'required',
          'session' => 'required',
        ];
    }
}
