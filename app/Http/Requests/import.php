<?php

namespace Crm\Components;

use Crm\Components\Abstract_Component as Component;

class Import extends Component
{
    const POST_TYPE = 'import';
    public function __construct($data)
    {
        $this->parentSlug = $data['parentSlug'];
        
        parent::__construct([
            'labels' => [
                'name' => 'Imports',
                'singular_name' => 'Import',
                'menu_name' => 'Imports',
                'name_admin_bar' => 'Import',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Import',
                'new_item' => 'New Import',
                'edit_item' => 'Edit Import',
                'view_item' => 'View Import',
                'all_items' => 'All Imports',
                'search_items' => 'Search Imports',
                'parent_item_colon' => 'Parent Imports:',
                'not_found' => 'No imports found.',
                'not_found_in_trash' => 'No imports found in Trash.',
            ],
            'show_in_menu' => $this->parentSlug,
            'supports' => array('title'),
            'capabilities' => array(
                'create_posts' => 'do_not_allow',
            ),
            'map_meta_cap' => true
        ]);
        add_action('admin_action_sp_import', [$this, 'importSpareParts']);
        add_filter('views_edit-import', [$this, 'renderImport']);
        add_action('admin_notices', [$this, 'renderNotice']);
        add_action('edit_form_after_title', [$this, 'renderPostContent']);
    }
    public function renderImport($views)
    {
        ob_start();
        include CRM_PLUGIN_DIR.'templates/import.php';
        $views['import'] = ob_get_clean();
        return $views;
    }

    public function renderNotice()
    {
        if(filter_var($_GET['success'], FILTER_VALIDATE_BOOLEAN)): ?>
            <div class="notice notice-success is-dismissible">
                <p>Import success</p>
            </div>
        <?php endif; if(isset($_GET['success']) && !filter_var($_GET['success'], FILTER_VALIDATE_BOOLEAN)): ?>
            <div class="notice notice-error is-dismissible">
                <p>Import fail</p>
            </div>
        <?php endif;
    }

    public function renderPostContent($content)
    {
        global $post;
        global $wpdb;
       
        $ids=$wpdb->get_results("SELECT meta_value FROM `nxuy_postmeta` WHERE post_id=".$post->ID." and meta_key like 'spare_parts_%'");
        $sp=array();
        $article=array();
        $name=array();
        $price=array();
        $quantity=array();
        $be=array();
        $ond=array();
        $ofd=array();
        foreach ($ids as $key => $value) {
            $sp[]=array(
                'article'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="article" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'name'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="name" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'price'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="price" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'quantity'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="count" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'be'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="be" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'ond'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="ond" ORDER BY `nxuy_postmeta`.`meta_key` DESC'),
                'ofd'=>$wpdb->get_results('SELECT meta_value FROM `nxuy_postmeta` WHERE post_id="'.$value->meta_value.'" and meta_key="ofd" ORDER BY `nxuy_postmeta`.`meta_key` DESC')
           );
        }

        $content = unserialize($post->post_content);
        ?>
        <?php if(isset($content)){ ?>
        <table class="widefat fixed" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col">Article</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Q-ty</th>
                    <th scope="col"># BE</th>
                    <th scope="col"># of drawings</th>
                    <th scope="col"># on the drawing</th>
                </tr>
            </thead>
            <tbody>
                 <?php if(!$content){ ?>
                    <?php foreach ($sp as $key => $value) {
                        if(!empty($value['article'][0]->meta_value)){//echo "<pre>";var_dump($value['article']);die;
                        ?>
                        <tr class="alternate">
                            <td><?=$value['article'][0]->meta_value?></td>
                            <td><?=$value['name'][0]->meta_value?></td>
                            <td><?=$value['price'][0]->meta_value?></td>
                            <td><?=$value['quantity'][0]->meta_value?></td>
                            <td><?=$value['be'][0]->meta_value?></td>
                            <td><?=$value['ofd'][0]->meta_value?></td>
                            <td><?=$value['ond'][0]->meta_value?></td>
                        </tr>

                       
                    <?php }} }?>
                
                <?php foreach($content as $row):?>
                <tr class="alternate">
                    <td><?=$row[0]; ?></td>
                    <td><?=$row[1]; ?></td>
                    <td><?=$row[2]; ?></td>
                    <td><?=$row[3]; ?></td>

                    <td><?=$row[4]; ?></td>
                    <td><?=$row[5]; ?></td>
                    <td><?=$row[6]; ?></td>
                </tr>
                <?php endforeach; ?>
                

            </tbody>
        </table><?php
    }}

    public function postTypeColumns($columns)
    {

        return $columns;
    }

    public function managePostTypeColumns($column, $post_id)
    {


    }
    public function sortPostTypeColumns($columns)
    {

    }
    public function sortPosts($query)
    {

    }
    private function importSP($file)
    {
        global $wpdb;
                // var_dump(the_field('#ondrawings', 985),
             
                // the_field('price', 985),
                // the_field('quantity', 985),
                // the_field('#ofdrawings', 985));die;
        $uploadDir = wp_upload_dir();
        
        $path = $uploadDir['path'].'/temp.csv';
        if ($file['type'] == 'text/csv' && $file['size'] <= 5242880) {
            move_uploaded_file($file['tmp_name'], $path);
            $data = [];
            $first = true;
            if (($handle = fopen($path, 'r')) !== false) {
                while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                    if($first) {
                        $first = false;
                        continue;
                    }
                    $data[] = $row;
                }
                fclose($handle);
            }
            // var_dump($data);die;
            $posts=array();
            foreach($data as $key=>$row) {
                

                $spid = $wpdb->get_var("SELECT $wpdb->posts.ID FROM $wpdb->posts, $wpdb->postmeta WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id
                    AND $wpdb->posts.post_title LIKE '%" . $row[1] . "%'
                    AND $wpdb->postmeta.meta_key = 'article'
                    AND $wpdb->postmeta.meta_value = '" . $row[0] . "'");
                // if($spid) {
                //     wp_update_post([
                //         'ID' => $spid,
                //         'post_date' => date('Y-m-d h:s')
                //     ]);
                // } else {
                    $postId = wp_insert_post([
                        'post_title' => $row[1],
                        'post_type' => 'spare_part',
                        'post_status' => 'publish',
                        // 'drawing_number'=>'ds'
                    ]);
                    array_push($posts,$postId);
                   
                    // update_field('spare_parts_0_spare_part', 1506, 1378);
                // }
                    // var_dump($row[4]);die;
                update_field('article', $row[0], $postId);
                update_field('name',$row[1],$postId);
                update_field('price', $row[2], $postId);
                update_field('count', $row[3], $postId);
                update_field('be', $row[4], $postId);
                update_field('ofd', $row[5], $postId);
                update_field('ond', $row[6], $postId);

                // var_dump($row);
            }
            // die;
            // var_dump($posts);die;
             $pumps=$wpdb->get_results('SELECT distinct(p.id)  FROM `nxuy_posts` p 
                        left join nxuy_postmeta as pm on pm.post_id=p.id 
                        WHERE pm.meta_value='.$row[4].' and p.post_status="publish" and pm.meta_key="uid"');
             $count=0;
           
                    foreach ($pumps as $pump) {
                          $count_parts=$wpdb->get_results("SELECT count( meta_value ) as count
                                            FROM `nxuy_postmeta`
                                            WHERE post_id = ".$pump->id."
                                            AND meta_key LIKE 'spare_parts_%'");
                            
                            $num=$count_parts[0]->count+1;
                        foreach ($posts as  $k=>$post) {
                            update_field('spare_parts_'.$num.'_spare_part', $post, $pump->id);
                            // update_post_meta( 1378, 'spare_parts', 2 );
                            $num++;
                            // var_dump($num);
                        }
                        // die;
                        delete_post_meta( $pump->id, 'spare_parts' );
                        add_post_meta( $pump->id, 'spare_parts', $num );
                        
                        $count=0;
                        
                        
                         
                    }
                    // die;
            wp_insert_post([
                'post_title' => date('Y-m-d h:s'),
                'post_type' => 'import',
                'post_content' => serialize($data),
                'post_status' => 'publish'
            ]);

            unlink($path);
        }
        return true;
    }
    private function importSPtoImport($file)
    {
        global $wpdb;
        global $post;
        $uploadDir = wp_upload_dir();
        $path = $uploadDir['path'].'/temp.csv';
        if ($file['type'] == 'text/csv' && $file['size'] <= 5242880) {
            move_uploaded_file($file['tmp_name'], $path);
            $data = [];
            $first = true;
            if (($handle = fopen($path, 'r')) !== false) {
                while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                    
                    if($first) {
                        $first = false;
                        continue;
                    }
                    $data[] = $row;
                }
                
                fclose($handle);
            }
            foreach($data as $row) {
                
                $spid = $wpdb->get_var("SELECT $wpdb->posts.ID FROM $wpdb->posts, $wpdb->postmeta WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id
                    AND $wpdb->posts.post_title LIKE '%" . $row[1] . "%'
                    AND $wpdb->postmeta.meta_key = 'article'
                    AND $wpdb->postmeta.meta_value = '" . $row[0] . "'");

                $pid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_title LIKE '%" . $row[3] . "%'");
                if($spid && $pid) {
                    update_field('field_57349b1eaa231', $row[2], $spid);
                    update_field('field_57349b09aa230', $row[4], $spid);
                    update_field('field_575ec741cf79f', $row[5], $spid);
                    $checkbox = [];
                    if(!!$row[6]) $checkbox[] = 'type_1';
                    if(!!$row[7]) $checkbox[] = 'type_2';
                    if(!!$row[8]) $checkbox[] = 'type_3';
                    update_field('field_5773c3d2e273d', $checkbox, $spid);
                    $spareParts = [];
                    if (have_rows('spare_parts', $pid)) {
                        $sps = get_field('spare_parts', $pid);
                        foreach ($sps as $key => $sp) {
                            $spareParts[$key] = ['spare_part' => $sp['spare_part']->ID];
                        }
                    }
                    $spareParts[] = [
                        'spare_part' => $spid
                    ];
                    update_field('field_57349bb566b32', array_unique($spareParts, SORT_REGULAR), $pid);


                }

            }
            unlink($path);
        }
        return true;
    }
    public function importSpareParts()
    {
        // var_dump($_FILES);die;
        try {
            if (isset($_FILES['file1']) && $_FILES['file1']['tmp_name']) {
                $f1 = $this->importSP($_FILES['file1']);
            }

            if (isset($_FILES['file2']) && $_FILES['file2']['tmp_name']) {
                $f2 = $this->importSPtoImport($_FILES['file2']);
            }
            if($f1 || $f2) {
                wp_redirect(admin_url('edit.php?post_type=import&success=true'));
                exit;
            }
        } catch (Exception $e) {
            wp_redirect(admin_url('edit.php?post_type=import&success=false'));
            exit;
        }
        wp_redirect(admin_url('edit.php?post_type=import&success=false'));
        exit;
    }
    public function getPaginataion()
    {
        return paginate_links([
            'base' => add_query_arg('pagenum', '%#%'),
            'format' => '',
            'prev_text' => __('&laquo;', 'text-domain'),
            'next_text' => __('&raquo;', 'text-domain'),
            'total' => 12,
            'current' => 1,
        ]);
    }
    public function getPageData()
    {
        return [
            'pagination' => $this->getPaginataion(),
        ];
    }
    // public function registerMenu()
    // {
    //     $pageData = $this->getPageData();
    //
    //     add_submenu_page($this->parentSlug, 'Import', 'Import', 'edit_files', '/crm-import', function () use ($pageData) {
    //         ob_start();
    //         include CRM_PLUGIN_DIR.'templates/import.php';
    //         echo ob_get_clean();
    //     });
    // }
}
