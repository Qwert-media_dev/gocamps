<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'body' =>'required',
            'slug' =>'unique:articleTranslate,slug,'.$this->article_id.',article_id'#'unique:articleTranslate,slug,$this->article_id',
            
        ];
    }
}
