<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VolunteerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'last_name' => 'required',
          'first_name' => 'required',
          'gender' => 'required',
          'citizenship' => 'required',
          'country' => 'required',
          'city' => 'required',
          'passport' => 'required',
          'birthday' => 'required',
          'email' => 'required|email',
          'skype' => 'required',
          'phone' => 'required|numeric',
          'phone_code' => 'required',
          'emergency_name' => 'required',
          'emergency_phone_code' => 'required',
          'emergency_phone' => 'required|numeric',
          'eng_level' => 'required',
          'other_langs' => 'required',
          'education' => 'required',
          'workplace' => 'required',
          'workplace_position' => 'required',
          'work_describe' => 'required',
          'about' => 'required',
          'why' => 'required',
          'facts' => 'required',
          'hear' => 'required',
          'session' => 'required',
          'been_in_ukraine' => 'required',
          'interested' => 'required',
          'shootings' => 'required',
          'agree' => 'required',
        ];
    }
}
