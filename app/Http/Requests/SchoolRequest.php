<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SchoolRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'title' => 'required',
          'title_eng' => 'required',
          'state' => 'required',
          'state_eng' => 'required',
          'city_type' => 'required',
          'city_type_eng' => 'required',
          'city' => 'required',
          'city_eng' => 'required',
          'street' => 'required',
          'street_eng' => 'required',
          'house' => 'required',
          'railway_station' => 'required',
          'experience' => 'required',
          'about_school' => 'required',
          'about_school_eng' => 'required',
          'about_city' => 'required',
          'about_city_eng' => 'required',
          'quantity_children' => 'required',
          'session' => 'required',
          'type_camp' => 'required',
          'share_volunteer' => 'required',
          'share_camp' => 'required',
          'camp_lang' => 'required',
          'sdirector_name' => 'required',
          'sdirector_phone' => 'required',
          'sdirector_email' => 'required',
          'teacher_name' => 'required',
          'teacher_phone' => 'required',
          'teacher_email' => 'required',
          'school_phone' => 'required',
          'school_email' => 'required',
          'video' => 'required',
      ];
    }
}
