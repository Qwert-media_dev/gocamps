<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\PageTranslate;
use App\Article;
use App\School;
use App\ArticleTranslate;
use App\MaterialTranslate;
use App\Material;
use App\Category;
use DB;
use Config;
use Redirect;
use LaravelLocalization;
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schools_array=array();

        /*foreach (School::where('status',5)->get() as $school) {
            $newtitle="";
            $newaddress="";
            $address=$school->country.' '.$school->city.' '.$school->street.' '. $school->house;
            // $address=str_replace(array(' ', '<', '>', '&', '{', '}', '*','"'), '', $address);
            $address=str_replace(array( '<', '>', '&', '{', '}', '*','"'), '', $address);
            $title=$school->title;
            // $title=str_replace(array(' ', '<', '>', '&', '{', '}', '*','"'), '',$title);
            $title=str_replace(array( '<', '>', '&', '{', '}', '*','"'), '',$title);
            $title=trim($title);
            $address=trim($address);
            $schools_array[] = array (
                'address' => $address,
                'title' => $title,
                'lat' => $school->lat,
                'lng' => $school->lng,
                'mid' => $school->id,
            );
        }*/
        foreach (School::all() as $school) {
            $newtitle="";
            $newaddress="";

            $suf = LaravelLocalization::getCurrentLocale() == 'en' ? '_eng' : '';
            $countryCol = 'country' . $suf;
            $titleCol = 'title' . $suf;
            $cityCol = 'city' . $suf;
            $streetCol = 'street' . $suf;

            if ($suf == '') {
                $address = $school->country.' '.$school->city.' '.$school->street.' '. $school->house;
            } else {
                $address = $school->house . ' ' . $school->$streetCol . ' ' . $school->$cityCol.' '. $school->$countryCol;
            }


            // $address=str_replace(array(' ', '<', '>', '&', '{', '}', '*','"'), '', $address);

            $address=str_replace(array( '<', '>', '&', '{', '}', '*','"'), '', $address);

            $title=$school->$titleCol;
            // $title=str_replace(array(' ', '<', '>', '&', '{', '}', '*','"'), '',$title);
            $title=str_replace(array( '<', '>', '&', '{', '}', '*','"'), '',$title);
            $title=trim($title);
            $address=trim($address);
            $schools_array[] = array (
                'address' => $address,
                'title' => $title,
                'lat' => $school->lat,
                'lng' => $school->lng,
                'mid' => $school->id,
            );
        }

        // dd($schools_array);

        //////////////////////////////// MAKS //////////////////////
        $category_id=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
        ->where('categoryTranslate.slug','news')/*->groupBy('category_id')*/
        ->where('lang',LaravelLocalization::getCurrentLocale())
        ->first()['category_id'];

        $articles=Article::orderBy('created_at', 'desc')->limit(3)->leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())
        ->where('articles.category_id',$category_id)
        ->get();
        /////////////////////////////////
        $category_id=1;

        $articles=Article::orderBy('created_at', 'desc')->limit(3)->leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())
        ->where('articles.category_id',$category_id)
        ->get();

        if($articles==null){
            $loc_category=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
            ->where('lang',LaravelLocalization::getCurrentLocale())
            ->where('categoryTranslate.category_id',$category_id)/*->groupBy('category_id')*/
            ->first();
            $loc_article=Article::leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })

            ->where('articleTranslate.slug',$slug)
                                            // ->where('articles.category_id',$category_id)
            /*->groupBy('article_id')*/
            ->first();
            if($loc_article==NULL)
                return view('errors.404',
                    array(

                        )
                    );
        }
            $articles=Article::orderBy('created_at', 'desc')->limit(3)->leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })
                ->where('lang',LaravelLocalization::getCurrentLocale())
                ->where('articles.category_id',$category_id)
                ->get();
             // return redirect()->route('article', [$loc_category,$article->slug]);

        return view('index',
            array(
              'model'=>Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
              ->where('lang',LaravelLocalization::getCurrentLocale())->where('pageTranslate.page_id',4)/*->groupBy('page_id')*/
              ->first(),
              'schools'=>json_encode($schools_array),
              'articles'=>$articles,
              'category_slug'=>Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
              ->where('lang',LaravelLocalization::getCurrentLocale())
              ->where('categoryTranslate.category_id',$category_id)/*->groupBy('category_id')*/
              ->first()['slug'],
              'locale' => LaravelLocalization::getCurrentLocale(),
              )
            );
    }

    public function getJson(Request $request)
    {
        $scs = School::where('status',5)->where('lat','<>','')->get();
        $retAr = [];
        $ret = new \stdClass();
        $ret->type = "FeatureCollection";

        // $suf = LaravelLocalization::getCurrentLocale() == 'en' ? '_eng' : '';
        // dd($request->l);
        $suf = $request->l == 'en' ? '_eng' : '';
        $countryCol = 'country' . $suf;
        $titleCol = 'title' . $suf;
        $cityCol = 'city' . $suf;
        $streetCol = 'street' . $suf;


        foreach ($scs as $sc) {
            // if ($sc->lat < 0) continue;
            // if ($sc->lng < 0) continue;
            // if (intval($sc->lat) < 1) dd($sc);
            // if (intval($sc->lng) < 1) dd($sc);
            $obj = new \stdClass();
            $obj->type = 'Feature';

            if ($suf == '') {
                $address = $sc->country.' '.$sc->city.' '.$sc->street.' '. $sc->house;
            } else {
                $address = $sc->house . ' ' . $sc->$streetCol . ' ' . $sc->$cityCol.' '. $sc->$countryCol;
            }
            $title=$sc->$titleCol;

            $obj->title = $title;

            $obj->geometry = new \stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = [(float)$sc->lng, (float)$sc->lat];

            $obj->properties = new \stdClass();
            $obj->properties->title = $title;
            $obj->properties->address = $address;

            $retAr[] = $obj;
        }
        $ret->features = $retAr;

        /*{
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [125.6, 10.1]
          },
          "properties": {
            "name": "Dinagat Islands"
          }
        }*/
        return json_encode($ret);
    }


    public function page($slug)
    {
        if($slug=='school'){
            return view('school.index',
                array(
                  'model'=>Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
                  ->where('lang',LaravelLocalization::getCurrentLocale())->where('pageTranslate.slug',$slug)/*->groupBy('page_id')*/
                  ->first()
                  )
                );
        }
        if($slug=='volunteer'){
            return view('volunteer.index',
                array(
                    'model'=>Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
                    ->where('lang',LaravelLocalization::getCurrentLocale())->where('pageTranslate.slug',$slug)/*->groupBy('page_id')*/
                    ->first()
                    )
                );
        }
        $page=Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())->where('pageTranslate.slug',$slug)->groupBy('page_id')
        ->first();

        if($page==null){

            $loc_page=Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
            ->where('pageTranslate.slug',$slug)->groupBy('page_id')
            ->first();
            if($loc_page==NULL)
                return view('errors.404',
                    array(

                        )
                    );
            $page=Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
            ->where('lang',LaravelLocalization::getCurrentLocale())->where('pageTranslate.page_id',$loc_page->page_id)->groupBy('page_id')
            ->first();
            return redirect()->route('page', [$page->slug]);

        }
        return view('page',
            array(
                'model'=>$page,
                )
            );
    }


    public function article($category, $slug)
    {
        $category_id = Category::join( 'categoryTranslate', 'categories.id', '=', 'categoryTranslate.category_id')
                                ->where( 'categoryTranslate.slug', $category )
                                ->groupBy('category_id')
                                ->first()['category_id'];
        $article = Article::join( 'articleTranslate', 'articles.id', '=', 'articleTranslate.article_id' )
                            ->where( 'lang', LaravelLocalization::getCurrentLocale() )
                            ->where( 'articleTranslate.slug', $slug )
                            ->where( 'articles.category_id', $category_id )
                            ->groupBy( 'article_id' )
                            ->first();

        if ( $article == null ) {
            $loc_category = Category::join( 'categoryTranslate', 'categories.id', '=', 'categoryTranslate.category_id' )
              ->where( 'lang', LaravelLocalization::getCurrentLocale() )
              ->where( 'categoryTranslate.category_id', $category_id )
              ->groupBy( 'category_id' )
              ->first();
            $loc_article = Article::join( 'articleTranslate', 'articles.id', '=', 'articleTranslate.article_id' )
              ->where( 'articleTranslate.slug', $slug )
              ->where( 'articles.category_id', $category_id )
              ->groupBy( 'article_id' )
              ->first();
            if ( $loc_article == NULL ) {
              return view('errors.404', []);
            } else {
              return view('articles.nolocal', ['model' => $loc_article]);
            }
            $article = Article::join( 'articleTranslate', 'articles.id', '=', 'articleTranslate.article_id' )
              ->where( 'lang', LaravelLocalization::getCurrentLocale() )
              ->where( 'articleTranslate.article_id', $loc_article->article_id )
              ->groupBy( 'article_id' )
              ->first();
            dd($article);
            return redirect()->route('article', [
              'category' => $loc_category->slug,
              'slug' => $article->slug
            ]);
        }
        return view('articles.article', ['model' => $article]);
    }
    public function category($category)
    {
        $category_id=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
        ->where('categoryTranslate.slug',$category)->groupBy('category_id')
        ->where('lang',LaravelLocalization::getCurrentLocale())
        ->first()['category_id'];

        $articles=Article::orderBy('created_at', 'desc')->leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())
        ->where('articles.category_id',$category_id)
        ->get();

        if(!isset($articles[0])){
            $cat_id=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
            ->where('categoryTranslate.slug',$category)->groupBy('category_id')
                                        // ->where('lang',LaravelLocalization::getCurrentLocale())
            ->first()['category_id'];
            $loc_category=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
            ->where('lang',LaravelLocalization::getCurrentLocale())
            ->where('categoryTranslate.category_id',$cat_id)->groupBy('category_id')
            ->first();
            if($loc_category==NULL)
                return view('errors.404',
                    array(

                        )
                    );
            $loc_article=Article::orderBy('created_at', 'desc')->leftJoin('articleTranslate', function($join) { $join->on('articles.id', '=', 'articleTranslate.article_id'); })
            ->where('articles.category_id',$loc_category->category_id)
            ->groupBy('article_id')
            ->first();

            return Redirect::route('category', array('category' => $loc_category->slug));

        }

        return view('news',
            array(
                'model'=>$articles,
                'category_slug'=>Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
                ->where('lang',LaravelLocalization::getCurrentLocale())
                ->where('categoryTranslate.category_id',$category_id)->groupBy('category_id')
                ->first()['slug']
                )
            );
    }

    public function material($slug)
    {
        $material=Material::leftJoin('materialTranslate', function($join) { $join->on('materials.id', '=', 'materialTranslate.material_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())->where('materialTranslate.slug',$slug)->groupBy('material_id')
        ->first();

        if($material==null){

            $loc_material=Material::leftJoin('materialTranslate', function($join) { $join->on('materials.id', '=', 'materialTranslate.material_id'); })
            ->where('materialTranslate.slug',$slug)->where('lang',LaravelLocalization::getCurrentLocale())->groupBy('material_id')
            ->first();

            if($loc_material==NULL)
                return view('errors.404',
                    array(

                        )
                    );
            $material=Material::leftJoin('materialTranslate', function($join) { $join->on('materials.id', '=', 'materialTranslate.material_id'); })
            ->where('lang',LaravelLocalization::getCurrentLocale())->where('materialTranslate.material_id',$loc_material->material_id)->groupBy('material_id')
            ->first();
            return redirect()->route('material-view', [$material->slug]);

        }
        return view('materials.view',
            array(
                'model'=>$material,
                )
            );
    }
    public function materials()
    {
        $materials=Material::leftJoin('materialTranslate', function($join) { $join->on('materials.id', '=', 'materialTranslate.material_id'); })
        ->where('lang',LaravelLocalization::getCurrentLocale())->groupBy('material_id')
        ->get();
        return view('materials.index',
            array(
                'model'=>$materials,
                )
            );
    }

    /**
     * Сохраняем-кешируем координаты школ (см. map.js)
     */
    public function saveMap(Request $request)
    {
        // dd($request);
        if (!$request->ajax()) die('OK');
        if (!$request->lat || !$request->lng || !$request->mid || !$request->tk) return ('OK');
        $sc = School::find($request->mid);
        if (!$sc || ($sc->lat != '' && $sc->lng != '')) return ('OK');
        $sc->lat = $request->lat;
        $sc->lng = $request->lng;
        $sc->save();
        return ('1');
    }
}
