<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VolunteerRequest;
use App\Http\Requests\VolunteerShortRequest;
use App\Volunteer;
use App\VolunteerYear;
use App\VolunteerTranslate;
use App;
use Config;
use DB;
use View;

class VolunteerController extends Controller
{


    public function create()
    {
        $empty = ['0'=>''];
        return view('volunteer.create',array(
            'yes_no'=>Config::get('list.yes_no'),
            'skill'=>Config::get('list.skill'),
            'session'=>Config::get('list.session'),
            'states'=>Config::get('list.states'),
            'countries'=>array_merge($empty,DB::table('countries')->pluck('nicename','iso')->toArray()),
            'phones'=>array_merge($empty,DB::table('countries')->pluck('phone_code','iso')->toArray()),
            'other_langs'=>array_merge($empty,Config::get('list.other_langs')),
            'hear'=>array_merge($empty,Config::get('list.hear')),
            'dietary'=>array_merge($empty,Config::get('list.dietary')),
            )
        );
    }

    public function short_create()
    {
        return view('volunteer.create_short',array(
            'session'=>Config::get('list.session'),
            'countries'=>array_merge(['0'=>''],DB::table('countries')->pluck('nicename','iso')->toArray()),
            'phones'=>array_merge(['0'=>''],DB::table('countries')->pluck('phone_code','iso')->toArray()),
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VolunteerRequest $request)
    {
        $volData = $request->except( '_token' );
        $volData['volunteer_id'] = Volunteer::max( 'volunteer_id' ) + 1;

        $tmpYear = date('Y');
        if (date('m') >= 9) {
          $year = $tmpYear . '-' . (int)($tmpYear + 1);
        } else {
          $year = (int)$tmpYear - 1 . '-' . $tmpYear;
        }

        $volData['year'] = $year;
        $volData['mails'] = '1';
        $volunteer = Volunteer::create( $volData );

        $to = $volunteer->email;
        $subject = Config::get( 'list.mails' )['1']['subject'];

        // текст письма
        $message = View::make( 'emails.email' . '1' )->render();

        // Для отправки HTML-письма должен быть установлен заголовок Content-type
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

        // Дополнительные заголовки
        $headers .= 'From: ' . Config::get('mail.from.name') . ' <' . Config::get('mail.from.address') . '>' . "\r\n";

        // Отправляем
        $result = mail( $to, $subject, $message, $headers );

        return redirect('success');
    }

    public function short_store(VolunteerShortRequest $request) {
      $volData = $request->except( '_token' );
      $volData['volunteer_id'] = Volunteer::max( 'volunteer_id' ) + 1;
      $volData['type'] = 1;

      // $volExists = Volunteer::where( 'passport', $volData['passport'] )->get();
      // if ( !isset( $volExists[0] ) ) {
      //   $volData['volunteer_id'] = Volunteer::max( 'volunteer_id' ) + 1;
      // } else {
      //   $volData['volunteer_id'] = $volExists[0]['volunteer_id'];
      // }

      $tmpYear = date('Y');
      if (date('m') >= 9) {
        $year = $tmpYear . '-' . (int)($tmpYear + 1);
      } else {
        $year = (int)$tmpYear - 1 . '-' . $tmpYear;
      }

      $volData['year'] = $year;
      $volData['mails'] = '1';
      $volunteer = Volunteer::create( $volData );

      $to = $volunteer->email;
      $subject = Config::get( 'list.mails' )['1']['subject'];

      // текст письма
      $message = View::make( 'emails.email' . '1' )->render();

      // Для отправки HTML-письма должен быть установлен заголовок Content-type
      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

      // Дополнительные заголовки
      $headers .= 'From: ' . Config::get('mail.from.name') . ' <' . Config::get('mail.from.address') . '>' . "\r\n";

      // Отправляем
      $result = mail( $to, $subject, $message, $headers );

      return redirect('/success');
    }
}
