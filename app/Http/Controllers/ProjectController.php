<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;


use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Project;

use LaravelLocalization;

class ProjectController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

       return view('projects.index',array('projects'=>Project::all()));

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {

        $model=Project::leftJoin('projectTranslate', function($join) { 

                $join->on('projects.id', '=', 'projectTranslate.project_id');

            })

            ->where('lang',LaravelLocalization::getCurrentLocale())->where('project_id',$id)

            ->first();   
        if($model==NULL)
                return view('errors.404',
                            array(
                            
                                )
                        );
        return view('projects.show',array('model'=>$model));

    }



}
