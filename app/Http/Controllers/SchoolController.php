<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SchoolRequest;
use App\Http\Requests\SchoolShortRequest;
use App\Http\Controllers\Controller;
use App\School;
use App\SchoolsYear;
use App\Adress;
use DB;
use Config;
use App;




class SchoolController extends Controller
{
    public function create()
    {
        return view('school.create',array(
            'quantity_children' =>Config::get('list.quantity_children'),
            'camp_type'         =>Config::get('list.camp_type'),
            'city_type'         =>Config::get('list.city_type'),
            'city_type_eng'     =>Config::get('list.city_type_eng'),
            'states'            =>Config::get('list.states'),
            'states_eng'        =>Config::get('list.states_eng'),
            'camp_lang'         =>Config::get('list.camp_lang'),
            'sessions'          =>Config::get('list.session'),
            )
        );
    }
    public function short_create()
    {
        return view('school.create_short',array(
            'quantity_children' =>Config::get('list.quantity_children'),
            'camp_type'         =>Config::get('list.camp_type'),
            'city_type'         =>Config::get('list.city_type'),
            'city_type_eng'     =>Config::get('list.city_type_eng'),
            'camp_lang'         =>Config::get('list.camp_lang'),
            'states'            =>Config::get('list.states'),
            'states_eng'        =>Config::get('list.states_eng'),
            'sessions'          =>Config::get('list.session'),
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(SchoolRequest $request)
    {
        $schoolData = $request->except( '_token' );
        $schoolData['school_id'] = School::max( 'school_id' ) + 1;

        $tmpYear = date('Y');
        if (date('m') >= 9) {
          $year = $tmpYear . '-' . (int)($tmpYear + 1);
        } else {
          $year = (int)$tmpYear - 1 . '-' . $tmpYear;
        }

        $schoolData['year'] = $year;
        $schoolData['status'] = 0;
        $school = School::create( $schoolData );

        return redirect('/success_school');
    }
    public function short_store(SchoolShortRequest $request)
    {
        $schoolData = $request->except( '_token' );
        $schoolData['school_id'] = School::max( 'school_id' ) + 1;
        $schoolData['type'] = 1;

        // $schoolExists = School::where( 'title', $schoolData['title'] )->get();
        // if ( !isset( $schoolExists[0] ) ) {
        //   $schoolData['school_id'] = School::max( 'school_id' ) + 1;
        // } else {
        //   $schoolData['school_id'] = $schoolExists[0]['school_id'];
        // }

        $tmpYear = date('Y');
        if (date('m') >= 9) {
          $year = $tmpYear . '-' . (int)($tmpYear + 1);
        } else {
          $year = (int)$tmpYear - 1 . '-' . $tmpYear;
        }

        $schoolData['year'] = $year;
        $schoolData['status'] = 0;
        $school = School::create( $schoolData );

        return redirect('/success_school');
    }
    public function destroy($id)
    {
        $translate= School::find($id)->delete();
        return redirect()->route('articles-index');
    }
}
