<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;

use App\User;
use App\UserRoles;
use App\Role;
use Hash;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
        return view('admin.users.index',array(
            'users'=>User::all()
            ));
        }
        return view('admin.permission');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.users.create',array(
                    'roles'=>Role::pluck('name','id')
                ));
            }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(Auth::user()->isAdmin()){
            $user=new User;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            $user->save();
            $userRole=new UserRoles();
            $userRole->role_id=$request->role;
            $userRole->user_id=$user->id;
            $userRole->save();
            return redirect()->route('users-edit',['id'=>$user->id]);
        }
        return view('admin.permission');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isAdmin()){
            return view('admin.users.update',array(
                    'user'=>User::find($id),
                    'roles'=>Role::pluck('name','id'),
                    'role'=>UserRoles::where('user_id','=',$id)->first()
                ));
        }
        return view('admin.permission');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        if(Auth::user()->isAdmin()){
            $user=User::find($id);
            $user->email=$request->email;
            if(!empty($request->password))
                $user->password=Hash::make($request->password);
            $user->save();
            $userRole=UserRoles::where('user_id','=',$id)->first();
            $userRole->role_id=$request->role;
            $userRole->user_id=$user->id;
            $userRole->save();
            return redirect()->route('users-edit',['id'=>$id]);
        }
        return view('admin.permission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isAdmin()){
            $user= User::find($id);

            $user->delete();

            return redirect()->route('users');
        }
        return view('admin.permission');

    }
}
