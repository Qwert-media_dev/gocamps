<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Volunteer;
use App\SchoolVolunteer;
use App\School;
use App\User;
use App\UserExport;
use App\VolunteerYear;
use App;
use Config;
use Mail;
use Auth;
use DB;
use Excel;
use View;
use File;

class VolunteerController extends Controller
{
    public function table(Request $request) {
      dd($request->all());
      return response()->json($request);
    }

    public function index($YEAR)
    {
        if ($YEAR == 'All') {
          $volunteers = Volunteer::orderBy('year', 'desc')->get();
        } else {
          $volunteers = Volunteer::where('year', $YEAR)->get();
        }

        $YN = ['1'=>'Yes','0'=>'No'];
        $E = ['0'=>'-'];
        $phones = array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray());
        $countries = array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray());

        return view('admin.volunteer.index',array(
            'YEAR' => $YEAR,
            'years'=>Volunteer::groupBy('year')->pluck('year'),
            'volunteers' => $volunteers,
            'columns' => Config::get('list.volunteerColumns'),
            'session'=>Config::get('list.table_session'),
            'strand'=>Config::get('list.strands'),
            'eng_level'=>array_merge($E, Config::get('list.skill')),
            'other_langs'=>array_merge($E, Config::get('list.other_langs')),
            'status' => Config::get('list.status_vol'),
            'state'=>Config::get('list.states'),
            'documents' => Config::get('list.documents'),
            'gender' => ['0'=>'Male', '1'=>'Female'],
            'phone_code'=>$phones,
            'alt_phone_code'=>$phones,
            'emergency_phone_code'=>$phones,
            'citizenship'=>$countries,
            'country'=>$countries,
            'interested'=>$YN,
            'shootings'=>$YN,
            'agree'=>$YN,
            'tickets'=>$YN,
            'visa'=>$YN,
            'payment'=>$YN,
            'been_in_ukraine'=>$YN,
            'hear'=>array_merge($E,Config::get('list.hear')),
            'dietary'=>array_merge($E,Config::get('list.dietary')),
            ));

    }

    public function index2($YEAR)
    {
        if ($YEAR == 'All') {
          $volunteers = Volunteer::orderBy('year', 'desc')->get();
          $allVolunteers = Volunteer::orderBy('year', 'desc')->get();
        } else {
          $volunteers = Volunteer::where('year', $YEAR)->get();
          $allVolunteers = Volunteer::orderBy('year', 'desc')->get();
        }

        // $statusAr = ['' => ''];
        $docsAr = array_merge(['' => ''], Config::get('list.documents'));
        $status = Config::get('list.status_vol');
        // $documents = Config::get('list.documents');

        // foreach ($allVolunteers as $av) {
        //   if ($av->status) {
        //     $statusAr[$av->status] = $status[$av->status];
        //   }
        // }

        $uv = UserExport::where('type', 'volvis')
                  ->where('user_id', auth()->user()->id)
                  ->first();
        if ($uv) {
          $uv = json_decode($uv->cols, true);
        } else {
          $uv = [];
        }

        $ue = UserExport::where('type', 'vol')
                  ->where('user_id', auth()->user()->id)
                  ->first();
        if ($ue) {
          $ue = json_decode($ue->cols, true);
        } else {
          $ue = [];
        }

        $filters = request()->session()->get('filters');

        $YN = [''=>'','1'=>'Yes','0'=>'No'];
        $E = [''=>'-'];
        $phones = array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray());
        $countries = array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray());

        $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                            ->where('role_id',2)
                            ->get()
                            ->pluck('email','email')
                            ->toArray();
        $managers = array_merge([''=>''], $managers);

        $inputs = [
          'city',
          'email',
          'skype',
          // 'schools',
          'citizenship_old',
          'country_old',
          'capacity',
          'education',
          'workplace',
          'workplace_position',
          'hear_other',
          'dietary_other',
          'special',
          'preferences',
          'comment',
        ];
        $selects = [
          'manager',
          'gender',
          'documents',
          'eng_level',
          'other_langs',
          'citizenship',
          'country',
          'state',
          'been_in_ukraine',
          'strand',
          'hear',
          'dietary',
          'tickets',
          'visa',
          'payment',
          'agree',
          'shootings',
          'interested',
        ];
        return view('admin.volunteer.index2',array(
            'YEAR' => $YEAR,
            'ue' => $ue,
            'uv' => $uv,
            'filters' => $filters,
            'years'=>Volunteer::groupBy('year')->pluck('year'),
            'inputs' => $inputs,
            'selects' => $selects,
            'manager' => $managers,
            // 'volunteers' => $volunteers,
            'columns' => Config::get('list.volunteerColumns'),
            'session'=>[''=>''] + Config::get('list.all_session'),
            'strand'=>Config::get('list.strands'),
            'eng_level'=>[''=>''] + $E, Config::get('list.skill'),
            'other_langs'=>[''=>''] + Config::get('list.other_langs'),
            // 'status' => Config::get('list.status_vol'),
            'state'=>Config::get('list.states'),
            'documents' => [''=>''] + Config::get('list.documents'),
            'gender' => ['' => '', '0'=>'Male', '1'=>'Female'],
            'phone_code'=>$phones,
            'alt_phone_code'=>$phones,
            'emergency_phone_code'=>$phones,
            'citizenship'=>$countries,
            'country'=>$countries,
            'interested'=>$YN,
            'shootings'=>$YN,
            'agree'=>$YN,
            'tickets'=>$YN,
            'visa'=>$YN,
            'payment'=>$YN,
            'been_in_ukraine'=>$YN,
            'hear'=>[''=>''] + Config::get('list.hear'),
            'dietary'=>[''=>''] + Config::get('list.dietary'),
            'statusAr' => [''=>''] + $status,
            'docsAr' => $docsAr,
            ));

    }

    public function processing(Request $request)
    {
      // print_r($request->all());
      // die();
      $ret = [];
      $YEAR = $request->y;
      $filters = [];

      // списки и настройки для выборки
      $YN = ['1'=>'Yes','0'=>'No'];
      $E = [''=>'-', '0'=>'-'];
      $phones = array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray());
      $countries = array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray());

      $years = Volunteer::groupBy('year')->pluck('year');
      $columns = Config::get('list.volunteerColumns');
      $session = Config::get('list.table_session');
      $strand = Config::get('list.strands');
      $eng_level = Config::get('list.skill');//array_merge($E, Config::get('list.skill'));
      $other_langs = Config::get('list.other_langs');//array_merge($E, Config::get('list.other_langs'));
      $status = Config::get('list.status_vol');
      $state = Config::get('list.states');
      $documents = Config::get('list.documents');
      $gender = ['0'=>'Male', '1'=>'Female'];
      $phone_code = $phones;
      $alt_phone_code = $phones;
      $emergency_phone_code = $phones;
      $citizenship = $countries;
      $country = $countries;
      $interested = $YN;
      $shootings = $YN;
      $agree = $YN;
      $tickets = $YN;
      $visa = $YN;
      $payment = $YN;
      $been_in_ukraine = $YN;
      $hear = array_merge($E,Config::get('list.hear'));
      $dietary = array_merge($E,Config::get('list.dietary'));

      // готовим сортировки и фильтры
      $dtData = $request->columns;
      $orderData = $request->order;
      /*order[0][column]:3
        order[0][dir]:desc
        */
      if (!$dtData && $filters = request()->session()->get('filters')) {
        $dtData = $filters;
      }

      if ($dtData) {
        $statusSearchData = $dtData[2]['search']['value'];//:5
        $sessSearchData = $dtData[3]['search']['value'];
        $citizenSearchData = $dtData[4]['search']['value'];
        $citizenOldSearchData = $dtData[5]['search']['value'];
        $genderSearchData = $dtData[7]['search']['value'];
        $countrySearchData = $dtData[8]['search']['value'];
        $countryOldSearchData = $dtData[9]['search']['value'];
        $citySearchData = $dtData[10]['search']['value'];
        $emailSearchData = $dtData[11]['search']['value'];
        $skypeSearchData = $dtData[12]['search']['value'];
        $stateSearchData = $dtData[13]['search']['value'];
        // $schoolSearchData = $dtData[14]['search']['value'];
        $beenSearchData = $dtData[18]['search']['value'];
        $capaSearchData = $dtData[19]['search']['value'];
        $engSearchData = $dtData[20]['search']['value'];
        $othSearchData = $dtData[21]['search']['value'];
        $eduSearchData = $dtData[22]['search']['value'];
        $workSearchData = $dtData[23]['search']['value'];
        $wposSearchData = $dtData[24]['search']['value'];
        $priorSearchData = $dtData[25]['search']['value'];
        $hearSearchData = $dtData[26]['search']['value'];
        $hearotherSearchData = $dtData[27]['search']['value'];
        $foodSearchData = $dtData[28]['search']['value'];
        $foodotherSearchData = $dtData[29]['search']['value'];
        $blogSearchData = $dtData[30]['search']['value'];
        $specSearchData = $dtData[31]['search']['value'];
        $locSearchData = $dtData[32]['search']['value'];
        $photoSearchData = $dtData[33]['search']['value'];
        $persSearchData = $dtData[34]['search']['value'];
        $ticketSearchData = $dtData[35]['search']['value'];
        $visaSearchData = $dtData[36]['search']['value'];
        $paySearchData = $dtData[37]['search']['value'];
        $docsSearchData = $dtData[38]['search']['value'];
        $manSearchData = $dtData[39]['search']['value'];
        $mancommentSearchData = $dtData[40]['search']['value'];

        $filters = $dtData;
        request()->session()->put('filters', $filters);
      }

      $srchData = $request->search;
      if ($srchData && trim($srchData['value']) != '') {
        $srchStr = trim($srchData['value']);
      }

      // делаем запрос
      if ($YEAR == 'All') {
        $allVolunteersPre = Volunteer::orderBy('id', 'desc');
      } else {
        $allVolunteersPre = Volunteer::where('year', $YEAR)->orderBy('id', 'desc');
      }

      if (isset($statusSearchData) && trim($statusSearchData) != '') {
        $allVolunteersPre->where('status', $statusSearchData);
      }

      if (isset($sessSearchData) && trim($sessSearchData) != '') {
        $allVolunteersPre->where('session', 'like', '%' . $sessSearchData . '%');
      }

      if (isset($docsSearchData) && trim($docsSearchData) != '') {
        $allVolunteersPre->where('documents', 'like', '%' . $docsSearchData . '%');
      }

      if (isset($genderSearchData) && trim($genderSearchData) != '') {
        $allVolunteersPre->where('gender', $genderSearchData);
      }

      if (isset($engSearchData) && trim($engSearchData) != '') {
        $allVolunteersPre->where('eng_level', $engSearchData+1);
      }
      if (isset($othSearchData) && trim($othSearchData) != '') {
        $allVolunteersPre->where('other_langs', $othSearchData);
      }
      if (isset($stateSearchData) && trim($stateSearchData) != '') {
        $allVolunteersPre->where('state', $stateSearchData)
                          ->orWhere('state', 'like', $stateSearchData . ',%')
                          ->orWhere('state', 'like', '%,' . $stateSearchData)
                          ->orWhere('state', 'like', '%,' . $stateSearchData . ',%');
      }

      // if (isset($stateSearchData) && trim($stateSearchData) != '' && isset($state[$stateSearchData])) {
      //   $allVolunteersPre->where('state', 'like', '%' . $state[$stateSearchData] . '%');
      // }
      if (isset($countrySearchData) && trim($countrySearchData) != '') {
        $allVolunteersPre->where('country', 'like', '%' . $countrySearchData . '%');
      }
      if (isset($countryOldSearchData) && trim($countryOldSearchData) != '') {
        $allVolunteersPre->where('country_old', 'like', '%' . $countryOldSearchData . '%');
      }
      if (isset($citizenSearchData) && trim($citizenSearchData) != '') {
        $allVolunteersPre->where('citizenship', $citizenSearchData);
      }
      if (isset($citizenOldSearchData) && trim($citizenOldSearchData) != '') {
        $allVolunteersPre->where('citizenship_old', 'like', '%' . $citizenOldSearchData . '%');
      }
      if (isset($citySearchData) && trim($citySearchData) != '') {
        $allVolunteersPre->where('city', 'like', '%' . $citySearchData . '%');
      }
      if (isset($emailSearchData) && trim($emailSearchData) != '') {
        $allVolunteersPre->where('email', 'like', '%' . $emailSearchData . '%');
      }
      if (isset($skypeSearchData) && trim($skypeSearchData) != '') {
        $allVolunteersPre->where('skype', 'like', '%' . $skypeSearchData . '%');
      }
      if (isset($beenSearchData) && trim($beenSearchData) != '') {
        $allVolunteersPre->where('been_in_ukraine', $beenSearchData);
      }
      if (isset($capaSearchData) && trim($capaSearchData) != '') {
        $allVolunteersPre->where('capacity', 'like', '%' . $capaSearchData . '%');
      }
      if (isset($eduSearchData) && trim($eduSearchData) != '') {
        $allVolunteersPre->where('education', 'like', '%' . $eduSearchData . '%');
      }
      if (isset($workSearchData) && trim($workSearchData) != '') {
        $allVolunteersPre->where('workplace', 'like', '%' . $workSearchData . '%');
      }
      if (isset($wposSearchData) && trim($wposSearchData) != '') {
        $allVolunteersPre->where('workplace_position', 'like', '%' . $wposSearchData . '%');
      }
      if (isset($priorSearchData) && trim($priorSearchData) != '') {
        $allVolunteersPre->where('strand', 'like', '%' . $priorSearchData . '%');
      }
      if (isset($hearSearchData) && trim($hearSearchData) != '') {
        $allVolunteersPre->where('hear', 'like', '%' . $hearSearchData . '%');
      }
      if (isset($hearotherSearchData) && trim($hearotherSearchData) != '') {
        $allVolunteersPre->where('hear_other', 'like', '%' . $hearotherSearchData . '%');
      }
      if (isset($foodSearchData) && trim($foodSearchData) != '') {
        $allVolunteersPre->where('dietary', $foodSearchData);
      }
      if (isset($foodotherSearchData) && trim($foodotherSearchData) != '') {
        $allVolunteersPre->where('dietary_other', 'like', '%' . $foodotherSearchData . '%');
      }
      if (isset($blogSearchData) && trim($blogSearchData) != '') {
        $allVolunteersPre->where('interested', 'like', '%' . $blogSearchData . '%');
      }
      if (isset($specSearchData) && trim($specSearchData) != '') {
        $allVolunteersPre->where('special', 'like', '%' . $specSearchData . '%');
      }
      if (isset($locSearchData) && trim($locSearchData) != '') {
        $allVolunteersPre->where('preferences', 'like', '%' . $locSearchData . '%');
      }
      if (isset($photoSearchData) && trim($photoSearchData) != '') {
        $allVolunteersPre->where('shootings', $photoSearchData);
      }
      if (isset($persSearchData) && trim($persSearchData) != '') {
        $allVolunteersPre->where('agree', $persSearchData);
      }
      if (isset($ticketSearchData) && trim($ticketSearchData) != '') {
        $allVolunteersPre->where('tickets', $ticketSearchData);
      }
      if (isset($visaSearchData) && trim($visaSearchData) != '') {
        $allVolunteersPre->where('visa', $visaSearchData);
      }
      if (isset($paySearchData) && trim($paySearchData) != '') {
        $allVolunteersPre->where('payment', $paySearchData);
      }
      if (isset($manSearchData) && trim($manSearchData) != '') {
        $allVolunteersPre->where('manager', 'like', '%' . $manSearchData . '%');
      }
      if (isset($mancommentSearchData) && trim($mancommentSearchData) != '') {
        $allVolunteersPre->where('comment', 'like', '%' . $mancommentSearchData . '%');
      }

      // print_r($allVolunteersPre->toSql());
      // die();
      // $volunteers = $volunteersPre->get();
      $allVolunteers = $allVolunteersPre->get();

      $len = $request->length;
      $start = $request->start;
      $i = 0;

      foreach ($allVolunteers as $V) {
        // $docs = '';
        // foreach($V->documents as $value) {
        //   if (!empty($value)) {
        //     $docs.= $documents[$value] . ',';
        //   }
        // }
        $sessions = '';
        foreach($V->session as $value) {
          if (!empty($value) && isset($session[$value])) {
            $sessions.= $session[$value] . ',';
          }
        }

        $tmpAr = [
          $V->id,
          "<a href=\"" . route('admin-volunteer-edit', ['id' => $V->id] ) . "\" target=\"_blank\">" .  $V->first_name . " " . $V->last_name . "</a>",
          ($V->status === NULL) ? '-' : $status[(int)$V->status],
          $sessions,

        ];

        foreach ($columns as $colGroup) {
          foreach ($colGroup as $cKey => $col) {
            if (!$col['hidden']) continue;

            if ($cKey == 'schools') {
              $tmpStr = '';
              foreach ($V->schools as $S) {
                $tmpStr.= '<a href="' . route('admin-schools-edit', $S->id) . '" target="_blank">' . $S->title . '</a><br/>';
              }
              $tmpAr[] = $tmpStr;
              continue;
            }

            if ($cKey == 'shorttype') {
              $tmpAr[] = $V->type;
              continue;
            }

            if ($cKey == 'created_at') {
              $tmpAr[] = $V->$cKey->format('d.m.Y H:i');
              continue;
            }

            if (isset($col['listed'])) {
              if ($cKey == 'session' || $cKey == 'documents' || $cKey == 'state') {
                $tmpStr = '';
                foreach($V->$cKey as $value) {
                  if (!empty($$cKey[$value]) && isset($$cKey[$value])) {
                    $tmpStr.= isset($$cKey[$value]) ? $$cKey[$value] . ',' : ',';
                  } else {
                    $tmpStr.= '**,'; // debug purposes
                  }
                }
                $tmpAr[] = $tmpStr;
              } else {
                if (isset($$cKey[$V->$cKey])) {
                  $tmpAr[] = ($V->$cKey === NULL) ? '-' : $$cKey[$V->$cKey];
                } else {
                  $tmpAr[] = '--';
                }
              }
            } else {
              $tmpAr[] = $V->$cKey;
            }
          }
        }

        // поиск по конкретным колонкам
        /*

        data-key="schools" data-hidden="true" data-column="14"
        */

        // если задана строка поиска, проверяем на соответствие
        if (isset($srchStr)) {
          $srchStr = urldecode($srchStr);
          // проверяем, есть ли нужный текст в данных
          $tmpStr = strtolower(implode(' ', $tmpAr));
          if (strpos($tmpStr, strtolower($srchStr)) === false) {
            continue;
          }
        } else if (isset($schoolSearchData) && trim($schoolSearchData) != '') {
          if (strpos(strtolower($tmpAr[14]), strtolower($schoolSearchData)) === false) {
            continue;
          }
        }

        $tmpAr[] = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-' . $V->id . '"><span class="glyphicon glyphicon-remove"></button>

          <!-- Modal -->
          <div id="myModal-' . $V->id . '" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing volunteer ' . $V->first_name . ' ' . $V->last_name . ' for ' . $V->year . ' year</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                  <a id="delete" class="btn btn-primary" href="' . route('admin-volunteer-delete', ['id' => $V->id]) . '">Yes</a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>
            </div>
          </div>';

        $ret[] = $tmpAr;
        $i++;
      }
      $count = $i;

      // ручная сортировка массива
      if (isset($orderData[0]) && isset($orderData[0]['column'])) {
        $oBy = $orderData[0]['column'];
        $oDir = $orderData[0]['dir'];
      } else {
        $oBy = '0';
        $oDir = 'desc';
      }

      uasort($ret, function($a, $b) use ($oBy, $oDir) {
        if ($oBy == 0) {
          $ret = intval($a[0] - $b[0]);
        } else {
          $ret = strcasecmp(strip_tags($a[$oBy]), strip_tags($b[$oBy]));
        }


        if ($oDir == 'desc') {
          $ret = -1 * $ret;
        }
        return $ret;
      });

      // сохраняем в сессию результаты фильтрации и сортировки
      $forStore = [];
      foreach ($ret as $key => $valAr) {
        $forStore[$valAr[0]] = $valAr[0];
      }

      request()->session()->put('vids', $forStore);


      // ручная пагинация
      $ret = array_slice($ret, $start, $len);

      return response()->json([
        "debug" => $forStore,
        "draw" => $request->draw,
        "recordsTotal" =>  $count,
        "recordsFiltered" =>  $count,
        "data" => $ret,
      ]);
    }

    public function saveUserCols(Request $request)
    {
      $active = $request->a;
      $user = $request->user();

      if (!$user || !$active) return;

      $fs['cols'] = json_encode($active);
      $fs['type'] = 'volvis';
      $fs['user_id'] = $user->id;

      $uv = UserExport::where('type', 'volvis')
                  ->where('user_id', $user->id)
                  ->first();
      if (!$uv) {
          $uv = UserExport::create($fs);
      } else {
          $uv->cols = $fs['cols'];
          $uv->save();
      }
    }

    public function create()
    {
        if(Auth::user()->isAdmin()) {
            $currentYear = new Volunteer();
            $action = route('volunteers-store');
            $title = 'Add volunteer';

            $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                                ->where('role_id',2)
                                ->get()
                                ->pluck('email','email')
                                ->toArray();

            return view('admin.volunteer.form',array(
                'currentYear' => $currentYear,
                'action' => $action,
                'title' => $title,
                'yes_no'=>Config::get('list.yes_no'),
                'gender' => ['0'=>'Male', '1'=>'Female'],
                'skill'=>Config::get('list.skill'),
                'session'=>Config::get('list.session'),
                'strand'=>Config::get('list.strands'),
                'states'=>Config::get('list.states'),
                'status'=>Config::get('list.status_vol'),
                'phones'=>DB::table('countries')->pluck('phone_code','iso'),
                'other_langs'=>array_merge(['0'=>''],Config::get('list.other_langs')),
                'hear'=>array_merge(['0'=>'-'],Config::get('list.hear')),
                'dietary'=>array_merge(['0'=>'-'],Config::get('list.dietary')),
                'countries'=>DB::table('countries')->pluck('nicename','iso'),
                'managers' => array_merge([''=>'-'],$managers),
                )
            );
        }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isAdmin()) {
            $volData = $request->except( '_token' );
            $volData['volunteer_id'] = Volunteer::max( 'volunteer_id' ) + 1;

            $tmpYear = date('Y');
            if (date('m') >= 9) {
              $year = $tmpYear . '-' . (int)($tmpYear + 1);
            } else {
              $year = (int)$tmpYear - 1 . '-' . $tmpYear;
            }

            $volData['year'] = $year;
            $volunteer = Volunteer::create( $volData );
            return redirect()->route('admin-volunteer-edit', ['id' => $volunteer->id]);
        }
        return view('admin.permission');
    }


    public function edit($id)
    {
        if ( Auth::user()->isAdmin() ) {
            $currentYear = Volunteer::find( $id );
            if (!$currentYear) {
              dd('Volunteer with ID ' . $id . ' doesn`t exists');
            }
            $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                                ->where('role_id',2)
                                ->get()
                                ->pluck('email','email')
                                ->toArray();

            $E = ['0'=>'-'];
            return view('admin.volunteer.form',array(
                'similar' => $currentYear->similar,
                'YEAR' => $currentYear->year,
                'currentYear' => $currentYear,
                'action' => route( 'volunteer-update', ['id' => $id] ),
                'title' => 'Edit volunteer',
                'years' => Volunteer::where( 'volunteer_id', $currentYear->volunteer_id )->get(),
                'documents' => Config::get('list.documents'),
                'gender' => ['0'=>'Male', '1'=>'Female'],
                'yes_no'=>Config::get('list.yes_no'),
                'skill'=>Config::get('list.skill'),
                'session'=>Config::get('list.all_session'),
                'strand'=>Config::get('list.strands'),
                'states'=>Config::get('list.states'),
                'status'=>Config::get('list.status_vol'),
                'phones'=>array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray()),
                'other_langs'=>array_merge(['0'=>''],Config::get('list.other_langs')),
                'hear'=>array_merge($E,Config::get('list.hear')),
                'dietary'=>array_merge($E,Config::get('list.dietary')),
                'countries'=>array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray()),
                'managers' => array_merge([''=>'-'],$managers),
                'mail'=>Config::get('list.mails'),
                )
            );
        }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Auth::user()->isAdmin() ) {
            $currentYear = Volunteer::find( $id );

            if ( isset($request->add_year) && $request->add_year != '0') {
                // добавляем новый год для волонтера
                $yearExists = Volunteer::where( ['volunteer_id' => $currentYear->volunteer_id, 'year' => $request->add_year] )->first();
                if ( $yearExists ) {
                    die('This YEAR already exists');
                }
                $newYear = Volunteer::create([
                    'volunteer_id' => $currentYear->volunteer_id,
                    'year' => $request->add_year,
                    'first_name' => $currentYear->first_name,
                    'last_name' => $currentYear->last_name,
                    'passport' => $currentYear->passport,
                    'citizenship' => $currentYear->citizenship,
                    'birthday' => $currentYear->birthday,
                    'gender' => $currentYear->gender,
                    'country' => $currentYear->country,
                    'city' => $currentYear->city,
                    'email' => $currentYear->email,
                    'phone_code' => $currentYear->phone_code,
                    'phone' => $currentYear->phone,
                    'alt_phone_code' => $currentYear->alt_phone_code,
                    'alt_phone' => $currentYear->alt_phone,
                    'dietary' => $currentYear->dietary,
                    'dietary_other' => $currentYear->dietary_other,
                    'special' => $currentYear->special,
                ]);

                return redirect( route( 'admin-volunteer-edit', ['id' => $newYear->id] ) );
            }

            $yearEdit = Volunteer::find( $id );
            $yearData = $request->except( '_token', 'add_year', 'created_at' );

            if ( isset($yearData['schools']) ) {
              $volSchools = SchoolVolunteer::where( 'volunteer_id', $yearEdit->id )->get();
              $hadSchools = !empty( $volSchools->toArray() );
              foreach ( $volSchools as $VS ) {
                $VS->delete();
                $delSchool = School::find( $VS->school_year_id );
                if ( $delSchool && empty( $delSchool->volunteers->toArray() ) ) {
                  $delSchool->status = 6;
                  $delSchool->save();
                }
              }

              if ( !empty( $yearData['schools'] ) ) {
                foreach ( $yearData['schools'] as $schoolId ) {
                  SchoolVolunteer::create( [
                    'school_year_id' => $schoolId,
                    'volunteer_id' => $yearEdit->id
                    ] );
                  $school = School::find( $schoolId );
                  $school->status = 5;
                  $school->save();
                }
                if ( $yearData['status'] != 11 ) {
                  $yearData['status'] = 7;
                }
              } elseif ( $hadSchools ) {
                $yearData['status'] = 6;
              }
            }
            unset( $yearData['schools'] );

            $yearEdit->fill( $yearData )->save();
            return redirect()->route('admin-volunteer-edit', ['id' => $currentYear->id]);
        }
        return view('admin.permission');

    }
    public function getSchools(Request $request, $id) {
      $volunteer = Volunteer::find( $id );
      return response()->json( $volunteer->getStateSchools( $request->state ) );
    }
    public function mergeForms($id, $similar) {
      $vol = Volunteer::find( $id );
      $similarVol = Volunteer::find( $similar );
      if ( $similarVol->year == $vol->year ) {
        $data = $vol->toArray();
        unset($data['id'],$data['volunteer_id'],$data['type'],$data['created_at'],$data['updated_at']);
        $similarVol->update($data);
        $vol->delete();
        return redirect()->route('admin-volunteer-edit', ['id' => $similarVol->id]);
      } else {
        $vol->volunteer_id = $similarVol->volunteer_id;
        $vol->type = 0;
        $vol->save();
        return redirect()->route('admin-volunteer-edit', ['id' => $vol->id]);
      }
    }
    public function checkForm(Request $request) {
      $curVol = Volunteer::find($request->currentId);
      $query = Volunteer::where('id', '<>', $request->currentId)
                          ->where('type', 0);
      if ( $request->firstname != '' ) {
        $query->where('first_name', $request->firstname);
      }
      if ( $request->lastname != '' ) {
        $query->where('last_name', $request->lastname);
      }
      $volunteers = $query->get();
      if ( !$volunteers ) {
        $response = "<h3 id='formResult'>No form found</h3>";
      } else {
        $response = "";
        foreach ( $volunteers as $V ) {
          $tmp = [];
          if ( $V->year == $curVol->year ) {
            $response .= "<h3>The long form: " . $V->first_name . " " . $V->last_name . " " . $V->year . " year</h3>
                          <a href='" . route('merge-volunteer-forms', ['id'=>$curVol->id, 'similar'=>$V->id]) . "' class='btn btn-default'>Upgrade</a>";
          } else {
            $response .= "<h3>The long form: " . $V->first_name . " " . $V->last_name . " " . $V->year . " year</h3>
                          <a href='" . route('merge-volunteer-forms', ['id'=>$curVol->id, 'similar'=>$V->id]) . "' class='btn btn-success'>Merge</a>";
          }
        }
      }
      return $response;
    }
    public function destroy($id)
    {
      $refer = request()->headers->get('referer');
      $single = strpos( $refer, 'edit' ) != false;

      if ( Auth::user()->isAdmin() ) {
          $volunteer = Volunteer::find( $id );
          $volunteer_id = $volunteer->volunteer_id;
          $volunteer->delete();
          $volLeast = Volunteer::where( 'volunteer_id', $volunteer_id )->orderBy( 'year', 'desc' )->first();
          $YEAR = $volunteer->year;
          $volunteer->delete();
          if ( $single && $volLeast != null ) {
            return redirect()->route('admin-volunteer-edit', ['id' => $volLeast->id] );
          } else {
            return redirect()->route( 'volunteers', $YEAR );
          }
      }
      return view('admin.permission');
    }

    public function export(Request $request, $YEAR) {
        $user = $request->user();
        $exportColumns = $request->col;
        // print_r($exportColumns);
        // dd();

        $forStore = request()->session()->get('vids');
        // dd($forStore, $request->all());

        $exportData = [
          'cols' => json_encode( array_values($exportColumns) ),
          'type' => 'vol',
          'user_id' => $user->id,
        ];
        $userExport = UserExport::where( ['type' => 'vol', 'user_id' => $user->id] )->first();
        if ( !$userExport ) {
            $userExport = UserExport::create( $exportData );
        } else {
            $userExport->cols = $exportData['cols'];
            $userExport->save();
        }

        // списки и настройки для выборки
        $YN = ['1'=>'Yes','0'=>'No'];
        $E = [''=>'-', '0'=>'-'];
        $phones = array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray());
        $countries = array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray());
        $state = array_merge($E,Config::get('list.states'));
        $years = Volunteer::groupBy('year')->pluck('year');
        $columns = Config::get('list.volunteerColumns');
        $session = Config::get('list.all_session');
        $strand = Config::get('list.strands');
        $eng_level = Config::get('list.skill');//array_merge($E, Config::get('list.skill'));
        $other_langs = Config::get('list.other_langs');//array_merge($E, Config::get('list.other_langs'));
        $status = Config::get('list.status_vol');
        $state = Config::get('list.states');
        $documents = Config::get('list.documents');
        $gender = ['0'=>'Male', '1'=>'Female'];
        $hear = array_merge($E,Config::get('list.hear'));
        $dietary = array_merge($E,Config::get('list.dietary'));

        $E = ['0'=>'-'];
        $yes_no = array('1'=>'Yes','0'=>'No');
        // $eng_level = array_merge($E,Config::get('list.skill'));
        // $other_langs = array_merge($E,Config::get('list.other_langs'));
        // $session = Config::get('list.all_session');
        // $strand = Config::get('list.strands');
        // $status = Config::get('list.status_vol');
        // $documents = Config::get('list.documents');
        // $columns = Config::get('list.volunteerColumns');
        // $gender = ['0'=>'Male','1'=>'Female'];
        // $state = Config::get('list.states');
        $manager = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                            ->where('role_id',2)
                            ->get()
                            ->pluck('email','email')
                            ->toArray();
        $interested = $shootings = $agree = $tickets = $visa = $payment = $been_in_ukraine =
          ['0'=>'No','1'=>'Yes'];
        $citizenship = $country = array_merge($E,DB::table('countries')->pluck('nicename','iso')->toArray());
        $phone_code = $alt_phone_code = $emergency_phone_code =
          array_merge($E,DB::table('countries')->pluck('phone_code','iso')->toArray());
        // $other_langs = array_merge($E,Config::get('list.other_langs'));
        // $hear = array_merge($E,Config::get('list.hear'));
        // $dietary = array_merge($E,Config::get('list.dietary'));

        if ($forStore !== null) {
          $volunteersEloq = Volunteer::whereIn('id', $forStore);
        } else if ( $YEAR == 'All') {
          $volunteersEloq = Volunteer::orderBy('id');
          $exportColumns['year'] = 1;
        } else {
          $volunteersEloq = Volunteer::where('year', $YEAR);
        }

        // if ( $request->has( 'filters' ) ) {
        //   foreach ( $request->filters as $key => $value ) {
        //     $coded = array_search( $value, $$key, true );
        //     $volunteersEloq = $volunteersEloq->where( function ($query) use ($key, $coded) {
        //         $query->where( $key, $coded )
        //               ->orWhere( $key, 'LIKE', $coded . ',%' )
        //               ->orWhere( $key, 'LIKE', '%,' . $coded )
        //               ->orWhere( $key, 'LIKE', '%,' . $coded . ',%');
        //       });
        //   }
        // }
        $volunteers = $volunteersEloq->get();

        $exportOutput = [];
        foreach ( $volunteers as $key => $V ) {
            $rowData = [];
            foreach ( $columns as $block => $cols ) {
                foreach ( $cols as $key => $col ) {
                    $title = $col['label'];
                    if ( !isset( $exportColumns[$key] )/* || !$exportColumns[$key]*/ ) {
                        continue;
                    }
                    if ( $key == 'schools' ) {
                      $tmpArr = [];
                      foreach ( $V->schools as $S ) {
                        $tmpArr[] = $S->title . '(ID: ' . $S->id . ')';
                      }
                      $rowData[$title] = implode( ', ', $tmpArr);
                      continue;
                    }
                    if ( $key == 'documents' || $key == 'session' || $key == 'state') {
                      $tmpArr = [];
                      foreach ( $V->$key as $value ) {
                        if ( !empty( $value ) ) {
                          $tmpArr[] = (isset($$key[$value])) ? $$key[$value] : '-';
                        }
                      }
                      $rowData[$title] = implode( ', ', $tmpArr);
                    } elseif ( $key == 'created_at' ) {
                      $rowData[$title] = $V->$key->format('d.m.Y H:i');
                    } elseif ( isset( $col['listed'] ) ) {
                      $rowData[$title] = ($V->$key != null && isset($$key[$V->$key])) ? $$key[$V->$key] : '-';
                    } else {
                      $rowData[$title] = $V->$key;
                    }
                }
            }
            $exportOutput[] = $rowData;
        }

        Excel::create('volunteer', function($excel) use($exportOutput) {
            // $exportOutput = array_merge($exportOutput, $exportOutput);
            $excel->setTitle('Volunteer export');
            $excel->sheet('Volunteer', function($sheet) use($exportOutput) {
                $sheet->fromArray($exportOutput, NULL, 'A1', false, true);
            });
        })->download('xlsx');
    }

    public function sendEmail($id, $mail){
        $volunteer = Volunteer::find( $id );
        $sended = $volunteer->mails;
        if ( !in_array( $mail, $sended ) ) {
          $sended[] = $mail;
        }

        $volunteer->mails = $sended;
        $volunteer->save();

        $to  = $volunteer->email; // обратите внимание на запятую
        $subject = Config::get( 'list.mails' )[$mail]['subject'];

        // текст письма
        $message = view( 'emails.email' . $mail );
        // Mail::send( 'emails.email' . $mail, [], function($message) use($subject, $to) {
        //   $message->subject($subject);
        //   $message->to($to);
        // });
        // return $message;

        // Для отправки HTML-письма должен быть установлен заголовок Content-type
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

        // Дополнительные заголовки
        $headers .= 'From: ' . Config::get('mail.from.name') . ' <' . Config::get('mail.from.address') . '>' . "\r\n";

        // Отправляем
        $result = mail( $to, $subject, $message, $headers );

        return response()->json(['sended' => true]);
    }
}
