<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\CategoryTranslate;
use DB;
use Config;
use Auth;
use LaravelLocalization;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.categories.index',array(
                            'categories'=>Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
                            ->where('lang',LaravelLocalization::getCurrentLocale())->groupBy('category_id')
                            ->get()));
        }
        return view('admin.permission');    
    }
    public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);        
       $st = preg_replace('|\s+|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.

       $slug;
       $str = self::transliteration($st);
       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }
    public function create()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.categories.create',array('category_id'=>isset($_GET['category_id'])?$_GET['category_id']:"",'layouts'=>$this->get_layouts()));
        }
        return view('admin.permission');    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if(Auth::user()->isAdmin()){
            $model = new Category();
            $model->layout=$request->layout;
            $model->save();
            $translate = new CategoryTranslate();
            $translate->slug=($request->slug)?$this->makeSlug($request->slug):$this->makeSlug($request->title);
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
            $translate->title = $request['title'];
            $translate->category_id=!empty($request->category_id)?$request->category_id:$model->id;
            $translate->lang = LaravelLocalization::getCurrentLocale();
            $translate->save();
            return redirect()->route('categories-edit', [!empty($request->category_id)?$request->category_id:$model->id]);
        }
        return view('admin.permission');    
    }
    public function edit($id)
    {
        if(Auth::user()->isAdmin()){
          $have_translate=0;
        if(DB::select('SELECT COUNT(category_id) as count FROM `categoryTranslate` WHERE category_id='.$id.' GROUP BY category_id')[0]->count>1)
          $have_translate=1;
          $have_langs=DB::select('SELECT lang FROM `categoryTranslate` WHERE category_id='.$id);
          $categories=DB::select("SELECT * FROM `categoryTranslate` at left join categories as a on a.id=at.category_id WHERE category_id in (SELECT category_id FROM `categoryTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY category_id HAVING count(category_id)<=1 ");
            return view('admin.categories.update',array(
                            'have_langs'=>$have_langs,
                            'layouts'=>$this->get_layouts(),
                            'have_translate'=>$have_translate,
                            'langs'=>Config::get('app.locales'),
                            'categories'=>$categories,
                            'category'=>Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
                            ->where('lang',LaravelLocalization::getCurrentLocale())->where('categories.id',$id)->groupBy('category_id')
                            ->first()));
        }
        return view('admin.permission');   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        if(Auth::user()->isAdmin()){

            if($request->category_id!=$id){
              $model= Category::find($request->category_id);
              $translate= new CategoryTranslate();
              $translate->lang=LaravelLocalization::getCurrentLocale();
              $translate->category_id=$request->category_id;
              $old_translate= CategoryTranslate::where('category_id',$request->old_id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();      
            }
            else{
              $model= Category::find($id);
              $translate = CategoryTranslate::where('category_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
              $translate->category_id=$model->id;
            }
            $model->layout=$request->layout;
            $model->save();
            $translate->slug=($request->slug)?$this->makeSlug($request->slug):$this->makeSlug($request->title);
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
            $translate->title = $request['title'];
            $translate->lang = LaravelLocalization::getCurrentLocale();
            $translate->save();
            $this->is_empty();
           if($request->category_id!=$model->id &&$request->category_id!="")
            return redirect()->route('categories-edit', [$request->category_id]);
            else
                return redirect()->route('categories-edit', [$model->id]);
            }
        return view('admin.permission');   
    }
    public function destroy($id)
    {
      if(Auth::user()->isAdmin()){
        $translate= CategoryTranslate::where('category_id',$id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        return redirect()->route('categories');
      }
        return view('admin.permission');    
    }
    public function unlink(Request $request, $id)
    {

      if(Auth::user()->isAdmin()){

        $old= Category::find($id);
        $model= new Category();
        $model->save();
        $old_translate=CategoryTranslate::where('category_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
        // var_dump($old_translate);die;
        $translate=new CategoryTranslate();
        $translate->lang=$old_translate->lang;
        $translate->category_id=$model->id;

        $translate->slug=!empty($old_translate->slug)?$old_translate->slug:$this->makeSlug($old_translate->title);
        $translate->seo_title=$old_translate->seo_title;
        $translate->keywords=$old_translate->keywords;

        $translate->description=$old_translate->description;
        $translate->title=$old_translate->title;
        $translate->save();
        CategoryTranslate::where('category_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        
        
        return redirect()->route('categories-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    
    public function is_empty()
    {
      $ids=DB::select('SELECT A.id FROM categories AS A LEFT OUTER JOIN categoryTranslate AS B ON A.id = B.category_id WHERE B.category_id IS NULL ');
      $ids_array=array();
      foreach ($ids as $key => $value) {
       $ids_array[]=$value->id;
      }
      Category::destroy($ids_array);
    }
    public function get_layouts(){
      $tmp =  array_diff(scandir('../resources/views/layouts'), array('..', '.'));
      $files=array();
      foreach ($tmp as $key => $value) {
        $files['layouts.'.explode('.', $value)[0]]=explode('.', $value)[0];
      }
      return $files;
    }

}
