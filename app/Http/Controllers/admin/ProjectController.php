<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProjectRequest;

use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectTranslate;
use DB;
use Config;
use LaravelLocalization;
use Auth;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
         return view('admin.projects.index',array(
                        'projects'=>Project::leftJoin('projectTranslate', function($join) { $join->on('projects.id', '=', 'projectTranslate.project_id'); })
                        ->where('lang',LaravelLocalization::getCurrentLocale())->groupBy('project_id')
                        ->get()));
        }
        return view('admin.permission');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.projects.create',array('project_id'=>isset($_GET['project_id'])?$_GET['project_id']:"",'layouts'=>$this->get_layouts()));
        }
        return view('admin.permission');    
    }
    public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);        
       $st = preg_replace('|\s+|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.

       $slug;
       $str = self::transliteration($st);
       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        if(Auth::user()->isAdmin()){
            $model= new Project();
            $model->layout=$request->layout;
            $model->save();
            $translate= new ProjectTranslate();
            $translate->title=$request->name;
            $translate->accordion=$request->accordion;
            $translate->slug=!empty($request->slug)?$request->slug:$this->makeSlug($request->title);
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
            $translate->project_id=!empty($request->project_id)?$request->project_id:$model->id;
            $translate->text=$request->text;
            $translate->lang=LaravelLocalization::getCurrentLocale();
            $translate->save();
            return redirect()->route('projects-edit', [!empty($request->project_id)?$request->project_id:$model->id]);
        }
        return view('admin.permission');    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isAdmin()){
            $projects=DB::select("SELECT * FROM projects p left join projectTranslate as pt on p.id=pt.project_id WHERE project_id in (SELECT project_id FROM `projectTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY project_id");
            $have_translate=0;
        if(DB::select('SELECT COUNT(project_id) as count FROM `projectTranslate` WHERE project_id='.$id.' GROUP BY project_id')[0]->count>1)
          $have_translate=1;
        $have_langs=DB::select('SELECT lang FROM `projectTranslate` WHERE project_id='.$id);
        $projects=DB::select("SELECT * FROM `projectTranslate` at left join projects as a on a.id=at.project_id WHERE project_id in (SELECT project_id FROM `projectTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY project_id HAVING count(project_id)<=1 ");
            return view('admin.projects.update',array(
                'have_langs'=>$have_langs,
                'have_translate'=>$have_translate,
                'langs'=>Config::get('app.locales'),
                'layouts'=>$this->get_layouts(),
                'projects'=>$projects,
                'project'=>Project::leftJoin('projectTranslate', function($join) {
                                          $join->on('projects.id', '=', 'projectTranslate.project_id');
                                        })->where('lang',LaravelLocalization::getCurrentLocale())->where('project_id',$id)->first()
            ));
        }
        return view('admin.permission');    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        if(Auth::user()->isAdmin()){
            if($request->project_id!=$id){
              $model= Project::find($request->project_id);
              $translate= new ProjectTranslate();
              $translate->lang=LaravelLocalization::getCurrentLocale();
              $translate->project_id=$request->project_id;
              // project::find($request->old_id)->delete();
              $old_translate= ProjectTranslate::where('project_id',$request->old_id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();        }
            else{
              $model= Project::find($id);
              $translate = ProjectTranslate::where('project_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
              $translate->project_id=$model->id;
            }
            $model->layout=$request->layout;
            $model->save();
            $translate->title=$request->title;
            $translate->slug=!empty($request->slug)?$request->slug:$this->makeSlug($request->title);
            $translate->accordion=$request->accordion;
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
                //ProjectTranslate::where('project_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
            $translate->project_id=$model->id;
            $translate->text=$request->text;

           
            $translate->save();


            $this->is_empty();
            if($request->project_id!=$model->id &&$request->project_id!="")
            return redirect()->route('projects-edit', [$request->project_id]);
            else
                return redirect('/'.$request->language.'/admin/projects/edit/'.$model->id);
            }
            return view('admin.permission');    

    }
    public function unlink(Request $request, $id)
    {

      if(Auth::user()->isAdmin()){

        $old= Project::find($id);
        $model= new Project();
        $model->save();
        $old_translate=ProjectTranslate::where('project_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
        $translate=new projectTranslate();
        $translate->title=$old_translate->title;
        $translate->lang=$old_translate->lang;
        $translate->project_id=$model->id;
        $translate->accordion=$old_translate->accordion;
        $translate->slug=!empty($old_translate->slug)?$old_translate->slug:$this->makeSlug($old_translate->title);
        $translate->seo_title=$old_translate->seo_title;
        $translate->keywords=$old_translate->keywords;

        $translate->description=$old_translate->description;
        $translate->text=$old_translate->text;
        $translate->save();
        ProjectTranslate::where('project_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        
        
        return redirect()->route('projects-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isAdmin()){
        $translate= ProjectTranslate::where('project_id',$id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        return redirect()->route('projects');
      }
        return view('admin.permission');    
    }
    public function is_empty()
    {
      $ids=DB::select('SELECT A.id FROM projects AS A LEFT OUTER JOIN projectTranslate AS B ON A.id = B.project_id WHERE B.project_id IS NULL ');
      $ids_array=array();
      foreach ($ids as $key => $value) {
       $ids_array[]=$value->id;
      }
      // var_dump($ids_array);die;
      project::destroy($ids_array);
    }
    public function get_layouts(){
      $tmp =  array_diff(scandir('../resources/views/layouts'), array('..', '.'));
      $files=array();
      foreach ($tmp as $key => $value) {
        $files['layouts.'.explode('.', $value)[0]]=explode('.', $value)[0];
      }
      return $files;
    }
}
