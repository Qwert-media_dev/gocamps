<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SchoolRequest;
use App\Http\Controllers\Controller;
use App\School;
use App\Volunteer;
use App\Adress;
use App\User;
use App\UserExport;
use App\SchoolsYear;
use App\SchoolVolunteer;
use DB;
use Config;
use Excel;
use App;
use Auth;
class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $rating=array('0'=>'n/a','1'=>'1','2'=>'2','3'=>'3','6'=>'3.5','4'=>'4','7'=>'4.5','5'=>'5');
    public $exportColumns;

    public function index($YEAR)
    {
        if ($YEAR == 'All') {
          // $schools = School::orderBy('year', 'desc')->groupBy('school_id')->limit(100)->get();
          $schools = School::orderBy('year', 'desc')->get();
        } else {
          $schools = School::where('year', $YEAR)->limit(100)->get();
        }

        $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                            ->where('role_id',2)
                            ->get()
                            ->pluck('email','email')
                            ->toArray();
        $managers[0] = 'Не выбрано';

        return view('admin.schools.index',array(
            'YEAR' => $YEAR,
            'years'=>School::groupBy('year')->pluck('year'),
            'schools'=> $schools,
            'quantity_children'=> ['0'=>'-'] + Config::get('list.quantity_children'),
            'type_camp'=> Config::get('list.camp_type'),
            'state'=> Config::get('list.states'),
            'city_type'=> Config::get('list.city_type'),
            'state_eng'=> Config::get('list.states_eng'),
            'city_type_eng'=> Config::get('list.city_type_eng'),
            'project'=> Config::get('list.project_admin'),
            'session'=> Config::get('list.table_session'),
            'status'=> Config::get('list.status'),
            'camp_lang'=> Config::get('list.camp_lang'),
            'columns' => Config::get('list.schoolColumns'),
            'share_volunteer' => ['1'=>'Taк','0'=>'Ні'],
            'share_camp' => ['1'=>'Taк','0'=>'Ні'],
            'experience' => ['1'=>'Taк','0'=>'Ні'],
            'training' => ['1'=>'Taк','0'=>'Ні'],
            'school_memorandum' => ['1'=>'Taк','0'=>'Ні'],
            'teacher_memorandum' => ['1'=>'Taк','0'=>'Ні'],
            'plan' => ['1'=>'Taк','0'=>'Ні'],
            'report' => ['1'=>'Taк','0'=>'Ні'],
            'paid' => ['1'=>'Taк','0'=>'Ні'],
            'managers' => $managers,
            'training_dates'=>Config::get('list.training_dates'),
            'goc_east'=>Config::get('list.goc_east'),
        ));
    }

    public function index2($YEAR)
    {
        if ($YEAR == 'All') {
          $schools = School::orderBy('id', 'desc')/*->limit(10)*/->get();
          $allSchools = School::orderBy('id', 'desc')->get();
        } else {
          $schools = School::where('year', $YEAR)->orderBy('id', 'desc')/*->limit(10)*/->get();
          $allSchools = School::where('year', $YEAR)->orderBy('id', 'desc')->get();
        }

        // готовим списки всех вариантов для status, state, project
        $statusAr = ['' => ''];
        $stateAr = ['' => ''];
        $prAr = ['' => ''];
        $status = Config::get('list.status');
        $state = Config::get('list.states');
        unset($state[0]);
        $project = Config::get('list.project_admin');

        // foreach ($allSchools as $as) {
        //   if ($as->status) {
        //     $statusAr[$as->status] = $status[$as->status];
        //   }
        //
        //   if ($as->state) {
        //     $stateAr[$as->state] = $state[$as->state];
        //   }
        //
        //   foreach($as->project as $value) {
        //     if(!empty($value)) {
        //       $prAr[$project[$value]] = $project[$value];
        //     }
        //   }
        // }

        $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                            ->where('role_id',2)
                            ->get()
                            ->pluck('email','email')
                            ->toArray();
        $managers = array_merge([''=>''], $managers);

        $uv = UserExport::where('type', 'schoolvis')
                  ->where('user_id', auth()->user()->id)
                  ->first();
        if ($uv) {
          $uv = json_decode($uv->cols, true);
        } else {
          $uv = [];
        }

        $ue = UserExport::where('type', 'school')
                  ->where('user_id', auth()->user()->id)
                  ->first();
        if ($ue) {
          $ue = json_decode($ue->cols, true);
        } else {
          $ue = [];
        }

        $filters = request()->session()->get('sfilters');
        $E = [''=>'-'];

        $inputs = [
          'city',
        ];
        $selects = [
          'city_type',
          'manager',
          'quantity_children',
          'share_camp',
          'type_camp',
          'paid',
          'share_volunteer',
          'rating',
          'session',
          'training',
          'camp_lang',
          'school_memorandum',
          'plan',
          'teacher_memorandum',
          'report',
          'training_dates',
          'goc_east',
        ];

        return view('admin.schools.index2',array(
            'YEAR' => $YEAR,
            'years'=>School::groupBy('year')->pluck('year'),
            'schools'=> $schools,
            'ue' => $ue,
            'uv' => $uv,
            'filters' => $filters,
            'inputs' => $inputs,
            'selects' => $selects,
            'quantity_children'=> [''=>''] + Config::get('list.quantity_children'),
            'type_camp'=> Config::get('list.camp_type'),
            'session'=> [''=>''] + Config::get('list.all_session'),
            'city_type'=> [''=>''] + Config::get('list.city_type'),
            'camp_lang'=> [''=>''] + Config::get('list.camp_lang'),
            'columns' => Config::get('list.schoolColumns'),
            'share_volunteer' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'share_camp' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'paid' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'plan' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'report' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'training' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'teacher_memorandum' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'school_memorandum' => [''=>'','1'=>'Taк','0'=>'Ні'],
            'rating' => [''=>''] + $this->rating,
            'training_date'=>[''=>''] + Config::get('list.training_dates'),
            'goc_east'=>[''=>''] + Config::get('list.goc_east'),
            'manager' => $managers,
            // 'statusAr' => $statusAr,
            // 'stateAr' => $stateAr,
            // 'prAr' => $prAr,
            'statusAr' => [''=>''] + $status,
            'stateAr' => [''=>''] + $state,
            'prAr' => ['' => ''] + $project,
            'school_memorandum' => ['' => '', '0' => 'Ні', '1'=>'Taк'],
            'training' => ['' => '', '0' => 'Ні', '1'=>'Taк'],
            'training_dates' => [''=>''] + Config::get('list.training_dates'),
            'report' => ['' => '', '0' => 'Ні', '1'=>'Taк'],
        ));
    }

    public function processing(Request $request)
    {
      // print_r($request->all());
      // die();
      $ret = [];
      $YEAR = $request->y;
      $filters = [];

      // списки и настройки для выборки
      $quantity_children = ['0'=>'-'] + Config::get('list.quantity_children');
      $type_camp = Config::get('list.camp_type');
      $state = Config::get('list.states');
      $city_type = Config::get('list.city_type');
      $state_eng = Config::get('list.states_eng');
      $city_type_eng = Config::get('list.city_type_eng');
      $project = Config::get('list.project_admin');
      $session = Config::get('list.all_session');
      $status = Config::get('list.status');
      $camp_lang = Config::get('list.camp_lang');
      $columns = Config::get('list.schoolColumns');
      $share_volunteer = ['1'=>'Taк','0'=>'Ні'];
      $share_camp = ['1'=>'Taк','0'=>'Ні'];
      $experience = ['1'=>'Taк','0'=>'Ні'];
      $years = School::groupBy('year')->pluck('year');
      $school_memorandum = ['' => '', '0' => 'Ні', '1'=>'Taк'];
      $training = ['' => '', '0' => 'Ні', '1'=>'Taк'];
      $rating = $this->rating;
      $training_dates = Config::get('list.training_dates');
      $report = ['' => '', '0' => 'Ні', '1'=>'Taк'];

      $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                            ->where('role_id',2)
                            ->get()
                            ->pluck('email','email')
                            ->toArray();
      $managers[0] = 'Не выбрано';


      // готовим сортировки и фильтры
      $dtData = $request->columns;
      $orderData = $request->order;
      /*order[0][column]:3
        order[0][dir]:desc
        */

      if (!$dtData && $filters = request()->session()->get('sfilters')) {
        $dtData = $filters;
      }

      if ($dtData) {
        $statusSearchData = $dtData[2]['search']['value'];//:5
        $stateSearchData = $dtData[3]['search']['value'];
        $projectSearchData = $dtData[4]['search']['value'];

        $cityTypeSearchData = $dtData[5]['search']['value'];
        $citySearchData = $dtData[6]['search']['value'];
        $managerSearchData = $dtData[13]['search']['value'];
        $childrenQtySearchData = $dtData[14]['search']['value'];
        $shareCampSearchData = $dtData[15]['search']['value'];
        $campTypeSearchData = $dtData[16]['search']['value'];
        $paidSearchData = $dtData[17]['search']['value'];
        $sahreVolSearchData = $dtData[18]['search']['value'];
        $ratingSearchData = $dtData[19]['search']['value'];
        $sessionSearchData = $dtData[21]['search']['value'];
        $campLangSearchData = $dtData[22]['search']['value'];
        $trainSearchData = $dtData[23]['search']['value'];
        $trdatesSearchData = $dtData[24]['search']['value'];
        $schmemoSearchData = $dtData[25]['search']['value'];
        $tememoSearchData = $dtData[26]['search']['value'];
        $planSearchData = $dtData[27]['search']['value'];
        $reportSearchData = $dtData[28]['search']['value'];
        $gocEastSearchData = $dtData[29]['search']['value'];

        $filters = $dtData;
        request()->session()->put('sfilters', $filters);
      }

      $srchData = $request->search;
      if ($srchData && trim($srchData['value']) != '') {
        $srchStr = trim($srchData['value']);
      }

      // делаем запрос

      if ($YEAR == 'All') {
        $allSchoolsPre = School::orderBy('id', 'desc');
      } else {
        $allSchoolsPre = School::where('year', $YEAR)->orderBy('id', 'desc');
      }

      if (isset($statusSearchData) && trim($statusSearchData) != '') {
        $allSchoolsPre->where('status', $statusSearchData);
      }

      if (isset($stateSearchData) && trim($stateSearchData) != '') {
        $allSchoolsPre->where('state', $stateSearchData);
      }

      if (isset($projectSearchData) && trim($projectSearchData) != '') {
        // $proj = array_flip(config('list.project_admin'));
        // if (isset($proj[$projectSearchData])) {
          // $schoolsPre->where('project', 'like', '%,' . $proj[$projectSearchData] . ',%')
          //           ->orWhere('project', 'like', $proj[$projectSearchData] . ',%')
          //           ->orWhere('project', 'like', '%,' . $proj[$projectSearchData]);
          // $allSchoolsPre->where('project', 'like', '%' . $proj[$projectSearchData] . ',%')
          //           ->orWhere('project', 'like', $proj[$projectSearchData] . ',%')
          //           ->orWhere('project', 'like', '%,' . $proj[$projectSearchData]);
          $allSchoolsPre->where('project', 'like', '%' . $projectSearchData . '%');
        // }
      }


      if (isset($cityTypeSearchData) && trim($cityTypeSearchData) != '') {
        $allSchoolsPre->where('city_type', $cityTypeSearchData);
      }
      if (isset($citySearchData) && trim($citySearchData) != '') {
        $allSchoolsPre->where('city', 'like', '%' . $citySearchData . '%');
      }
      if (isset($managerSearchData) && trim($managerSearchData) != '') {
        $allSchoolsPre->where('manager', 'like', '%' . $managerSearchData . '%');
      }
      if (isset($childrenQtySearchData) && trim($childrenQtySearchData) != '') {
        $allSchoolsPre->where('quantity_children', $childrenQtySearchData);
      }
      if (isset($shareCampSearchData) && trim($shareCampSearchData) != '') {
        $allSchoolsPre->where('share_camp', $shareCampSearchData);
      }
      if (isset($campTypeSearchData) && trim($campTypeSearchData) != '') {
        $allSchoolsPre->where('type_camp', $campTypeSearchData);
      }
      if (isset($paidSearchData) && trim($paidSearchData) != '') {
        $allSchoolsPre->where('paid', $paidSearchData);
      }
      if (isset($sahreVolSearchData) && trim($sahreVolSearchData) != '') {
        $allSchoolsPre->where('share_volunteer', $sahreVolSearchData);
      }
      if (isset($ratingSearchData) && trim($ratingSearchData) != '') {
        $allSchoolsPre->where('rating', $ratingSearchData);
      }
      if (isset($sessionSearchData) && trim($sessionSearchData) != '') {
        $allSchoolsPre->where('session', 'like', '%' . $sessionSearchData . '%');
      }
      if (isset($campLangSearchData) && trim($campLangSearchData) != '') {
        $allSchoolsPre->where('camp_lang', 'like', '%' . $campLangSearchData . '%');
      }
      if (isset($trainSearchData) && trim($trainSearchData) != '') {
        $allSchoolsPre->where('training', $trainSearchData);
      }
      if (isset($trdatesSearchData) && trim($trdatesSearchData) != '') {
        $allSchoolsPre->where('training_dates', 'like', '%' . $trdatesSearchData . '%');
      }
      if (isset($schmemoSearchData) && trim($schmemoSearchData) != '') {
        $allSchoolsPre->where('school_memorandum', $schmemoSearchData);
      }
      if (isset($tememoSearchData) && trim($tememoSearchData) != '') {
        $allSchoolsPre->where('teacher_memorandum', $tememoSearchData);
      }
      if (isset($planSearchData) && trim($planSearchData) != '') {
        $allSchoolsPre->where('plan', $planSearchData);
      }
      if (isset($reportSearchData) && trim($reportSearchData) != '') {
        $allSchoolsPre->where('report', $reportSearchData);
      }
      if (isset($gocEastSearchData) && trim($gocEastSearchData) != '') {
        $allSchoolsPre->where('goc_east', 'like', '%' . $gocEastSearchData . '%');
      }

      // $schools = $schoolsPre->get();
      $allSchools = $allSchoolsPre->get();

      $len = $request->length;
      $start = $request->start;
      $i = 0;

      foreach ($allSchools as $S) {
        $proj = '';
        foreach($S->project as $value) {
          if (!empty($value)) {
            $proj.= $project[$value] . ',';
          }
        }

        $tmpAr = [
          $S->id,
          "<a href=\"" . route('admin-schools-edit', ['id' => $S->id] ) . "\" target=\"_blank\">" . $S->title . "</a>",
          $status[(int)$S->status],
          ($S->state == NULL) ? '-' : $state[(int)$S->state],
          $proj,

        ];

        foreach ($columns as $colGroup) {
          foreach ($colGroup as $cKey => $col) {
            if (!$col['hidden']) continue;
            if ($cKey == 'volunteers') {
              $tmpStr = '';
              foreach ($S->volunteers as $V) {
                $tmpStr.= '<a href="' . route('admin-volunteer-edit', $V->id) . '" target="_blank">'. $V->first_name . ' ' . $V->last_name . '</a><br/>';
              }
              $tmpAr[] = $tmpStr;
              continue;
            }
            if ($cKey == 'created_at') {
              $tmpAr[] = $S->$cKey->format('d.m.Y H:i');
              continue;
            }
            if ($cKey == 'shorttype') {
              $tmpAr[] = $S->type;
              continue;
            }

            if (isset($col['listed'])) {
              if (in_array($cKey,['session','camp_lang','training_dates','goc_east'])) {
                $tmpStr = '';
                foreach($S->$cKey as $value) {
                  $tmpStr.= isset($$cKey[$value]) ? $$cKey[$value] . ',' : '';
                }
                $tmpAr[] = $tmpStr;
              } else {
                $tmpAr[] = (isset($$cKey[$S->$cKey])) ? $$cKey[$S->$cKey] : '-';
              }
            } else {
              $tmpAr[] = $S->$cKey;
            }
          }
        }

        // если задана строка поиска, проверяем на соответствие
        if (isset($srchStr)) {
          $srchStr = urldecode($srchStr);
          // проверяем, есть ли нужный текст в данных
          $tmpStr = mb_strtolower(implode(' ', $tmpAr));
          if (strpos($tmpStr, mb_strtolower($srchStr)) === false) {
            // print_r($srchStr);
            // die();
            continue;
          }
        }

        $tmpAr[] = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-'. $S->id . '"><span class="glyphicon glyphicon-remove"></button>

          <!-- Modal -->
          <div id="myModal-' . $S->id . '" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Removing school ' . $S->title . ' for ' . $S->year . ' year</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="delete" class="btn btn-primary" onclick="window.location.href=\'/admin/schools/delete/' . $S->id . '\'">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
              </div>
            </div>
          </div>';

        $ret[] = $tmpAr;
        $i++;
      }
      $count = $i;

      // ручная сортировка массива
      if (isset($orderData[0]) && isset($orderData[0]['column'])) {
        $oBy = $orderData[0]['column'];
        $oDir = $orderData[0]['dir'];
      } else {
        $oBy = '2';
        $oDir = 'asc';
      }

      uasort($ret, function($a, $b) use ($oBy, $oDir) {
        if ($oBy == 0) {
          $ret = intval($a[0] - $b[0]);
        } else {
          $ret = strcasecmp(strip_tags($a[$oBy]), strip_tags($b[$oBy]));
        }

        if ($oDir == 'desc') {
          $ret = -1 * $ret;
        }
        return $ret;
      });

      // сохраняем в сессию результаты фильтрации и сортировки
      $forStore = [];
      foreach ($ret as $key => $valAr) {
        $forStore[$valAr[0]] = $valAr[0];
      }

      request()->session()->put('sids', $forStore);

      // ручная пагинация
      $ret = array_slice($ret, $start, $len);

      return response()->json([
        "debug" => $forStore,
        "draw" => $request->draw,
        "recordsTotal" =>  $count,
        "recordsFiltered" =>  $count,
        "data" => $ret,
      ]);
    }

    public function saveUserCols(Request $request)
    {
      $active = $request->a;
      $user = $request->user();

      if (!$user || !$active) return;

      $fs['cols'] = json_encode($active);
      $fs['type'] = 'schoolvis';
      $fs['user_id'] = $user->id;

      $uv = UserExport::where('type', 'schoolvis')
                  ->where('user_id', $user->id)
                  ->first();
      if (!$uv) {
          $uv = UserExport::create($fs);
      } else {
          $uv->cols = $fs['cols'];
          $uv->save();
      }
    }

    public function create()
    {
        if(Auth::user()->isAdmin()) {
            $currentYear = new School();
            $action = route('schools-store');
            $title = 'Add school';

            $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                                ->where('role_id',2)
                                ->get()
                                ->pluck('email','email')
                                ->toArray();

            return view('admin.schools.form',array(
                'currentYear' => $currentYear,
                'action' => $action,
                'title' => $title,
                'strands'=>Config::get('list.strands'),
                'quantity_children'=>Config::get('list.quantity_children'),
                'camp_type'=>Config::get('list.camp_type'),
                'states'=>Config::get('list.states'),
                'city_type'=>Config::get('list.city_type'),
                'city_type_eng'=>Config::get('list.city_type_eng'),
                'states_eng'=>Config::get('list.states_eng'),
                'project'=>Config::get('list.project_admin'),
                'session'=>Config::get('list.session'),
                'status'=>Config::get('list.status'),
                'camp_lang'=>Config::get('list.camp_lang'),
                'rating'=>$this->rating,
                'managers' => array_merge([''=>'-'],$managers),
                'training_dates'=>Config::get('list.training_dates'),
                'goc_east'=>Config::get('list.goc_east'),

                )
            );
        }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Auth::user()->isAdmin() ) {
            $schoolData = $request->except( '_token' );
            $schoolData['school_id'] = School::max( 'school_id' ) + 1;

            $tmpYear = date('Y');
            if (date('m') >= 9) {
              $year = $tmpYear . '-' . (int)($tmpYear + 1);
            } else {
              $year = (int)$tmpYear - 1 . '-' . $tmpYear;
            }

            $schoolData['year'] = $year;
            $school = School::create( $schoolData );
            return redirect()->route( 'admin-schools-edit', ['id' => $school->school_id] );
        }
        return view('admin.permission');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Auth::user()->isAdmin() ) {
            $currentYear = School::find( $id );
            if (!$currentYear) {
              dd('School with ID ' . $id . ' doesn`t exists');
            }
            $action = route( 'schools-update', ['id' => $id] );
            $title = 'Edit school';
            $years = School::where( 'school_id', $currentYear->school_id )->get();
            $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                                ->where('role_id',2)
                                ->get()
                                ->pluck('email','email')
                                ->toArray();

            return view('admin.schools.form',array(
                'YEAR' => $currentYear->year,
                'action' => $action,
                'title' => $title,
                'years' => $years,
                'currentYear' => $currentYear,
                'quantity_children' => Config::get('list.quantity_children'),
                'camp_type' => Config::get('list.camp_type'),
                'states' => Config::get('list.states'),
                'city_type'=> Config::get('list.city_type'),
                'city_type_eng'=> Config::get('list.city_type_eng'),
                'session'=> Config::get('list.all_session'),
                'states_eng' => Config::get('list.states_eng'),
                'project' => Config::get('list.project_admin'),
                'status' => Config::get('list.status'),
                'camp_lang' => Config::get('list.camp_lang'),
                'rating' => $this->rating,
                'managers' => [''=>'-'] + $managers,
                'training_dates'=>Config::get('list.training_dates'),
                'goc_east'=>Config::get('list.goc_east'),
              )
            );
        }
        return view('admin.permission');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        if ( Auth::user()->isAdmin() ) {

            $currentYear = School::find( $id );

            if ( isset($request->add_year) && $request->add_year != '0' ) {
                // добавляем новый год для школы
                $yearExists = School::where( ['school_id' => $currentYear->school_id, 'year' => $request->add_year] )->first();
                if ( $yearExists ) {
                    die('This YEAR already exists');
                }

                $data = array_except( $currentYear->getAttributes(), [
                  'id', 'comment', 'manager', 'paid',
                  'rating', 'status', 'school_memorandum',
                  'plan', 'teacher_memorandum', 'report',
                  'trainig_dates', 'goc_east', 'project', 'updated_at'
                ] );
                $data['type'] = 0;
                $data['year'] = $request->add_year;
                $data['status'] = 0;
                $data['old_id'] = 0;

                $newYear = School::create($data);
                return redirect( route( 'admin-schools-edit', ['id' => $newYear->id] ) );
            }

            $yearEdit = School::find( $id );
            $yearData = $request->except( '_token', 'add_year', 'created_at' );

            if ( isset( $yearData['volunteers'] ) ) {
              $schoolVols = SchoolVolunteer::where( 'school_year_id', $yearEdit->id )->get();
              $hadVols = !empty( $schoolVols->toArray() );
              foreach ( $schoolVols as $SV ) {
                $SV->delete();
                $delVol = Volunteer::find( $SV->volunteer_id );
                if ( $delVol && empty( $delVol->schools->toArray() ) ) {
                  $delVol->status = 6;
                  $delVol->save();
                }
              }

              if ( !empty( $yearData['volunteers'] ) ) {
                foreach ( $yearData['volunteers'] as $volunteerId ) {
                  SchoolVolunteer::create( [
                    'school_year_id' => $yearEdit->id,
                    'volunteer_id' => $volunteerId
                    ] );
                  $vol = Volunteer::find( $volunteerId );
                  if ( $vol->status != 11 ) {
                    $vol->status = 7;
                  }
                  if ( !in_array($yearEdit->state, $vol->state) ) {
                    $vol->state = array_merge($vol->state, [$yearEdit->state]);
                  }
                  $vol->save();
                }
                $yearData['status'] = 5;
              } elseif ( $hadVols ) {
                $yearData['status'] = 6;
              }
            }
            unset( $yearData['volunteers'] );

            $yearEdit->fill( $yearData )->save();
            return redirect()->route( 'admin-schools-edit', ['id' => $yearEdit->id] );
        }
        return view( 'admin.permission' );
    }
    public function mergeForms($id, $similar) {
      $school = School::find( $id );
      $similarSchool = School::find( $similar );
      if ( $similarSchool->year == $school->year ) {
        $data = $school->toArray();
        unset($data['id'],$data['school_id'],$data['type'],$data['created_at'],$data['updated_at']);
        $similarSchool->update($data);
        $school->delete();
        return redirect()->route('admin-schools-edit', ['id' => $similarSchool->id]);
      } else {
        $school->school_id = $similarSchool->school_id;
        $school->type = 0;
        $school->save();
        return redirect()->route('admin-schools-edit', ['id' => $school->id]);
      }
    }
    public function checkForm(Request $request) {
      $curSchool = School::find($request->currentId);
      $schools = School::where('id', '<>', $request->currentId)
                        ->where('type', '<>', 1)
                        ->where('title', 'LIKE', '%' . $request->title . '%')
                        ->get();
      if ( !$schools ) {
        $response = "<h3 id='formResult'>No form found</h3>";
      } else {
        $response = "";
        foreach ( $schools as $S ) {
          $tmp = [];
          if ( $S->year == $curSchool->year ) {
            $response .= "<h3>The long form: " . $S->title . " " . $S->year . " year</h3>
                          <a href='" . route('merge-school-forms', ['id'=>$curSchool->id, 'similar'=>$S->id]) . "' class='btn btn-default'>Upgrade</a>";
          } else {
            $response .= "<h3>The long form: " . $S->title . " " . $S->year . " year</h3>
                          <a href='" . route('merge-school-forms', ['id'=>$curSchool->id, 'similar'=>$S->id]) . "' class='btn btn-success'>Merge</a>";
          }
        }
      }
      return $response;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
      $refer = request()->headers->get('referer');
      $single = strpos( $refer, 'edit' ) != false;
      if ( Auth::user()->isAdmin() ) {
          $school = School::find( $id );
          $school_id = $school->school_id;
          $school->delete();
          $schoolLeast = School::where( 'school_id', $school_id )->orderBy( 'year', 'desc' )->first();
          $YEAR = $school->year;
          if ( $single && $schoolLeast != null ) {
            return redirect()->route('admin-schools-edit', ['id' => $schoolLeast->id] );
          } else {
            return redirect()->route( 'admin-schools', $YEAR );
          }
      }
      return view( 'admin.permission' );
    }

    public function export(Request $request, $YEAR) {
        $user = $request->user();
        $exportColumns = $request->col;

        $forStore = request()->session()->get('sids');

        // dd($forStore, $request->all());

        $exportData = [
          'cols' => json_encode(array_values($exportColumns)),
          'type' => 'school',
          'user_id' => $user->id,
        ];

        $userExport = UserExport::where( ['type' => 'school', 'user_id' => $user->id] )->first();
        if ( !$userExport ) {
            $userExport = UserExport::create( $exportData );
        } else {
            $userExport->cols = $exportData['cols'];
            $userExport->save();
        }

        // списки и настройки для выборки
        $quantity_children = ['0'=>'-'] + Config::get('list.quantity_children');
        $type_camp = Config::get('list.camp_type');
        $state = Config::get('list.states');
        $city_type = Config::get('list.city_type');
        $state_eng = Config::get('list.states_eng');
        $city_type_eng = Config::get('list.city_type_eng');
        $project = Config::get('list.project_admin');
        $session = Config::get('list.all_session');
        $status = Config::get('list.status');
        $camp_lang = Config::get('list.camp_lang');
        $columns = Config::get('list.schoolColumns');
        $share_volunteer = ['1'=>'Taк','0'=>'Ні'];
        $share_camp = ['1'=>'Taк','0'=>'Ні'];
        $experience = ['1'=>'Taк','0'=>'Ні'];
        $years = School::groupBy('year')->pluck('year');
        $school_memorandum = ['' => '', '0' => 'Ні', '1'=>'Taк'];
        $training = ['' => '', '0' => 'Ні', '1'=>'Taк'];
        $rating = $this->rating;
        $training_dates = Config::get('list.training_dates');
        $report = ['' => '', '0' => 'Ні', '1'=>'Taк'];
        $goc_east = Config::get('list.goc_east');

        $managers = User::leftJoin('users_roles as ur','ur.user_id', '=', 'users.id')
                              ->where('role_id',2)
                              ->get()
                              ->pluck('email','email')
                              ->toArray();

        if ($forStore !== null) {
          $schoolsEloq = School::whereIn('id', $forStore);
        } else if ( $YEAR == 'All') {
          $schoolsEloq = School::orderBy('id');
          $exportColumns['year'] = 1;
        } else {
          $schoolsEloq = School::where('year', $YEAR);
        }

        // if ( $request->has( 'filters' ) ) {
        //   foreach ( $request->filters as $key => $value ) {
        //     $coded = array_search( $value, $$key, true );
        //     $schoolsEloq = $schoolsEloq->where( function ($query) use ($key, $coded) {
        //         $query->where( $key, $coded )
        //               ->orWhere( $key, 'LIKE', $coded . ',%' )
        //               ->orWhere( $key, 'LIKE', '%,' . $coded )
        //               ->orWhere( $key, 'LIKE', '%,' . $coded . ',%');
        //       });
        //   }
        // }
        $schools = $schoolsEloq->get();

        $exportOutput = [];
        foreach ( $schools as $key => $S ) {
            $rowData = [];
            foreach ( $columns as $block => $cols ) {
                foreach ( $cols as $key => $col ) {
                    $title = $col['label'];
                    if ( !isset( $exportColumns[$key] ) /*|| !$exportColumns[$key]*/ ) {
                        continue;
                    }
                    if ( $key == 'volunteers' ) {
                      $tmpArr = [];
                      foreach ( $S->volunteers as $V ) {
                        $tmpArr[] = $V->first_name . ' ' . $V->last_name . '(ID: ' . $V->id . ')';
                      }
                      $rowData[$title] = implode( ', ', $tmpArr);
                      continue;
                    }
                    if ( in_array($key,['project','session','camp_lang','trainig_dates','goc_east']) ) {
                      $tmpArr = [];
                      foreach ( $S->$key as $value ) {
                        if ( !empty( $value ) ) {
                          $tmpArr[] = (isset($$key[$value])) ? $$key[$value] : '-';
                        }
                      }
                      $rowData[$title] = implode( ', ', $tmpArr);
                    } elseif ( $key == 'created_at' ) {
                      $rowData[$title] = $S->$key->format('d.m.Y H:i');
                    } elseif ( isset( $col['listed'] ) ) {
                      $rowData[$title] = ($S->$key != NULL && isset($$key[$S->$key])) ? $$key[$S->$key] : '-';
                    } else {
                      $rowData[$title] = $S->$key;
                    }
                }
            }
            $exportOutput[] = $rowData;
        }
        // dd($forStore, $request->all(), $exportColumns, $exportOutput);
        Excel::create('schools', function($excel) use($exportOutput) {
            $excel->setTitle('Schools export');
            $excel->sheet('Schools', function($sheet) use($exportOutput) {
                $sheet->fromArray($exportOutput, NULL, 'A1', false, true);
            });
        })->download('xlsx');
    }

    public function getFiles(){
       $result = array();

       $cdir = scandir('pdf');
      //  foreach ($cdir as $key => $value)
      //  {
      //     if (!in_array($value,array(".","..")))
      //     {
      //        if (is_dir('pdf' . DIRECTORY_SEPARATOR . $value))
      //        {
      //           $result[$value] = dirToArray('pdf' . DIRECTORY_SEPARATOR . $value);
      //        }
      //        else
      //        {
      //           $result[] = $value;
      //        }
      //     }
      // }
       $count=0;
          foreach ($cdir as $key => $value) {
             $school=School::where('pdf',$value)->get();
             if(empty($school->id)){
                echo $value." ";
                $count++;
                }
            // die;
          }
        var_dump($count);
    }
}
