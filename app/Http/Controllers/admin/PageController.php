<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PageRequest;
use App\Http\Controllers\Controller;
use App\Page;
use App\PageTranslate;
use DB;
use Config;
use Auth;
use LaravelLocalization;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.pages.index',array(
                            'pages'=>Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
                            ->where('lang',LaravelLocalization::getCurrentLocale())->groupBy('page_id')
                            ->get()));
        }
        return view('admin.permission');    
    }
    public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);        
       $st = preg_replace('|\s+|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.

       $slug;
       $str = self::transliteration($st);
       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }
    public function create()
    {
        if(Auth::user()->isAdmin()){
            return view('admin.pages.create',array(
                'page_id'=>isset($_GET['page_id'])?$_GET['page_id']:"",
                'layouts'=>$this->get_layouts()
                )
            );
        }
        return view('admin.permission');    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        if(Auth::user()->isAdmin()){
            $model = new Page();
            $model->layout=$request->layout;
            $model->save();
            $translate = new PageTranslate();
            $translate->slug=($request->slug)?$this->makeSlug($request->slug):$this->makeSlug($request->title);
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
            $translate->title = $request['title'];
            $translate->content = $request['content'];
            $translate->page_id=!empty($request->page_id)?$request->page_id:$model->id;
            $translate->lang = LaravelLocalization::getCurrentLocale();
            $translate->save();
            return redirect()->route('pages-edit', [!empty($request->page_id)?$request->page_id:$model->id]);
        }
        return view('admin.permission');    
    }
    public function edit($id)
    {
        if(Auth::user()->isAdmin()){
          $have_translate=0;
        if(DB::select('SELECT COUNT(page_id) as count FROM `pageTranslate` WHERE page_id='.$id.' GROUP BY page_id')[0]->count>1)
          $have_translate=1;
          $have_langs=DB::select('SELECT lang FROM `pageTranslate` WHERE page_id='.$id);
          $pages=DB::select("SELECT * FROM `pageTranslate` at left join pages as a on a.id=at.page_id WHERE page_id in (SELECT page_id FROM `pageTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY page_id HAVING count(page_id)<=1 ");
            return view('admin.pages.update',array(
                            'layouts'=>$this->get_layouts(),
                            'have_langs'=>$have_langs,
                            'have_translate'=>$have_translate,
                            'langs'=>Config::get('app.locales'),
                            'page_id'=>isset($_GET['page_id'])?$_GET['page_id']:"",
                            'pages'=>$pages,
                            'page'=>Page::leftJoin('pageTranslate', function($join) { $join->on('pages.id', '=', 'pageTranslate.page_id'); })
                            ->where('lang',LaravelLocalization::getCurrentLocale())->where('pages.id',$id)->groupBy('page_id')
                            ->first()));
        }
        return view('admin.permission');   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        if(Auth::user()->isAdmin()){

            if($request->page_id!=$id){
              $model= Page::find($request->page_id);
              $translate= new PageTranslate();
              $translate->lang=LaravelLocalization::getCurrentLocale();
              $translate->page_id=$request->page_id;
              $old_translate= PageTranslate::where('page_id',$request->old_id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();      
            }
            else{
              $model= Page::find($id);
              $translate = PageTranslate::where('page_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
              $translate->page_id=$model->id;
            }
            $model->layout=$request->layout;
            $model->save();
            $translate->slug=($request->slug)?$this->makeSlug($request->slug):$this->makeSlug($request->title);
            $translate->seo_title=$request->seo_title;
            $translate->keywords=$request->keywords;
            $translate->description=$request->description;
            $translate->title = $request->title;
            $translate->content = $request->content;
            $translate->lang = LaravelLocalization::getCurrentLocale();
            $translate->save();
            $this->is_empty();
           if($request->page_id!=$model->id &&$request->page_id!="")
            return redirect()->route('pages-edit', [$request->page_id]);
            else
                return redirect()->route('pages-edit', [$model->id]);
            }
        return view('admin.permission');   
    }
    public function destroy($id)
    {
      if(Auth::user()->isAdmin()){
        $translate= PageTranslate::where('page_id',$id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        return redirect()->route('pages');
      }
        return view('admin.permission');    
    }
    public function unlink(Request $request, $id)
    {

      if(Auth::user()->isAdmin()){

        $old= Page::find($id);
        $model= new Page();
        $model->save();
        $old_translate=PageTranslate::where('page_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
        $translate=new PageTranslate();
        $translate->lang=$old_translate->lang;
        $translate->page_id=$model->id;

        $translate->slug=!empty($old_translate->slug)?$old_translate->slug:$this->makeSlug($old_translate->title);
        $translate->seo_title=$old_translate->seo_title;
        $translate->keywords=$old_translate->keywords;

        $translate->description=$old_translate->description;
        $translate->title=$old_translate->title;
        $translate->content=$old_translate->content;
        $translate->save();
        PageTranslate::where('page_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        
        
        return redirect()->route('pages-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    
    public function is_empty()
    {
      $ids=DB::select('SELECT A.id FROM pages AS A LEFT OUTER JOIN pageTranslate AS B ON A.id = B.page_id WHERE B.page_id IS NULL ');
      $ids_array=array();
      foreach ($ids as $key => $value) {
       $ids_array[]=$value->id;
      }
      // var_dump($ids_array);die;
      Page::destroy($ids_array);
    }

    public function get_layouts(){
      $tmp =  array_diff(scandir('../resources/views/layouts'), array('..', '.'));
      $files=array();
      foreach ($tmp as $key => $value) {
        $files['layouts.'.explode('.', $value)[0]]=explode('.', $value)[0];
      }
      // var_dump($files);die;
      return $files;
    }

}
