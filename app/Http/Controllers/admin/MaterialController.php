<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\MaterialRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Material;
use App\MaterialTranslate;
use App\Category;
use App;
use LaravelLocalization;
use Config;
use DB;
use Auth;
class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
        
        
        return view('admin.materials.index',array(
                        'materials'=>Material::leftJoin('materialTranslate', function($join) {
                                      $join->on('materials.id', '=', 'materialTranslate.material_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->get()
                            ));
         }
        return view('admin.permission');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);        
       $st = preg_replace('|\s+|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.

       $slug;
       $str = self::transliteration($st);
       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }
    public function create()
    {
        if(Auth::user()->isAdmin()){

          return view('admin.materials.create',array(
              'langs'=>Config::get('app.locales'),
              'material_id'=>isset($_GET['material_id'])?$_GET['material_id']:"",
              'layouts'=>$this->get_layouts()
              )
          );
        }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialRequest $request)
    {

      if(Auth::user()->isAdmin()){
        $model=new Material();
        if($request->image){
            $image = $model->id . '.' . 
                $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(
                    base_path() . '/public/materials/', $image
                );
                $model->image=$image;
        }
        $model->layout=$request->layout;
        $model->created_at=date("Y-m-d H:i:s");
        $model->updated_at=date("Y-m-d H:i:s");
        
        $model->save();
        $translate=new MaterialTranslate();
        $translate->slug=($request->slug)?$request->slug:$this->makeSlug($request->title);
        $translate->seo_title=$request->seo_title;
        $translate->keywords=$request->keywords;

        $translate->description=$request->description;
        $translate->title=$request->title;
        $translate->body=$request->body;
        $translate->material_id=!empty($request->material_id)?$request->material_id:$model->id;
        $translate->lang=LaravelLocalization::getCurrentLocale();
        $translate->save();
        

         return redirect()->route('materials-edit', [!empty($request->material_id)?$request->material_id:$model->id]);
      }
        return view('admin.permission');

    }

    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->isAdmin()){
        $have_translate=0;
        if(DB::select('SELECT COUNT(material_id) as count FROM `materialTranslate` WHERE material_id='.$id.' GROUP BY material_id')[0]->count>1)
          $have_translate=1;
        $have_langs=DB::select('SELECT lang FROM `materialTranslate` WHERE material_id='.$id);
        $materials=DB::select("SELECT * FROM `materialTranslate` at left join materials as a on a.id=at.material_id WHERE material_id in (SELECT material_id FROM `materialTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY material_id HAVING count(material_id)<=1 ");
        $material=Material::leftJoin('materialTranslate', function($join) {
                                      $join->on('materials.id', '=', 'materialTranslate.material_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->where('material_id',$id)->first();
        return view('admin.materials.update',array(
            'have_langs'=>$have_langs,
            'have_translate'=>$have_translate,
            'langs'=>Config::get('app.locales'),
            'materials'=>$materials,
            'material'=>$material,
            'layouts'=>$this->get_layouts()
            
        ));

      }
        return view('admin.permission');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MaterialRequest $request, $id)
    {
      if(Auth::user()->isAdmin()){
        if($request->material_id!=$id){
          $model= Material::find($request->material_id);
          $translate= new MaterialTranslate();
          $translate->lang=LaravelLocalization::getCurrentLocale();
          $translate->material_id=$request->material_id;
          // Material::find($request->old_id)->delete();
          $old_translate= MaterialTranslate::where('material_id',$request->old_id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();        }
        else{
          $model= Material::find($id);
          $translate = MaterialTranslate::where('material_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
          $translate->material_id=$model->id;
        }
        $model->layout=$request->layout;
        if($request->image){
        $image = $model->id . '.' . 
            $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/materials/', $image
            );
             $model->image=$image;
        }
        $model->updated_at=date("Y-m-d H:i:s");
       
        $model->save();
        $translate->slug=($request->slug)?$this->makeSlug($request->slug):$this->makeSlug($request->title);
        $translate->seo_title=$request->seo_title;
        $translate->keywords=$request->keywords;
        $translate->description=$request->description;
        $translate->title=$request->title;
        $translate->body=$request->body;
        $translate->save();
        
        $this->is_empty();
        if($request->material_id!=$model->id &&$request->material_id!="")
            return redirect()->route('materials-edit', [$request->material_id]);
        else
            return redirect()->route('materials-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    public function unlink(Request $request, $id)
    {

      if(Auth::user()->isAdmin()){

        $old= Material::find($id);
        $model= new Material();
        $model->created_at=date("Y-m-d H:i:s");
        $model->image=$old->image;
        $model->save();
        $old_translate=MaterialTranslate::where('material_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
        $translate=new MaterialTranslate();
        $translate->lang=$old_translate->lang;
        $translate->material_id=$model->id;

        $translate->slug=!empty($old_translate->slug)?$old_translate->slug:$this->makeSlug($old_translate->title);
        $translate->seo_title=$old_translate->seo_title;
        $translate->keywords=$old_translate->keywords;

        $translate->description=$old_translate->description;
        $translate->title=$old_translate->title;
        $translate->body=$old_translate->body;
        $translate->save();
        MaterialTranslate::where('material_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        
        
        return redirect()->route('materials-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    public function destroy($id)
    {
      if(Auth::user()->isAdmin()){
        $translate= MaterialTranslate::where('material_id',$id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        return redirect()->route('materials');
      }
        return view('admin.permission');    
    }
    public function is_empty()
    {
      $ids=DB::select('SELECT A.id FROM materials AS A LEFT OUTER JOIN materialTranslate AS B ON A.id = B.material_id WHERE B.material_id IS NULL ');
      $ids_array=array();
      foreach ($ids as $key => $value) {
       $ids_array[]=$value->id;
      }
      // var_dump($ids_array);die;
      Material::destroy($ids_array);
    }
    public function get_layouts(){
      $tmp =  array_diff(scandir('../resources/views/layouts'), array('..', '.'));
      $files=array();
      foreach ($tmp as $key => $value) {
        $files['layouts.'.explode('.', $value)[0]]=explode('.', $value)[0];
      }
      return $files;
    }
    public function remove_image($id){
        $model= Material::find($id);
        $model->image="";
        $model->save();
    }
}
