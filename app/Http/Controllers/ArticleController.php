<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use App\ArticleTranslate;
use App\Category;
use App;
use LaravelLocalization;
use Config;
use DB;
use Auth;
class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin()){
        
        
        return view('admin.articles.index',array(
                        'articles'=>Article::leftJoin('articleTranslate', function($join) {
                                      $join->on('articles.id', '=', 'articleTranslate.article_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->simplePaginate(20)
                            ));
         }
        return view('admin.permission');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);        
       $st = preg_replace('|\s+|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.

       $slug;
       $str = self::transliteration($st);
       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }
    public function create()
    {
        if(Auth::user()->isAdmin()){

          return view('admin.articles.create',array(
              'categories'=>Category::leftJoin('categoryTranslate', function($join) {
                                      $join->on('categories.id', '=', 'categoryTranslate.category_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->get(),
              'langs'=>Config::get('app.locales'),
              'article_id'=>isset($_GET['article_id'])?$_GET['article_id']:""
              )
          );
        }
        return view('admin.permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {

      if(Auth::user()->isAdmin()){
        $model= new Article();
        $model->category_id=$request->category;
        $model->created_at=date("Y-m-d H:i:s");
        $model->updated_at=date("Y-m-d H:i:s");
        $model->image=$request->image;
        $model->save();
        $translate=new ArticleTranslate();
        $translate->slug=($request->slug)?$request->slug:$this->makeSlug($request->title);
        $translate->seo_title=$request->seo_title;
        $translate->keywords=$request->keywords;

        $translate->description=$request->description;
        $translate->title=$request->title;
        $translate->body=$request->body;
        $translate->article_id=!empty($request->article_id)?$request->article_id:$model->id;
        $translate->lang=LaravelLocalization::getCurrentLocale();
        $translate->save();
        if($request->image){
            $image = $model->id . '.' . 
                $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(
                    base_path() . '/public/image/', $image
                );
        }

         return redirect()->route('articles-edit', [!empty($request->article_id)?$request->article_id:$model->id]);
      }
        return view('admin.permission');

    }

    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->isAdmin()){
        $have_translate=0;
        if(DB::select('SELECT COUNT(article_id) as count FROM `articleTranslate` WHERE article_id='.$id.' GROUP BY article_id')[0]->count>1)
          $have_translate=1;
        $have_langs=DB::select('SELECT lang FROM `articleTranslate` WHERE article_id='.$id);
        $articles=DB::select("SELECT * FROM `articleTranslate` at left join articles as a on a.id=at.article_id WHERE article_id in (SELECT article_id FROM `articleTranslate` WHERE lang!='".LaravelLocalization::getCurrentLocale()."' ) GROUP BY article_id HAVING count(article_id)<=1 ");
        $article=Article::leftJoin('articleTranslate', function($join) {
                                      $join->on('articles.id', '=', 'articleTranslate.article_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->where('article_id',$id)->first();
        $category_slug=Category::leftJoin('categoryTranslate', function($join) { $join->on('categories.id', '=', 'categoryTranslate.category_id'); })
                                        ->where('lang',LaravelLocalization::getCurrentLocale())
                                        ->where('categoryTranslate.category_id',$article->category_id)->groupBy('category_id')
                                        
                                        ->first()['slug'];
        return view('admin.articles.update',array(
            'have_langs'=>$have_langs,
            'have_translate'=>$have_translate,
            'categories'=>Category::with('translate')->get()->lists('translate.title','id'),
            'langs'=>Config::get('app.locales'),
            'articles'=>$articles,
            'article'=>Article::leftJoin('articleTranslate', function($join) {
                                      $join->on('articles.id', '=', 'articleTranslate.article_id');
                                    })->where('lang',LaravelLocalization::getCurrentLocale())->where('article_id',$id)->first()

            
        ));

      }
        return view('admin.permission');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
      if(Auth::user()->isAdmin()){
        if($request->article_id!=$id){
          $model= Article::find($request->article_id);
          $translate= new ArticleTranslate();
          $translate->lang=LaravelLocalization::getCurrentLocale();
          $translate->article_id=$request->article_id;
          // Article::find($request->old_id)->delete();
          $old_translate= ArticleTranslate::where('article_id',$request->old_id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();        }
        else{
          $model= Article::find($id);
          $translate = ArticleTranslate::where('article_id',$model->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
          $translate->article_id=$model->id;
        }
        $model->category_id=$request->category;
        $model->created_at=date("Y-m-d H:i:s");
        $model->image=$request->image;
        $model->save();
        $translate->slug=!empty($request->slug)?$request->slug:$this->makeSlug($request->title);
        $translate->seo_title=$request->seo_title;
        $translate->keywords=$request->keywords;
        $translate->description=$request->description;
        $translate->title=$request->title;
        $translate->body=$request->body;
        $translate->save();
        if($request->image){
        $image = $model->id . '.' . 
            $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/image/', $image
            );
        }
        $this->is_empty();
        if($request->article_id!=$model->id &&$request->article_id!="")
            return redirect()->route('articles-edit', [$request->article_id]);
        else
            return redirect()->route('articles-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    public function unlink(Request $request, $id)
    {

      if(Auth::user()->isAdmin()){

        $old= Article::find($id);
        $model= new Article();
        $model->category_id=$old->category_id;
        $model->created_at=date("Y-m-d H:i:s");
        $model->image=$old->image;
        $model->save();
        $old_translate=ArticleTranslate::where('article_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->first();
        // var_dump($old_translate);die;
        $translate=new ArticleTranslate();
        $translate->lang=$old_translate->lang;
        $translate->article_id=$model->id;

        $translate->slug=!empty($old_translate->slug)?$old_translate->slug:$this->makeSlug($old_translate->title);
        $translate->seo_title=$old_translate->seo_title;
        $translate->keywords=$old_translate->keywords;

        $translate->description=$old_translate->description;
        $translate->title=$old_translate->title;
        $translate->body=$old_translate->body;
        $translate->save();
        ArticleTranslate::where('article_id',$old->id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        $this->is_empty();
        
        
        return redirect()->route('articles-edit', [$model->id]);

      }
        return view('admin.permission');    
    }
    public function destroy($id)
    {
      if(Auth::user()->isAdmin()){
        $translate= ArticleTranslate::where('article_id',$id)->where('lang',LaravelLocalization::getCurrentLocale())->delete();
        return redirect()->route('articles');
      }
        return view('admin.permission');    
    }
    public function is_empty()
    {
      $ids=DB::select('SELECT A.id FROM articles AS A LEFT OUTER JOIN articleTranslate AS B ON A.id = B.article_id WHERE B.article_id IS NULL ');
      $ids_array=array();
      foreach ($ids as $key => $value) {
       $ids_array[]=$value->id;
      }
      // var_dump($ids_array);die;
      Article::destroy($ids_array);
    }
}
