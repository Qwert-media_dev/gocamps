<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use Auth;
use Redirect;

class UserController extends Controller

{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('users.register',array());

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

       $user= new User();

        $user->email= $request->email;

        $user->password=Hash::make($request->password);

        $user->save();

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request){

         return view('users.login',array());

    }

    public function auth(Request $request){

       if (Auth::attempt(array('email' => $request->email, 'password' => $request->password)))

        {

            return Redirect::intended('admin/');

        }

        else

            return Redirect::back()->withErrors(['Auth error', 'Wrong login or password']);

        

    }
    public function logout() {

        Auth::logout();
        return Redirect::route('index');

    }

}
