<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslate extends Model
{
    protected $guarded = [];
    public $timestamps=false;
    public $table='projectTranslate';
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
